/*
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
*/
'use strict';

import * as VCard from '../common/import/vcard.js';
import { CustomPersonalField, CustomOrganizationField } from '../common/CustomField.js';
import { assert } from 'tiny-esm-test-runner';
const { is } = assert;

export function setup() {
  CustomPersonalField.init([
    { field:  'x-company-name',
      label:  '会社名' },
    { field:  'x-group-name',
      label:  '組織名' }
  ])
  CustomOrganizationField.init([]);
}

test_parse.parameters = {
  'single input': {
    input: `
BEGIN:VCARD
VERSION:4.0
addressbook_name:public
UID:0001
FN:フルネーム
TITLE:リーダー
ORG:組織;グループ;サブグループ
TEL:外線TEL:000-0000-0000;内線TEL:000-0000-0000;携帯:000-0000-0000;PHS:000-0000-0000
EMAIL:user@example.jp
NOTE:注意書き
 2行目
\t3行目
\r
SORT-STRING:00000
X-COMPANY-NAME:組織
X-GROUP-NAME:グループ
X-UNKNOWN-FIELD:未知のフィールド
END:VCARD
    `,
    expected: [
      {
        contact: {
          properties: {
            DisplayName: 'フルネーム',
            JobTitle: 'リーダー',
            PagerNumber: '外線TEL:000-0000-0000;内線TEL:000-0000-0000;携帯:000-0000-0000;PHS:000-0000-0000',
            PrimaryEmail: 'user@example.jp',
            Notes: '注意書き2行目3行目',
          },
          id: '0001',
        },
        customValues: {
          version: 4,
          'sort-string': ['00000'],
          'x-company-name': ['組織'],
          'x-group-name': ['グループ'],
          'x-unknown-field': ['未知のフィールド'],
          org: ['組織', 'グループ', 'サブグループ'],
        },
        addressBookId: 'public',
      },
    ],
  },
  'multiple inputs': {
    input: `
BEGIN:VCARD
VERSION:4.0
addressbook_name:public
UID:0001
FN:フルネームA
TITLE:リーダー
ORG:組織A;グループA;サブグループA
TEL:外線TEL:000-0000-0000;内線TEL:000-0000-0000;携帯:000-0000-0000;PHS:000-0000-0000
EMAIL:userA@example.jp
NOTE:注意書きA
SORT-STRING:00000
X-COMPANY-NAME:組織A
X-GROUP-NAME:グループA
X-UNKNOWN-FIELD:未知のフィールド
END:VCARD
BEGIN:VCARD
VERSION:4.0
addressbook_name:private
UID:0002
FN:フルネームB
TITLE:リーダー
ORG:組織B;グループB;サブグループB
EMAIL:userB@example.jp
NOTE:注意書きB
SORT-STRING:00000
X-COMPANY-NAME:組織B
X-GROUP-NAME:グループB
END:VCARD
    `,
    expected: [
      {
        contact: {
          properties: {
            DisplayName: 'フルネームA',
            JobTitle: 'リーダー',
            PagerNumber: '外線TEL:000-0000-0000;内線TEL:000-0000-0000;携帯:000-0000-0000;PHS:000-0000-0000',
            PrimaryEmail: 'userA@example.jp',
            Notes: '注意書きA',
          },
          id: '0001',
        },
        customValues: {
          version: 4,
          'sort-string': ['00000'],
          'x-company-name': ['組織A'],
          'x-group-name': ['グループA'],
          'x-unknown-field': ['未知のフィールド'],
          org: ['組織A', 'グループA', 'サブグループA'],
        },
        addressBookId: 'public',
      },
      {
        contact: {
          properties: {
            DisplayName: 'フルネームB',
            JobTitle: 'リーダー',
            PrimaryEmail: 'userB@example.jp',
            Notes: '注意書きB',
          },
          id: '0002',
        },
        customValues: {
          version: 4,
          'sort-string': ['00000'],
          'x-company-name': ['組織B'],
          'x-group-name': ['グループB'],
          org: ['組織B', 'グループB', 'サブグループB'],
        },
        addressBookId: 'private',
      },
    ],
  },
  'should be sorted': {
    input: `
BEGIN:VCARD
VERSION:4.0
FN:フルネームA
SORT-STRING:00003
END:VCARD
BEGIN:VCARD
VERSION:4.0
FN:フルネームB
SORT-STRING:00002
END:VCARD
BEGIN:VCARD
VERSION:4.0
FN:フルネームC
SORT-STRING:00001
END:VCARD
    `,
    expected: [
      {
        contact: {
          properties: {
            DisplayName: 'フルネームC',
          },
        },
        customValues: {
          version: 4,
          'sort-string': ['00001'],
        },
      },
      {
        contact: {
          properties: {
            DisplayName: 'フルネームB',
          },
        },
        customValues: {
          version: 4,
          'sort-string': ['00002'],
        },
      },
      {
        contact: {
          properties: {
            DisplayName: 'フルネームA',
          },
        },
        customValues: {
          version: 4,
          'sort-string': ['00003'],
        },
      },
    ],
  },
  'field ending with CR+LF': {
    input: `
BEGIN:VCARD
VERSION:4.0
FN:フルネーム
NOTE:注意書き
\r\nSORT-STRING:00000
END:VCARD
    `,
    expected: [
      {
        contact: {
          properties: {
            DisplayName: 'フルネーム',
            Notes: '注意書き',
          },
        },
        customValues: {
          version: 4,
          'sort-string': ['00000'],
        },
      },
    ],
  },
  'field ending with more white spaces': {
    input: `
BEGIN:VCARD
VERSION:4.0
FN:スペース+CR
NOTE:注意書き
 \rSORT-STRING:00000
END:VCARD
BEGIN:VCARD
VERSION:4.0
FN:スペース+CR+LF
NOTE:注意書き
 \r\nSORT-STRING:00000
END:VCARD
BEGIN:VCARD
VERSION:4.0
FN:タブ+CR
NOTE:注意書き
\t\rSORT-STRING:00000
END:VCARD
BEGIN:VCARD
VERSION:4.0
FN:タブ+CR+LF
NOTE:注意書き
\t\r\nSORT-STRING:00000
END:VCARD
    `,
    expected: [
      {
        contact: {
          properties: {
            DisplayName: 'スペース+CR',
            Notes: '注意書き',
          },
        },
        customValues: {
          version: 4,
          'sort-string': ['00000'],
        },
      },
      {
        contact: {
          properties: {
            DisplayName: 'スペース+CR+LF',
            Notes: '注意書き',
          },
        },
        customValues: {
          version: 4,
          'sort-string': ['00000'],
        },
      },
      {
        contact: {
          properties: {
            DisplayName: 'タブ+CR',
            Notes: '注意書き',
          },
        },
        customValues: {
          version: 4,
          'sort-string': ['00000'],
        },
      },
      {
        contact: {
          properties: {
            DisplayName: 'タブ+CR+LF',
            Notes: '注意書き',
          },
        },
        customValues: {
          version: 4,
          'sort-string': ['00000'],
        },
      },
    ],
  },
};
export async function test_parse({ input, expected }) {
  for (const entry of expected) {
    entry.contact.properties.Custom1 = JSON.stringify(entry.customValues);
  }
  const parsed = await VCard.parse(input.trim(), {
    addressBookIdField: 'addressbook_name',
  });
  is(
    expected,
    parsed
  );
}
