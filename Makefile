NPM_MOD_DIR := $(CURDIR)/node_modules
NPM_BIN_DIR := $(NPM_MOD_DIR)/.bin

PACKAGE_NAME = corporative-contacts

.PHONY: xpi install_dependency install_hook lint format init_extlib update_extlib install_extlib test

all: xpi

install_dependency:
	[ -e "$(NPM_BIN_DIR)/eslint" -a -e "$(NPM_BIN_DIR)/jsonlint-cli" ] || npm install

install_hook:
	echo '#!/bin/sh\nmake lint' > "$(CURDIR)/.git/hooks/pre-commit" && chmod +x "$(CURDIR)/.git/hooks/pre-commit"

lint: install_dependency
	"$(NPM_BIN_DIR)/eslint" . --ext=.js --report-unused-disable-directives
	find . -type d -name node_modules -prune -o -type f -name '*.json' -print | xargs "$(NPM_BIN_DIR)/jsonlint-cli"

format: install_dependency
	"$(NPM_BIN_DIR)/eslint" . --ext=.js --report-unused-disable-directives --fix

xpi: init_extlib install_extlib lint
	rm -f ./*.xpi
	zip -r -9 $(PACKAGE_NAME)-we.xpi manifest.json common background ui options resources extlib _locales LICENSE -x '*/.*' >/dev/null 2>/dev/null

init_extlib:
	git submodule update --init

update_extlib:
	git submodule foreach 'git checkout better-performance || git checkout trunk || git checkout main || git checkout master && git pull'

install_extlib: install_dependency
	rm -f extlib/*.js extlib/*.css
	cp submodules/webextensions-lib-configs/Configs.js extlib/; echo 'export default Configs;' >> extlib/Configs.js
	cp submodules/webextensions-lib-options/Options.js extlib/; echo 'export default Options;' >> extlib/Options.js
	cp submodules/webextensions-lib-l10n/l10n.js extlib/; echo 'export default l10n;' >> extlib/l10n.js
	cp submodules/webextensions-lib-dialog/dialog.js extlib/
	cp submodules/webextensions-lib-dialog/dialog.css extlib/
	#cp node_modules/tabulator-tables/dist/js/tabulator.es2015.js extlib/
	cp submodules/patched-tabulator/dist/js/tabulator.es2015.js extlib/
	cp node_modules/tabulator-tables/dist/css/tabulator_simple.min.css extlib/
	cp submodules/webextensions-lib-resizable-box/resizable-box.js extlib/
	echo '/* tabbis.js: licensed under the MIT License by Jens Törnell */' > extlib/tabbis.es6.min.js; cat submodules/tabbis.js/assets/js/dist/tabbis.es6.min.js >> extlib/tabbis.es6.min.js; sed -i -r -e 's/(function tabbis)/export \1/' extlib/tabbis.es6.min.js
	#cp submodules/tabbis.js/assets/css/dist/style-default.min.css extlib/
	cp submodules/webextensions-lib-dom-updater/src/diff.js extlib/
	cp submodules/webextensions-lib-dom-updater/src/dom-updater.js extlib/
	cp submodules/webextensions-lib-event-listener-manager/EventListenerManager.js extlib/
	#cp node_modules/dav/dav.min.js extlib/; echo '' >> extlib/dav.min.js; echo 'export const dav = window.dav;' >> extlib/dav.min.js
	echo '/* dav.js: licensed under MPL 2.0; Original: https://github.com/lambdabaa/dav; Authentication header support: https://github.com/himselfv/tasks-ig-dav/tree/httpauth; Some more modifications: https://github.com/clear-code/dav */' > extlib/dav.js; cat node_modules/dav/dav.js >> extlib/dav.js; echo '' >> extlib/dav.js; echo 'export const dav = window.dav;' >> extlib/dav.js
	cp submodules/webextensions-lib-menu-ui/MenuUI.js extlib/; echo 'export default MenuUI;' >> extlib/MenuUI.js
	cp submodules/rfc4180.js/rfc4180.js extlib/; sed -i -e 's/var RFC4180/export const RFC4180/' -e 's/var XMLHttpRequest/const MyXHR/' -e 's/new XMLHttpRequest/new MyXHR/g' extlib/rfc4180.js
	cp submodules/encoding/encodings.json extlib/
	cp submodules/webextensions-lib-rich-confirm/RichConfirm.js extlib/; echo 'export default RichConfirm;' >> extlib/RichConfirm.js

test:
	npm run test
