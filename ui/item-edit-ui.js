/*
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
*/
'use strict';

import '/extlib/l10n.js';
import { DOMUpdater } from '/extlib/dom-updater.js';

import {
  sanitizeForHTMLText,
  toDOMDocumentFragment,
} from '/common/common.js';
import * as VCard from '/common/import/vcard.js';

const CATEGORY_NAME_SANITIZER = new RegExp(VCard.CATEGORIES_SEPARATOR, 'g');

export function buildAddressBooksSelector(addressBooks) {
  const addressBooksSelector = document.querySelector('[data-item-field="parentId"]');
  DOMUpdater.update(
    addressBooksSelector,
    toDOMDocumentFragment(
      addressBooks.map(addressBook =>
        `<option value="${sanitizeForHTMLText(addressBook.id)}"
                 ${addressBook.readOnly ? 'disabled' : ''}
                >${sanitizeForHTMLText(addressBook.name)}</option>`
      ).join(''),
      addressBooksSelector
    )
  );
}

export function buildCategories(item, { allCategories }) {
  const container = document.querySelector('#categories-container');
  const selectedCategories = new Set(item.categories);
  const summaryField = `
    <p id="categories-summary"
       class="summary"
      ><input id="categories-summary-field"
              type="text"
              value=${JSON.stringify(sanitizeForHTMLText(item.categories.join(',')))}
              readonly></p>
  `.trim();
  const knownCategories = Array.from(
    allCategories,
    category => `<option id="known-category:${sanitizeForHTMLText(category)}" value=${JSON.stringify(sanitizeForHTMLText(category))}>`
  ).join('');
  const inputField = `
    <p id="add-category-container"
       class="detail primary"
       ><input id="add-category-input-field"
               type="text"
               placeholder=${JSON.stringify(sanitizeForHTMLText(browser.i18n.getMessage('ui_item_newCategoryPlaceholder')))}
               list="known-categories">
        <datalist id="known-categories">${knownCategories}</datalist></p>
  `.trim();
  const contents = [
    summaryField,
    inputField,
    '<ul id="categories-list" class="detail">',
    ...Array.from(allCategories, category =>
      `<li id="category-item:${sanitizeForHTMLText(category)}"
          ><label id="category-label:${sanitizeForHTMLText(category)}"
                 ><input id="category-checkbox:${sanitizeForHTMLText(category)}"
                         type="checkbox"
                         value="${sanitizeForHTMLText(category)}"
                         ${selectedCategories.has(category) ? 'checked' : ''}
                        >${sanitizeForHTMLText(category)}</label></li>`
    ),
    '</ul>',
  ];
  DOMUpdater.update(container, toDOMDocumentFragment(contents.join(''), container));
}

export function initDetailContainers() {
  for (const container of document.querySelectorAll('.has-detail')) {
    container.addEventListener('transitionstart', _event => {
      container.classList.remove('animation-finished');
    });
    container.addEventListener('transitionend', _event => {
      container.classList.add('animation-finished');
    });
    document.addEventListener('focusin', event => {
      const focused = !!(
        event.target &&
        typeof event.target.closest == 'function' &&
        container.contains(event.target)
      );
      if (focused == container.classList.contains('focused') ||
          event.target.matches('[role="tabpanel"]')) // when a checkbox label is clicked, the tabpanel gets focus unexpectedly...
        return;
      const focusToDetailField = focused && event.target.closest('.summary');
      container.classList.toggle('focused', focused);
      if (focusToDetailField)
        container.querySelector('.detail.primary input, .detail input').focus();
    });
  }
}

export function initCategoriesEventHandlers(item, { allCategories }) {
  const categoriesContainer = document.querySelector('#categories-container');
  categoriesContainer.addEventListener('keydown', event => {
    if (!event.target.matches('input[type="text"]') ||
        event.key != 'Enter' ||
        event.isComposing)
      return;
    event.stopPropagation();
    event.preventDefault();
    const name = event.target.value.replace(CATEGORY_NAME_SANITIZER, '');
    item.categories = Array.from(new Set([...item.categories, name]));
    allCategories.add(name);
    event.target.value = '';
    buildCategories(item, { allCategories });
  });
  categoriesContainer.addEventListener('change', event => {
    if (!event.target.matches('input[type="checkbox"]'))
      return;
    const name = event.target.value;
    const categories = new Set(item.categories);
    if (categories.has(name) == event.target.checked)
      return;
    if (event.target.checked)
      categories.add(name);
    else
      categories.delete(name);
    item.categories = Array.from(categories);
    document.querySelector('#categories-summary-field').value = item.categories.join(',');
  });
}
