/*
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
*/
'use strict';

import * as Dialog from '/extlib/dialog.js';
import { DOMUpdater } from '/extlib/dom-updater.js';

import {
  configs,
  log,
  sanitizeForHTMLText,
  toDOMDocumentFragment,
} from '/common/common.js';
import * as BackgroundConnection from '/ui/background-connection.js';
import { ChatProvider } from '/common/ChatProvider.js';
import * as Commands from '/ui/commands.js';
import * as Constants from '/common/constants.js';
import { Contact, MailingList, normalizeItem } from '/common/Contact.js';
import { CustomPersonalField, CustomOrganizationField } from '/common/CustomField.js';
import * as DataStore from '/ui/data-store.js';

let mItem;

const mChatButtonContainer = document.querySelector('#item-details-start-chat-button-container');
const mChatButton = document.querySelector('#item-details-start-chat-button');
let mChatProvider;

const mContactsContainer = document.querySelector('#item-details-mailing-list-contacts');

export function init() {
  mContactsContainer.addEventListener('click', event => {
    const link = event.target.closest('a[data-contact-id]');
    if (!link ||
        event.button != 0)
      return true;
    event.stopPropagation();
    event.preventDefault();
    Commands.editItem(Contact.get(link.dataset.contactId));
    return false;
  });
  mContactsContainer.addEventListener('keydown', event => {
    const link = event.target.closest('a[data-contact-id]');
    if (!link ||
        (event.key != 'Enter' && event.key != ' '))
      return true;
    event.stopPropagation();
    event.preventDefault();
    Commands.editItem(Contact.get(link.dataset.contactId));
    return false;
  });

  mChatProvider = configs.preferredChatProvider && ChatProvider.get(configs.preferredChatProvider);
  Dialog.initButton(mChatButton, _event => {
    mChatProvider.open(mItem);
  });
}

function rebuildCustomFields() {
  const personalFieldNames = [];
  const personalFields = [];
  for (const field of CustomPersonalField.getAll()) {
    if (field.bindTo)
      continue;
    personalFieldNames.push(field.fieldPath);
    personalFields.push(`
      <tr id=${JSON.stringify(sanitizeForHTMLText('custom-personal-field:' + field.id))}
          data-item-fields=${JSON.stringify(sanitizeForHTMLText(field.fieldPath))}>
        <th id=${JSON.stringify(sanitizeForHTMLText('custom-personal-field:' + field.id + ':label'))}
           >${sanitizeForHTMLText(field.label)}</th>
        <td id=${JSON.stringify(sanitizeForHTMLText('custom-personal-field:' + field.id + ':value'))}
            data-item-field=${JSON.stringify(sanitizeForHTMLText(field.fieldPath))}
           ></td>
      </tr>
    `.trim());
  }
  const personalFieldsContainer = document.querySelector('#custom-personal-information-fields');
  DOMUpdater.update(personalFieldsContainer, toDOMDocumentFragment(personalFields.join(''), personalFieldsContainer));
  personalFieldsContainer.dataset.itemFields = personalFieldNames.join(' ');
  personalFieldsContainer.closest('fieldset').dataset.itemFields = [
    personalFieldsContainer.previousElementSibling.dataset.itemFields,
    ...personalFieldNames,
  ].join(' ');

  const organizationFieldNames = [];
  const organizationFields = [];
  for (const field of CustomOrganizationField.getAll()) {
    if (field.bindTo)
      continue;
    organizationFieldNames.push(field.fieldPath);
    organizationFields.push(`
      <tr id=${JSON.stringify(sanitizeForHTMLText('custom-organization-field:' + field.id))}
          data-item-fields=${JSON.stringify(sanitizeForHTMLText(field.fieldPath))}>
        <th id=${JSON.stringify(sanitizeForHTMLText('custom-organization-field:' + field.id + ':label'))}
           >${sanitizeForHTMLText(field.label)}</th>
        <td id=${JSON.stringify(sanitizeForHTMLText('custom-organization-field:' + field.id + ':value'))}
            data-item-field=${JSON.stringify(sanitizeForHTMLText(field.fieldPath))}
           ></td>
      </tr>
    `.trim());
  }
  const organizationFieldsContainer = document.querySelector('#custom-organization-information-fields');
  DOMUpdater.update(organizationFieldsContainer, toDOMDocumentFragment(organizationFields.join(''), organizationFieldsContainer));
  organizationFieldsContainer.dataset.itemFields = organizationFieldNames.join(' ');
  organizationFieldsContainer.closest('fieldset').dataset.itemFields = [
    organizationFieldsContainer.previousElementSibling.dataset.itemFields,
    ...organizationFieldNames,
  ].join(' ');
}

function rebuildIMPPFields() {
  const fieldNames = [];
  const fields = [];
  for (const provider of ChatProvider.getAll()) {
    if (!provider)
      continue;
    const fieldName = provider.fieldPath;
    fieldNames.push(fieldName);
    const linkMatcher = provider.linkMatcher ? `data-link-matcher="${provider.linkMatcher}"` : '';
    const linkPrefix  = provider.linkPrefix ? `data-link-prefix="${provider.linkPrefix}"` : '';
    fields.push(`
      <tr id=${JSON.stringify(sanitizeForHTMLText('impp-field:' + provider.id))}
          data-item-fields=${JSON.stringify(sanitizeForHTMLText(fieldName))}>
        <th id=${JSON.stringify(sanitizeForHTMLText('impp-field:' + provider.id + ':label'))}
           >${sanitizeForHTMLText(provider.label || provider.field || provider)}</th>
        <td id=${JSON.stringify(sanitizeForHTMLText('impp-field:' + provider.id + ':value'))}
            data-item-field=${JSON.stringify(sanitizeForHTMLText(fieldName))}
            ${linkMatcher}
            ${linkPrefix}
            class="text-selectable"></td>
      </tr>
    `.trim());
  }

  const imppContainer = document.querySelector('#impp-fields');
  DOMUpdater.update(imppContainer, toDOMDocumentFragment(fields.join(''), imppContainer));
  imppContainer.closest('fieldset').dataset.itemFields = fieldNames.join(' ');
}

export async function update(item = null) {
  log('render item details ', item);
  mItem = item && normalizeItem(await (item.type == 'contact' ? DataStore.getRawContact(item.id) : browser.mailingLists.get(item.id)));
  log(' => ', mItem);
  if (mItem)
    await mItem.initialized;

  rebuildCustomFields();
  rebuildIMPPFields();

  for (const field of document.querySelectorAll('[data-item-field]')) {
    field.classList.add('hidden');
    const rawValue = mItem && mItem.getFieldValue(field.dataset.itemField);
    let stringValue = String(rawValue || '').trim();
    const linkify = input => {
      if (!field.dataset.linkMatcher)
        return input;
      const matcher = new RegExp(field.dataset.linkMatcher, 'gi');
      return input.replace(matcher, matched => {
        const href = `${field.dataset.linkPrefix || ''}${matched}`;
        const target = Constants.BROWSER_URL_MATCHER.test(href) ? ' target="_blank"' : '';
        return `<a href="${sanitizeForHTMLText(href)}" ${target}>${sanitizeForHTMLText(matched)}</a>`;
      });
    };
    //log(`${field.dataset.itemField} => ${rawValue}`);
    switch (field.localName) {
      case 'img':
        field.src = stringValue;
        break;

      case 'input':
      case 'textarea':
        field.value = stringValue;
        break;

      case 'ul':
      case 'ol': {
        const arrayValue = Array.isArray(rawValue) ? rawValue : [rawValue];
        DOMUpdater.update(field, toDOMDocumentFragment(arrayValue.map(value => `<li>${linkify(value)}</li>`).join(''), field));
      }; break;

      default:
        stringValue = linkify(stringValue);
        if (field.localName != 'pre')
          stringValue = stringValue.split(/\s*;\s*/).join('\n').trim();
        DOMUpdater.update(field, toDOMDocumentFragment(stringValue, field));
        break;
    }
    field.classList.toggle('hidden', stringValue == '');
  }

  for (const container of document.querySelectorAll('[data-item-fields]')) {
    const keys = container.dataset.itemFields.split(/\s+/);
    let visibleCount = 0;
    for (const key of keys) {
      if (!document.querySelector(`[data-item-field="${key}"].hidden`))
        visibleCount++;
    }
    container.classList.toggle('hidden', visibleCount == 0);
  }

  for (const node of document.querySelectorAll('[data-item-type]')) {
    if (!node.classList.contains('hidden'))
      node.classList.toggle('hidden', !mItem || (mItem.type != node.dataset.itemType));
  }

  mChatButtonContainer.classList.toggle('hidden', !mItem || !mChatProvider || !mChatProvider.generateLinkFor(mItem));

  if (mItem && mItem.type == 'mailingList') {
    DOMUpdater.update(mContactsContainer, toDOMDocumentFragment(mItem.contacts.map(contact => `
      <tr><td class="text-selectable"
             >${sanitizeForHTMLText(contact.email)}</td>
          <td class="text-selectable"
             ><a href="#"
                 data-contact-id=${JSON.stringify(sanitizeForHTMLText(contact.id))}
                >${sanitizeForHTMLText(contact.visibleDisplayName)}</a></td></tr>
    `.trim()).join(''), mContactsContainer));
    mContactsContainer.closest('fieldset').classList.remove('hidden');
  }
  else {
    mContactsContainer.closest('fieldset').classList.add('hidden');
  }
}


Contact.onUpdated.addListener((contactId, _updatedFields, contact) => {
  if (mItem && mItem.id == contactId)
    update(contact);
});

Contact.onRemoved.addListener((_parentId, contactId) => {
  if (mItem && mItem.id == contactId)
    update();
});

BackgroundConnection.onMessage.addListener(async message => {
  switch (message && message.type) {
    case Constants.COMMAND_PUSH_ITEM_UPDATE: {
      const item = Contact.get(message.id) || MailingList.get(message.id);
      if (mItem &&
          item &&
          mItem.id == message.id)
        update(item);
    }; break;
  }
});

MailingList.onUpdated.addListener((mailingListId, _updatedFields, mailingList) => {
  if (mItem && mItem.id == mailingListId)
    update(mailingList);
});

MailingList.onRemoved.addListener((_parentId, mailingListId) => {
  if (mItem && mItem.id == mailingListId)
    update();
});

MailingList.onContactAdded.addListener((mailingList, _contact) => {
  if (mItem && mItem.id == mailingList.id)
    update(mailingList);
});

MailingList.onContactDeleted.addListener((mailingList, _contactId) => {
  if (mItem && mItem.id == mailingList.id)
    update();
});
