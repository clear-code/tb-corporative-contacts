/*
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
*/
'use strict';

import EventListenerManager from '/extlib/EventListenerManager.js';
import MenuUI from '/extlib/MenuUI.js';
import Tabulator from '/extlib/tabulator.es2015.js';

import {
  configs,
  log,
  orderItems,
} from '/common/common.js';
import * as BackgroundConnection from '/ui/background-connection.js';
import * as Commands from '/ui/commands.js';
import * as Constants from '/common/constants.js';
import * as DataStore from '/ui/data-store.js';
import * as Menu from './menu.js';
import { TableNavigation } from '/common/TableNavigation.js';

let mStaticAddressBooks = [];
let mAddressBooks = [];
let mFilteredAddressBooks = [];
let mTable;
let mContextMenu;
const mAddressBooksFilter = document.querySelector('#address-books-filter');
//const mProgressBar = document.querySelector('#address-books-progress-bar');

export const onSelectionChanged = new EventListenerManager();

function objectify(item) {
  const cloned = Object.assign({}, item);
  if (cloned._children) {
    cloned._children = cloned._children.reduce((children, child) => {
      if ((child.isOrganization &&
           configs.showOrganizationsInTree) ||
          (child.isCategory &&
           configs.showCategoriesInTree) ||
          (child.isUncategorized &&
           configs.showUncategorizedInTree))
        children.push(objectify(child));
      return children;
    }, []);
    if (cloned._children.length == 0)
      delete cloned._children;
  }
  return cloned;
}

function normalizeGroup(group) {
  return DataStore.getAddressBook(group.id) || DataStore.getSubGroup(group.id);
}

export function getAddressBooks() {
  return mFilteredAddressBooks;
}

function getFilteredAddressBooks() {
  switch (configs.lastAddressBooksFilter) {
    case 'local':
      return mAddressBooks.filter(addressBook => !addressBook.hidden && addressBook.isLocal);

    case 'remote':
      return mAddressBooks.filter(addressBook => !addressBook.hidden && addressBook.isRemote);

    case 'all':
    default:
      return mAddressBooks.filter(addressBook => !addressBook.hidden);
  }
}

export async function load() {
  BackgroundConnection.sendMessage({
    type: Constants.COMMAND_FETCH_ALL_ADDRESS_BOOKS,
  });
}

let mInitiallyLoadedResolver;
const mInitiallyLoaded = new Promise(resolev => {
  mInitiallyLoadedResolver = resolev;
});

async function initialLoad(addressBooks) {
  mAddressBooks = addressBooks;
  log('addressbooks loaded: ', mAddressBooks.map(a => `${a.name}:${a.index}`));

  const staticAddressBooksDefinitions = configs.staticAddressBooks;
  const staticAddressBooksById = new Map();
  const userAddressBooks = [];
  for (const addressBook of mAddressBooks) {
    if (addressBook.staticId) {
      staticAddressBooksById.set(addressBook.staticId, addressBook);
    }
    else {
      userAddressBooks.push(addressBook);
    }
  }
  mStaticAddressBooks = Array.from(Object.keys(staticAddressBooksDefinitions), (id, index) => {
    const addressBook = staticAddressBooksById.get(id);
    addressBook.index = index;
    return addressBook;
  });

  mAddressBooks = [
    ...mStaticAddressBooks,
    ...orderItems(userAddressBooks, {
      getItemIndex(addressBook) {
        return Math.max(-1, addressBook.index - mStaticAddressBooks.length);
      },
      /*
      onInitialized(addressBook) {
      },
      */
      onOrdered(addressBook, index) {
        addressBook.index = index + mStaticAddressBooks.length;
        BackgroundConnection.sendMessage({
          type:   Constants.COMMAND_PUSH_GROUP_UPDATE,
          id:     addressBook.id,
          fields: {
            index: addressBook.index,
          }
        });
      },
    }),
  ]
  mFilteredAddressBooks = getFilteredAddressBooks();
  await mTable.setData(mFilteredAddressBooks.map(objectify));
  mInitiallyLoadedResolver();
}

function dataTreeStartExpanded(row, _level) {
  return row.getData().expanded;
}

function dataTreeRowExpanded(row, _level) {
  const group = row.getData();
  normalizeGroup(group).expanded = group.expanded = true;
  BackgroundConnection.sendMessage({
    type:   Constants.COMMAND_PUSH_GROUP_UPDATE,
    id:     group.id,
    fields: {
      expanded: group.expanded,
    }
  });
}

function dataTreeRowCollapsed(row, _level) {
  const group = row.getData();
  normalizeGroup(group).expanded = group.expanded = false;
  BackgroundConnection.sendMessage({
    type:   Constants.COMMAND_PUSH_GROUP_UPDATE,
    id:     group.id,
    fields: {
      expanded: group.expanded,
    }
  });
}

function rowSelected(row) {
  onSelectionChanged.dispatch(normalizeGroup(row.getData()));
}

function rowDblClick(event, row) {
  window.getSelection().removeAllRanges(); // clear selection produced by Tabulator

  const group = row.getData();
  if (group.editableInUI) {
    return Commands.editAddressBook(group);
  }

  row.treeToggle();
}

function onItemsOpen(groups) {
  if (groups.length > 0 &&
      groups[0].editableInUI)
    Commands.editAddressBook(groups[0]);
}

function onItemsDelete(groups) {
  const addressBooks = groups.filter(group => group.deletable);
  if (addressBooks.length > 0)
    Commands.deleteAddressBook(addressBooks[0]);
}

function canMoveRow(row, { newParent } = {}) {
  const group = row.getData();
  if (!group.parentId) {
    return (
      !newParent &&
      group.movable &&
      row.getPosition(true) >= mStaticAddressBooks.length
    );
  }
  return newParent && row.getData().parentId == newParent.getData().id;
}

async function onItemMoved(group, position) {
  if (group.hasOwnIndex) {
    const normalized = normalizeGroup(group);
    normalized.index = position;
    BackgroundConnection.sendMessage({
      type:   Constants.COMMAND_PUSH_GROUP_UPDATE,
      id:     normalized.id,
      fields: {
        index: normalized.index,
      }
    });
  }
}

function onItemsMoved(groups) {
  mFilteredAddressBooks = mTable.getData().map(normalizeGroup);

  const relatedAddressBooks = new Set(groups.map(group => {
    const normalized = normalizeGroup(group);
    return DataStore.getAddressBook(normalized.ownerAddressBookId) || normalized;
  }));
  for (const addressBook of relatedAddressBooks) {
    BackgroundConnection.sendMessage({
      type: Constants.COMMAND_MOVE_UNCATEGORIZED_TO_END,
      id:   addressBook.id,
    });
  }
}

async function onItemAttached(_group, { newParent } = {}) {
  const parent = normalizeGroup(newParent);
  newParent._children = parent._children = newParent._children.map(normalizeGroup);
  for (let i = 0, maxi = newParent._children.length; i < maxi; i++) {
    const child = newParent._children[i];
    const normalized = normalizeGroup(child);
    normalized.index = child.index = i;
    BackgroundConnection.sendMessage({
      type:   Constants.COMMAND_PUSH_GROUP_UPDATE,
      id:     child.id,
      fields: {
        index: child.index,
      }
    });
  }
  BackgroundConnection.sendMessage({
    type: Constants.COMMAND_SORT_CHILDREN_BY_INDEX,
    id:   parent.id,
  });
}

function throttledAddressBooksTreeUpdate() {
  if (throttledAddressBooksTreeUpdate.throttled)
    clearTimeout(throttledAddressBooksTreeUpdate.throttled);
  throttledAddressBooksTreeUpdate.throttled = setTimeout(async () => {
    throttledAddressBooksTreeUpdate.throttled = null;
    mFilteredAddressBooks = getFilteredAddressBooks();
    const oldSelectedItems = mTable.getSelectedRows().map(row => row.getData());
    await mTable.setData(mFilteredAddressBooks.map(objectify));
    // We cannot select sub group row via piblic API for now...
    if (oldSelectedItems.length == 0)
      return;
    const newSelectedId = oldSelectedItems[0].ownerAddressBookId || oldSelectedItems[0].id;
    if (mFilteredAddressBooks.some(addressBook => addressBook.id == newSelectedId))
      mTable.selectRow([newSelectedId])
  }, 150);
}


let mLastContextItem;

function rowContext(event, row) {
  event.preventDefault();
  mLastContextItem = normalizeGroup(row.getData());
  const selectedItemIds = mTable.getSelectedRows().map(row => row.getIndex());
  if (selectedItemIds.length > 0 && !selectedItemIds.some(id => id == mLastContextItem.id))
    mTable.deselectRow(selectedItemIds);
  mTable.selectRow([mLastContextItem.id]);
  updateContextMenu(event);
  mContextMenu.open({
    left: event.clientX,
    top:  event.clientY
  });
}

function onContextMenuCommand(item, event) {
  Menu.onAddressBooksCommand({
    item,
    addressBook: mLastContextItem,
    event,
    menuUI: mContextMenu,
  });
  mContextMenu.close();
}

function updateContextMenu(_event) {
  Menu.updateAddressBooksMenu({
    menu:        document.querySelector('#address-book-tree-context'),
    addressBook: mLastContextItem,
  });
}


export function init() {
  mTable = new Tabulator('#address-books-tree', {
    data: mFilteredAddressBooks,
    dataIsolated: true,
    dataTree: true,
    dataTreeElementColumn: 'name',
    dataTreeStartExpanded,
    dataTreeRowExpanded,
    dataTreeRowCollapsed,
    layout: 'fitColumns',
    headerSort: false,
    tooltips: true,
    columns: [
      {
        title: '',
        field: 'color',
        width: 16,
        formatter(cell, _formatterParams, _onRendered) {
          const colorTip = document.createElement('span');
          colorTip.classList.add('color-tip');
          const group = cell.getData();
          if (group.color)
            colorTip.style.background = group.color;
          return colorTip;
        },
      },
      { title: '', field: 'name' },
      {
        title: '',
        field: 'status',
        width: 20,
        formatter(cell, _formatterParams, _onRendered) {
          const icon = document.createElement('span');
          icon.classList.add('svg-icon');
          icon.classList.add('status-icon');
          const group = normalizeGroup(cell.getData());
          if (group.status)
            icon.classList.add(group.status);
          return icon;
        },
      },
    ],
    columnMinWidth: 16,
    resizableColumns: false,
    selectable: 1,
    selectableRangeMode: 'click',
    rowSelected,
    rowDblClick,
    rowContext,
    keybindings: false,
    movableRows: true,
  });
  mTable.$navigation = new TableNavigation(mTable, {
    onItemsOpen,
    onItemsDelete,
    canMoveRow,
    onItemMoved,
    onItemsMoved,
    onItemAttached,
    normalizer: normalizeGroup,
    exporter: objectify,
  });

  mContextMenu = new MenuUI({
    root: document.querySelector('#address-book-tree-context'),
    onCommand: onContextMenuCommand,
  });

  // for blank area
  document.querySelector('#address-books').addEventListener('contextmenu', event => {
    event.preventDefault();

    const rows = mTable.getSelectedRows();
    mLastContextItem = rows.length > 0 && normalizeGroup(rows[0].getData()) || null;

    updateContextMenu(event);
    mContextMenu.open({
      left: event.clientX,
      top:  event.clientY
    });
  });

  mAddressBooksFilter.addEventListener('change', _event => {
    configs.lastAddressBooksFilter = mAddressBooksFilter.value;
    throttledAddressBooksTreeUpdate();
  });
  mAddressBooksFilter.value = configs.lastAddressBooksFilter;
}


const deletedAddressBookIds = new Set();
let throttledDeleteTimer = null;

BackgroundConnection.onMessage.addListener(async message => {
  switch (message && message.type) {
    case Constants.COMMAND_PUSH_ALL_ADDRESS_BOOKS:
      initialLoad(message.addressBooks);
      break;

    case Constants.COMMAND_PUSH_NEW_ADDRESS_BOOK: {
      await mInitiallyLoaded;
      const addressBook = message.addressBook;
      mAddressBooks.push(addressBook);
      mFilteredAddressBooks = getFilteredAddressBooks();
      if (mFilteredAddressBooks.includes(addressBook))
        mTable.addData([objectify(addressBook)]);
    }; break;

    case Constants.COMMAND_PUSH_GROUP_UPDATE:
      await mInitiallyLoaded;
      if (DataStore.getAddressBook(message.id))
        mTable.updateData([{ id: message.id, ...message.fields }]);
      break;

    case Constants.COMMAND_PUSH_ADDRESS_BOOK_DELETE: {
      await mInitiallyLoaded;
      deletedAddressBookIds.add(message.id);
      if (throttledDeleteTimer)
        clearTimeout(throttledDeleteTimer);
      throttledDeleteTimer = setTimeout(() => {
        throttledDeleteTimer = null;
        //log('deletedAddressBookIds ', deletedAddressBookIds);

        const selectedRows = mTable.getSelectedRows();
        const wasActive = selectedRows.length > 0 && deletedAddressBookIds.has(selectedRows[0].getIndex());

        let lastFoundIndex = -1;
        for (let i = mAddressBooks.length - 1; i > -1; i--) {
          const addressBook = mAddressBooks[i];
          //log('  addressBook.id ', addressBook.id);
          if (!deletedAddressBookIds.has(addressBook.id))
            continue;
          lastFoundIndex = i;
          mAddressBooks.splice(i, 1);
          deletedAddressBookIds.delete(addressBook.id);
          if (deletedAddressBookIds.size == 0)
            break;
        }
        if (lastFoundIndex > -1) {
          //log('lastFoundIndex ', lastFoundIndex, mAddressBooks.length);
          for (let i = lastFoundIndex, maxi = mAddressBooks.length; i < maxi; i++) {
            //log(i, mAddressBooks[i], mAddressBooks[i] && mAddressBooks[i].index, ' => ', i);
            mAddressBooks[i].index = i;
            BackgroundConnection.sendMessage({
              type:   Constants.COMMAND_PUSH_GROUP_UPDATE,
              id:     mAddressBooks[i].id,
              fields: {
                index: mAddressBooks[i].index,
              },
            });
          }
        }
        throttledAddressBooksTreeUpdate();

        if (wasActive)
          onSelectionChanged.dispatch(null);
      }, 150);
    }; break;

    case Constants.COMMAND_PUSH_GROUP_UPDATE:
    case Constants.COMMAND_PUSH_ALL_CHILDREN_DISAPPEARED:
    case Constants.COMMAND_PUSH_NEW_GROUP_CHILD:
      await mInitiallyLoaded;
      throttledAddressBooksTreeUpdate();
      break;

    case Constants.COMMAND_PUSH_GROUP_CHILD_DELETE: {
      await mInitiallyLoaded;
      const selectedRows = mTable.getSelectedRows();
      const wasActive = selectedRows.length > 0 && selectedRows[0].getIndex() == message.childId;
      throttledAddressBooksTreeUpdate();
      if (wasActive)
        onSelectionChanged.dispatch(null);
    }; break;
  }
});
