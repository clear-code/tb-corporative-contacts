/*
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
*/
'use strict';

import * as Dialog from '/extlib/dialog.js';
import EventListenerManager from '/extlib/EventListenerManager.js';
import Tabulator from '/extlib/tabulator.es2015.js';

import {
  configs,
  log,
  doProgressively,
} from '/common/common.js';
import * as BackgroundConnection from '/ui/background-connection.js';
import { ChatProvider } from '/common/ChatProvider.js';
import * as Commands from '/ui/commands.js';
import * as Constants from '/common/constants.js';
import * as DataStore from '/ui/data-store.js';
import { TableNavigation } from '/common/TableNavigation.js';

let mTable;
let mChatProvider;
const mSelection = new Map();
const mChatButton = document.querySelector('#selected-items-start-chat-button');

export const onAdded = new EventListenerManager();
export const onRemoved = new EventListenerManager();

function objectify(item) {
  return {
    label: item.label,
    id:    item.id,
    type:  item.type,
  };
}

export function add(item) {
  if (mSelection.has(item.id))
    return;
  item = DataStore.getContact(item.id) || DataStore.getMailingList(item.id);
  item.$selected = true;
  mSelection.set(item.id, item);
  onAdded.dispatch(item);
  reserveToUpdatePalette();
}

export function remove(item) {
  if (!item || !mSelection.has(item.id))
    return;
  item = DataStore.getContact(item.id) || DataStore.getMailingList(item.id);
  item.$selected = false;
  mSelection.delete(item.id);
  onRemoved.dispatch(item);
  reserveToUpdatePalette();
}

export function has(item) {
  return mSelection.has(item.id) || mSelection.has(item);
}

export function clear({ except } = {}) {
  for (const item of mSelection.values()) {
    if (except && item.id == except.id)
      continue;
    item.$selected = false;
    mSelection.delete(item.id);
    onRemoved.dispatch(item);
  }
  reserveToUpdatePalette();
}

export function getSize() {
  return mSelection.size;
}

export function getAll() {
  return Array.from(mSelection.values());
}

function reserveToUpdatePalette() {
  if (reserveToUpdatePalette.throttled)
    clearTimeout(reserveToUpdatePalette.throttled);
  reserveToUpdatePalette.throttled = setTimeout(() => {
    reserveToUpdatePalette.throttled = undefined;
    updatePalette();
  }, 150);
}
reserveToUpdatePalette.throttled = undefined;

async function updatePalette() {
  const startedAt = updatePalette.startedAt = Date.now();
  const itemDetailsBox = document.querySelector('#item-details');
  const selectionPalette = document.querySelector('#selection-palette');

  if (mSelection.size <= 1) {
    selectionPalette.classList.add('hidden');
    itemDetailsBox.classList.remove('hidden');
  }

  mChatProvider = configs.preferredChatProvider && ChatProvider.get(configs.preferredChatProvider);
  const allAvailableChatProviderIds = mChatProvider && new Set(Array.from(mSelection.values(), item => item.availableChatProviderIds || []).flat());
  log('chatProvider: ', configs.preferredChatProvider, ' => ', mChatProvider, allAvailableChatProviderIds);
  mChatButton.classList.toggle('hidden', !mChatProvider || !mChatProvider.linkPrefix || !allAvailableChatProviderIds.has(configs.preferredChatProvider));

  /*
  mTable.setData(Array.from(mSelection.values(), objectify));
  */
  mTable.setData([]);
  const completed = await doProgressively({
    items: Array.from(mSelection.values(), objectify),
    task: async items => {
      if (updatePalette.startedAt != startedAt)
        return false;
      await mTable.addData(items);
    },
    chunkSize: configs.itemsRenderChunkSize,
    interval:  configs.itemsRenderInterval,
  });

  if (completed && mSelection.size > 1) {
    itemDetailsBox.classList.add('hidden');
    selectionPalette.classList.remove('hidden');
  }
}

(async () => {
  mTable = new Tabulator('#selected-items', {
    data: [],
    dataIsolated: true,
    layout: 'fitColumns',
    layoutColumnsOnNewData: true,
    height: '100%',
    tooltips: true,
    index: 'id',
    columns: [
      {
        field: 'label',
        widthGrow: 1,
      },
      {
        formatter(_cell, _formatterParams, _onRendered) {
          return '<button>X</button>';
        },
        width: 32,
        widthShrink: 1,
        hozAlign: 'center',
        cellClick: (_event, cell) => {
          const data = cell.getRow().getData();
          const item = DataStore.getContact(data.id) || DataStore.getMailingList(data.id);
          remove(item);
        },
      },
    ],
    selectable: false,
    keybindings: false,
    movableRows: false,
  });
  mTable.$navigation = new TableNavigation(mTable, {
    onItemsDelete(items) {
      for (const item of items) {
        remove(item);
      }
    },
    normalizer: item => DataStore.getContact(item.id) || DataStore.getMailingList(item.id),
    exporter: objectify,
  });

  Dialog.initButton(document.querySelector('#selected-items-start-composition-button'), _event => {
    const to = Commands.itemsToEmails(mSelection.values());
    if (to.length == 0)
      return;
    browser.compose.beginNew({ to });
  });

  Dialog.initButton(document.querySelector('#selected-items-clear-button'), _event => {
    clear();
  });

  await configs.$loaded;

  Dialog.initButton(mChatButton, _event => {
    log('selected: ', mSelection.keys());
    if (mChatProvider)
      mChatProvider.open(Array.from(mSelection.values()));
  });

  BackgroundConnection.onMessage.addListener(message => {
    switch (message && message.type) {
      case Constants.COMMAND_PUSH_ITEM_DELETE:
        if (!mSelection.has(message.id))
          return;
        mSelection.delete(message.id);
        reserveToUpdatePalette();
        break;
    }
  });
})();
