/*
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
*/
'use strict';

import { DOMUpdater } from '/extlib/dom-updater.js';
import EventListenerManager from '/extlib/EventListenerManager.js';
import MenuUI from '/extlib/MenuUI.js';
import Tabulator from '/extlib/tabulator.es2015.js';

import {
  configs,
  log,
  orderItems,
  sanitizeForHTMLText,
  toDOMDocumentFragment,
  doProgressively,
} from '/common/common.js';
import * as BackgroundConnection from '/ui/background-connection.js';
import * as Commands from '/ui/commands.js';
import * as Constants from '/common/constants.js';
import * as DataStore from '/ui/data-store.js';
import * as DB from '/common/db.js';
import { ItemsSearcher } from '/common/ItemsSearcher.js';
import * as Menu from './menu.js';
import * as Selection from './selection.js';
import { TableNavigation } from '/common/TableNavigation.js';

let mItems = [];
let mTable;
let mParent;
let mAddressBook;
let mContextMenu;
let mHeaderContextMenu;
const mProgressBar = document.querySelector('#items-progress-bar');

function objectify(item) {
  return Object.assign({}, item);
}

function normalizeItem(item) {
  return DataStore.getContact(item.id) || DataStore.getMailingList(item.id);
}

export const onFocusChanged = new EventListenerManager();

const mSearcher = new ItemsSearcher();
let mSearchCanceled = false;
let mSearchProcessedCount;
let mSearchTotalCount;

mSearcher.bindTo(document.querySelector('#search-field'));

mSearcher.onCleared.addListener(_searcher => {
  mTable.setData([]);
  //mTable.clearFilter(true);
  mTable.hideColumn('color');
  mTable.showColumn('visibleIndex');
  mTable.setData(mItems.map(objectify));
});

let mOnReceiveAllMailingLists = null;
let mOnReceiveAllContacts = null;

mSearcher.onStarted.addListener(async _searcher => {
  mTable.setData([]);
  mTable.hideColumn('visibleIndex');
  mTable.showColumn('color');
  //mTable.setFilter('searchString', 'keywords', query, { matchAll: true });
  const [mailingLists, contacts] = await Promise.all([
    new Promise(resolve => {
      mOnReceiveAllMailingLists = resolve;
      BackgroundConnection.sendMessage({
        type: Constants.COMMAND_FETCH_ALL_MAILING_LISTS,
      });
    }),
    new Promise(resolve => {
      mOnReceiveAllContacts = resolve;
      BackgroundConnection.sendMessage({
        type: Constants.COMMAND_FETCH_ALL_CONTACTS,
      });
    }),
  ]);
  mOnReceiveAllMailingLists = null;
  mOnReceiveAllContacts = null;
  const items = [
    ...mailingLists,
    ...contacts,
  ];
  mSearchCanceled = false;
  mSearchTotalCount = items.length;
  mSearchProcessedCount = 0;
  mProgressBar.value = 0;
  return items;
});

mSearcher.onFound.addListener(async (searcher, items, query) => {
  /*
  mTable.addData(items.map(objectify));
  */
  const completed = await doProgressively({
    items,
    task: async items => {
      if (mSearchCanceled || searcher.activeQuery != query)
        return false;
      await mTable.addData(items.map(objectify));
      mSearchProcessedCount += items.length;
      mProgressBar.value = Math.floor(mSearchProcessedCount / mSearchTotalCount * 100);
    },
    chunkSize: configs.itemsRenderChunkSize,
    interval:  configs.itemsRenderInterval,
  });
  if (completed)
    mProgressBar.value = 100;
});

mSearcher.onCanceled.addListener(async _searcher => {
  mSearchCanceled = true;
  mOnReceiveAllMailingLists = null;
  mOnReceiveAllContacts = null;
});


function getParentId() {
  return mParent && mParent.id;
}

let mInitiallyLoadedResolver;
let mInitiallyLoaded = Promise.resolve(true);

export async function load(parent) {
  mItems = [];
  mSearcher.clear();

  if (!parent) {
    mParent = null;
    mAddressBook = null;
    mTable.setData(mItems.map(objectify));
    return;
  }

  mParent = parent;
  mAddressBook = DataStore.getAddressBook(parent.ownerAddressBookId) || parent;

  mTable.setSort([
    { column: 'visibleIndex', dir: 'asc' },
  ]);

  mInitiallyLoaded = new Promise(resolev => {
    mInitiallyLoadedResolver = resolev;
  });
  BackgroundConnection.sendMessage({
    type:     Constants.COMMAND_FETCH_ITEMS_OF,
    parentId: getParentId(),
  });
}

async function initialLoad(items) {
  const metadata = mParent && await DB.getItemsMetadataFromAddressBook(mAddressBook) || {};
  const indexKey = `index-${getParentId()}`;
  log('indexKey = ', indexKey);
  const sortedItems = (items || []).sort((a, b) => (a.sortString || '') - (b.sortString || ''));
  mItems = orderItems(sortedItems, {
    getItemIndex(item) {
      const data = metadata[item.id];
      return (data && indexKey in data) ? data[indexKey] : -1;
    },
    onOrdered(item, index) {
      item.index = index;
      item.visibleIndex = index + 1;
      item.$selected = Selection.has(item.id);
    },
  });
  await renderItems();
  mInitiallyLoadedResolver();
}

async function renderItems() {
  renderItems.active = true;
  /*
  mTable.setData(mItems.map(objectify));
  */
  mTable.setData([]);
  const renderingParentId = getParentId();
  const completed = await await doProgressively({
    items: mItems,
    task: async items => {
      if (getParentId() != renderingParentId)
        return false;
      await mTable.addData(items.map(objectify));
      updateSelection();
    },
    chunkSize: configs.itemsRenderChunkSize,
    interval:  configs.itemsRenderInterval,
  });
  if (completed)
    renderItems.active = false;
  const selectedIds = mItems.reduce((ids, item) => {
    if (item.$selected)
      ids.push(item.id);
    return ids;
  }, []);
  if (selectedIds.length > 0) {
    mTable.selectRow(selectedIds);
    const rows = mTable.getSelectedRows();
    if (rows.length > 0)
      mTable.scrollToRow(rows[0], 'center', false);
  }
}
renderItems.active = false;

function updateSelection() {
  const ids = Array.from(document.querySelectorAll('input:checked'), checkbox => {
    const row = mTable.getRow(checkbox.closest('.tabulator-row'));
    //const item = normalizeItem(row.getData());
    return row.getIndex();
  });
  mTable.selectRow(ids);
}

function onCheckboxColClicked(event, cell) {
  let checkbox = event.target.closest('input[type="checkbox"]');
  if (!checkbox) {
    const checkboxCell = event.target.closest('.tabulator-cell[tabulator-field="$selected"]');
    if (!checkboxCell)
      return;
    checkbox = checkboxCell.querySelector('input[type="checkbox"]');
    checkbox.checked = !checkbox.checked;
  }

  event.stopImmediatePropagation();

  const item = normalizeItem(cell.getData());

  const row = cell.getRow();
  if (checkbox.checked) {
    Selection.add(item);
    row.select();
  }
  else {
    Selection.remove(item);
    row.deselect();
  }
}

function rowSelectionChanged(items, rows) {
  const toBeUnselectedData = new Map();
  const toBeSelectedData = [];

  const selectedItems = new Set(items.map(normalizeItem));
  for (const checkbox of document.querySelectorAll('input:checked')) {
    const row = mTable.getRow(checkbox.closest('.tabulator-row'));
    if (!row)
      continue;
    const item = normalizeItem(row.getData());
    if (!item || // item going to be removed
        selectedItems.has(item))
      continue;
    checkbox.checked = false;
    Selection.remove(item);
    toBeUnselectedData.set(item.id, { id: item.id, $selected: false });
  }

  for (let i = 0, maxi = items.length; i < maxi; i++) {
    const item = normalizeItem(items[i]);
    Selection.add(item);
    toBeUnselectedData.delete(item.id);
    toBeSelectedData.push({ id: item.id, $selected: true });
    const row = rows[i];
    const element = row.getElement();
    const checkbox = element && element.querySelector('input[type="checkbox"]')
    if (checkbox)
      checkbox.checked = true;
  }

  const updateItems = [...toBeUnselectedData.values(), ...toBeSelectedData];
  if (mTable)
    mTable.updateData(updateItems);
  const renderingParentId = getParentId();
  rowSelectionChanged.lastUpdate = rowSelectionChanged.lastUpdate.then(() => doProgressively({
    items: updateItems,
    task: async items => {
      if (getParentId() != renderingParentId)
        return false;
      await mTable.updateData(items);
    },
    chunkSize: configs.itemsRenderChunkSize,
    interval:  configs.itemsRenderInterval,
  }));

  onFocusChanged.dispatch(items.length == 1 ? normalizeItem(items[0]) : null);
}
rowSelectionChanged.lastUpdate = Promise.resolve();

function rowClick(event, row) {
  if (event.target.closest('input[type="checkbox"]') ||
      event.ctrlKey ||
      event.shiftKey)
    return;

  log('clear old selection by click');
  const newlyFocused = row.getData();
  mTable.updateData(Selection.getAll().map(item => ({
    id:        item.id,
    $selected: item.id == newlyFocused.id,
  })));
  Selection.clear({ except: normalizeItem(newlyFocused) });
}

function rowDblClick(event, row) {
  window.getSelection().removeAllRanges(); // clear selection produced by Tabulator

  const item = normalizeItem(row.getData());
  onItemsOpen([item])
}

function onItemsOpen(items) {
  if (configs.editItemOnDblClick)
    Commands.editItem(items[0]);
  else
    Commands.startCompositionAsTo(items);
}

function onItemsDelete(items) {
  Commands.deleteItems(items);
}
Commands.onItemsDeleting.addListener(items => {
  const sortedItems = items.slice(0).sort((a, b) => a.index - b.index);
  let lastIndex = sortedItems[0].index - 1;
  for (const item of sortedItems) {
    if (item.index - 1 == lastIndex) {
      lastIndex = item.index;
      continue;
    }
    if (item.index > 0)
      mTable.selectRow([mItems[item.index - 1].id]);
    return;
  }
  const lastItem = sortedItems[sortedItems.length - 1];
  const newSelectedIndex = (lastItem.index == mItems.length - 1) ?
    sortedItems[0].index - 1 :
    lastItem.index + 1;
  if (newSelectedIndex > -1)
    mTable.selectRow([mItems[newSelectedIndex].id]);
});

function canMoveRow() {
  const sorters = mTable.getSorters();
  if (sorters.length != 1 ||
      sorters[0].field != 'visibleIndex') {
    // We cannot handle moved rows if they are sorted with any non-index field.
    return false;
  }

  const isDesc = sorters[0].dir == 'desc';
  if (isDesc) {
    // When rows are sorted in the reversed order, row.getPosition() looks to return invalid value...
    // We cannot handle such a broken row move for now.
    return false;
  }

  return true;
}

function onItemMoved(item, position) {
  item = normalizeItem(item);
  item.index = position;
  item.visibleIndex = position + 1;
  DB.getItemMetadata(item)
    .then(metadata => {
      if (!metadata)
        metadata = {};
      metadata[`index-${getParentId()}`] = position;
      return DB.setItemMetadata(item, metadata);
    })
    .catch(error => {
      log('failed to save updated index: ', error);
    });
}

function onItemsMoved(_items) {
  mItems = mTable.getData().map(normalizeItem);
  updateSelection();
}

function onFocusMoved(row, { expand } = {}) {
  if (expand)
    return;

  log('clear old selection by keyboard navigation');
  const newlyFocused = row.getData();
  mTable.updateData(Selection.getAll().map(item => ({
    id:        item.id,
    $selected: item.id == newlyFocused.id,
  })));
  Selection.clear({ except: normalizeItem(newlyFocused) });
}

function onKeyDown(event) {
  if (event.target.closest('input, textarea'))
    return;

  const accelKey = /^mac/i.test(navigator.platform) ? event.metaKey : event.ctrlKey;
  switch (event.key) {
    case 'Enter':
      // open
      break;

    case 'a':
      if (accelKey &&
          !event.altKey &&
          !event.shiftKey) {
        event.stopPropagation();
        event.preventDefault();
        mTable.selectRow(mTable.getRows());
      }
      break;
  }
}


let mLastContextItem;

function rowContext(event, row) {
  event.preventDefault();
  mLastContextItem = normalizeItem(row.getData());
  if (!Selection.getAll().some(item => item.id == mLastContextItem.id))
    Selection.clear();
  Selection.add(mLastContextItem);
  mTable.selectRow([mLastContextItem.id]);
  updateContextMenu(event);
  mContextMenu.open({
    left: event.clientX,
    top:  event.clientY
  });
}

function onContextMenuCommand(item, _event) {
  const items = Selection.getSize() > 0 ? Selection.getAll() : mLastContextItem ? [mLastContextItem] : [];
  // eslint-disable-next-line no-unused-expressions
  (
    Menu.onContactsCommand({
      item,
      addressBook: mAddressBook,
      contacts: items,
      menuUI: mContextMenu,
    }) ||
    Menu.onStartCompositionCommand({
      item,
      contacts: items,
    })
  );
  mContextMenu.close();
}

function updateContextMenu(_event) {
  const items = Selection.getSize() > 0 ?
    Selection.getAll() :
    mLastContextItem ?
      [mLastContextItem] :
      [];
  const addressBook = mAddressBook && DataStore.getAddressBook(mAddressBook.id);
  Menu.updateContactsMenu({
    menu: document.querySelector('#items-table-context'),
    addressBook,
    contacts: items,
  });
}


Commands.onItemCreated.addListener(async item => {
  await DataStore.waitUntilTracked(item);
  await mTable.selectRow([item.id]);
  mTable.scrollToRow(item.id, 'center', false);
});

Commands.onItemsPasted.addListener(async ({ sourceItems, pastedItems }) => {
  await Promise.all(pastedItems.map(DataStore.waitUntilTracked));
  await mTable.deselectRow(sourceItems.map(item => item.id));
  const rows = mTable.getSelectedRows();
  if (rows.length > 0)
    await mTable.deselectRow(rows);
  await mTable.selectRow(pastedItems.map(item => item.id));
  mTable.scrollToRow(pastedItems[0].id, 'center', false);
});

Commands.onItemsDuplicated.addListener(async ({ sourceItems, duplicatedItems }) => {
  await Promise.all(duplicatedItems.map(DataStore.waitUntilTracked));
  await mTable.deselectRow(sourceItems.map(item => item.id));
  const rows = mTable.getSelectedRows();
  if (rows.length > 0)
    await mTable.deselectRow(rows);
  await mTable.selectRow(duplicatedItems.map(item => item.id));
  mTable.scrollToRow(duplicatedItems[0].id, 'center', false);
});

Commands.onItemsMerged.addListener(async ({ mergedContactId, sourceContacts }) => {
  await DataStore.waitUntilContactTracked(mergedContactId);
  await mTable.deselectRow(sourceContacts.map(contact => contact.id));
  const rows = mTable.getSelectedRows();
  if (rows.length > 0)
    await mTable.deselectRow(rows);
  await mTable.selectRow([mergedContactId]);
  mTable.scrollToRow(mergedContactId, 'center', false);
});


function updateHeaderContextMenu() {
  const menu = document.querySelector('#items-table-header-context');
  const contents = [
    ...Constants.ITEM_VISIBLE_FIELDS.map(field => {
      const visible = configs[`columnVisibility_${field}`];
      const label = browser.i18n.getMessage(`contact_field_${field}`);
      const safeField = sanitizeForHTMLText(field);
      return `
        <li id="items-table-header-context-${safeField}"
            class="checkbox ${visible ? 'checked' : ''}"
            data-field-name=${JSON.stringify(safeField)}
           >${sanitizeForHTMLText(label)}</li>
      `.trim();
    }),
    `<li id="items-table-header-context-separator"
          class="separator"></li>`,
    `<li id="items-table-header-context-reset"
        >${sanitizeForHTMLText(browser.i18n.getMessage('ui_headerContext_reset_label'))}</li>`,
  ].join('');
  DOMUpdater.update(menu, toDOMDocumentFragment(contents, menu));
}

function onHeaderContextMenuCommand(item, _event) {
  if (item.id == 'items-table-header-context-reset') {
    for (const field of Constants.ITEM_VISIBLE_FIELDS) {
      const key = `columnVisibility_${field}`;
      configs[key] = configs.$default[key];
      if (configs[key]) {
        mTable.showColumn(field);
      }
      else {
        mTable.hideColumn(field);
      }
    }
    mHeaderContextMenu.close();
    return;
  }
  const field = item.dataset.fieldName;
  const toBeShown = !item.classList.contains('checked');
  configs[`columnVisibility_${field}`] = toBeShown;
  if (toBeShown) {
    mTable.showColumn(field);
  }
  else {
    mTable.hideColumn(field);
  }
  mHeaderContextMenu.close();
}


function handleAddedItem(item) {
  item.index = mItems.length;
  item.visibleIndex = item.index + 1;
  mItems.push(item);
  handleAddedItem.addedItems.add(item);
  if (handleAddedItem.throttled)
    clearTimeout(handleAddedItem.throttled);
  handleAddedItem.throttled = setTimeout(() => {
    handleAddedItem.throttled = null;
    const items = Array.from(handleAddedItem.addedItems);
    handleAddedItem.addedItems.clear();
    if (!renderItems.active)
      mTable.addData(items.map(objectify));
  }, 150);
}
handleAddedItem.throttled = null;
handleAddedItem.addedItems = new Set();


export function init() {
  const itemsTableContainer = document.querySelector('#items');
  itemsTableContainer.addEventListener('keydown', onKeyDown, { capture: true });

  mTable = new Tabulator('#items-table', {
    data: mItems,
    dataIsolated: true,
    layout: 'fitColumns',
    tooltips: true,
    index: 'id',
    columns: [
      {
        title: browser.i18n.getMessage('ui_items_column_index'),
        field: 'visibleIndex',
        width: 32,
        widthShrink: 1,
        sorter: 'number',
      },
      {
        title: '',
        field: 'color',
        width: 1,
        formatter(cell, _formatterParams, onRendered) {
          const backgroundColor = mSearcher.active ? cell.getData().backgroundColor : '';
          const textColor       = mSearcher.active ? cell.getData().textColor : '';

          const colorTip = document.createElement('span');
          colorTip.classList.add('color-tip');
          const tipStyle = colorTip.style;
          tipStyle.background = backgroundColor;
          tipStyle.color      = textColor;

          onRendered(() => {
            const rowStyle = cell.getRow().getElement().style;
            rowStyle.background = backgroundColor;
            rowStyle.color      = textColor;
          });

          return colorTip;
        },
        visible: false,
      },
      {
        title: browser.i18n.getMessage('ui_items_column_selected'),
        field: '$selected',
        visible: configs.columnVisibility_selected,
        tooltip: false,
        formatter: 'tickCross',
        formatterParams: {
          allowTruthy: true,
          tickElement: '<input type="checkbox" checked>',
          crossElement: '<input type="checkbox">',
        },
        cellClick: onCheckboxColClicked,
        width: 32,
        widthShrink: 1,
      },
      {
        title: browser.i18n.getMessage('ui_items_column_icon'),
        field: 'type',
        width: 1,
        widthShrink: 1,
        formatter(cell, _formatterParams, _onRendered) {
          const icon = document.createElement('span');
          icon.classList.add('svg-icon');
          icon.classList.add(`type-${cell.getData().type}`);
          return icon;
        },
      },
      ...Constants.ITEM_VISIBLE_FIELDS.map(field => {
        return {
          title: browser.i18n.getMessage(`contact_field_${field}`),
          field,
          visible: configs[`columnVisibility_${field}`],
        };
      }),
    ],
    initialSort: [
      { column: 'visibleIndex', dir: 'asc' },
    ],
    selectable: true,
    selectableRangeMode: 'click',
    rowSelectionChanged,
    rowClick,
    rowDblClick,
    rowContext,
    keybindings: false,
    movableRows: true,
    history: true, // required to cancel impossible row movement
  });
  mTable.$navigation = new TableNavigation(mTable, {
    onItemsOpen,
    onItemsDelete,
    canMoveRow,
    onItemMoved,
    onItemsMoved,
    onFocusMoved,
    normalizer: normalizeItem,
    exporter: objectify,
  });

  mContextMenu = new MenuUI({
    root: document.querySelector('#items-table-context'),
    onCommand: onContextMenuCommand,
  });
  mHeaderContextMenu = new MenuUI({
    root: document.querySelector('#items-table-header-context'),
    onCommand: onHeaderContextMenuCommand,
  });

  // for blank area
  itemsTableContainer.addEventListener('contextmenu', event => {
    event.preventDefault();

    const rows = mTable.getSelectedRows();
    mLastContextItem = rows.length > 0 && normalizeItem(rows[0].getData()) || null;

    if (event.target.closest('[role="columnheader"]')) {
      updateHeaderContextMenu(event);
      mHeaderContextMenu.open({
        left: event.clientX,
        top:  event.clientY
      });
    }
    else {
      updateContextMenu(event);
      mContextMenu.open({
        left: event.clientX,
        top:  event.clientY
      });
    }
  });

  Selection.onRemoved.addListener(item => {
    for (const row of mTable.getSelectedRows()) {
      const id = row.getIndex();
      if (id == item.id)
        mTable.deselectRow(row);
    }
  });
}

const removedItemIds = new Set();
let throttledRemoveTimer = null;

BackgroundConnection.onMessage.addListener(async message => {
  switch (message && message.type) {
    case Constants.COMMAND_PUSH_ALL_CONTACTS:
      await mInitiallyLoaded;
      if (typeof mOnReceiveAllContacts == 'function')
        mOnReceiveAllContacts(message.contacts);
      break;

    case Constants.COMMAND_PUSH_ALL_MAILING_LISTS:
      await mInitiallyLoaded;
      if (typeof mOnReceiveAllMailingLists == 'function')
        mOnReceiveAllMailingLists(message.mailingLists);
      break;

    case Constants.COMMAND_PUSH_ITEMS_OF:
      if (message.parentId == getParentId())
        initialLoad(message.items);
      break;

    case Constants.COMMAND_PUSH_NEW_ITEM:
      await mInitiallyLoaded;
      if (message.parentId == getParentId())
        handleAddedItem(message.item);
      break;

    case Constants.COMMAND_PUSH_ITEM_UPDATE:
      await mInitiallyLoaded;
      mTable.updateData([{ id: message.id, ...message.fields }]);
      break;

    case Constants.COMMAND_PUSH_ITEM_DELETE: {
      await mInitiallyLoaded;
      removedItemIds.add(message.id);
      if (throttledRemoveTimer)
        clearTimeout(throttledRemoveTimer);
      throttledRemoveTimer = setTimeout(() => {
        throttledRemoveTimer = null;

        let lastFoundIndex = -1;
        for (let i = mItems.length - 1; i > -1; i--) {
          const item = mItems[i];
          if (!removedItemIds.has(item.id))
            continue;
          lastFoundIndex = i;
          mItems.splice(i, 1);
          removedItemIds.delete(item.id);
          if (removedItemIds.size == 0)
            break;
        }
        if (lastFoundIndex > -1) {
          for (let i = lastFoundIndex, maxi = mItems.length; i < maxi; i++) {
            //log(mItems[i], mItems[i].index, ' => ', i);
            mItems[i].index = i;
            mItems[i].visibleIndex = i + 1;
          }
        }
        renderItems();
      }, 150);
    }; break;
  }
});
