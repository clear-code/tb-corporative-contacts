/*
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
*/
'use strict';

import { DOMUpdater } from '/extlib/dom-updater.js';

import {
  configs,
  sanitizeForHTMLText,
  toDOMDocumentFragment,
} from '/common/common.js';
import * as BackgroundConnection from '/ui/background-connection.js';
import { ChatProvider } from '/common/ChatProvider.js';
import * as Commands from '/ui/commands.js';
import * as Constants from '/common/constants.js';
import * as DataStore from '/ui/data-store.js';


export function onAddressBooksCommand({ item, addressBook, event, menuUI }) {
  switch (item.dataset.command) {
    case 'createAddressBook':
      Commands.createAddressBook();
      return true;

    case 'editAddressBook':
      Commands.editAddressBook(addressBook);
      return true;

    case 'removeAddressBook':
      Commands.deleteAddressBook(addressBook);
      return true;

    case 'syncAllAddressBooks': {
      const force = event.shiftKey;
      Commands.syncAll({ force });
    }; return true;

    case 'printAddressBook':
      Commands.printContacts({ addressBookId: addressBook.id });
      return true;

    case 'importAddressBookFromFile':
      // The context menu blocks "click" event for kickign the type=file
      // input field, so we need to close the menu at firs.
      menuUI.close().then(() => Commands.importFromFile());
      return true;

    default:
      return false;
  }
}

export function updateAddressBooksMenu({ menu, addressBook }) {
  menu.querySelector('[data-command="editAddressBook"]')
    .classList
    .toggle('disabled', !addressBook || !addressBook.editableInUI);
  menu.querySelector('[data-command="removeAddressBook"]')
    .classList
    .toggle('disabled', !addressBook || !addressBook.deletable);
  menu.querySelector('[data-command="printAddressBook"]')
    .classList
    .toggle('disabled', !addressBook || !addressBook.printable);
}


export function onContactsCommand({ item, addressBook, contacts, menuUI }) {
  const commandItem = item.closest('[data-command]')
  const command     = commandItem && commandItem.dataset.command;
  switch (command) {
    case 'createContact':
      Commands.createContact(addressBook.id);
      return true;

    case 'createMailingList': {
      const defaultMembers = contacts.filter(item => item.canBecomeMember);
      Commands.createMailingList(addressBook.id, {
        defaultMembers: defaultMembers.length > 1 ? defaultMembers : [],
      });
    }; return true;

    case 'editContact':
      Commands.editItem(contacts[0]);
      return true;

    case 'deleteContacts':
      Commands.deleteItems(contacts);
      return true;

    case 'addNewCategory':
      Commands.addNewCategory(contacts);
      return true;

    case 'shareContacts':
      Commands.shareItems(contacts);
      return true;

    case 'findMessagesFromContacts':
      return true;

    case 'openMap':
      Commands.openMap(contacts);
      return true;

    case 'openURLs':
      Commands.openURLs(contacts);
      return true;

    case 'cutContacts':
      Commands.cut(contacts);
      return true;

    case 'copyContacts':
      Commands.copy(contacts);
      return true;

    case 'pasteContacts':
      Commands.paste(addressBook.id);
      return true;

    case 'pasteContactsAsEntry':
      return true;

    case 'printContacts':
      Commands.printContacts({ contacts });
      return true;

    case 'duplicateContacts':
      Commands.duplicate(contacts);
      return true;

    case 'mergeContacts':
      Commands.mergeContacts(contacts, { addressBook });
      return true;

    default:
      if (/^toggleCategory:(.+)$/.test(command)) {
        const category = item.dataset.category;
        if (item.classList.contains('checked')) { // remove
          for (const contact of contacts) {
            if (!contact.categories.includes(category))
              continue;
            const categories = contact.categories.filter(name => name != category);
            contact.categories = categories;
            BackgroundConnection.sendMessage({
              type:   Constants.COMMAND_PUSH_ITEM_UPDATE,
              id:     contact.id,
              fields: {
                categories,
              },
            });
          }
        }
        else { // add
          for (const contact of contacts) {
            if (contact.categories.includes(category))
              continue;
            const categories = [...contact.categories, category];
            contact.categories = categories;
            BackgroundConnection.sendMessage({
              type:   Constants.COMMAND_PUSH_ITEM_UPDATE,
              id:     contact.id,
              fields: {
                categories,
              },
            });
          }
        }
        return true;
      }
      else if (/^startChat:(.+)$/.test(command)) {
        const provider = ChatProvider.get(item.dataset.provider);
        if (provider) {
          // The context menu blocks "click" event for kickign the type=file
          // input field, so we need to close the menu at firs.
          menuUI.close().then(() => provider.open(contacts));
        }
        return true;
      }
      return false;
  }
}

export function updateContactsMenu({ menu, addressBook, contacts }) {
  const emails    = Commands.itemsToEmails(contacts);
  const addresses = Commands.itemsToAddresses(contacts);
  const urls      = Commands.itemsToURLs(contacts);
  const readOnly  = addressBook && addressBook.readOnly;

  const allCategories = [];
  const allAvailableChatProviderIds = [];
  for (const contact of contacts) {
    allCategories.push(...contact.categories);
    allAvailableChatProviderIds.push(...contact.availableChatProviderIds);
  }
  const checkedCategories      = new Set(allCategories);
  const availableChatProviders = [...new Set(allAvailableChatProviderIds)].map(id => ChatProvider.get(id));

  rebuildCategoriesMenu(menu, checkedCategories);
  rebuildChatProviders(menu, availableChatProviders);

  menu.querySelector('[data-command="createContact"]').classList.toggle('disabled', readOnly);
  menu.querySelector('[data-command="createMailingList"]').classList.toggle('disabled', readOnly);
  menu.querySelector('[data-command="editContact"]').classList.toggle('disabled', readOnly || contacts.length == 0);
  menu.querySelector('[data-command="deleteContacts"]').classList.toggle('disabled', readOnly || contacts.length == 0);
  menu.querySelector('[data-command="setCategory"]').classList.toggle('disabled', readOnly || contacts.length == 0);
  menu.querySelector('[data-command="startCompositionAsTo"]').classList.toggle('disabled', emails.length == 0);
  menu.querySelector('[data-command="startCompositionAsCc"]').classList.toggle('disabled', emails.length == 0);
  menu.querySelector('[data-command="startCompositionAsBcc"]').classList.toggle('disabled', emails.length == 0);
  menu.querySelector('[data-command="shareContacts"]').classList.toggle('disabled', contacts.length == 0);
  menu.querySelector('[data-command="startChat"]').classList.toggle('disabled', availableChatProviders.length == 0);
  menu.querySelector('[data-command="mergeContacts"]').classList.toggle('disabled', contacts.filter(contact => contact.mergable).length == 0);
  menu.querySelector('[data-command="openMap"]').classList.toggle('disabled', addresses.length == 0);
  menu.querySelector('[data-command="openURLs"]').classList.toggle('disabled', urls.length == 0);
  menu.querySelector('[data-command="cutContacts"]').classList.toggle('disabled', readOnly || contacts.length == 0);
  menu.querySelector('[data-command="copyContacts"]').classList.toggle('disabled', contacts.length == 0);
  menu.querySelector('[data-command="pasteContacts"]').classList.toggle('disabled', readOnly || Commands.getItemsInClipboard().length == 0);
  menu.querySelector('[data-command="printContacts"]').classList.toggle('disabled', contacts.length == 0);
  menu.querySelector('[data-command="duplicateContacts"]').classList.toggle('disabled', readOnly || contacts.length == 0);
}

function rebuildCategoriesMenu(menu, checkedCategories) {
  const parent = menu.querySelector('[data-command="setCategory"] ul');
  if (!parent.id)
    parent.id = `submenu-${Date.now()}-${Math.floor(Math.random() * Math.pow(2, 24))}`;
  DOMUpdater.update(parent, toDOMDocumentFragment([
    `<li id=${JSON.stringify(sanitizeForHTMLText(parent.id + ':add-new-category'))}
         data-command="addNewCategory"
        >${sanitizeForHTMLText(browser.i18n.getMessage('ui_context_items_newCategory_label'))}</li>`,
    `<li id=${JSON.stringify(sanitizeForHTMLText(parent.id + ':separat-rafter-add-new-category'))}
         class="separator"></li>`,
    ...Array.from(DataStore.availableCategories, category =>
      `<li id=${JSON.stringify(sanitizeForHTMLText(parent.id + ':category:' + category))}
           data-command="toggleCategory:${sanitizeForHTMLText(category)}"
           data-category=${JSON.stringify(sanitizeForHTMLText(category))}
           class="checkbox ${checkedCategories.has(category) ? 'checked' : ''}"
          >${sanitizeForHTMLText(category)}</li>`
    ),
  ].join(''), parent));
}

function rebuildChatProviders(menu, availableChatProviders) {
  const parent = menu.querySelector('[data-command="startChat"] ul');
  if (!parent.id)
    parent.id = `submenu-${Date.now()}-${Math.floor(Math.random() * Math.pow(2, 24))}`;
  const preferredProvider = availableChatProviders
    .find(provider => provider.id == configs.preferredChatProvider);
  const preferredItem = preferredProvider && providerToMenuItem(preferredProvider, parent);
  const otherProviders = availableChatProviders
    .filter(provider => provider.id != configs.preferredChatProvider || !preferredItem);
  DOMUpdater.update(parent, toDOMDocumentFragment([
    preferredItem || '',
    (preferredItem &&
     otherProviders.length > 0 &&
     `<li id=${JSON.stringify(sanitizeForHTMLText(parent.id + ':separator'))}
          class="separator"></separator>`) || '',
    ...otherProviders.map(provider => providerToMenuItem(provider, parent)),
  ].join(''), parent));
}

function providerToMenuItem(provider, parent) {
  return `
    <li id=${JSON.stringify(sanitizeForHTMLText(parent.id + ':provider:' + provider.id))}
        data-command="startChat:${sanitizeForHTMLText(provider.id)}"
        data-provider=${JSON.stringify(sanitizeForHTMLText(provider.id))}
       >${sanitizeForHTMLText(provider.label || provider.field)}</li>
  `.trim();
}


export function onToolsCommands({ item }) {
  switch (item.dataset.command) {
    case 'openItemsInWindow':
      configs.openUIInWindow = true;
      return true;

    case 'openItemsInTab':
      configs.openUIInWindow = false;
      return true;

    case 'options':
      browser.runtime.openOptionsPage();
      return true;

    default:
      return false;
  }
}

export function updateToolsMenu({ menu }) {
  menu.querySelector('[data-command="openItemsInWindow"]')
    .classList
    .toggle('hidden', configs.openUIInWindow);
  menu.querySelector('[data-command="openItemsInTab"]')
    .classList
    .toggle('hidden', !configs.openUIInWindow);
}


export function onStartCompositionCommand({ item, contacts }) {
  switch (item.dataset.command) {
    case 'startCompositionAsTo':
      Commands.startCompositionAsTo(contacts);
      return true;

    case 'startCompositionAsCc':
      Commands.startCompositionAsCc(contacts);
      return true;

    case 'startCompositionAsBcc':
      Commands.startCompositionAsBcc(contacts);
      return true;

    default:
      return false;
  }
}
