/*
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
*/
'use strict';

import * as Dialog from '/extlib/dialog.js';
import '/extlib/l10n.js';
import MenuUI from '/extlib/MenuUI.js';
import * as ResizableBox from '/extlib/resizable-box.js';
import { tabbis } from '/extlib/tabbis.es6.min.js';

import {
  configs,
  //log,
} from '/common/common.js';
import * as AddressBooksTree from './address-books-tree.js';
import * as Commands from '/ui/commands.js';
import '/ui/common.js';
import * as Constants from '/common/constants.js';
import * as DataStore from '/ui/data-store.js';
import * as ItemsTable from './items-table.js';
import * as ItemDetails from './item-details.js';
import * as Menu from './menu.js';
import * as Selection from './selection.js';

let mLastActiveAddressBookId;
let mMainMenu;
let mStartCompositionMenu;

const mMainMenuButton          = document.querySelector('#main-menu-button');
const mSyncAllAddressBooksButton = document.querySelector('#sync-all-address-books-button');
const mStartCompositionButton  = document.querySelector('#start-composition-button');
const mStartCompositionMenuButton = document.querySelector('#start-composition-menu-button');
const mOptionsButton           = document.querySelector('#options-button');
const mCreateMailingListButton = document.querySelector('#create-mailing-list-button');
const mShowItemButton          = document.querySelector('#show-item-button');
const mDeleteItemsButton       = document.querySelector('#delete-items-button');

function onConfigChanged(key) {
  switch (key) {
    case 'debug':
      document.documentElement.classList.toggle('debug', configs.debug);
      break;

      /*
    case itemsTabMailPopularityEnabled:
      for (const node of document.querySelectorAll('[data-tab-id="item-details-tab-mail-popularity"], [data-tab-id="item-details-panel-mail-popularity"]')) {
        node.classList.toggle('hidden', !configs[key]);
      }
      break;
      */

    case 'itemsTabTechnicalEnabled':
      for (const node of document.querySelectorAll('[data-tab-id="item-details-tab-technical"], [data-tab-id="item-details-panel-technical"]')) {
        node.classList.toggle('hidden', !configs[key]);
      }
      break;

    case 'itemsTabVCardEnabled':
      for (const node of document.querySelectorAll('[data-tab-id="item-details-tab-vcard"], [data-tab-id="item-details-panel-vcard"]')) {
        node.classList.toggle('hidden', !configs[key]);
      }
      break;

    case 'showToolbarSyncButton':
      mSyncAllAddressBooksButton.classList.toggle('hidden', !configs[key]);
      break;

    case 'showToolbarStartCompositionButton':
      mStartCompositionButton.classList.toggle('hidden', !configs[key]);
      mStartCompositionMenuButton.classList.toggle('hidden', !configs[key]);
      break;

    case 'showToolbarOptionsButton':
      mOptionsButton.classList.toggle('hidden', !configs[key]);
      break;

    case 'showToolbarSearchField':
      document.querySelector('#search-field').classList.toggle('hidden', !configs[key]);
      break;

    case 'showToolbarCreateMailingListButton':
      mCreateMailingListButton.classList.toggle('hidden', !configs[key]);
      break;

    case 'showToolbarShowItemButton':
      mShowItemButton.classList.toggle('hidden', !configs[key]);
      break;

    case 'showToolbarDeleteItemsButton':
      mDeleteItemsButton.classList.toggle('hidden', !configs[key]);
      break;

    case 'showToolbarAddressBooksFilter':
      document.querySelector('#address-books-filter').classList.toggle('hidden', !configs[key]);
      break;

  }
}
configs.$addObserver(onConfigChanged);


function updateToolbarButtons() {
  const addressBook = mLastActiveAddressBookId && DataStore.getAddressBook(mLastActiveAddressBookId);
  mCreateMailingListButton.disabled = !addressBook || addressBook.readOnly;
  mShowItemButton.disabled          = !addressBook || Selection.getSize() == 0;
  mDeleteItemsButton.disabled       = !addressBook || addressBook.readOnly || Selection.getSize() == 0;
}

Selection.onAdded.addListener(updateToolbarButtons);
Selection.onRemoved.addListener(updateToolbarButtons);


function onMainMenuCommand(item, event) {
  const addressBook = mLastActiveAddressBookId && DataStore.getAddressBook(mLastActiveAddressBookId);
  const contacts    = Selection.getSize() > 0 ? Selection.getAll() : [];
  // eslint-disable-next-line no-unused-expressions
  (
    Menu.onAddressBooksCommand({
      item,
      addressBook,
      event,
      menuUI: mMainMenu,
    }) ||
    Menu.onContactsCommand({
      item,
      addressBook,
      contacts,
      menuUI: mMainMenu,
    }) ||
    Menu.onStartCompositionCommand({
      item,
      contacts,
    }) ||
    Menu.onToolsCommands({
      item
    })
  );
  mMainMenu.close();
}

function updateMainMenu() {
  const menu        = document.querySelector('#main-menu');
  const addressBook = mLastActiveAddressBookId && DataStore.getAddressBook(mLastActiveAddressBookId);
  const contacts    = Selection.getSize() > 0 ? Selection.getAll() : [];
  Menu.updateAddressBooksMenu({
    menu,
    addressBook,
  });
  Menu.updateContactsMenu({
    menu,
    addressBook,
    contacts,
  });
  Menu.updateToolsMenu({
    menu,
  });
}

function onStartCompositionCommand(item, _event) {
  const contacts = Selection.getSize() > 0 ? Selection.getAll() : [];
  return Menu.onStartCompositionCommand({
    item,
    contacts,
  });
}


function onKeyDown(event) {
  if (event.target.closest('input, textarea'))
    return;

  const addressBook = mLastActiveAddressBookId && DataStore.getAddressBook(mLastActiveAddressBookId);
  const contacts    = Selection.getSize() > 0 ? Selection.getAll() : addressBook ? addressBook.contacts : [];

  const accelKey = /^mac/i.test(navigator.platform) ? event.metaKey : event.ctrlKey;
  switch (event.key) {
    case 'p':
      if (accelKey &&
          !event.altKey &&
          !event.shiftKey) {
        event.stopPropagation();
        event.preventDefault();
        Commands.printContacts(contacts, {
          printId: addressBook && addressBook.id,
          title:   addressBook && browser.i18n.getMessage('ui_printContacts_documentTitle_addressBook', [addressBook.name]),
        });
      }
      break;
  }
}


document.addEventListener('copy', event => {
  if (event.target.closest('input, textarea'))
    return;

  const addressBook = mLastActiveAddressBookId && DataStore.getAddressBook(mLastActiveAddressBookId);
  const contacts    = Selection.getSize() > 0 ? Selection.getAll() : [];
  if (!addressBook ||
      contacts.length == 0)
    return;

  event.stopPropagation();
  event.preventDefault();
  Commands.copy(contacts);
});

document.addEventListener('cut', event => {
  if (event.target.closest('input, textarea'))
    return;

  const addressBook = mLastActiveAddressBookId && DataStore.getAddressBook(mLastActiveAddressBookId);
  const contacts    = Selection.getSize() > 0 ? Selection.getAll() : [];
  if (!addressBook ||
      contacts.length == 0)
    return;

  event.stopPropagation();
  event.preventDefault();
  Commands.cut(contacts);
});

document.addEventListener('paste', event => {
  if (event.target.closest('input, textarea'))
    return;

  const addressBook = mLastActiveAddressBookId && DataStore.getAddressBook(mLastActiveAddressBookId);
  if (!addressBook)
    return;

  event.stopPropagation();
  event.preventDefault();
  Commands.paste(addressBook.id);
});



function onMessage(message, _sender) {
  switch (message && message.type) {
    case Constants.COMMAND_FOCUS_TO_ITEMS_DIALOG:
      window.focus();
      return Promise.resolve(true);
  }
}

window.addEventListener('DOMContentLoaded', async () => {
  await configs.$loaded;

  document.addEventListener('keydown', onKeyDown, { capture: true });

  browser.runtime.onMessage.addListener(onMessage);
  window.addEventListener('unload', () => {
    browser.runtime.onMessage.removeListener(onMessage);
  }, { once: true });


  // toolbar

  mMainMenu = new MenuUI({
    root:      document.querySelector('#main-menu'),
    onCommand: onMainMenuCommand,
  });
  mMainMenuButton.addEventListener('contextmenu', event => {
    event.preventDefault();
    updateMainMenu();
    mMainMenu.open({
      left: event.clientX,
      top:  event.clientY
    });
  });
  Dialog.initButton(mMainMenuButton, event => {
    updateMainMenu();
    mMainMenu.open({
      left: event.clientX,
      top:  event.clientY
    });
  });

  Dialog.initButton(mSyncAllAddressBooksButton, _event => {
    Commands.syncAll({ force: event.shiftKey });
  });

  Dialog.initButton(mStartCompositionButton, _event => {
    const items = Selection.getSize() > 0 ? Selection.getAll() : [];
    Commands.startCompositionAsTo(items);
  });

  mStartCompositionMenu = new MenuUI({
    root: document.querySelector('#start-composition-menu'),
    onCommand: onStartCompositionCommand,
  });
  mStartCompositionButton.addEventListener('contextmenu', event => {
    event.preventDefault();
    mStartCompositionMenu.open({
      left: event.clientX,
      top:  event.clientY
    });
  });
  Dialog.initButton(mStartCompositionMenuButton, event => {
    mStartCompositionMenu.open({
      left: event.clientX,
      top:  event.clientY
    });
  });

  Dialog.initButton(mOptionsButton, () => {
    browser.runtime.openOptionsPage();
  });

  Dialog.initButton(mCreateMailingListButton, () => {
    const items = Selection.getSize() > 0 ? Selection.getAll() : [];
    const defaultContacts = items.filter(item => item.type == 'contact');
    Commands.createMailingList(mLastActiveAddressBookId, {
      defaultContacts,
    });
  });
  Dialog.initButton(mShowItemButton, () => {
    const items = Selection.getSize() > 0 ? Selection.getAll() : [];
    if (items.length > 0)
      Commands.editItem(items[0]);
  });
  Dialog.initButton(mDeleteItemsButton, () => {
    const items = Selection.getSize() > 0 ? Selection.getAll() : [];
    if (items.length > 0)
      Commands.deleteItems(items);
  });

  updateToolbarButtons();

  // resize

  ResizableBox.init(configs.itemsBoxSizes);
  window.addEventListener(ResizableBox.TYPE_RESIZED, event => {
    configs.itemsBoxSizes = event.detail;
  });

  // UI components

  AddressBooksTree.init();
  AddressBooksTree.onSelectionChanged.addListener(group => {
    ItemsTable.load(group);
    mLastActiveAddressBookId = group && (group.ownerAddressBookId || group.id);
    updateToolbarButtons();
  });

  ItemsTable.init();
  ItemsTable.onFocusChanged.addListener(item => ItemDetails.update(item));

  ItemDetails.init();
  ItemDetails.update();

  tabbis();
  /*
  document.addEventListener('tabbis', event => {
    const { tab, pane } = event.detail;
    console.log(tab, pane);
  }, false );
  */

  onConfigChanged('debug');
  //onConfigChanged('itemsTabMailPopularityEnabled');
  onConfigChanged('itemsTabTechnicalEnabled');
  onConfigChanged('itemsTabVCardEnabled');
  onConfigChanged('showToolbarSyncButton');
  onConfigChanged('showToolbarStartCompositionButton');
  onConfigChanged('showToolbarOptionsButton');
  onConfigChanged('showToolbarSearchField');
  onConfigChanged('showToolbarCreateMailingListButton');
  onConfigChanged('showToolbarShowItemButton');
  onConfigChanged('showToolbarDeleteItemsButton');
  onConfigChanged('showToolbarAddressBooksFilter');
  document.documentElement.classList.add('initialized');

  if (location.search.includes('mode=window')) {
    await Dialog.notifyReady();

    window.addEventListener('resize', () => {
      configs.itemsDialogWidth = window.outerWidth;
      configs.itemsDialogHeight = window.outerHeight;
    });
    window.addEventListener(Dialog.TYPE_MOVED, event => {
      configs.itemsDialogLeft = event.detail.left;
      configs.itemsDialogTop = event.detail.top;
    });
  }

  await AddressBooksTree.load();
}, { once: true });


// for debugging
window.configs      = configs;
window.DataStore = DataStore;
