/*
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
*/
'use strict';

import * as Dialog from '/extlib/dialog.js';
import '/extlib/l10n.js';
//import * as ResizableBox from '/extlib/resizable-box.js';
import { tabbis } from '/extlib/tabbis.es6.min.js';

import {
  configs,
  log,
  orderItems,
} from '/common/common.js';
import { AddressBook } from '/common/AddressBook.js';
import { ChatProvider } from '/common/ChatProvider.js';
import '/ui/common.js';
import * as Constants from '/common/constants.js';
import { Contact, MailingList, normalizeItem } from '/common/Contact.js';
import * as DataStore from '/ui/data-store.js';
import * as ItemEditUI from '/ui/item-edit-ui.js';
import { ItemsSearcher } from '/common/ItemsSearcher.js';
import { TableNavigation } from '/common/TableNavigation.js';
import Tabulator from '/extlib/tabulator.es2015.js';

let mParams;
let mMailingList;
let mAddressBook;
let mAddressBooks;
let mAllCategories = [];

let mCandidateContacts = [];
let mCandidateContactsTable;
let mMemberContactIds = new Set();
let mMemberContactsTable;

const mAcceptButton = document.querySelector('#accept');
const mCancelButton = document.querySelector('#cancel');

const mSearcher = new ItemsSearcher();

mSearcher.bindTo(document.querySelector('#contacts-search-field'));

mSearcher.onCleared.addListener(_searcher => {
  mCandidateContacts = [];
  updateCandidateContacts();
})

mSearcher.onStarted.addListener(async _searcher => {
  mCandidateContacts = [];
  updateCandidateContacts();
  const contacts = await browser.contacts.list(mAddressBook.id);
  return contacts.map(contact => Contact.get(contact.id) || new Contact(contact));
});

mSearcher.onFound.addListener((_searcher, items) => {
  mCandidateContacts.push(...items);
  updateCandidateContacts();
});

function onConfigChange(key) {
  const value = configs[key];
  switch (key) {
    case 'debug':
      document.documentElement.classList.toggle('debug', value);
      break;
  }
}

function onMessage(message, _sender) {
  switch (message && message.type) {
    case Constants.COMMAND_FOCUS_TO_MAILING_LIST_DIALOG:
      if (mMailingList && message.mailingListId == mMailingList.id) {
        window.focus();
        return Promise.resolve(true);
      }
      break;
  }
}

function objectify(item) {
  return {
    label: item.label,
    id:    item.id,
    type:  item.type,
  };
}

function fillFields() {
  for (const field of document.querySelectorAll('[data-item-field]')) {
    const value = String(mMailingList && mMailingList.getFieldValue(field.dataset.itemField) || '').trim();
    switch (field.localName) {
      case 'img':
        field.src = value;
        break;

      case 'input':
      case 'textarea':
      case 'select':
        field.value = value;
        break;

      default:
        field.textContent = value;
        break;
    }
    field.disabled = mAddressBook && mAddressBook.readOnly;
  }

  const organizations = (mMailingList && mMailingList.organizations || []).slice(0);
  for (const field of document.querySelectorAll('#organizations-fields input[type="text"]')) {
    field.value = organizations.shift() || '';
    field.disabled = mAddressBook && mAddressBook.readOnly;
  }
}

function updateCandidateContacts() {
  mCandidateContactsTable.setData(mCandidateContacts.filter(contact => !mMemberContactIds.has(contact.id)).map(contact => objectify(contact)));
}

async function updateMemberContacts() {
  mMemberContactsTable.setData(Array.from(mMemberContactIds, id => objectify(Contact.get(id) || MailingList.get(id))));
}

async function saveChanges() {
  if (!mMailingList)
    return;

  for (const field of document.querySelectorAll('[data-item-field]')) {
    if (!field.classList.contains('dirty'))
      continue;

    const name = field.dataset.itemField;
    const oldValue = mMailingList.getFieldValue(name) || '';
    const newValue = field.localName == 'img' ? field.src : field.value;
    if (newValue == oldValue)
      continue;

    log('set value: ', name, { oldValue, newValue });
    mMailingList.setFieldValue(name, newValue);
  }

  await mMailingList.save();

  const currentContactIds = new Set(mMailingList.contacts.map(contact => contact.id));
  const allContactIds = new Set([
    ...currentContactIds,
    ...mMemberContactIds,
  ]);
  for (const contactId of allContactIds) {
    if (currentContactIds.has(contactId) &&
        !mMemberContactIds.has(contactId)) {
      mMailingList.removeContact(contactId);
    }
    else if (!currentContactIds.has(contactId) &&
             mMemberContactIds.has(contactId)) {
      mMailingList.addContact(contactId);
    }
  }
}

function checkRequiredFields() {
  if (checkRequiredFields.throttled)
    clearTimeout(checkRequiredFields.throttled);

  checkRequiredFields.throttled = setTimeout(() => {
    checkRequiredFields.throttled = null;
    const requiredFields = document.querySelectorAll('input[required]');
    const filledFields = Array.from(requiredFields, field => field.value).filter(value => !!value);
    mAcceptButton.disabled = filledFields.length < requiredFields.length;
  }, 150);
}
checkRequiredFields.throttled = null;

function updateListsSize() {
  const topContainerWidth = document.querySelector('[data-panes]').getBoundingClientRect().width;
  for (const container of document.querySelectorAll('.items-list-container')) {
    container.style.width = `calc((${topContainerWidth}px - 3em) / 2 - 1em)`;
  }
}

configs.$loaded.then(async () => {
  const [params, addressBooks] = await Promise.all([
    Dialog.getParams(),
    browser.addressBooks.list(false)
      .then(addressBooks => addressBooks.map(addressBook => new AddressBook(addressBook))),
  ]);
  mParams = params;
  mAllCategories = new Set(mParams.categories.sort());
  log('mailing list dialog initialize ', mParams);

  const [mailingList, memberContacts, defaultContacts, addressBook] = await Promise.all([
    mParams.mailingListId && browser.mailingLists.get(mParams.mailingListId),
    mParams.mailingListId && browser.mailingLists.listMembers(mParams.mailingListId),
    mParams.defaultContactIds && Promise.all(mParams.defaultContactIds.map(id => DataStore.getRawContact(id))) || [],
    AddressBook.get(mParams.addressBookId),
    ...addressBooks.map(addressBook => addressBook.initialized),
  ]);
  if (mailingList)
    mailingList.contacts = memberContacts;
  mMailingList = new MailingList(mailingList || {
    parentId: addressBook && addressBook.id,
    contacts: [],
  });
  mMemberContactIds = new Set([
    ...mMailingList.contacts.map(contact => (normalizeItem(contact), contact.id)),
    ...defaultContacts.map(contact => (normalizeItem(contact), contact.id)),
  ]);
  await mMailingList.initialized;
  mAddressBook = addressBook;
  mAddressBooks = orderItems(addressBooks, {
    getItemIndex(addressBook) {
      return addressBook.index;
    },
    onInitialized(_addressBook) {
    },
    onOrdered(addressBook, index) {
      addressBook.index = index;
    },
  });

  const title = mailingList ? mMailingList.DisplayName : browser.i18n.getMessage('ui_new_mailingList_title');
  document.title = browser.i18n.getMessage('ui_mailingList_title', [title]);

  browser.runtime.onMessage.addListener(onMessage);
  window.addEventListener('unload', () => {
    browser.runtime.onMessage.removeListener(onMessage);
  }, { once: true });

  ItemEditUI.buildAddressBooksSelector(mAddressBooks);
  ItemEditUI.buildCategories(mMailingList, {
    allCategories: mAllCategories,
  });
  ItemEditUI.initDetailContainers();
  ItemEditUI.initCategoriesEventHandlers(mMailingList, {
    allCategories: mAllCategories,
  });

  mCandidateContactsTable = new Tabulator('#candidate-contacts', {
    data: [],
    dataIsolated: true,
    layout: 'fitColumns',
    height: '100%',
    tooltips: true,
    index: 'id',
    columns: [
      {
        field: 'label',
        widthGrow: 1,
      },
      {
        formatter(_cell, _formatterParams, _onRendered) {
          return '<button>+</button>';
        },
        width: 32,
        widthShrink: 1,
        hozAlign: 'center',
        cellClick: (_event, cell) => {
          mMemberContactIds.add(cell.getRow().getIndex());
          updateCandidateContacts();
          updateMemberContacts();
        },
      },
    ],
    selectable: false,
    keybindings: false,
    movableRows: false,
  });
  mCandidateContactsTable.$navigation = new TableNavigation(mCandidateContactsTable, {
    normalizer: normalizeItem,
    exporter: objectify,
  });

  mMemberContactsTable = new Tabulator('#member-contacts', {
    data: [],
    dataIsolated: true,
    layout: 'fitColumns',
    height: '100%',
    tooltips: true,
    index: 'id',
    columns: [
      {
        field: 'label',
        widthGrow: 1,
      },
      {
        formatter(_cell, _formatterParams, _onRendered) {
          return '<button>X</button>';
        },
        width: 32,
        widthShrink: 1,
        hozAlign: 'center',
        cellClick: (_event, cell) => {
          mMemberContactIds.delete(cell.getRow().getIndex());
          updateCandidateContacts();
          updateMemberContacts();
        },
      },
    ],
    selectable: false,
    keybindings: false,
    movableRows: false,
  });
  mMemberContactsTable.$navigation = new TableNavigation(mMemberContactsTable, {
    normalizer: normalizeItem,
    exporter: objectify,
  });

  updateMemberContacts();

  if (mAddressBook)
    await mAddressBook.initialized;
  fillFields();
  checkRequiredFields();

  document.addEventListener('change', event => {
    const field = event.target;
    field.classList.add('dirty');
  });

  document.addEventListener('input', _event => {
    checkRequiredFields();
  });

  configs.$addObserver(onConfigChange);
  onConfigChange('debug');

  //ResizableBox.init(configs.mailingListDialogBoxSizes);

  tabbis();
  /*
  document.addEventListener('tabbis', event => {
    const { tab, pane } = event.detail;
    console.log(tab, pane);
  }, false );
  */

  const chatProvider = configs.preferredChatProvider && ChatProvider.get(configs.preferredChatProvider);
  document.querySelector('#mailing-list-start-chat-button-container').classList.toggle('hidden', !chatProvider);
  if (chatProvider) {
    Dialog.initButton(document.querySelector('#mailing-list-start-chat-button'), _event => {
      chatProvider.open(mMailingList);
    });
  }

  Dialog.initButton(mAcceptButton, async _event => {
    if (!mAddressBook || !mAddressBook.readOnly)
      await saveChanges();
    Dialog.accept();
  });
  if (mAddressBook && mAddressBook.readOnly) {
    mAcceptButton.textContent = browser.i18n.getMessage('ui_mailingList_closeButton_label');
    mCancelButton.classList.add('hidden');
  }
  else {
    mAcceptButton.textContent = browser.i18n.getMessage('ui_mailingList_saveButton_label');
    mCancelButton.classList.remove('hidden');
    Dialog.initCancelButton(mCancelButton);
  }

  await Dialog.notifyReady();

  updateListsSize();

  window.addEventListener('resize', () => {
    configs.mailingListDialogWidth = window.outerWidth;
    configs.mailingListDialogHeight = window.outerHeight;
    updateListsSize();
  });
  window.addEventListener(Dialog.TYPE_MOVED, event => {
    configs.mailingListDialogLeft = event.detail.left;
    configs.mailingListDialogTop = event.detail.top;
  });

  //window.addEventListener(ResizableBox.TYPE_RESIZED, event => {
  //  configs.mailingListDialogBoxSizes = event.detail;
  //});
  log('mailing list dialog initialize done');
});
