/*
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
*/
'use strict';

import * as Dialog from '/extlib/dialog.js';
import EventListenerManager from '/extlib/EventListenerManager.js';
import RichConfirm from '/extlib/RichConfirm.js';

import {
  configs,
  openURLs as openURLsCommon,
  sanitizeForHTMLText,
} from '/common/common.js';
import * as BackgroundConnection from '/ui/background-connection.js';
import * as Constants from '/common/constants.js';
import { Contact, normalizeItem } from '/common/Contact.js';
import * as DataStore from '/ui/data-store.js';
import * as ImportUI from '/ui/import-ui.js';
import * as VCard from '/common/import/vcard.js';

const URL_ADDRESS_BOOK = browser.runtime.getURL('/ui/address-book/address-book.html');
const URL_CONTACT = browser.runtime.getURL('/ui/contact/contact.html');
const URL_MAILING_LIST = browser.runtime.getURL('/ui/mailing-list/mailing-list.html');
const URL_MERGE_CONTACTS = browser.runtime.getURL('/ui/merge-contacts/merge-contacts.html');
const URL_PRINT_PREVIEW = browser.runtime.getURL('/ui/print/print.html');

export const onItemCreated = new EventListenerManager();
export const onItemsDeleting = new EventListenerManager();
export const onItemsDeleted = new EventListenerManager();
export const onItemsPasted = new EventListenerManager();
export const onItemsDuplicated = new EventListenerManager();
export const onItemsMerged = new EventListenerManager();


/* address books */

export function createAddressBook() {
  const dialogParams = {
    url:    URL_ADDRESS_BOOK,
    width:  configs.addressBookDialogWidth,
    //height: configs.addressBookDialogWidth,
  };
  if (typeof configs.addressBookDialogLeft == 'number')
    dialogParams.left = configs.addressBookDialogLeft;
  if (typeof configs.addressBookDialogTop == 'number')
    dialogParams.top = configs.addressBookDialogTop;
  return Dialog.open(
    dialogParams,
    {}
  );
}

export async function editAddressBook(addressBook) {
  if (!addressBook.editableInUI)
    return;

  const focused = await browser.runtime.sendMessage({
    type:          Constants.COMMAND_FOCUS_TO_ADDRESS_BOOK_DIALOG,
    addressBookId: addressBook.id,
  }).catch(_error => false);
  if (focused)
    return;

  const dialogParams = {
    url:    URL_ADDRESS_BOOK,
    width:  configs.addressBookDialogWidth,
    //height: configs.addressBookDialogWidth,
  };
  if (typeof configs.addressBookDialogLeft == 'number')
    dialogParams.left = configs.addressBookDialogLeft;
  if (typeof configs.addressBookDialogTop == 'number')
    dialogParams.top = configs.addressBookDialogTop;
  return Dialog.open(
    dialogParams,
    {
      addressBookId: addressBook.id,
    }
  );
}

export async function deleteAddressBook(addressBook) {
  if (!addressBook || !addressBook.deletable)
    return;
  const result = await RichConfirm.show({
    modal: true,
    type: 'common-dialog',
    message: browser.i18n.getMessage('confirmToDeleteThisAddressbook_message', [addressBook.name]),
    buttons: [
      browser.i18n.getMessage('confirmToDeleteThisAddressbook_accept'),
      browser.i18n.getMessage('confirmToDeleteThisAddressbook_cancel'),
    ],
  });
  if (result.buttonIndex != 0)
    return;
  browser.addressBooks.delete(addressBook.id);
}

export async function syncAll({ force } = {}) {
  for (const addressBook of DataStore.getAllAddressBooks()) {
    browser.runtime.sendMessage({
      type:          Constants.COMMAND_SYNC_ADDRESS_BOOK,
      addressBookId: addressBook.id,
      force,
    });
  }
}

export function importFromFile() {
  return ImportUI.importFromFile();
}


/* contacts */

export async function createContact(addressBookId, { referenceContact } = {}) {
  const dialogParams = {
    url:    URL_CONTACT,
    width:  configs.contactDialogWidth,
    height: configs.contactDialogWidth
  };
  if (typeof configs.contactDialogLeft == 'number')
    dialogParams.left = configs.contactDialogLeft;
  if (typeof configs.contactDialogTop == 'number')
    dialogParams.top = configs.contactDialogTop;
  const fieldValues = referenceContact ?
    referenceContact.export(Array.from([
      ...Contact.KNOWN_FIELDS,
      ...Contact.KNOWN_VIRTUAL_FIELDS,
      ...Contact.KNOWN_EXTRA_FIELDS,
    ])) :
    {};
  const result = await Dialog.open(
    dialogParams,
    {
      addressBookId,
      categories: Array.from(DataStore.availableCategories),
      fieldValues,
    }
  );
  if (result.detail.createdId) {
    onItemCreated.dispatch({ type: 'contact', id: result.detail.createdId });
    return result.detail.createdId;
  }
}

async function editContact(contact) {
  const focused = await browser.runtime.sendMessage({
    type:      Constants.COMMAND_FOCUS_TO_CONTACT_DIALOG,
    contactId: contact.id,
  }).catch(_error => false);
  if (focused)
    return;

  const dialogParams = {
    url:    URL_CONTACT,
    width:  configs.contactDialogWidth,
    height: configs.contactDialogWidth
  };
  if (typeof configs.contactDialogLeft == 'number')
    dialogParams.left = configs.contactDialogLeft;
  if (typeof configs.contactDialogTop == 'number')
    dialogParams.top = configs.contactDialogTop;
  return Dialog.open(
    dialogParams,
    {
      contactId:     contact.id,
      addressBookId: contact.parentId,
      categories:    Array.from(DataStore.availableCategories),
    }
  );
}


/* mailing list */

export async function createMailingList(addressBookId, { defaultMembers } = {}) {
  const dialogParams = {
    url:    URL_MAILING_LIST,
    width:  configs.mailingListDialogWidth,
    height: configs.mailingListDialogWidth
  };
  if (typeof configs.mailingListDialogLeft == 'number')
    dialogParams.left = configs.mailingListDialogLeft;
  if (typeof configs.mailingListDialogTop == 'number')
    dialogParams.top = configs.mailingListDialogTop;
  const defaultContactIds = (defaultMembers || [])
    .filter(contact => contact.canBecomeMember && contact.parentId == addressBookId)
    .map(contact => contact.id);
  const result = await Dialog.open(
    dialogParams,
    {
      addressBookId,
      categories: Array.from(DataStore.availableCategories),
      defaultContactIds,
    }
  );
  if (result.detail.createdId) {
    onItemCreated.dispatch({ type: 'mailingList', id: result.detail.createdId });
    return result.detail.createdId;
  }
}

async function editMailingList(mailingList) {
  const focused = await browser.runtime.sendMessage({
    type:          Constants.COMMAND_FOCUS_TO_MAILING_LIST_DIALOG,
    mailingListId: mailingList.id,
  }).catch(_error => false);
  if (focused)
    return;

  const dialogParams = {
    url:    URL_MAILING_LIST,
    width:  configs.mailingListDialogWidth,
    height: configs.mailingListDialogWidth
  };
  if (typeof configs.mailingListDialogLeft == 'number')
    dialogParams.left = configs.mailingListDialogLeft;
  if (typeof configs.mailingListDialogTop == 'number')
    dialogParams.top = configs.mailingListDialogTop;
  return Dialog.open(
    dialogParams,
    {
      mailingListId: mailingList.id,
      addressBookId: mailingList.parentId,
      categories:    Array.from(DataStore.availableCategories),
    }
  );
}


/* item (contact and mailing list) */

export async function editItem(item) {
  switch (item.type) {
    case 'contact':
      return editContact(item);

    case 'mailingList':
      return editMailingList(item);

    default:
      throw new Error('unknown type');
  }
}


export async function deleteItems(items) {
  onItemsDeleting.dispatch(items);
  const promises = [];
  for (const item of items) {
    switch (item.type) {
      case 'contact':
        promises.push(browser.contacts.delete(item.id));
        break;
      case 'mailingList':
        promises.push(browser.mailingLists.delete(item.id));
        break;
      default:
        break;
    }
  }
  await Promise.all(promises);
  onItemsDeleted.dispatch(items);
}

export async function addNewCategory(items) {
  const result = await RichConfirm.show({
    modal: true,
    type: 'common-dialog',
    content: [
      `<p style="margin-bottom: 0.5em;">${sanitizeForHTMLText(browser.i18n.getMessage('promptNewCategoryName_message'))}</p>`,
      `<p style="display: flex; flex-directon: column; align-items: stretch;"
         ><input type="text"
                 name="name"
                 style="width: 100%;"></p>`,
    ].join(''),
    buttons: [
      browser.i18n.getMessage('promptNewCategoryName_accept'),
      browser.i18n.getMessage('promptNewCategoryName_cancel'),
    ],
  });
  if (result.buttonIndex != 0)
    return;

  const category = result.values.name;
  if (!category)
    return;
  for (const item of items) {
    if (item.categories.includes(category))
      continue;
    const categories = [...item.categories, category];
    item.categories = categories;
    BackgroundConnection.sendMessage({
      type:   Constants.COMMAND_PUSH_ITEM_UPDATE,
      id:     item.id,
      fields: {
        categories,
      },
    });
  }
}


export function startCompositionAsTo(items) {
  const emails = itemsToEmails(items);
  browser.compose.beginNew({ to: emails });
}

export function startCompositionAsCc(items) {
  const emails = itemsToEmails(items);
  browser.compose.beginNew({ cc: emails });
}

export function startCompositionAsBcc(items) {
  const emails = itemsToEmails(items);
  browser.compose.beginNew({ bcc: emails });
}

export function itemsToEmails(items) {
  return Array.from(
    new Set(
      Array.from(
        items,
        item => item.email
      ).flat().filter(recipient => !!recipient)
    )
  );
}


export async function shareItems(items) {
  const attachments = await Promise.all(items.map(async item => {
    const wrapped = normalizeItem(item);
    await wrapped.initialized;
    const file = new File([wrapped.vCard], wrapped.vCardFileName, { type: VCard.MIME_TYPE });
    return {
      name: wrapped.vCardFileName,
      file,
    };
  }));
  browser.compose.beginNew({ attachments });
}


export function openMap(items) {
  const addresses = itemsToAddresses(items);
  if (addresses.length <= 0)
    return;
  const url = browser.i18n.getMessage(`mapProvider_${configs.mapProvider}_url`);
  openURLsCommon(addresses.map(address => url.replace(/%S/gi, address)));
}

export function itemsToAddresses(items) {
  return Array.from(
    new Set(
      Array.from(
        items,
        item => item.address
      ).flat().filter(address => !!address)
    )
  );
}

export function openURLs(items) {
  const urls = itemsToURLs(items);
  if (urls.length <= 0)
    return;
  openURLsCommon(urls);
}

export function itemsToURLs(items) {
  return Array.from(
    new Set(
      Array.from(
        items,
        item => item.url
      ).flat().filter(url => !!url)
    )
  );
}

const mCutItemIds = new Set();
const mCopyItemIds = new Set();

export function getItemsInClipboard() {
  return [
    ...mCutItemIds,
    ...mCopyItemIds,
  ].map(id => DataStore.getContact(id) || DataStore.getMailingList(id)).filter(item => !!item);
}

export function cut(items) {
  mCutItemIds.clear();
  mCopyItemIds.clear();
  for (const item of items) {
    mCutItemIds.add(item.id);
  }
}

export function copy(items) {
  mCutItemIds.clear();
  mCopyItemIds.clear();
  for (const item of items) {
    mCopyItemIds.add(item.id);
  }
}

const mWaitingMove = new Map();
const mWaitingCopy = new Map();

export async function paste(addressBookId) {
  const items = getItemsInClipboard();
  const shouldMove = mCutItemIds.size > 0;

  for (const id of mCutItemIds) {
    mCopyItemIds.add(id);
  }
  mCutItemIds.clear();

  const promisedPasted = [];
  for (const item of items) {
    if (shouldMove && !DataStore.getAddressBook(item.parentId).readOnly) {
      if (addressBookId != item.parentId) {
        promisedPasted.push(new Promise(resolve => {
          mWaitingMove.set(`${item.id}:${item.parentId}=>${addressBookId}`, resolve);
          BackgroundConnection.sendMessage({
            type: Constants.COMMAND_MOVE_ITEM_TO,
            id:   item.id,
            destinationParentId: addressBookId,
          });
        }));
      }
    }
    else {
      promisedPasted.push(new Promise(resolve => {
        mWaitingCopy.set(`${item.id}:${item.parentId}=>${addressBookId}`, resolve);
        BackgroundConnection.sendMessage({
          type: Constants.COMMAND_COPY_ITEM_TO,
          id:   item.id,
          destinationParentId: addressBookId,
        });
      }));
    }
  }
  const pastedItems = await Promise.all(promisedPasted);

  onItemsPasted.dispatch({
    sourceItems: items,
    pastedItems,
  });
}


export async function duplicate(items) {
  const promisedDuplicated = [];
  for (const item of items) {
    promisedDuplicated.push(new Promise(resolve => {
      mWaitingCopy.set(`${item.id}:${item.parentId}=>${item.parentId}`, resolve);
      BackgroundConnection.sendMessage({
        type: Constants.COMMAND_COPY_ITEM_TO,
        id:   item.id,
        destinationParentId: item.parentId,
      });
    }));
  }
  const duplicatedItems = await Promise.all(promisedDuplicated);

  onItemsDuplicated.dispatch({
    sourceItems: items,
    duplicatedItems,
  });
}


export async function printContacts({ addressBookId, contacts } = {}) {
  contacts = contacts ? [...new Set(contacts.filter(item => item.contacts || item).flat())] : [];
  const contactIds = contacts.map(contact => contact.id);
  const printId = addressBookId || contactIds.slice(0).join('\n');
  const focused = await browser.runtime.sendMessage({
    type: Constants.COMMAND_FOCUS_TO_PRINT_PREVIEW,
    printId,
  }).catch(_error => false);
  if (focused)
    return;

  const dialogParams = {
    url:    URL_PRINT_PREVIEW,
    //width:  configs.printPreviewDialogWidth,
    //height: configs.printPreviewDialogWidth,
  };
  /*
  if (typeof configs.printPreviewDialogLeft == 'number')
    dialogParams.left = configs.printPreviewDialogLeft;
  if (typeof configs.printPreviewDialogTop == 'number')
    dialogParams.top = configs.printPreviewDialogTop;
  */
  await Dialog.openInTab(
    dialogParams,
    {
      addressBookId,
      contactIds,
      printId,
    }
  );
}


export async function mergeContacts(items, { addressBook }) {
  const contacts = items.filter(contact => contact.mergable);
  const contactIds = contacts.map(contact => contact.id);
  const focused = await browser.runtime.sendMessage({
    type: Constants.COMMAND_FOCUS_TO_MERGE_CONTACTS_DIALOG,
    contactIds,
  }).catch(_error => false);
  if (focused)
    return;

  const dialogParams = {
    url:    URL_MERGE_CONTACTS,
    width:  configs.mergeContactsDialogWidth,
    //height: configs.mergeContactsDialogWidth,
  };
  if (typeof configs.mergeContactsDialogLeft == 'number')
    dialogParams.left = configs.mergeContactsDialogLeft;
  if (typeof configs.mergeContactsDialogTop == 'number')
    dialogParams.top = configs.mergeContactsDialogTop;
  const result = await Dialog.open(
    dialogParams,
    {
      addressBookId: addressBook.id,
      contactIds,
    }
  );
  if (result.detail.createdId) {
    onItemsMerged.dispatch({
      mergedContactId: result.detail.createdId,
      sourceContacts: contacts,
    });
    return result.detail.createdId;
  }
}


BackgroundConnection.onMessage.addListener(async message => {
  switch (message && message.type) {
    case Constants.COMMAND_PUSH_MOVED_ITEM: {
      const key = `${message.sourceItemId}:${message.sourceParentId}=>${message.item.parentId}`;
      const resolver = mWaitingMove.get(key);
      if (resolver) {
        mWaitingMove.delete(key);
        resolver(message.item);
      }
    }; break;

    case Constants.COMMAND_PUSH_COPIED_ITEM: {
      const key = `${message.sourceItemId}:${message.sourceParentId}=>${message.item.parentId}`;
      const resolver = mWaitingCopy.get(key);
      if (resolver) {
        mWaitingCopy.delete(key);
        resolver(message.item);
      }
    }; break;
  }
});
