/*
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
*/
'use strict';

import EventListenerManager from '/extlib/EventListenerManager.js';

import {
  configs,
  log,
} from '/common/common.js';
import * as Constants from '/common/constants.js';

export const onMessage = new EventListenerManager();

const mId = `${Date.now()}-${Math.floor(Math.random() * Math.pow(2, 24))}`;

let mConnectionPort = null;
let mHeartbeatTimer = null;
let mPromisedStartedResolver;
let mPromisedStarted = new Promise((resolve, _reject) => {
  mPromisedStartedResolver = resolve;
});

function init() {
  if (!mPromisedStartedResolver)
    return;
  mPromisedStartedResolver();
  mPromisedStartedResolver = undefined;
  mPromisedStarted = undefined;

  mConnectionPort = browser.runtime.connect({
    name: `${Constants.COMMAND_REQUEST_CONNECT_PREFIX}${mId}`
  });
  mConnectionPort.onMessage.addListener(onConnectionMessage);
  mConnectionPort.onDisconnect.addListener(() => {
    log(`Disconnected accidentally: try to reconnect.`);
    location.reload();
  });
  if (mHeartbeatTimer)
    clearInterval(mHeartbeatTimer);
  mHeartbeatTimer = setInterval(() => {
    sendMessage({
      type: Constants.COMMAND_HEARTBEAT
    });
  }, configs.heartbeatInterval);
}

let mReservedMessages = [];
let mOnFrame;

export function sendMessage(message) {
  // Se should not send messages immediately, instead we should throttle
  // it and bulk-send multiple messages, for better user experience.
  // Sending too much messages in one event loop may block everything
  // and makes Thunderbird like frozen.
  //mConnectionPort.postMessage(message);
  mReservedMessages.push(message);
  if (!mOnFrame) {
    mOnFrame = () => {
      mOnFrame = null;
      const messages = mReservedMessages;
      mReservedMessages = [];
      mConnectionPort.postMessage(messages);
    };
    // Because sidebar is always visible, we may not need to avoid using
    // window.requestAnimationFrame. I just use a timer instead just for
    // a unity with common/sidebar-connection.js.
    //window.requestAnimationFrame(mOnFrame);
    setTimeout(mOnFrame, 0);
  }
}

async function onConnectionMessage(message) {
  if (Array.isArray(message))
    return message.forEach(onConnectionMessage);

  switch (message.type) {
    case 'echo': // for testing
      mConnectionPort.postMessage(message);
      break;

    default:
      if (mPromisedStarted)
        await mPromisedStarted;
      onMessage.dispatch(message);
      break;
  }
}

browser.runtime.onMessage.addListener((message, _sender) => {
  switch (message && message.type) {
    case Constants.COMMAND_PING_TO_CLIENT:
      if (message.clientId == mId)
        return Promise.resolve(true);
      break;
  }
});

init();
