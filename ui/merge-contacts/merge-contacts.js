/*
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
*/
'use strict';

import * as Dialog from '/extlib/dialog.js';
import { DOMUpdater } from '/extlib/dom-updater.js';
import '/extlib/l10n.js';
//import * as ResizableBox from '/extlib/resizable-box.js';

import {
  configs,
  log,
  sanitizeForHTMLText,
  toDOMDocumentFragment,
} from '/common/common.js';
import * as Commands from '/ui/commands.js';
import '/ui/common.js';
import * as Constants from '/common/constants.js';
import { Contact } from '/common/Contact.js';
import { CustomPersonalField, CustomOrganizationField } from '/common/CustomField.js';
import * as DataStore from '/ui/data-store.js';

let mParams;
let mMergedContact;
const mContacts = [];

const mPreviewButton = document.querySelector('#preview-button');
const mCreateButton  = document.querySelector('#create-button');
const mReplaceButton = document.querySelector('#replace-button');
const mCancelButton  = document.querySelector('#cancel-button');

function onConfigChange(key) {
  const value = configs[key];
  switch (key) {
    case 'debug':
      document.documentElement.classList.toggle('debug', value);
      break;
  }
}

function onMessage(message, _sender) {
  switch (message && message.type) {
    case Constants.COMMAND_FOCUS_TO_MERGE_CONTACTS_DIALOG:
      if (mContacts.map(contact => contact.id).sort().join('\n') == message.contactIds.sort().join('\n')) {
        window.focus();
        return Promise.resolve(true);
      }
      break;
  }
}

function rebuildCustomFields() {
  const personalFields = [];
  for (const field of CustomPersonalField.getAll()) {
    if (field.bindTo)
      continue;
    personalFields.push(`
      <tr id=${JSON.stringify(sanitizeForHTMLText('custom-personal-field:' + field.id))}
          data-item-fields=${JSON.stringify(sanitizeForHTMLText(field.fieldPath))}
          data-label=${JSON.stringify(sanitizeForHTMLText(field.label))}></tr>
    `.trim());
  }
  const personalFieldsContainer = document.querySelector('#custom-personal-information-fields');
  DOMUpdater.update(personalFieldsContainer, toDOMDocumentFragment(personalFields.join(''), personalFieldsContainer));

  const organizationFields = [];
  for (const field of CustomOrganizationField.getAll()) {
    if (field.bindTo)
      continue;
    organizationFields.push(`
      <tr id=${JSON.stringify(sanitizeForHTMLText('custom-organization-field:' + field.id))}
          data-item-fields=${JSON.stringify(sanitizeForHTMLText(field.fieldPath))}
          data-label=${JSON.stringify(sanitizeForHTMLText(field.label))}></tr>
    `.trim());
  }
  const organizationFieldsContainer = document.querySelector('#custom-organization-information-fields');
  DOMUpdater.update(organizationFieldsContainer, toDOMDocumentFragment(organizationFields.join(''), organizationFieldsContainer));
}

function buildFieldColumns() {
  for (const node of document.querySelectorAll('[data-item-field]')) {
    const field = node.dataset.itemField;
    let found = false;
    let hasDifferentValue = false;
    let lastLabel;
    const columns = mContacts.map((contact, index) => {
      const label = Array.isArray(contact[field]) ?
        contact[field].join('\n') :
        contact[field];
      let checked = '';
      if (!found &&
          contact[field] &&
          label) {
        found = true;
        checked = 'checked';
        mMergedContact[field] = contact[field];
      }
      if (index > 0 &&
          label != lastLabel) {
        hasDifferentValue = true;
      }
      lastLabel = label;
      return !label ? '' : `
        <td id=${JSON.stringify(sanitizeForHTMLText('contact:' + contact.id + ':' + field))}
           ><label id=${JSON.stringify(sanitizeForHTMLText('contact:' + contact.id + ':' + field + ':label'))}
                  ><input id=${JSON.stringify(sanitizeForHTMLText('contact:' + contact.id + ':' + field + ':label'))}
                          name=${JSON.stringify(sanitizeForHTMLText('field:' + field + ':radio'))}
                          type="radio"
                          data-field=${JSON.stringify(sanitizeForHTMLText(field))}
                          value=${JSON.stringify(sanitizeForHTMLText(contact.id))}
                          ${checked}
                         ><span>${sanitizeForHTMLText(label)}</span></label></td>
      `.trim();
    });
    if (!found ||
        !hasDifferentValue) {
      node.classList.add('hidden');
      continue;
    }

    DOMUpdater.update(node, toDOMDocumentFragment([
      `<th id=${JSON.stringify(sanitizeForHTMLText('field:' + field + ':label'))}
          >${sanitizeForHTMLText(node.dataset.label)}</th>`,
      ...columns,
    ].join(''), node));
  }
}

function applyFields() {
  const radioButtons = [
    // Apply "Custom1"-"Custom4" fields at first, because categories and
    // some metadata are stored in the field, and unexpectedly overrides
    // applied categoreis or other fields.
    ...document.querySelectorAll('input[type="radio"][data-field^="Custom"]:checked'),
    ...document.querySelectorAll('input[type="radio"]:not([data-field^="Custom"]):checked'),
  ];
  for (const radio of radioButtons) {
    const contact = Contact.get(radio.value);
    if (!contact)
      continue;
    const field = radio.dataset.field;
    mMergedContact[field] = contact[field];
  }
}

configs.$loaded.then(async () => {
  mParams = await Dialog.getParams();

  const contacts = await Promise.all(mParams.contactIds && mParams.contactIds.map(id => DataStore.getRawContact(id)) || []);
  mContacts.push(...contacts.map(contact => new Contact(contact)));
  mMergedContact = new Contact({
    parentId: mParams.addressBookId,
    properties: {},
  });

  browser.runtime.onMessage.addListener(onMessage);
  window.addEventListener('unload', () => {
    browser.runtime.onMessage.removeListener(onMessage);
  }, { once: true });

  rebuildCustomFields();
  buildFieldColumns();

  configs.$addObserver(onConfigChange);
  onConfigChange('debug');

  Dialog.initButton(mPreviewButton, async _event => {
    applyFields();
    const createdContactId = await Commands.createContact(mParams.addressBookId, {
      referenceContact: mMergedContact,
    });
    await Contact.waitUntilCreated(createdContactId);
    const createdContact = Contact.get(createdContactId);
    if (createdContact)
      Dialog.accept({
        createdId: createdContact.id,
      });
  });
  Dialog.initButton(mCreateButton, async _event => {
    applyFields();
    await mMergedContact.save();
    Dialog.accept({
      createdId: mMergedContact.id,
    });
  });
  Dialog.initButton(mReplaceButton, async _event => {
    applyFields();
    await mMergedContact.save();
    await Promise.all(mContacts.map(contact => browser.contacts.delete(contact.id)));
    Dialog.accept({
      createdId: mMergedContact.id,
    });
  });
  Dialog.initCancelButton(mCancelButton);

  await Dialog.notifyReady();

  window.addEventListener('resize', () => {
    configs.mergeContactsDialogWidth = window.outerWidth;
    configs.mergeContactsDialogHeight = window.outerHeight;
  });
  window.addEventListener(Dialog.TYPE_MOVED, event => {
    configs.mergeContactsDialogLeft = event.detail.left;
    configs.mergeContactsDialogTop = event.detail.top;
  });

  //window.addEventListener(ResizableBox.TYPE_RESIZED, event => {
  //  configs.mergeContactsDialogBoxSizes = event.detail;
  //});
  log('merge contacts dialog initialize done');
});
