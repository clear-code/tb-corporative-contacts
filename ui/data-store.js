/*
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
*/
'use strict';

import {
  log,
} from '/common/common.js';
import * as BackgroundConnection from '/ui/background-connection.js';
import * as Constants from '/common/constants.js';
import { normalizeGroup } from '/common/AddressBook.js';
import { normalizeItem } from '/common/Contact.js';

const mAddressBooksById = new Map();
const mSubGroupsById = new Map();
const mContactsById = new Map();
const mMailingListsById = new Map();

export const availableCategories = new Set();

export async function waitUntilTracked(data) {
  switch (data.type) {
    case 'contact':
      return waitUntilContactTracked(data.id);

    case 'mailingList':
      return waitUntilMailingListTracked(data.id);
  }
}


export function getAddressBook(id) {
  return mAddressBooksById.get(id);
}

function addAddressBook(addressBook) {
  return mAddressBooksById.set(addressBook.id, addressBook);
}

function removeAddressBook(id) {
  mAddressBooksById.delete(id);
}

export function getAllAddressBooks() {
  return Array.from(mAddressBooksById.values());
}


export function getSubGroup(id) {
  return mSubGroupsById.get(id);
}

function addSubGroup(subGroup) {
  return mSubGroupsById.set(subGroup.id, subGroup);
}

function removeSubGroup(id) {
  mSubGroupsById.delete(id);
}


const mWaitingContacts = new Map();

export function getContact(id) {
  return mContactsById.get(id);
}

// Workaround for https://gitlab.com/clear-code/tb-corporative-contacts/-/issues/49
export async function getRawContact(id) {
  try {
    const contact = await browser.contacts.get(id);
    if (contact) {
      log('DataStore.getRawContact: got value via API ', id, contact);
      return contact;
    }
  }
  catch(_error) {
    // browser.contacts.get() fails if the contact is moved between address books via the header bar...
  }
  const cached = await browser.runtime.sendMessage({
    type: Constants.COMMAND_FETCH_RAW_CONTACT,
    id,
  });
  log('DataStore.getRawContact: got cached value ', id, cached);
  return cached;
}

function addContact(contact) {
  mContactsById.set(contact.id, contact);
  const resolvers = mWaitingContacts.get(contact.id);
  mWaitingContacts.delete(contact.id);
  if (resolvers) {
    for (const resolver of resolvers) {
      resolver(contact);
    }
  }
  return contact;
}

function removeContact(id) {
  mContactsById.delete(id);
}

export async function waitUntilContactTracked(id) {
  const contact = mContactsById.get(id);
  if (contact)
    return contact;
  return new Promise(resolve => {
    const resolvers = mWaitingContacts.get(id) || [];
    resolvers.push(resolve);
    mWaitingContacts.set(id, resolvers)
  });
}


const mWaitingMailingLists = new Map();

export function getMailingList(id) {
  return mMailingListsById.get(id);
}

function addMailingList(mailingList) {
  mMailingListsById.set(mailingList.id, mailingList);
  const resolvers = mWaitingMailingLists.get(mailingList.id);
  mWaitingMailingLists.delete(mailingList.id);
  if (resolvers) {
    for (const resolver of resolvers) {
      resolver(mailingList);
    }
  }
  return mailingList;
}

function removeMailingList(id) {
  mMailingListsById.delete(id);
}

export async function waitUntilMailingListTracked(id) {
  const mailingList = mMailingListsById.get(id);
  if (mailingList)
    return mailingList;
  return new Promise(resolve => {
    const resolvers = mWaitingMailingLists.get(id) || [];
    resolvers.push(resolve);
    mWaitingMailingLists.set(id, resolvers)
  });
}


function trackSubGroups(parent) {
  if (!parent._children)
    return;
  for (const child of parent._children) {
    addSubGroup(child);
    trackSubGroups(child);
  }
}

function untrackSubGroups(parent) {
  if (!parent._children)
    return;
  for (const child of parent._children) {
    removeSubGroup(child);
    untrackSubGroups(child);
  }
}

BackgroundConnection.onMessage.addListener(async message => {
  switch (message && message.type) {
    case Constants.COMMAND_PUSH_ALL_ADDRESS_BOOKS:
      for (const addressBook of message.addressBooks) {
        addAddressBook(addressBook);
        trackSubGroups(addressBook);
      }
      break;

    case Constants.COMMAND_PUSH_NEW_ADDRESS_BOOK:
      addAddressBook(message.addressBook);
      break;

    case Constants.COMMAND_PUSH_GROUP_UPDATE: {
      const group = getAddressBook(message.id) || getSubGroup(message.id);
      if (!group ||
          Object.keys(message.fields).length == 0)
        break;
      const wrapped = normalizeGroup(group);
      for (const [field, value] of Object.entries(message.fields)) {
        group[field] = value;
        if (wrapped)
          wrapped[field] = value;
      }
    }; break;

    case Constants.COMMAND_PUSH_ADDRESS_BOOK_DELETE: {
      const addressBook = getAddressBook(message.id);
      if (addressBook)
        untrackSubGroups(addressBook);
      removeAddressBook(message.id);
    }; break;

    case Constants.COMMAND_PUSH_NEW_GROUP_CHILD: {
      const parent = getAddressBook(message.parentId) || getSubGroup(message.parentId);
      if (!parent)
        break;
      if (!parent._children)
        parent._children = [];
      if (!parent._children.some(child => child.id == message.child.id))
        parent._children.push(message.child);
      addSubGroup(message.child);
      trackSubGroups(message.child);
    }; break;

    case Constants.COMMAND_PUSH_ALL_CHILDREN_DISAPPEARED: {
      const parent = getAddressBook(message.id) || getSubGroup(message.id);
      if (!parent)
        break;
      delete parent._children;
    }; break;

    case Constants.COMMAND_PUSH_GROUP_CHILD_DELETE: {
      const parent = getAddressBook(message.parentId) || getSubGroup(message.parentId);
      if (parent && parent._children) {
        const index = parent._children.findIndex(child => child.id == message.childId);
        if (index > -1)
          parent._children.splice(index, 1);
      }
      removeSubGroup(message.childId);
    }; break;


    case Constants.COMMAND_PUSH_ALL_CONTACTS:
      for (const contact of message.contacts) {
        addContact(contact);
      }
      break;

    case Constants.COMMAND_PUSH_ALL_MAILING_LISTS:
      for (const mailingList of message.mailingLists) {
        addMailingList(mailingList);
      }
      break;

    case Constants.COMMAND_PUSH_NEW_ITEM:
      switch (message.item.type) {
        case 'contact':
          addContact(message.item);
          break;

        case 'mailingList':
          addMailingList(message.item);
          break;
      }
      break;

    case Constants.COMMAND_PUSH_ITEMS_OF:
      for (const item of message.items) {
        switch (item.type) {
          case 'contact':
            addContact(item);
            break;

          case 'mailingList':
            addMailingList(item);
            break;
        }
      }
      break;

    case Constants.COMMAND_PUSH_ITEM_UPDATE: {
      const item = getContact(message.id) || getMailingList(message.id);
      if (!item ||
          Object.keys(message.fields).length == 0)
        break;
      const wrapped = normalizeItem(item);
      for (const [field, value] of Object.entries(message.fields)) {
        item[field] = value;
        if (wrapped)
          wrapped[field] = value;
      }
    }; break;

    case Constants.COMMAND_PUSH_ITEM_DELETE:
      removeContact(message.id);
      removeMailingList(message.id);
      break;

    case Constants.COMMAND_PUSH_ITEM_MOVE: {
      const item = getContact(message.id);
      if (item)
        item.parentId = message.newParentId;
    }; break;

    case Constants.COMMAND_PUSH_MAILING_LIST_CONTACT_ADD: {
      const parent = getMailingList(message.id);
      if (!parent)
        return;
      parent.contacts.push(message.contact);
    }; break;

    case Constants.COMMAND_PUSH_MAILING_LIST_CONTACT_REMOVE: {
      const parent = getMailingList(message.id);
      if (!parent)
        return;
      const index = parent.contacts.findIndex(contact => contact.id == message.contactId);
      if (index > -1)
        parent.contacts.splice(index, 1);
    }; break;

    case Constants.COMMAND_PUSH_ALL_AVAILABLE_CATEGORIES:
      for (const category of message.categories) {
        availableCategories.add(category);
      }
      break;

    case Constants.COMMAND_PUSH_NEW_AVAILABLE_CATEGORY:
      availableCategories.add(message.category);
      break;
  }
});

BackgroundConnection.sendMessage({
  type: Constants.COMMAND_FETCH_ALL_AVAILABLE_CATEGORIES,
});
