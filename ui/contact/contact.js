/*
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
*/
'use strict';

import * as Dialog from '/extlib/dialog.js';
import { DOMUpdater } from '/extlib/dom-updater.js';
import '/extlib/l10n.js';
//import * as ResizableBox from '/extlib/resizable-box.js';
import { tabbis } from '/extlib/tabbis.es6.min.js';

import {
  configs,
  log,
  orderItems,
  sanitizeForHTMLText,
  toDOMDocumentFragment,
} from '/common/common.js';
import { AddressBook } from '/common/AddressBook.js';
import { ChatProvider } from '/common/ChatProvider.js';
import '/ui/common.js';
import * as Constants from '/common/constants.js';
import { Contact } from '/common/Contact.js';
import { CustomPersonalField, CustomOrganizationField } from '/common/CustomField.js';
import * as DataStore from '/ui/data-store.js';
import * as ItemEditUI from '/ui/item-edit-ui.js';

let mParams;
let mContact;
let mAddressBook;
let mAddressBooks;
let mAllCategories = [];

const mAcceptButton = document.querySelector('#accept');
const mCancelButton = document.querySelector('#cancel');

function onConfigChange(key) {
  const value = configs[key];
  switch (key) {
    case 'debug':
      document.documentElement.classList.toggle('debug', value);
      break;
  }
}

function onMessage(message, _sender) {
  switch (message && message.type) {
    case Constants.COMMAND_FOCUS_TO_CONTACT_DIALOG:
      if (mContact && message.contactId == mContact.id) {
        window.focus();
        return Promise.resolve(true);
      }
      break;
  }
}

function buildCustomFields() {
  const personalFields = [];
  for (const field of CustomPersonalField.getAll()) {
    if (field.bindTo)
      continue;
    personalFields.push(`
      <tr>
        <th><label for="custom-personal-${personalFields.length}"
                  >${sanitizeForHTMLText(field.label)}</label></th>
        <td><input id="custom-personal-${personalFields.length}"
                   type="text"
                   data-item-field=${JSON.stringify(sanitizeForHTMLText(field.fieldPath))}></td>
      </tr>
    `.trim());
  }
  const personalFieldsContainer = document.querySelector('#custom-personal-information-fields');
  DOMUpdater.update(personalFieldsContainer, toDOMDocumentFragment(personalFields.join(''), personalFieldsContainer));

  const organizationFields = [];
  for (const field of CustomOrganizationField.getAll()) {
    if (field.bindTo)
      continue;
    organizationFields.push(`
      <tr>
        <th><label for="custom-organization-${organizationFields.length}"
                  >${sanitizeForHTMLText(field.label)}</label></th>
        <td><input id="custom-organization-${organizationFields.length}"
                   type="text"
                   data-item-field=${JSON.stringify(sanitizeForHTMLText(field.fieldPath))}></td>
      </tr>
    `.trim());
  }
  const organizationFieldsContainer = document.querySelector('#custom-organization-information-fields');
  DOMUpdater.update(organizationFieldsContainer, toDOMDocumentFragment(organizationFields.join(''), organizationFieldsContainer));
}

function buildOrganizationsFields() {
  const fields = [];
  for (const label of configs.organizationLabels) {
    fields.push(`
      <tr>
        <th><label for="organization-${fields.length}"
                  >${sanitizeForHTMLText(label)}</label></th>
        <td><input id="organization-${fields.length}"
                   type="text"></td>
      </tr>
    `.trim());
  }
  const container = document.querySelector('#organizations-fields');
  DOMUpdater.update(container, toDOMDocumentFragment(fields.join(''), container));
}

function buildAddressFields() {
  const fieldNames = (browser.i18n.getMessage('displayAddressFormat') == Constants.ADDRESS_FORMAT_TOP_DOWN) ?
    {
      Country:  'country-name',
      ZipCode:  'postal-code',
      State:    'address-level1',
      City:     'address-level2',
      Address:  'street-address',
      Address2: 'street-address',
    } :
    {
      Address:  'street-address',
      Address2: 'street-address',
      City:     'address-level2',
      State:    'address-level1',
      ZipCode:  'postal-code',
      Country:  'country-name',
    };

  const contents = [];
  for (const prefix of ['Work', 'Home']) {
    const fields = [];
    for (const [field, autocomplete] of Object.entries(fieldNames)) {
      fields.push(`
        <tr>
          <th><label for="${sanitizeForHTMLText(prefix + field)}"
                    >${sanitizeForHTMLText(browser.i18n.getMessage('contact_field_' + prefix + field))}</label></th>
          <td><input id="${sanitizeForHTMLText(prefix + field)}"
                     type="text"
                     data-item-field="${sanitizeForHTMLText(prefix + field)}"
                     autocomplete="${autocomplete}"></td>
        </tr>
      `.trim());
    }
    contents.push(`
      <tr>
        <th><label for="address-${prefix}-summary">${sanitizeForHTMLText(browser.i18n.getMessage('contact_field_' + prefix))}</label></th>
        <td class="has-detail"
           ><input id="address-${prefix}-summary"
                   type="text"
                   class="summary"
                   readonly
                  ><div class="detail address-fields"
                       ><table>${fields.join('')}</table></div></td>
      </tr>
    `.trim());
  }
  const container = document.querySelector('#address-fields');
  DOMUpdater.update(container, toDOMDocumentFragment(contents.join(''), container));

  container.addEventListener('change', updateAddressSummaries);
}

function updateAddressSummaries() {
  for (const summary of document.querySelectorAll('#address-fields .summary')) {
    summary.value = Array.from(summary.nextSibling.querySelectorAll('input[type="text"]'), field => field.value).join(' ');
  }
}

function buildIMMPFields() {
  const fields = [];
  for (const provider of ChatProvider.getAll()) {
    if (!provider)
      continue;
    fields.push(`
      <tr>
        <th><label for="${sanitizeForHTMLText(provider.fieldPath)}"
                  >${sanitizeForHTMLText(provider.label || provider.fieldPath || provider)}</label></th>
        <td><input id="${sanitizeForHTMLText(provider.fieldPath)}"
                   type="text"
                   data-item-field="${sanitizeForHTMLText(provider.fieldPath)}"
                   autocomplete="impp"></td>
      </tr>
    `.trim());
  }
  const container = document.querySelector('#impp-fields');
  DOMUpdater.update(container, toDOMDocumentFragment(fields.join(''), container));
}

function initImageChooserEventHandlers() {
  const chooseImageButton = document.querySelector('#choose-image-button');
  if (mAddressBook && mAddressBook.readOnly) {
    chooseImageButton.classList.add('hidden');
  }
  else {
    chooseImageButton.classList.remove('hidden');
    const imageFileField = document.querySelector('#choose-image-file-field');
    const imageElement   = document.querySelector('#contact-image-container img');
    imageFileField.addEventListener('change', _event => {
      setImageFromFile(imageFileField.files[0], imageElement);
    });
    Dialog.initButton(imageElement, async _event => {
      imageFileField.click();
    });
    Dialog.initButton(chooseImageButton, async _event => {
      imageFileField.click();
    });
  }
}

function fillFields() {
  for (const field of document.querySelectorAll('[data-item-field]')) {
    const value = String(mContact && mContact.getFieldValue(field.dataset.itemField) || '').trim();
    switch (field.localName) {
      case 'img':
        field.src = value;
        break;

      case 'input':
      case 'textarea':
      case 'select':
        field.value = value;
        break;

      default:
        field.textContent = value;
        break;
    }
    field.disabled = mAddressBook && mAddressBook.readOnly;
  }

  const organizations = (mContact && mContact.organizations || []).slice(0);
  for (const field of document.querySelectorAll('#organizations-fields input[type="text"]')) {
    field.value = organizations.shift() || '';
    field.disabled = mAddressBook && mAddressBook.readOnly;
  }
}

function updateDisplayName() {
  const original = mContact.generatedDisplayName;
  mContact.NameField1 = document.querySelector('#NameField1').value;
  mContact.NameField2 = document.querySelector('#NameField2').value;
  if (mContact.generatedDisplayName != original &&
      !mContact.userDefinedDisplayName) {
    const displayNameField = document.querySelector('#visibleDisplayName');
    displayNameField.value = mContact.generatedDisplayName;
    displayNameField.classList.add('dirty');
  }
}

function setImageFromFile(file, img) {
  if (!file)
    return;
  const reader = new FileReader();
  reader.addEventListener('load', () => {
    img.src = reader.result;
  }, { once: true });
  reader.readAsDataURL(file);
}

async function saveChanges() {
  if (!mContact)
    return;

  for (const field of document.querySelectorAll('[data-item-field]')) {
    if (!field.classList.contains('dirty'))
      continue;

    const name = field.dataset.itemField;
    const oldValue = mContact.getFieldValue(name) || '';
    const newValue = field.localName == 'img' ? field.src : field.value;
    if (newValue == oldValue)
      continue;

    log('set value: ', name, { oldValue, newValue });
    mContact.setFieldValue(name, newValue);
  }

  const oldOrganizations = mContact && mContact.organizations || [];
  const updatedOrganizations = Array.from(document.querySelectorAll('#organizations-fields input[type="text"]'), field => field.value || '');
  const newOrganizations = updatedOrganizations.concat(oldOrganizations.slice(updatedOrganizations.length));
  if (newOrganizations.join('\n') != oldOrganizations.join('\n')) {
    mContact.setFieldValue('organizations', newOrganizations);
  }

  await mContact.save();
}

configs.$loaded.then(async () => {
  const [params, addressBooks] = await Promise.all([
    Dialog.getParams(),
    browser.addressBooks.list(false)
      .then(addressBooks => addressBooks.map(addressBook => new AddressBook(addressBook))),
  ]);
  mParams = params;
  mAllCategories = new Set(mParams.categories.sort());
  log('contact dialog initialize ', mParams);

  const [contact, addressBook] = await Promise.all([
    mParams.contactId && DataStore.getRawContact(mParams.contactId),
    AddressBook.get(mParams.addressBookId),
    ...addressBooks.map(addressBook => addressBook.initialized),
  ]);
  mContact = new Contact(contact || {
    parentId:   addressBook && addressBook.id,
    properties: {},
  });
  if (mParams.fieldValues) {
    for (const [field, value] of Object.entries(mParams.fieldValues)) {
      if (value !== undefined)
        mContact[field] = value;
    }
  }
  mAddressBook = addressBook;
  mAddressBooks = orderItems(addressBooks, {
    getItemIndex(addressBook) {
      return addressBook.index;
    },
    onInitialized(_addressBook) {
    },
    onOrdered(addressBook, index) {
      addressBook.index = index;
    },
  });

  const title = contact ? mContact.DisplayName : browser.i18n.getMessage('ui_new_contact_title');
  document.title = browser.i18n.getMessage('ui_contact_title', [title]);

  browser.runtime.onMessage.addListener(onMessage);
  window.addEventListener('unload', () => {
    browser.runtime.onMessage.removeListener(onMessage);
  }, { once: true });

  ItemEditUI.buildAddressBooksSelector(mAddressBooks);
  ItemEditUI.buildCategories(mContact, {
    allCategories: mAllCategories,
  });
  buildCustomFields();
  buildOrganizationsFields();
  buildAddressFields();
  buildIMMPFields();
  ItemEditUI.initDetailContainers();
  ItemEditUI.initCategoriesEventHandlers(mContact, {
    allCategories: mAllCategories,
  });
  initImageChooserEventHandlers();

  if (mAddressBook)
    await mAddressBook.initialized;
  fillFields();
  updateAddressSummaries();

  document.addEventListener('change', event => {
    const field = event.target;
    field.classList.add('dirty');
  });

  document.querySelector('#NameField1').addEventListener('input', updateDisplayName);
  document.querySelector('#NameField2').addEventListener('input', updateDisplayName);

  configs.$addObserver(onConfigChange);
  onConfigChange('debug');

  //ResizableBox.init(configs.contactDialogBoxSizes);

  tabbis();
  /*
  document.addEventListener('tabbis', event => {
    const { tab, pane } = event.detail;
    console.log(tab, pane);
  }, false );
  */

  const chatProvider = configs.preferredChatProvider && ChatProvider.get(configs.preferredChatProvider);
  document.querySelector('#contact-start-chat-button-container').classList.toggle('hidden', !chatProvider);
  if (chatProvider) {
    Dialog.initButton(document.querySelector('#contact-start-chat-button'), _event => {
      chatProvider.open(mContact);
    });
  }

  Dialog.initButton(mAcceptButton, async _event => {
    if (!mAddressBook || !mAddressBook.readOnly)
      await saveChanges();
    Dialog.accept({
      createdId: !mParams.contactId && mContact.id,
    });
  });
  if (mAddressBook && mAddressBook.readOnly) {
    mAcceptButton.textContent = browser.i18n.getMessage('ui_contact_closeButton_label');
    mCancelButton.classList.add('hidden');
  }
  else {
    mAcceptButton.textContent = browser.i18n.getMessage('ui_contact_saveButton_label');
    mCancelButton.classList.remove('hidden');
    Dialog.initCancelButton(mCancelButton);
  }

  await Dialog.notifyReady();

  window.addEventListener('resize', () => {
    configs.contactDialogWidth = window.outerWidth;
    configs.contactDialogHeight = window.outerHeight;
  });
  window.addEventListener(Dialog.TYPE_MOVED, event => {
    configs.contactDialogLeft = event.detail.left;
    configs.contactDialogTop = event.detail.top;
  });

  //window.addEventListener(ResizableBox.TYPE_RESIZED, event => {
  //  configs.contactDialogBoxSizes = event.detail;
  //});
  log('contact dialog initialize done');
});
