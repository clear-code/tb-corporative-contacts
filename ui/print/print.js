/*
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
*/
'use strict';

import * as Dialog from '/extlib/dialog.js';
import '/extlib/l10n.js';

import {
  configs,
  doProgressively,
  log,
  sanitizeForHTMLText,
  toDOMDocumentFragment,
} from '/common/common.js';
import { ChatProvider } from '/common/ChatProvider.js';
import '/ui/common.js';
import * as Constants from '/common/constants.js';
import { Contact } from '/common/Contact.js';
import * as DataStore from '/ui/data-store.js';
import * as VCard from '/common/import/vcard.js';

let mParams;
let mPrintId;
const mContacts = [];

const mPrintButton = document.querySelector('#print-button');
const mCloseButton = document.querySelector('#close-button');

function onConfigChange(key) {
  const value = configs[key];
  switch (key) {
    case 'debug':
      document.documentElement.classList.toggle('debug', value);
      break;
  }
}

function onMessage(message, _sender) {
  switch (message && message.type) {
    case Constants.COMMAND_FOCUS_TO_PRINT_PREVIEW:
      if (mPrintId == message.printId) {
        window.focus();
        return Promise.resolve(true);
      }
      break;
  }
}

async function rebuildPrintableContents() {
  const startedAt = rebuildPrintableContents.lastStartTime = Date.now();
  const container = document.querySelector('#printable');
  const range = document.createRange();
  range.selectNodeContents(container);
  range.deleteContents();
  range.detach();

  mPrintButton.disabled = true;

  const progressBar = document.querySelector('#progress-bar');
  progressBar.value = 0;

  let processedCount = 0;

  const completed = await doProgressively({
    items: mContacts,
    task: items => {
      if (rebuildPrintableContents.lastStartTime != startedAt)
        return false;
      processedCount += items.length;
      progressBar.value = Math.floor(processedCount / mContacts.length * 100);
      container.appendChild(toDOMDocumentFragment(items.map(contactToHTML).join(''), container));
    },
    chunkSize: configs.itemsPrintChunkSize,
    interval:  configs.itemsPrintInterval,
  });

  progressBar.value = 100;
  if (completed)
    mPrintButton.disabled = false;
}
rebuildPrintableContents.lastStartTime = 0;

function contactToHTML(contact) {
  return [
    `<section id=${JSON.stringify(sanitizeForHTMLText('contact:' + contact.id))}>`,
    `<table id=${JSON.stringify(sanitizeForHTMLText('contact:' + contact.id + ':container'))}>`,
    !configs.printDisplayName ? '' : printGroup({
      group: 'displayName',
      contact,
      heading: printHeading({
        group: 'displayName',
        caption: browser.i18n.getMessage('ui_contact_displayName_caption'),
        contact,
      }),
      fields: [
        printRow({
          field: 'visibleDisplayName',
          label: browser.i18n.getMessage('ui_contact_displayName_caption'),
          contact,
        }),
      ],
    }),
    !configs.printPersonalInformation ? '' : printGroup({
      group: 'personalInformation',
      contact,
      heading: printHeading({
        group: 'personalInformation',
        caption: browser.i18n.getMessage('ui_contact_personalInformation_caption'),
        contact,
      }),
      fields: [
        nameFieldRow({ field: 'NameField1', contact }),
        nameFieldRow({ field: 'NameField2', contact }),
        printRow({ field: 'SpouseName', contact }),
        printRow({ field: 'FamilyName', contact }),
        printRow({ field: 'namePrefix', contact }),
        printRow({ field: 'nameSuffix', contact }),
        printRow({ field: 'NickName', contact }),
        printRow({ field: 'gender', contact }),
        printRow({ field: 'birthDay', contact }),
        printRow({
          field: 'birthPlace',
          value: contact.getFieldValue('extra.birth'),
          contact,
        }),
        printRow({ field: 'deathDay', contact }),
        printRow({
          field: 'deathPlace',
          value: contact.getFieldValue('extra.x-death-place'),
          contact,
        }),
        printRow({ field: 'anniversaryDay', contact }),
      ],
    }),
    !configs.printOrganizationInformation ? '' : printGroup({
      group: 'organizationInformation',
      contact,
      heading: printHeading({
        group: 'organizationInformation',
        caption: browser.i18n.getMessage('ui_contact_organizationInformation_caption'),
        contact,
      }),
      fields: [
        printRow({ field: 'Company', contact }),
        printRow({ field: 'Department', contact }),
        printRow({
          field: 'organizations',
          value: contact.organizations.join(VCard.VALUE_SEPARATOR),
          contact,
        }),
        printRow({ field: 'JobTitle', contact }),
        printRow({
          field: 'role',
          value: contact.getFieldValue('extra.role'),
          contact,
        }),
      ],
    }),
    !configs.printCategory ? '' : printGroup({
      group: 'categories',
      contact,
      heading: printHeading({
        group: 'categories',
        caption: browser.i18n.getMessage('ui_contact_categories_caption'),
        contact,
      }),
      fields: [
        printRow({
          field: 'categories',
          value: contact.categories.join(VCard.VALUE_SEPARATOR),
          label: browser.i18n.getMessage('ui_contact_categories_caption'),
          contact,
        }),
      ],
    }),
    !configs.printAddress ? '' : printGroup({
      group: 'address',
      contact,
      heading: printHeading({
        group: 'address',
        caption: browser.i18n.getMessage('ui_contact_address_caption'),
        contact,
      }),
      fields: [
        printRow({ field: 'displayWorkAddress', contact }),
        printRow({ field: 'displayHomeAddress', contact }),
      ],
    }),
    !configs.printPhone ? '' : printGroup({
      group: 'phone',
      contact,
      heading: printHeading({
        group: 'phone',
        caption: browser.i18n.getMessage('ui_contact_phone_caption'),
        contact,
      }),
      fields: [
        printRow({ field: 'WorkPhone', contact }),
        printRow({ field: 'HomePhone', contact }),
        printRow({ field: 'FaxNumber', contact }),
        printRow({ field: 'PagerNumber', contact }),
        printRow({ field: 'CellularNumber', contact }),
      ],
    }),
    !configs.printEmail ? '' : printGroup({
      group: 'email',
      contact,
      heading: printHeading({
        group: 'email',
        caption: browser.i18n.getMessage('ui_contact_email_caption'),
        contact,
      }),
      fields: [
        printRow({ field: 'PrimaryEmail', contact }),
        printRow({ field: 'SecondEmail', contact }),
      ],
    }),
    !configs.printImpp ? '' : printGroup({
      group: 'impp',
      contact,
      heading: printHeading({
        group: 'impp',
        caption: browser.i18n.getMessage('ui_contact_impp_caption'),
        contact,
      }),
      fields: ChatProvider.getAll().map(provider =>
        printRow({
          field: `immp:${provider.id}`,
          label: provider.label,
          value: contact.getFieldValue(provider.fieldPath),
          contact,
        })
      ),
    }),
    !configs.printUrl ? '' : printGroup({
      group: 'url',
      contact,
      heading: printHeading({
        group: 'url',
        caption: browser.i18n.getMessage('ui_contact_url_caption'),
        contact,
      }),
      fields: [
        printRow({ field: 'WebPage1', contact }),
        printRow({ field: 'WebPage2', contact }),
      ],
    }),
    !configs.printNotes ? '' : printGroup({
      group: 'Notes',
      contact,
      heading: printHeading({
        group: 'Notes',
        caption: browser.i18n.getMessage('contact_field_Notes'),
        contact,
      }),
      fields: [
        printRow({ field: 'Notes', contact }),
      ],
    }),
    '</table></section>',
  ].join('');
}

function printGroup({ group, contact, heading, fields }) {
  const rows = fields.join('');
  return !rows ? '' : `
    <tbody id=${JSON.stringify(sanitizeForHTMLText('contact:' + contact.id + ':' + group + 'Group'))}>
      ${heading}
      ${rows}
    </tbody>
  `.trim();
}

function printHeading({ group, caption, contact }) {
  return !configs.printHeadings ? '' : `
    <tr id=${JSON.stringify(sanitizeForHTMLText('contact:' + contact.id + ':' + group + 'Heading'))}
       ><th colspan="2"
           >${sanitizeForHTMLText(caption)}</th>
    </tr>
  `.trim();
}

function nameFieldRow({ field, contact }) {
  const value         = contact[field];
  const phoneticValue = contact['Phonetic' + field];
  const contents = value && phoneticValue ?
    `
      <ruby>
        <rb>${sanitizeForHTMLText(value)}</rb>
        <rt>${sanitizeForHTMLText(phoneticValue)}</rt>
      </ruby>
    `.trim() :
    sanitizeForHTMLText(value || phoneticValue);
  return !value ? '' : `
    <tr id=${JSON.stringify(sanitizeForHTMLText('contact:' + contact.id + ':' + field))}>
      <th>${sanitizeForHTMLText(browser.i18n.getMessage('contact_field_' + field))}</th>
      <td>${contents}</td>
    </tr>
  `.trim();
}

function printRow({ field, value, label, contact }) {
  if (value === undefined)
    value = contact[field];
  return !value ? '' : `
    <tr id=${JSON.stringify(sanitizeForHTMLText('contact:' + contact.id + ':' + field))}>
      <th>${!configs.printFieldNames ? '' : sanitizeForHTMLText(label || browser.i18n.getMessage('contact_field_' + field))}</th>
      <td>${sanitizeForHTMLText(value)}</td>
    </tr>
  `.trim();
}

configs.$loaded.then(async () => {
  mParams = await Dialog.getParams();

  mPrintId = mParams.printId;

  let title;

  if (mParams.addressBookId) {
    const [addressBook, contacts] = await Promise.all([
      browser.addressBooks.get(mParams.addressBookId),
      browser.contacts.list(mParams.addressBookId),
    ]);
    title = browser.i18n.getMessage('ui_printContacts_documentTitle_addressBook', [addressBook.name]);
    mContacts.push(...contacts.map(contact => new Contact(contact)));
  }
  else {
    const contacts = await Promise.all(mParams.contactIds && mParams.contactIds.map(id => DataStore.getRawContact(id)) || []);
    mContacts.push(...contacts.map(contact => new Contact(contact)));
    title = browser.i18n.getMessage('ui_printContacts_documentTitle_default', [mContacts.length]);
  }

  browser.runtime.onMessage.addListener(onMessage);
  window.addEventListener('unload', () => {
    browser.runtime.onMessage.removeListener(onMessage);
  }, { once: true });

  for (const checkbox of document.querySelectorAll('input[type="checkbox"]')) {
    checkbox.checked = configs[checkbox.id];
    checkbox.addEventListener('change', () => {
      configs[checkbox.id] = checkbox.checked;
      rebuildPrintableContents();
    });
  }

  rebuildPrintableContents();

  configs.$addObserver(onConfigChange);
  onConfigChange('debug');

  Dialog.initButton(mPrintButton, async _event => {
    document.title = title;
    print();
    document.title = browser.i18n.getMessage('ui_printPreview_title');
  });
  Dialog.initCancelButton(mCloseButton);

  await Dialog.notifyReady();

  /*
  window.addEventListener('resize', () => {
    configs.mergeContactsDialogWidth = window.outerWidth;
    configs.mergeContactsDialogHeight = window.outerHeight;
  });
  window.addEventListener(Dialog.TYPE_MOVED, event => {
    configs.mergeContactsDialogLeft = event.detail.left;
    configs.mergeContactsDialogTop = event.detail.top;
  });
  */

  //window.addEventListener(ResizableBox.TYPE_RESIZED, event => {
  //  configs.mergeContactsDialogBoxSizes = event.detail;
  //});
  log('print preview initialize done');
});
