/*
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
*/
'use strict';

import RichConfirm from '/extlib/RichConfirm.js';

import {
  clone,
  configs,
  sanitizeForHTMLText,
} from '/common/common.js';
import * as Constants from '/common/constants.js';
import * as CSV from '/common/import/csv.js';
import * as FileDecryption from '/common/file-decryption.js';
import * as VCard from '/common/import/vcard.js';


class AddressBookFinder {
  constructor(addressBooks) {
    this.$addressBooksById = new Map();
    this.$addressBooksByName = new Map();
    for (const addressBook of addressBooks) {
      this.add(addressBook);
    }
  }

  add(addressBook) {
    this.$addressBooksById.set(addressBook.id, addressBook);
    this.$addressBooksByName.set(addressBook.name, addressBook);
  }

  byId(id) {
    return this.$addressBooksById.get(id) || null;
  }

  byName(name) {
    return this.$addressBooksByName.get(name) || null;
  }

  sanitizeName(baseName) {
    if (!this.$addressBooksByName.has(baseName))
      return baseName;

    let safeName = baseName;
    let count = 1;
    while (this.$addressBooksByName.has(safeName)) {
      safeName = `${baseName}(${count++})`;
    }
    return safeName;
  }
}


const mFileField = document.createElement('input');
mFileField.setAttribute('type', 'file');
mFileField.setAttribute('accept', `${VCard.MIME_TYPE},${CSV.MIME_TYPE},.${CSV.SUFFIX},.encrypted,.*`);
mFileField.setAttribute('style', 'display: none;');
document.body.appendChild(mFileField);

mFileField.addEventListener('change', async _event => {
  const chosenFile = mFileField.files.item(0);
  const { file, contents } = await readFile(chosenFile);
  if (!contents)
    return;

  const baseName = file.name.replace(/\.[^\.]+$/, '');
  const finder = new AddressBookFinder(await browser.addressBooks.list(false));

  let addressBookFromFileName = finder.byName(baseName);
  const addressBookNameFromFileName = finder.sanitizeName(baseName);
  if (addressBookFromFileName) {
    const result = await RichConfirm.show({
      modal: true,
      type: 'common-dialog',
      message: browser.i18n.getMessage('import_confirmToUnifyExistingAddressBook_message'),
      buttons: [
        browser.i18n.getMessage('import_confirmToUnifyExistingAddressBook_accept'),
        browser.i18n.getMessage('import_confirmToUnifyExistingAddressBook_cancel'),
      ],
    });
    if (result.buttonIndex != 0)
      addressBookFromFileName = null;
  }

  let contacts;
  if (isVCard(file)) {
    contacts = await VCard.parse(contents, {
      forceCreate: !addressBookFromFileName,
      addressBookIdField: configs.importFieldForAddressBookId,
    });
  }
  else if (isCSV(file)) {
    contacts = await CSV.parse(contents, {
      forceCreate: !addressBookFromFileName,
      addressBookIdField: configs.importFieldForAddressBookId,
    });
  }
  if (!contacts)
    return;

  const contactsForAddressBook = new Map();
  for (const contact of contacts) {
    const id   = configs.importFieldForAddressBookIdToIdMapping[contact.addressBookId] || contact.addressBookId;
    const name = configs.importFieldForAddressBookIdToNameMapping[contact.addressBookId] || contact.addressBookId;
    const addressBook = (
      finder.byId(contact.addressBookId) || finder.byId(id) ||
      finder.byName(contact.addressBookId) || finder.byName(name) ||
      null
    );
    const importInfo = contactsForAddressBook.get(addressBook || name) || {
      addressBook,
      addressBookName: name,
      contacts:        [],
    };
    importInfo.contacts.push(contact.contact);
    contactsForAddressBook.set(addressBook || name, importInfo);
  }
  const addressBookIdsOrNamesToBeCleared = new Set(configs.addressBooksToBeClearedBeforeImport);
  for (const importInfo of contactsForAddressBook.values()) {
    if (importInfo.contacts.length == 0)
      continue;
    const destinationAddressBook = (importInfo.addressBookName ? finder.byName(importInfo.addressBookName) : null) || addressBookFromFileName;
    const addressBookName = (importInfo.addressBookName ? importInfo.addressBookName : null) || addressBookNameFromFileName;
    const replaceAll = !!(
      destinationAddressBook &&
      (addressBookIdsOrNamesToBeCleared.has(destinationAddressBook.id) ||
       addressBookIdsOrNamesToBeCleared.has(destinationAddressBook.name))
    );
    const { addressBook } = await browser.runtime.sendMessage({
      type:          Constants.COMMAND_IMPORT_CONTACTS,
      contacts:      importInfo.contacts,
      addressBookId: destinationAddressBook && destinationAddressBook.id,
      addressBookName,
      replaceAll,
    });
    finder.add(addressBook);
  }
});

async function readFile(file) {
  const nullResult = { file, contents: null };
  if (file.name.toLowerCase().endsWith('.encrypted')) {
    let retrying = false;
    let decrypted;
    while (true) {
      const result = await RichConfirm.show({
        modal: true,
        type: 'dialog',
        content: [
          `<p style="margin-bottom: 0.5em;">${sanitizeForHTMLText(browser.i18n.getMessage(retrying ? 'import_propmptPassword_retryMessage' : 'import_propmptPassword_message'))}</p>`,
          `<p style="display: flex; flex-directon: column; align-items: stretch;"
             ><input type="password"
                     name="password"
                     style="width: 100%;"></p>`,
        ].join(''),
        buttons: [
          browser.i18n.getMessage('import_propmptPassword_accept'),
          browser.i18n.getMessage('import_propmptPassword_cancel'),
        ],
      });
      if (result.buttonIndex != 0)
        return nullResult;

      const password = result.values.password;
      try {
        decrypted = await FileDecryption.decryptFileContents(file, { password });
        break;
      }
      catch(_error) {
        retrying = true;
      }
    }
    const name = file.name.replace(/\.encrypted$/i, '');

    const suffix = file.name.toLowerCase().split('.').pop();
    const type = suffix == CSV.SUFFIX || suffix == CSV.SUFFIX_TAB ?
      CSV.MIME_TYPE :
      suffix == VCard.SUFFIX ?
        VCard.MIME_TYPE :
        'text/plain';

    let contents;
    if (configs.importEncryptedSourceStaticTextEncoding) {
      contents = (new TextDecoder(configs.importEncryptedSourceStaticTextEncoding)).decode(decrypted);
    }
    else {
      chooseEncoding:
      while (true) {
        const encoding = await chooseEncoding('text/x-encrypted');
        if (!encoding)
          return nullResult;
        contents = (new TextDecoder(encoding)).decode(decrypted);
        switch (await previewContents(contents)) {
          case 0: break chooseEncoding; // OK
          case 1: continue; // Choose Again
          default: return nullResult; // Cancel
        }
      }
    }
    return {
      file: { name, type },
      contents,
    };
  }
  else if (isVCard(file)) {
    return { file, contents: await file.text() };
  }
  else if (isCSV(file)) {
    let contents;
    chooseEncoding:
    while (true) {
      const encoding = await chooseEncoding(file.type);
      if (!encoding)
        return nullResult;
      contents = await readFileAsEncodedIn(file, encoding);
      switch (await previewContents(contents)) {
        case 0: break chooseEncoding; // OK
        case 1: continue; // Choose Again
        default: return nullResult; // Cancel
      }
    }
    return { file, contents };
  }

  return nullResult;
}

async function previewContents(contents) {
  const result = await RichConfirm.show({
    modal: true,
    type: 'dialog',
    content: [
      `<p style="margin-bottom: 0.5em;">${sanitizeForHTMLText(browser.i18n.getMessage('import_confirmEncoding_message'))}</p>`,
      `<textarea name="preview"
                 cols="${Math.min(contents.split('\n').map(line => line.length).sort().reverse()[0], 100)}"
                 rows="10"
                 readonly></textarea>`,
    ].join(''),
    onShown(container, { contents }) {
      container.querySelector('[name="preview"]').value = contents;
    },
    inject: {
      contents,
    },
    buttons: [
      browser.i18n.getMessage('import_confirmEncoding_accept'),
      browser.i18n.getMessage('import_confirmEncoding_rechoose'),
      browser.i18n.getMessage('import_confirmEncoding_cancel'),
    ],
  });
  return result.buttonIndex;
}

async function readFileAsEncodedIn(file, encoding) {
  return new Promise((resolve, reject) => {
    const reader = new FileReader();
    reader.onerror = reject;
    reader.onload = event => {
      resolve(event.target.result);
    };
    reader.readAsText(file, encoding);
  });
}

export function importFromFile() {
  mFileField.click();
}

function isVCard(file) {
  return (
    file.type == VCard.MIME_TYPE ||
    file.name.toLowerCase().endsWith(`.${VCard.SUFFIX}`)
  );
}

function isCSV(file) {
  return (
    file.type == CSV.MIME_TYPE ||
    file.name.toLowerCase().endsWith(`.${CSV.SUFFIX}`) ||
    file.name.toLowerCase().endsWith(`.${CSV.SUFFIX_TAB}`)
  );
}

async function chooseEncoding(type) {
  const selector = await createEncodingsSelector();
  const result = await RichConfirm.show({
    modal: true,
    type: 'dialog',
    content: [
      `<p style="margin-bottom: 0.5em;">${sanitizeForHTMLText(browser.i18n.getMessage('import_chooseEncoding_message'))}</p>`,
      `<p style="display: flex; flex-directon: column; align-items: stretch;">${selector}</p>`,
    ].join(''),
    onShown(container, { selected }) {
      if (selected)
        container.querySelector('[name="encoding"]').value = selected;
    },
    inject: {
      selected: configs.lastImportEncodingForType[type] || 'UTF-8',
    },
    buttons: [
      browser.i18n.getMessage('import_chooseEncoding_accept'),
      browser.i18n.getMessage('import_chooseEncoding_cancel'),
    ],
  });
  if (result.buttonIndex != 0)
    return null;
  if (result.values.encoding) {
    const values = clone(configs.lastImportEncodingForType);
    values[type] = result.values.encoding;
    configs.lastImportEncodingForType = values;
  }
  return result.values.encoding || 'UTF-8';
}

async function createEncodingsSelector() {
  if (createEncodingsSelector.cachedForm)
    return createEncodingsSelector.cachedForm;

  const request = new XMLHttpRequest();
  await new Promise((resolve, reject) => {
    request.open('GET', browser.runtime.getURL('extlib/encodings.json'), true);
    request.overrideMimeType('text/plain');
    request.addEventListener('load', resolve, { once: true });
    request.addEventListener('error', reject, { once: true });
    request.send(null);
  });
  const categories = JSON.parse(request.responseText);
  const selector = [
    `<select name="encoding"
             size="10"
             style="width: 100%">`,
  ];
  for (const category of categories) {
    const key = `import_chooseEncoding_category_${category.heading.replace(/[^a-z0-9_]+/gi, '_')}`;
    const caption = browser.i18n.getMessage(key);
    selector.push(`<optgroup label=${JSON.stringify(sanitizeForHTMLText(caption == key ? category.heading : caption))}>`);
    for (const encoding of category.encodings) {
      const safeValue = sanitizeForHTMLText(encoding.name);
      selector.push(`<option value=${JSON.stringify(safeValue)}>${safeValue}</option>`);
    }
    selector.push('</optgroup>');
  }
  return createEncodingsSelector.cachedForm = selector.join('');
}
createEncodingsSelector.cachedForm = null;
