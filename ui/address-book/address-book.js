/*
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
*/
'use strict';

import * as Dialog from '/extlib/dialog.js';
import '/extlib/l10n.js';
import { tabbis } from '/extlib/tabbis.es6.min.js';

import {
  configs,
  log,
} from '/common/common.js';
import { AddressBook } from '/common/AddressBook.js';
import '/ui/common.js';
import * as Constants from '/common/constants.js';
import { Contact } from '/common/Contact.js';

let mParams;
let mAddressBook;

const mAcceptButton = document.querySelector('#accept');
const mCancelButton = document.querySelector('#cancel');

function onConfigChange(key) {
  const value = configs[key];
  switch (key) {
    case 'debug':
      document.documentElement.classList.toggle('debug', value);
      break;
  }
}

function onMessage(message, _sender) {
  switch (message && message.type) {
    case Constants.COMMAND_FOCUS_TO_ADDRESS_BOOK_DIALOG:
      if (mAddressBook && message.addressBookId == mAddressBook.id) {
        window.focus();
        return Promise.resolve(true);
      }
      break;
  }
}

function onChangeSyncType() {
  const syncType = document.querySelector('#syncType').value;
  document.documentElement.dataset.syncType = syncType;
  Dialog.sizeToContent();
}

function fillFields() {
  for (const field of document.querySelectorAll('[data-address-book-field]')) {
    const value = mAddressBook[field.dataset.addressBookField];
    switch (field.localName) {
      case 'input':
        if (field.type == 'checkbox') {
          field.checked = !!value;
          break;
        }
      case 'select':
        if (value instanceof Promise)
          value.then(resolved => field.value = resolved);
        else
          field.value = value;
        break;
    }
    field.disabled = mAddressBook.frozen;
  }
}

async function saveChanges() {
  if (!mAddressBook)
    return;

  for (const field of document.querySelectorAll('[data-address-book-field]')) {
    if (!field.classList.contains('dirty'))
      continue;

    const name = field.dataset.addressBookField;
    const oldValue = mAddressBook[name];
    const newValue = field.type == 'checkbox' ? field.checked : field.value;
    if (newValue == oldValue)
      continue;

    log('set value: ', name, { oldValue, newValue });
    mAddressBook[name] = newValue;
  }

  await mAddressBook.save();
}

configs.$loaded.then(async () => {
  mParams = await Dialog.getParams();
  log('address book dialog initialize ', mParams);

  const addressBook = mParams.addressBookId && await browser.addressBooks.get(mParams.addressBookId);
  mAddressBook = new AddressBook(addressBook || {
    id:   null,
    name: '',
  });
  document.title = browser.i18n.getMessage('ui_addressBook_title', [addressBook ? mAddressBook.name : browser.i18n.getMessage('ui_new_addressBook_title')]);

  browser.runtime.onMessage.addListener(onMessage);
  window.addEventListener('unload', () => {
    browser.runtime.onMessage.removeListener(onMessage);
  }, { once: true });

  await mAddressBook.initialized;
  if (!mAddressBook.id)
    mAddressBook.setRandomColor();
  if (mAddressBook.frozen)
    document.querySelector('#readOnly').checked = true;
  fillFields();
  onChangeSyncType();

  document.addEventListener('change', event => {
    const field = event.target;
    field.classList.add('dirty');

    if (field.id == 'syncType')
      onChangeSyncType();
  });

  configs.$addObserver(onConfigChange);
  onConfigChange('debug');

  tabbis();

  Dialog.initButton(document.querySelector('#syncNow'), async _event => {
    mAddressBook.sync();
  });

  Dialog.initButton(document.querySelector('#syncNowForce'), async _event => {
    mAddressBook.sync({ force: true });
  });

  Dialog.initButton(mAcceptButton, async _event => {
    if (!mAddressBook.frozen)
      await saveChanges();
    Dialog.accept();
  });
  if (mAddressBook.frozen) {
    mAcceptButton.textContent = browser.i18n.getMessage('ui_addressBook_closeButton_label');
    mCancelButton.classList.add('hidden');
  }
  else {
    mAcceptButton.textContent = browser.i18n.getMessage('ui_addressBook_saveButton_label');
    mCancelButton.classList.remove('hidden');
    Dialog.initCancelButton(mCancelButton);
  }

  document.querySelector('#customDisplayNameFormatPlaceholders').innerText = [
    ...Contact.KNOWN_FIELDS,
    ...Contact.KNOWN_VIRTUAL_FIELDS,
    ...Contact.KNOWN_EXTRA_FIELDS,
  ].map(field => `{{${field}}}`).join('\n');

  await Dialog.notifyReady();
  Dialog.sizeToContent();

  window.addEventListener('resize', () => {
    configs.addressBookDialogWidth = window.outerWidth;
    //configs.addressBookDialogHeight = window.outerHeight;
  });
  window.addEventListener(Dialog.TYPE_MOVED, event => {
    configs.addressBookDialogLeft = event.detail.left;
    configs.addressBookDialogTop = event.detail.top;
  });

  //window.addEventListener(ResizableBox.TYPE_RESIZED, event => {
  //  configs.contactDialogBoxSizes = event.detail;
  //});
  log('address book dialog initialize done');
});
