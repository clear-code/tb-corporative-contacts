/*
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
*/
'use strict';

import {
  configs,
  log,
} from '/common/common.js';
import * as Constants from '/common/constants.js';

const BUILTIN_PROVIDERS = Constants.BUILTIN_CHAT_PROVIDER_FIELDS.map(field => ({ field, label: browser.i18n.getMessage(`contact_field_${field}`) }));

export class ChatProvider {
  constructor(definition, { builtIn } = {}) {
    this.$definition = definition;
    this.builtIn = builtIn || false;

    ChatProvider.instances.set(this.id, this)
  }

  destroy() {
    ChatProvider.instances.delete(this.id);
  }

  get id() {
    return this.$definition.field;
  }

  get fieldPath() {
    if (this.builtIn)
      return this.$definition.field;
    else
      return `extra.${this.$definition.field.toLowerCase()}`;
  }

  get label() {
    return this.$definition.label || this.builtIn && browser.i18n.getMessage(`contact_field_${this.id}`) || this.id;
  }

  get linkMatcher() {
    return this.$definition.linkMatcher;
  }

  get linkPrefix() {
    return this.$definition.linkPrefix;
  }

  get contactsSeparator() {
    return 'contactsSeparator' in this.$definition ? this.$definition.contactsSeparator : ',';
  }

  open(items) {
    const url = this.generateLinkFor(items);
    log('chat url: ', url);
    if (!url || Constants.FORBIDDEN_URL_MATCHER.test(url))
      throw new Error(`unabel to open: ${url}`);

    if (Constants.BROWSER_URL_MATCHER.test(url)) {
      log('=> open in tab');
      browser.tabs.create({ active: true, url });
    }
    else {
      log('=> load');
      const link = document.createElement('a');
      link.href = url;
      link.style.position = 'fixed';
      link.style.display = 'none';
      document.body.appendChild(link);
      link.click();
      setTimeout(() => {
        link.parentNode.removeChild(link);
      }, 0);
    }
  }

  generateLinkFor(items) {
    if (!this.linkPrefix)
      return '';

    if (!Array.isArray(items))
      items = [items];

    const ids = new Set(items.map(item => {
      switch (item.type) {
        case 'contact':
          return item.getFieldValue(this.fieldPath);
        case 'mailingList':
          return item.contacts.map(contact => contact.getFieldValue(this.fieldPath));
        default:
          throw new Error('unknown type item');
      }
    }).flat().filter(id => !!id));
    if (ids.size == 0)
      return '';

    return `${this.linkPrefix}${Array.from(ids).join(this.contactsSeparator)}`;
  }

  hasAccount(contact) {
    return !!contact.getFieldValue(this.fieldPath);
  }
}

ChatProvider.instances = new Map();

ChatProvider.get = id => {
  return ChatProvider.instances.get(id);
};

ChatProvider.getAll = () => {
  return Array.from(ChatProvider.instances.values());
};

ChatProvider.BUILTIN_PROVIDERS = BUILTIN_PROVIDERS;

for (const provider of BUILTIN_PROVIDERS) {
  new ChatProvider(provider, { builtIn: true });
}

function onConfigChanged(key) {
  switch (key) {
    case 'customChatProviders':
      for (const provider of ChatProvider.getAll()) {
        if (!provider.builtIn)
          provider.destroy();
      }
      for (const provider of configs.customChatProviders) {
        new ChatProvider(provider);
      }
      break;
  }
}
configs.$addObserver(onConfigChanged);
configs.$loaded.then(() => {
  onConfigChanged('customChatProviders');
});
