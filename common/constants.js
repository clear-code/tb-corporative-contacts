/*
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
*/
'use strict';

export const COMMAND_FOCUS_TO_ITEMS_DIALOG = 'focus-to-items-dialog';
export const COMMAND_FOCUS_TO_ADDRESS_BOOK_DIALOG = 'focus-to-addressBook-dialog';
export const COMMAND_FOCUS_TO_CONTACT_DIALOG = 'focus-to-contact-dialog';
export const COMMAND_FOCUS_TO_MAILING_LIST_DIALOG = 'focus-to-mailing-list-dialog';
export const COMMAND_FOCUS_TO_MERGE_CONTACTS_DIALOG = 'focus-to-merge-contacts-dialog';
export const COMMAND_FOCUS_TO_PRINT_PREVIEW = 'focus-to-print-preview';
export const COMMAND_SYNC_ADDRESS_BOOK = 'sync-address-book';
export const COMMAND_TRY_CONNECT_ADDRESS_BOOK = 'try-connect-address-book';

export const COMMAND_REQUEST_CONNECT_PREFIX = 'request-connect:';
export const COMMAND_PING_TO_CLIENT = 'ping-to-client';
export const COMMAND_HEARTBEAT = 'heartbeat';

export const COMMAND_FETCH_ALL_ADDRESS_BOOKS = 'fetch-all-address-books';
export const COMMAND_PUSH_ALL_ADDRESS_BOOKS = 'push-all-address-books';
export const COMMAND_PUSH_GROUP_UPDATE = 'push-group-update';
export const COMMAND_PUSH_NEW_ADDRESS_BOOK = 'push-new-address-book';
export const COMMAND_PUSH_ADDRESS_BOOK_DELETE = 'push-address-book-delete';
export const COMMAND_PUSH_ALL_CHILDREN_DISAPPEARED = 'push-al-children-disappeared';
export const COMMAND_PUSH_NEW_GROUP_CHILD = 'push-new-group-child';
export const COMMAND_PUSH_GROUP_CHILD_DELETE = 'push-group-child-delete';
export const COMMAND_MOVE_UNCATEGORIZED_TO_END = 'move-uncategorized-to-end';
export const COMMAND_SORT_CHILDREN_BY_INDEX = 'sort-children-by-index';
export const COMMAND_IMPORT_CONTACTS = 'import-contacts';

export const COMMAND_FETCH_RAW_CONTACT = 'fetch-raw-contact'; // Workaround for https://gitlab.com/clear-code/tb-corporative-contacts/-/issues/49
export const COMMAND_FETCH_ALL_CONTACTS = 'fetch-all-contacts';
export const COMMAND_PUSH_ALL_CONTACTS = 'push-all-contacts';
export const COMMAND_FETCH_ALL_MAILING_LISTS = 'fetch-all-mailing-lists';
export const COMMAND_PUSH_ALL_MAILING_LISTS = 'push-all-mailing-lists';
export const COMMAND_FETCH_ITEMS_OF = 'fetch-items-of';
export const COMMAND_PUSH_ITEMS_OF = 'push-items-of';
export const COMMAND_PUSH_NEW_ITEM = 'push-new-item';
export const COMMAND_PUSH_ITEM_UPDATE = 'push-item-update';
export const COMMAND_MOVE_ITEM_TO = 'move-item-to';
export const COMMAND_PUSH_MOVED_ITEM = 'push-moved-item';
export const COMMAND_COPY_ITEM_TO = 'copy-item-to';
export const COMMAND_PUSH_COPIED_ITEM = 'push-copied-item';
export const COMMAND_PUSH_ITEM_DELETE = 'push-item-delete';
export const COMMAND_PUSH_ITEM_MOVE = 'push-item-move';
export const COMMAND_PUSH_MAILING_LIST_CONTACT_ADD = 'push-mailing-list-contact-add';
export const COMMAND_PUSH_MAILING_LIST_CONTACT_REMOVE = 'push-mailing-list-contact-remove';
export const COMMAND_FETCH_ALL_AVAILABLE_CATEGORIES = 'fetch-all-available-categories';
export const COMMAND_PUSH_ALL_AVAILABLE_CATEGORIES = 'push-all-available-categories';
export const COMMAND_PUSH_NEW_AVAILABLE_CATEGORY = 'push-new-available-category';

export const NOTIFY_ADDRESS_BOOK_METADATA_UPDATED = 'address-book-metadata-updated';
export const NOTIFY_ADDRESS_BOOK_READY_TO_SYNC = 'address-book-ready-to-sync';
export const NOTIFY_MAILING_LIST_METADATA_UPDATED = 'mailing-list-metadata-updated';

export const GROUP_TYPE_ORGANIZATION = 'organization';
export const GROUP_TYPE_CATEGORY     = 'category';

export const SYNC_TYPE_LOCAL   = 'local';
export const SYNC_TYPE_CARDDAV = 'cardDAV';

export const AUTH_METHOD_AUTO   = 'auto';
export const AUTH_METHOD_DIGEST = 'digest';
export const AUTH_METHOD_BASIC  = 'basic';

export const CONTACT_EXTRA_FIELDS_STORE = 'Custom1';
export const CONTACT_EXTRA_STATUS_STORE = 'Custom2';

export const UNCATEGORIZED_ID = '<<uncategorized>>';

export const ADDRESS_FORMAT_TOP_DOWN = 'topdown';
export const ADDRESS_FORMAT_BOTTOM_UP = 'bottomup';

export const FORBIDDEN_URL_MATCHER = /^(about|chrome|resource|file):/;
export const BROWSER_URL_MATCHER = /^(https?|ftp):/;

export const BUILTIN_CHAT_PROVIDER_FIELDS = [
  // Google Talk (closed at 2017)
  // => Google Hangouts (closed at 2021)
  // => Google Chat / Google Meets
  '_GoogleTalk',
  // closed at 2017
  '_AimScreenName',
  '_Yahoo',
  '_Skype',
  '_QQ',
  // MSN Messenger (closed at 2013)
  // => Windows Live Messenger (closed at 2013)
  // => Skype
  '_MSN',
  '_ICQ',
  // XMPP
  '_JabberId',
  '_IRC',
];

export const GROUP_FIELDS = [
  'index',
  '_children',
  'id',
  'staticId',
  'type',
  'color',
  'name',
  'status',
  'readOnly',
  'frozen',
  'expanded',
  'hidden',
  'parentId',
  'ownerAddressBookId',
  'isOrganization',
  'isCategory',
  'isUncategorized',
  'editableInUI',
  'deletable',
  'hasOwnIndex',
  'movable',
];

export const ITEM_VISIBLE_FIELDS = [
  'NameField1',
  'NameField2',
  'PhoneticNameField1',
  'PhoneticNameField2',
  'visibleDisplayName',
  'SpouseName',
  'FamilyName',
  'namePrefix',
  'nameSuffix',
  'NickName',
  'gender',
  'PrimaryEmail',
  'SecondEmail',
  'ChatName',
  'PreferMailFormat',
  'WorkPhone',
  'HomePhone',
  'FaxNumber',
  'PagerNumber',
  'CellularNumber',
  'displayHomeAddress',
  'WebPage2',
  'birthDay',
  'anniversaryDay',
  'JobTitle',
  'Department',
  'displayWorkAddress',
  'WebPage1',
  'categories',
  'Other',
  'Custom1',
  'Custom2',
  'Custom3',
  'Custom4',
  'Notes',
  ...BUILTIN_CHAT_PROVIDER_FIELDS,
  'Photo',
];

export const ITEM_FIELDS = [
  ...ITEM_VISIBLE_FIELDS,
  'id',
  'parentId',
  'organizationId',
  'type',
  'color',
  'backgroundColor',
  'textColor',
  'label',
  'email',
  'address',
  'url',
  'searchString',
  'sortString',
  'availableChatProviderIds',
  'canBecomeMember',
  'mergable',
];

