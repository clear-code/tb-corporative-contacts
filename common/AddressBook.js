/*
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
*/
'use strict';

import EventListenerManager from '/extlib/EventListenerManager.js';

import {
  configs,
  doProgressively,
  log,
  orderItems,
} from '/common/common.js';
import * as Constants from '/common/constants.js';
import * as DB from '/common/db.js';
import * as Encryption from '/common/encryption.js';
import { Item, Contact, MailingList, normalizeItem } from '/common/Contact.js';
import * as VCard from '/common/import/vcard.js';

class Group {
  constructor() {
    this.$childById = new Map();
    this.invalidateCache();
    this.initialized = Promise.resolve(true);
  }

  get editableInUI() {
    return false;
  }

  get deletable() {
    return false;
  }

  get hasOwnIndex() {
    return true;
  }

  get movable() {
    return true;
  }

  get printable() {
    return false;
  }

  addChild(child) {
    if (!this._children)
      this._children = [];

    this._children.push(child);
    this.$childById.set(child.id, child);
    this.invalidateCache();
    this.constructor.onChildAdded.dispatch(this, child);
  }

  removeChild(child) {
    if (!this._children)
      return;

    const index = this._children.indexOf(child);
    if (index < 0)
      return;

    this._children.splice(index, 1);
    this.$childById.delete(child.id);
    this.invalidateCache();
    this.constructor.onChildRemoved.dispatch(this, child.id);
    if (this._children.length > 0)
      return;

    delete this._children;
    this.constructor.onAllChildrenDisappeared.dispatch(this);
  }

  sortChildrenByIndex() {
    this._children.sort((a, b) => (a.index || 0) - (b.index || 0));
    this.updateChildrenIndices();
  }

  destroy() {
    const parent = this.parent;
    if (parent)
      parent.removeChild(this);
  }

  invalidateCache() {
    this.invalidateExportedCache();
  }

  invalidateExportedCache() {
    this.$exported = undefined;
    this.$exportedKeys = undefined;
  }

  export(properties) {
    if (!properties)
      properties = Constants.GROUP_FIELDS;

    const exportedKeys = properties.join('\n');
    if (this.$exported && this.$exportedKeys == exportedKeys)
      return this.$exported;

    this.$exportedKeys = exportedKeys;
    const exported = {};
    for (const property of properties) {
      if (property in this)
        exported[property] = this[property];
    }

    if (exported._children)
      exported._children = exported._children.map(child => child.export(properties));

    return this.$exported = exported;
  }
}

const ADDRESS_BOOK_FIELDS = new Set([
  'name',
  'readOnly',
]);

const UPDATABLE_ADDRESS_BOOK_FIELDS = new Set([
  'name',
]);

export class AddressBook extends Group {
  constructor(addressBook) {
    super(addressBook);
    this.$addressBook = addressBook;
    this.$itemById = new Map();
    this.$dirtyProperties = new Set();
    this.$dirtyMetadata = new Set();
    this.$decryptedCardDAVPassword = null;
    this.$cardDAVPasswordEncryptionPromises = [];
    this.items = [];

    for (const key of Object.keys(addressBook)) {
      if (key in this)
        continue;
      Object.defineProperty(this, key, {
        enumerable: true,
        configurable: true,
        get() { return this.$addressBook[key]; },
        set(newValue) {
          if (newValue == this[key])
            return newValue;
          this.$dirtyProperties.add(key);
          this.invalidateCache();
          return this.$addressBook[key] = newValue;
        },
      });
    }

    this.$completelyInitialized = false;
    this.initialized = this.loadMetadata().then(() => {
      if (this.id) {
        try{
          Item.setAddressBookColor(this.id, this.color);
          if (AddressBook.syncEnabled &&
              configs.syncOnStartup &&
              this.syncType != Constants.SYNC_TYPE_LOCAL &&
              this.periodicalSync)
            this.startSync({ start: true });
        }
        catch(error){
          console.error(error);
        }
      }
      this.$completelyInitialized = true;
      return true;
    });

    if (this.id)
      AddressBook.instances.set(this.id, this);
  }

  get editableInUI() {
    return true;
  }

  get deletable() {
    return !this.staticId;
  }

  get hasOwnIndex() {
    return true;
  }

  get movable() {
    return !this.staticId;
  }

  get printable() {
    return true;
  }

  async loadMetadata() {
    this.$metadata = this.id && (await DB.getAddressBookMetadata(this.$addressBook)) || {};
    await this.decryptCardDAVPassword();
    return true;
  }

  async startLoad() {
    const childrenByParent = new Map();
    childrenByParent.set(this.$addressBook, new Set());

    const allSubGroups = [];
    const topLevelSubGroups = [];
    await doProgressively({
      items: [
        ...(this.$addressBook.mailingLists || []),
        ...(this.$addressBook.contacts || []),
      ].map(normalizeItem)
        .sort((a, b) => (a.sortString || '') - (b.sortString || '')),
      task: async items => {
        return Promise.all(items.map(async item => {
          const { topLevel, subLevel } = await this.addItem(item, { local: true });
          allSubGroups.push(...topLevel, ...subLevel);
          if (topLevel.length > 0)
            topLevelSubGroups.push(...topLevel);
        }));
      },
      chunkSize: configs.itemsLoadChunkSize,
      interval:  configs.itemsLoadInterval,
    });

    const subGroups = new Set(allSubGroups);
    for (const group of subGroups) {
      if (!group._children)
        continue;

      group._children = orderItems(group._children, {
        getItemIndex: child => {
          return child.path in this.subGroupIndices ? this.subGroupIndices[child.path] : -1;
        },
      });
    }

    if (topLevelSubGroups.length > 0) {
      const subGroups = Array.from(new Set(topLevelSubGroups)).sort((a, b) => {
        if (a.isOrganization == b.isOrganization)
          return 0; // a.name - b.name;
        if (a.isOrganization)
          return -1;
        return 1;
      });
      this._children = orderItems(subGroups, {
        getItemIndex: child => {
          return (child.path in this.subGroupIndices) ?
            this.subGroupIndices[child.path] :
            -1;
        },
      });
      this.moveUncategorizedToEnd();
    }
  }

  moveUncategorizedToEnd() {
    if (!this._children)
      return;
    const uncategorizedIndex = this._children.findIndex(group => group.isUncategorized);
    if (uncategorizedIndex > -1) {
      const uncategorized = this._children.splice(uncategorizedIndex, 1);
      this._children.push(...uncategorized);
    }
  }

  async addItem(item, { local } = {}) {
    let added = false;
    if (!this.$itemById.has(item.id)) {
      this.items.push(item);
      this.$itemById.set(item.id, item);
      added = true;
    }

    await item.initialized;

    let parent = this;
    const initializedSubGroups = { topLevel: [], subLevel: [] };
    const organizations = item.organizations;
    for (let i = 0, maxi = organizations.length; i < maxi; i++) {
      const path = `organization${VCard.VALUE_SEPARATOR}${organizations.slice(0, i + 1).join(VCard.VALUE_SEPARATOR)}`;
      const id = `${this.id}${VCard.VALUE_SEPARATOR}${path}`;
      const name = organizations[i];
      // eslint-disable-next-line no-use-before-define
      const organization = SubGroup.get(id) || new SubGroup({
        id,
        type: Constants.GROUP_TYPE_ORGANIZATION,
        path,
        name,
        parent,
        addressBook: this,
      });
      if (i == 0)
        initializedSubGroups.topLevel.push(organization);
      else
        initializedSubGroups.subLevel.push(organization);
      if (!parent._children || !parent.$childById.has(organization.id))
        parent.addChild(organization);
      if (configs.showContactForMiddleLevelOrganizations || i == maxi - 1)
        organization.addItem(item);
      parent = organization;
    }

    for (const category of item.internalCategories) {
      const path = `category${VCard.VALUE_SEPARATOR}${category}`;
      const id = `${this.id}${VCard.VALUE_SEPARATOR}${path}`;
      const name = category;
      // eslint-disable-next-line no-use-before-define
      const categoryGroup = SubGroup.get(id) || new SubGroup({
        id,
        type: Constants.GROUP_TYPE_CATEGORY,
        path,
        name,
        parent: this,
        addressBook: this,
      });
      initializedSubGroups.topLevel.push(categoryGroup);
      if (!this._children || !this.$childById.has(categoryGroup.id))
        this.addChild(categoryGroup);
      categoryGroup.addItem(item);
    }

    this.moveUncategorizedToEnd();

    if (!this.$completelyInitialized)
      local = true;
    if (added)
      AddressBook.onItemAdded.dispatch(this, item, { local });
    else
      AddressBook.onItemUpdated.dispatch(this, item, { local });

    return initializedSubGroups;
  }

  async updateItem(item) {
    if (!this.$itemById.has(item.id))
      return;

    await this.addItem(item);

    if (item.type != 'contact')
      return;

    // eslint-disable-next-line no-use-before-define
    const groups = SubGroup.getAllFromAddressBook(this.id).sort((a, b) => b.id.length - a.id.length);
    for (const group of groups) {
      group.cleanUpUnownedContact(item);
    }
  }

  async deleteAllItems() {
    await this.initialized;
    const items = await browser.contacts.list(this.id);
    await Promise.all(items.map(item => {
      if (item.type == 'contact')
        return browser.contacts.delete(item.id);
      else
        return browser.mailingLists.delete(item.id);
    }));
  }

  async apply(addressBook) {
    if (addressBook.id && this.$addressBook.id && addressBook.id != this.$addressBook.id)
      throw new Error('cannot apply address book information of different address book');

    await this.initialized;

    const updated = {};
    for (const field of ADDRESS_BOOK_FIELDS) {
      const value = addressBook[field];
      if (value != this[field]) {
        this[field] = value;
        updated[field] = value;
        this.$dirtyProperties.delete(field);
      }
    }
    return updated;
  }

  async applyMetadata(updates) {
    await this.initialized;
    const updated = {};
    for (const [field, value] of Object.entries(updates)) {
      if (value != this[field]) {
        if (field != 'cardDAVPassword')
          this[field] = value;
        updated[field] = value;
        this.$dirtyMetadata.delete(field);
      }
    }
    if ('cardDAVPassword' in updated)
      await this.decryptCardDAVPassword();
    if ('readOnly' in updated || 'readOnlyLocally' in updated || 'synchronizing' in updated)
      updated.status = this.status;
    return updated;
  }

  async decryptCardDAVPassword() {
    if (!this.$metadata.cardDAVPassword)
      return;
    this.$decryptedCardDAVPassword = await Encryption.decrypt(this.$metadata.cardDAVPassword).catch(error => {
      console.log('failed to decrypt password data: ', this.$metadata.cardDAVPassword, error);
      return '';
    });
  }

  get raw() {
    return this.$addressBook;
  }

  get index() {
    if ('$index' in this)
      return this.$index;

    this.$index = 'index' in this.$metadata ? this.$metadata.index : -1;
    if (this.$index < 0) {
      this.$index = configs.addressBooks_defaultOrder.indexOf(this.$addressBook.id);
      if (this.$index < 0)
        this.$index = configs.addressBooks_defaultOrder.indexOf(this.$addressBook.name);
      if (this.$index > -1)
        this.$index = 1;
      const offset = Object.keys(configs.staticAddressBooks).length;
      this.$index += offset;
    }
    return this.$index;
  }

  set index(newValue) {
    if (newValue == this.$index)
      return newValue;

    DB.getAddressBookMetadata(this.$addressBook).then(async metadata => {
      try {
        if (!metadata)
          metadata = {};
        metadata.index = newValue;
        await DB.setAddressBookMetadata(this.$addressBook, {
          ...(this.$metadata || {}), // it can be changed while updating...
          ...metadata,
        });
      }
      catch(error) {
        log('failed to save updated index: ', error);
      }
    });
    return this.$index = this.setMetadata('index', newValue);
  }

  getMetadata(field) {
    return this.$metadata && this.$metadata[field];
  }

  setMetadata(field, newValue) {
    if (newValue == this.getMetadata(field))
      return newValue;
    if (!this.$completelyInitialized)
      throw new Error('not initialized yet');
    this.$dirtyMetadata.add(field);
    this.invalidateCache();
    return this.$metadata[field] = newValue;
  }

  get staticId() {
    const id = this.getMetadata('staticId');
    return (id && (id in configs.staticAddressBooks)) ? id : null;
  }

  set staticId(newValue) {
    if (!(newValue in configs.staticAddressBooks))
      return newValue;
    return this.setMetadata('staticId', newValue);
  }

  get status() {
    if (this.synchronizing)
      return 'synchronizing';
    if (this.readOnly)
      return 'readonly';
    return '';
  }

  set status(newValue) {
    return newValue;
  }

  get color() {
    return this.getMetadata('color') || '';
  }

  set color(newValue) {
    if (this.id)
      Item.setAddressBookColor(this.id, newValue);
    return this.setMetadata('color', newValue);
  }

  get frozen() {
    if (this.staticId)
      return true;
    return this.$addressBook.readOnly || false;
  }

  get readOnly() {
    return this.readOnlyLocally || this.frozen;
  }

  set readOnly(newValue) {
    this.readOnlyLocally = newValue;
    return this.readOnly;
  }

  get readOnlyLocally() {
    return this.getMetadata('readOnlyLocally') || this.frozen;
  }

  set readOnlyLocally(newValue) {
    if (newValue == this.readOnlyLocally)
      return newValue;
    return this.setMetadata('readOnlyLocally', newValue);
  }

  /* for fields encryption support
  get encrypted() {
    return this.getMetadata('encrypted');
  }

  set encrypted(newValue) {
    return this.setMetadata('encrypted', newValue);
  }
  */

  get customDisplayNameFormat() {
    return this.getMetadata('customDisplayNameFormat') || configs.defaultCustomDisplayNameFormat;
  }

  set customDisplayNameFormat(newValue) {
    return this.setMetadata('customDisplayNameFormat', newValue);
  }

  get hidden() {
    return this.getMetadata('hidden');
  }

  set hidden(newValue) {
    return this.setMetadata('hidden', newValue);
  }

  get syncType() {
    return this.getMetadata('syncType') || Constants.SYNC_TYPE_LOCAL;
  }

  set syncType(newValue) {
    return this.setMetadata('syncType', newValue);
  }

  get isLocal() {
    return this.syncType == Constants.SYNC_TYPE_LOCAL;
  }

  get isRemote() {
    return this.syncType == Constants.SYNC_TYPE_CARDDAV;
  }

  get periodicalSync() {
    const enabled = this.getMetadata('periodicalSync');
    if (typeof enabled == 'boolean')
      return enabled;
    return configs.defaultPeriodicalSync;
  }

  set periodicalSync(newValue) {
    this.setMetadata('periodicalSync', newValue);

    if (newValue && AddressBook.syncEnabled)
      this.startSync();
    else
      this.stopSync();

    return newValue;
  }

  get periodicalSyncIntervalMilliSeconds() {
    return this.periodicalSyncIntervalMinutes * 60 * 1000;
  }

  get periodicalSyncIntervalMinutes() {
    const interval = this.getMetadata('periodicalSyncIntervalMinutes');
    if (typeof interval == 'number')
      return interval;
    return configs.defaultPeriodicalSyncIntervalMinutes;
  }

  set periodicalSyncIntervalMinutes(newValue) {
    this.setMetadata('periodicalSyncIntervalMinutes', newValue);

    if (this.periodicalSync && AddressBook.syncEnabled)
      this.startSync();
    else
      this.stopSync();

    return newValue;
  }

  get lastSyncTime() {
    return this.getMetadata('lastSyncTime') || 0;
  }

  set lastSyncTime(newValue) {
    return this.setMetadata('lastSyncTime', newValue);
  }

  get synchronizing() {
    return this.getMetadata('synchronizing');
  }

  set synchronizing(newValue) {
    return this.setMetadata('synchronizing', newValue);
  }

  get cardDAVURL() {
    return this.getMetadata('cardDAVURL') || '';
  }

  set cardDAVURL(newValue) {
    return this.setMetadata('cardDAVURL', newValue);
  }

  get cardDAVUserName() {
    const parentId = this.inheritCardDAVAuthInfoFrom;
    if (parentId) {
      const parent = AddressBook.get(parentId) || AddressBook.getAll().find(addressBook => addressBook.staticId == parentId);
      if (parent)
        return parent.cardDAVUserName;
    }
    return this.getMetadata('cardDAVUserName') || '';
  }

  set cardDAVUserName(newValue) {
    return this.setMetadata('cardDAVUserName', newValue);
  }

  get cardDAVPassword() {
    const parentId = this.inheritCardDAVAuthInfoFrom;
    if (parentId) {
      const parent = AddressBook.get(parentId) || AddressBook.getAll().find(addressBook => addressBook.staticId == parentId);
      if (parent)
        return parent.cardDAVPassword;
    }
    return this.$decryptedCardDAVPassword || '';
  }

  set cardDAVPassword(newValue) {
    if (newValue == this.cardDAVPassword)
      return newValue;
    this.$dirtyMetadata.add('cardDAVPassword');
    this.invalidateCache();
    const promisedEncrypted = Encryption.encrypt(newValue).then(encrypted => {
      this.$metadata.cardDAVPassword = encrypted;
      const index = this.$cardDAVPasswordEncryptionPromises.indexOf(promisedEncrypted);
      if (index > -1)
        this.$cardDAVPasswordEncryptionPromises.splice(index, 1);
    });
    this.$cardDAVPasswordEncryptionPromises.push(promisedEncrypted);
    return this.$decryptedCardDAVPassword = newValue;
  }

  get cardDAVAuthMethod() {
    const parentId = this.inheritCardDAVAuthInfoFrom;
    if (parentId) {
      const parent = AddressBook.get(parentId) || AddressBook.getAll().find(addressBook => addressBook.staticId == parentId);
      if (parent)
        return parent.cardDAVAuthMethod;
    }
    return this.getMetadata('cardDAVAuthMethod') || configs.defaultCardDAVAuthMethod;
  }

  set cardDAVAuthMethod(newValue) {
    return this.setMetadata('cardDAVAuthMethod', newValue);
  }

  get cardDAVSyncToken() {
    return this.getMetadata('cardDAVSyncToken');
  }

  set cardDAVSyncToken(newValue) {
    return this.setMetadata('cardDAVSyncToken', newValue);
  }

  get inheritCardDAVAuthInfoFrom() {
    return this.getMetadata('inheritCardDAVAuthInfoFrom');
  }

  set inheritCardDAVAuthInfoFrom(newValue) {
    return this.setMetadata('inheritCardDAVAuthInfoFrom', newValue);
  }

  get expanded() {
    if ('$expanded' in this)
      return this.$expanded;

    return this.$expanded = this.$metadata && 'expanded' in this.$metadata ?
      this.$metadata.expanded :
      this.$addressBook.id in configs.addressBooks_defaultExpanded ?
        configs.addressBooks_defaultExpanded[this.$addressBook.id] :
        this.$addressBook.name in configs.addressBooks_defaultExpanded ?
          configs.addressBooks_defaultExpanded[this.$addressBook.name] :
          false;
  }

  set expanded(newValue) {
    if (newValue == this.$expanded)
      return newValue;

    this.$expanded = !!newValue;
    DB.getAddressBookMetadata(this.$addressBook).then(async metadata => {
      if (!metadata)
        metadata = {};
      metadata.expanded = this.$expanded;
      await DB.setAddressBookMetadata(this.$addressBook, metadata);
    });
  }

  get subGroupsExpandedState() {
    if ('$subGroupsExpandedState' in this)
      return this.$subGroupsExpandedState;

    return this.$subGroupsExpandedState = this.$metadata.subGroupsExpandedState ||
      configs.subGroups_defaultExpanded[this.$addressBook.id] ||
      configs.subGroups_defaultExpanded[this.$addressBook.name] ||
      {};
  }

  set subGroupsExpandedState(newValue) {
    this.$subGroupsExpandedState = newValue;
    DB.getAddressBookMetadata(this.$addressBook).then(async metadata => {
      if (!metadata)
        metadata = {};
      const state = {
        ...(metadata.subGroupsExpandedState || {}),
        ...newValue,
      };
      this.$subGroupsExpandedState = metadata.subGroupsExpandedState = state;
      await DB.setAddressBookMetadata(this.$addressBook, metadata);
    });
    return newValue;
  }

  get subGroupIndices() {
    if ('$subGroupIndices' in this)
      return this.$subGroupIndices;

    return this.$subGroupIndices = this.$metadata.subGroupIndices ||
      configs.subGroups_defaultOrder[this.$addressBook.id] ||
      configs.subGroups_defaultOrder[this.$addressBook.name] ||
      {};
  }

  set subGroupIndices(newValue) {
    this.$subGroupIndices = newValue;
    DB.getAddressBookMetadata(this.$addressBook).then(async metadata => {
      if (!metadata)
        metadata = {};
      const state = {
        ...(metadata.subGroupIndices || {}),
        ...newValue,
      };
      this.$subGroupIndices = metadata.subGroupIndices = state;
      await DB.setAddressBookMetadata(this.$addressBook, metadata);
    });
    return newValue;
  }

  updateChildrenIndices() {
    const indices = {
      ...this.subGroupIndices,
    };
    this._children.forEach((child, index) => {
      indices[child.path] = index;
    });
    this.subGroupIndices = indices;
  }

  removeItem(itemId) {
    const item = this.$itemById.get(itemId);
    if (!item)
      return;

    this.$itemById.delete(itemId);
    const index = this.items.findIndex(item => item.id == itemId);
    if (index > -1)
      this.items.splice(index, 1);

    const local = !this.$completelyInitialized;
    AddressBook.onItemRemoved.dispatch(this, itemId, { local });

    const collectChildren = parent => {
      if (!parent._children)
        return [parent];
      return [parent, ...parent._children.map(collectChildren)].flat();
    };
    for (const child of collectChildren(this).flat()) {
      child.removeItem(itemId);
    }
  }

  async save() {
    if (!this.dirty)
      return;

    const promises = [];
    let created = false;

    if (this.$dirtyProperties.size > 0) {
      const updates = {};
      for (const field of this.$dirtyProperties) {
        if (field in this.$addressBook &&
            UPDATABLE_ADDRESS_BOOK_FIELDS.has(field))
          updates[field] = this.$addressBook[field];
      }
      this.$dirtyProperties.clear();
      if (this.id) {
        if (Object.keys(updates).length > 0) {
          log('Updating address book: ', this.id, updates);
          promises.push(browser.addressBooks.update(this.id, updates));
        }
      }
      else {
        log('Creating address book: ', updates);
        try {
          const createdId = await browser.addressBooks.create(updates);
          log('Created as: ', createdId);
          this.$addressBook = await browser.addressBooks.get(createdId);
          if (!this.color) {
            this.setRandomColor();
            await this.save();
          }
          created = true;
        }
        catch(error) {
          console.error(error);
        }
      }
    }

    if (this.$dirtyMetadata.size > 0) {
      log('Updating address metadata: ', this.id, this.$metadata);
      promises.push(Promise.all(this.$cardDAVPasswordEncryptionPromises).then(async () => {
        await DB.setAddressBookMetadata(this.$addressBook, this.$metadata);
        const updates = {};
        for (const field of this.$dirtyMetadata) {
          updates[field] = this.$metadata[field];
        }
        browser.runtime.sendMessage({
          type:          Constants.NOTIFY_ADDRESS_BOOK_METADATA_UPDATED,
          addressBookId: this.id,
          updates,
        });
        this.$dirtyMetadata.clear();
        AddressBook.onUpdated.dispatch(this.id, updates, this);
      }));
    }

    if (promises.length > 0)
      await Promise.all(promises);

    if (created) {
      if (AddressBook.syncEnabled)
        this.sync({ forcce: true });
      else
        browser.runtime.sendMessage({
          type:          Constants.NOTIFY_ADDRESS_BOOK_READY_TO_SYNC,
          addressBookId: this.id,
        });
    }
  }

  get dirty() {
    return this.$dirtyProperties.size > 0 || this.$dirtyMetadata.size > 0;
  }

  async sync({ force } = {}) {
    AddressBook.onSyncRequested.dispatch(this, { force });
    if (!AddressBook.syncEnabled)
      browser.runtime.sendMessage({
        type:          Constants.COMMAND_SYNC_ADDRESS_BOOK,
        addressBookId: this.id,
        force,
      });
  }

  startSync({ start } = {}) {
    if (this.$syncTimer)
      this.clearTimeout(this.$syncTimer);

    const doSync = async () => {
      await this.sync();
      this.lastSyncTime = Date.now();
      this.save();
      this.$syncTimer = setTimeout(doSync, this.periodicalSyncIntervalMilliSeconds);
    };
    const delay = start ?
      Math.max(0, this.syncOnStartupDelay) :
      this.periodicalSyncIntervalMilliSeconds;
    const delta = Math.max(this.lastSyncTime ? Date.now() - this.lastSyncTime : 0);
    this.$syncTimer = setTimeout(doSync, delay - delta);
  }

  stopSync() {
    if (this.$syncTimer)
      this.clearTimeout(this.$syncTimer);
    this.$syncTimer = null;
  }

  async tryConnect() {
    const localResult = await AddressBook.onTryConnectRequested.dispatch(this);
    if (!AddressBook.syncEnabled) {
      const remoteResult = await browser.runtime.sendMessage({
        type:          Constants.COMMAND_TRY_CONNECT_ADDRESS_BOOK,
        addressBookId: this.id,
      });
      return [localResult, remoteResult].some(succeeded => !!succeeded);
    }
    return localResult;
  }

  setRandomColor() {
    this.color = `#${Math.floor(Math.random() * Math.pow(2, 24)).toString(16)}`;
  }

  destroy() {
    super.destroy();
    if (this.$addressBook) {
      const id = this.id;
      this.$addressBook = { id };
      AddressBook.onRemoved.dispatch(id);
    }
    AddressBook.instances.delete(this.id);
  }
}

AddressBook.syncEnabled = false;

AddressBook.instances = new Map();

AddressBook.get = id => {
  return AddressBook.instances.get(id);
};

AddressBook.getAll = () => {
  return Array.from(AddressBook.instances.values());
};

AddressBook.onCreated = new EventListenerManager();
AddressBook.onUpdated = new EventListenerManager();
AddressBook.onRemoved = new EventListenerManager();
AddressBook.onChildAdded = new EventListenerManager();
AddressBook.onChildRemoved = new EventListenerManager();
AddressBook.onAllChildrenDisappeared = new EventListenerManager();
AddressBook.onItemAdded = new EventListenerManager();
AddressBook.onItemUpdated = new EventListenerManager();
AddressBook.onItemRemoved = new EventListenerManager();
AddressBook.onSyncRequested = new EventListenerManager();
AddressBook.onTryConnectRequested = new EventListenerManager();


function onConfigChanged(key) {
  switch (key) {
    case 'defaultPeriodicalSync':
    case 'defaultPeriodicalSyncIntervalMinutes':
      for (const instance of AddressBook.getAll()) {
        instance.stopSync();
        if (instance.periodicalSync)
          instance.startSync();
      }
      break;
  }
}
configs.$addObserver(onConfigChanged);



export class SubGroup extends Group {
  constructor({ id, type, path, name, parent, addressBook }) {
    super({ id, path, name, parent, addressBook });
    this.id = id;
    this.type = type;
    this.path = path;
    this.$name = name;
    this.items = [];
    this.parentId = parent.id;
    this.ownerAddressBookId = addressBook.id;
    this.$itemById = new Map();

    SubGroup.instances.set(id, this);
  }

  get isOrganization() {
    return this.type == Constants.GROUP_TYPE_ORGANIZATION;
  }

  get isCategory() {
    return this.type == Constants.GROUP_TYPE_CATEGORY;
  }

  get isUncategorized() {
    return this.path == `category${VCard.VALUE_SEPARATOR}${Constants.UNCATEGORIZED_ID}`;
  }

  get name() {
    return this.isUncategorized ? configs.uncategorizedLabel : this.$name;
  }
  set name(newValue) {
    return name;
  }

  get parent() {
    return SubGroup.get(this.parentId) || AddressBook.get(this.parentId);
  }

  async addItem(item) {
    if (!this.$itemById.has(item.id)) {
      this.items.push(item);
      this.$itemById.set(item.id, item);

      SubGroup.onItemAdded.dispatch(this, item);
    }
  }

  get expanded() {
    return AddressBook.get(this.ownerAddressBookId).subGroupsExpandedState[this.path] || false;
  }

  set expanded(newValue) {
    if (newValue == this.expanded)
      return newValue;

    const addressBook = AddressBook.get(this.ownerAddressBookId);
    const state = {
      ...addressBook.subGroupsExpandedState,
    };
    state[this.path] = !!newValue;
    addressBook.subGroupsExpandedState = state;
    return newValue;
  }

  updateChildrenIndices() {
    const addressBook = AddressBook.get(this.ownerAddressBookId);
    const indices = {
      ...addressBook.subGroupIndices,
    };
    this._children.forEach((child, index) => {
      indices[child.path] = index;
    });
    addressBook.subGroupIndices = indices;
  }

  removeItem(itemId) {
    const item = this.$itemById.get(itemId);
    if (!item)
      return;
    this.$itemById.delete(itemId);
    const index = this.items.findIndex(item => item.id == itemId);
    if (index > -1)
      this.items.splice(index, 1);

    if (this.items.length == 0 &&
        (!this._children || this._children.length == 0))
      this.destroy();
  }

  cleanUpUnownedContact(item) {
    if (!this.$itemById.has(item.id) ||
        (this.isOrganization &&
         (item.organizationId == this.id ||
          item.organizationId.startsWith(`${this.id}${VCard.VALUE_SEPARATOR}`))) ||
        (this.isCategory &&
         item.internalCategories.includes(this.$name)))
      return;

    this.removeItem(item.id);
  }

  destroy() {
    const parent = this.parent;
    super.destroy();
    SubGroup.onRemoved.dispatch(this.id, parent);

    if (parent.type != 'addressBook' &&
        parent.items.length == 0 &&
        (!parent._children || parent._children.length == 0))
      parent.destroy();

    SubGroup.instances.delete(this.id);
  }
}

SubGroup.instances = new Map();

SubGroup.get = id => {
  return SubGroup.instances.get(id);
};

SubGroup.getAll = () => {
  return Array.from(SubGroup.instances.values());
};

SubGroup.getAllFromAddressBook = addressBookId => {
  return SubGroup.getAll().filter(group => group.ownerAddressBookId == addressBookId);
};

SubGroup.onRemoved = new EventListenerManager();
SubGroup.onChildAdded = new EventListenerManager();
SubGroup.onChildRemoved = new EventListenerManager();
SubGroup.onAllChildrenDisappeared = new EventListenerManager();
SubGroup.onItemAdded = new EventListenerManager();


Contact.onCreated.addListener(async contact => {
  const addressBook = AddressBook.get(contact.parentId);
  if (!addressBook)
    return;

  await addressBook.initialized;
  addressBook.addItem(contact);
});

Contact.onUpdated.addListener(async (contactId, _updated, contact) => {
  const addressBook = AddressBook.get(contact.parentId);
  if (!addressBook)
    return;

  await addressBook.initialized;
  addressBook.updateItem(contact);
});

Contact.onRemoved.addListener(async (parentId, contactId) => {
  const addressBook = AddressBook.get(parentId);
  if (!addressBook)
    return;
  await addressBook.initialized;
  addressBook.removeItem(contactId);
});

Contact.onMoved.addListener(async (contactId, oldParentId, newParentId) => {
  const fromAddressBook = AddressBook.get(oldParentId);
  const toAddressBook   = AddressBook.get(newParentId);
  const contact         = Contact.get(contactId);
  if (!fromAddressBook ||
      !toAddressBook ||
      !contact)
    return;
  await Promise.all([
    fromAddressBook.initialized,
    toAddressBook.initialized,
    contact.initialized,
  ]);
  fromAddressBook.removeItem(contactId);
  toAddressBook.addItem(contact);
});

MailingList.onCreated.addListener(async mailingList => {
  const addressBook = AddressBook.get(mailingList.parentId);
  if (!addressBook)
    return;

  await addressBook.initialized;
  addressBook.addItem(mailingList);
});

MailingList.onUpdated.addListener(async (mailingListId, _updated, mailingList) => {
  const addressBook = AddressBook.get(mailingList.parentId);
  if (!addressBook)
    return;

  await addressBook.initialized;
  addressBook.updateItem(mailingList);
});

MailingList.onRemoved.addListener(async (parentId, mailingListId) => {
  const addressBook = AddressBook.get(parentId);
  if (!addressBook)
    return;
  await addressBook.initialized;
  addressBook.removeItem(mailingListId);
});


AddressBook.autoTrack = () => {
  browser.addressBooks.onCreated.addListener(async addressBook => {
    log('addressBooks.onCreated: ', addressBook);
    if (AddressBook.get(addressBook.id))
      return;
    const instance = new AddressBook(addressBook);
    await instance.initialized;
    AddressBook.onCreated.dispatch(instance);
  });
};

browser.addressBooks.onUpdated.addListener(async addressBook => {
  log('addressBooks.onUpdated: ', addressBook);
  const instance = AddressBook.get(addressBook.id) || new AddressBook(addressBook);;
  const updated = await instance.apply(addressBook);
  AddressBook.onUpdated.dispatch(addressBook.id, updated, instance);
});

browser.addressBooks.onDeleted.addListener(async id => {
  const instance = AddressBook.get(id);
  if (!instance)
    return;
  await instance.initialized;
  instance.destroy();
});

browser.runtime.onMessage.addListener((message, _sender) => {
  switch (message && message.type) {
    case Constants.COMMAND_SYNC_ADDRESS_BOOK:
      if (AddressBook.syncEnabled) {
        const instance = AddressBook.get(message.addressBookId)
        if (instance)
          instance.sync({ force: message.force });
      }
      break;

    case Constants.COMMAND_TRY_CONNECT_ADDRESS_BOOK:
      if (AddressBook.syncEnabled) {
        const instance = AddressBook.get(message.addressBookId)
        if (instance)
          return instance.tryConnect();
      }
      break;

    case Constants.NOTIFY_ADDRESS_BOOK_METADATA_UPDATED: {
      const instance = AddressBook.get(message.addressBookId);
      if (instance) {
        instance.applyMetadata(message.updates).then(updated => {
          instance.initialized.then(() => {
            AddressBook.onUpdated.dispatch(instance.id, updated, instance);
          });
        });
      }
      else {
        browser.addressBooks.get(message.addressBookId).then(addressBook => {
          const newInstance = AddressBook.get(addressBook.id) || new AddressBook(addressBook);
          newInstance.initialized.then(() => {
            AddressBook.onUpdated.dispatch(addressBook.id, message.updates, newInstance);
          });
        });
      }
    }; break;

    case Constants.NOTIFY_ADDRESS_BOOK_READY_TO_SYNC: {
      const instance = AddressBook.get(message.addressBookId);
      if (instance) {
        instance.sync({ force: true });
      }
    }; break;
  }
});


export function normalizeGroup(group) {
  if (group.type == 'addressBook')
    return AddressBook.get(group.id) || new AddressBook(group);

  return SubGroup.get(group.id) || new SubGroup(group);
}
