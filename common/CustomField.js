/*
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
*/
'use strict';

export class CustomField {
  constructor(definition) {
    this.$definition = definition;
    this.constructor.instances.set(this.id, this)
  }

  destroy() {
    this.constructor.instances.delete(this.id);
  }

  get id() {
    return this.$definition.field;
  }

  get vCardField() {
    return this.$definition.field.toUpperCase();
  }

  get fieldPath() {
    return `extra.${this.$definition.field.toLowerCase()}`;
  }

  get bindTo() {
    return this.$definition.bindTo;
  }

  get label() {
    return this.$definition.label || this.id;
  }
}

CustomField.instances = new Map();
CustomField.get = id => {
  return CustomField.instances.get(id);
};

CustomField.getAll = () => {
  return Array.from(CustomField.instances.values());
};


export class CustomPersonalField extends CustomField {
}
CustomPersonalField.instances = new Map();
CustomPersonalField.get = id => {
  return CustomPersonalField.instances.get(id);
};
CustomPersonalField.getAll = () => {
  return Array.from(CustomPersonalField.instances.values());
};
CustomPersonalField.init = definitions => {
  for (const field of CustomPersonalField.getAll()) {
    field.destroy();
  }
  for (const definition of definitions) {
    new CustomPersonalField(definition);
  }
};

export class CustomOrganizationField extends CustomField {
}
CustomOrganizationField.instances = new Map();
CustomOrganizationField.get = id => {
  return CustomOrganizationField.instances.get(id);
};
CustomOrganizationField.getAll = () => {
  return Array.from(CustomOrganizationField.instances.values());
};
CustomOrganizationField.init = definitions => {
  for (const field of CustomOrganizationField.getAll()) {
    field.destroy();
  }
  for (const definition of definitions) {
    new CustomOrganizationField(definition);
  }
};

export function bindToConfigs(configs) {
  function onConfigChanged(key) {
    switch (key) {
      case 'customPersonalInformationFields':
        CustomPersonalField.init(configs.customPersonalInformationFields);
        break;

      case 'customOrganizationInformationFields':
        CustomOrganizationField.init(configs.customOrganizationInformationFields);
        break;
    }
  }
  configs.$addObserver(onConfigChanged);
  configs.$loaded.then(() => {
    onConfigChanged('customPersonalInformationFields');
    onConfigChanged('customOrganizationInformationFields');
  });
}
