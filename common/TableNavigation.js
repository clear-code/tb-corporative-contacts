/*
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
*/
'use strict';

import {
  log,
} from '/common/common.js';

export class TableNavigation {
  constructor(table, { onItemsOpen, onItemsDelete, canMoveRow, onItemMoved, onItemsMoved, onItemDetached, onItemAttached, onFocusMoved, normalizer, exporter } = {}) {
    this.$table = table;
    this.$lastFocusedId = null;
    this.$lastAnchorId = null;
    this.$canMultiselect = this.$table.options.selectable === true || this.$table.options.selectable > 1;
    this.$onItemsOpen = onItemsOpen;
    this.$onItemsDelete = onItemsDelete;
    this.$canMoveRow = canMoveRow;
    this.$onItemMoved = onItemMoved;
    this.$onItemsMoved = onItemsMoved;
    this.$onItemDetached = onItemDetached;
    this.$onItemAttached = onItemAttached;
    this.$onFocusMoved = onFocusMoved;
    this.$normalizer = normalizer || (input => input);
    this.$exporter = exporter || (input => input);

    this.reserveToBackupCurrentData();

    this.overrideCallbacks();
    this.invalidateCache();

    table.element.addEventListener('mousedown', event => {
      if (event.target.closest('input[type="checkbox"], .tabulator-data-tree-control'))
        return;
      const row = table.getRow(event.target.closest('.tabulator-row'));
      if (!row)
        return;
      this.$lastFocusedId = this.$lastAnchorId = row.getIndex();
    }, { capture: true });

    table.element.addEventListener('keydown', this.onKeyDown.bind(this), { capture: true });
    table.element.addEventListener('focus', this.onFocus.bind(this), { capture: true });
  }

  overrideCallbacks() {
    this.overrideCallback('tableBuilt');
    this.overrideCallback('columnMoved');
    this.overrideCallback('columnVisibilityChanged');
    this.overrideCallback('rowAdded');
    this.overrideCallback('rowUpdated');
    this.overrideCallback('rowDeleted');
    this.overrideCallback('rowMoved');
    this.overrideCallback('dataSorted');
    this.overrideCallback('dataTreeRowExpanded');
    this.overrideCallback('dataTreeRowCollapsed');
  }

  overrideCallback(callbackName) {
    const original = this.$table.options[callbackName];
    this.$table.options[callbackName] = (...args) => {
      this.invalidateCache();
      if (typeof this[callbackName] == 'function')
        this[callbackName].call(this, ...args);
      if (typeof original == 'function')
        original.call(this.$table.options, ...args);
    };
  }

  onKeyDown(event) {
    log('onKeyDown: ', event);
    switch (event.key) {
      case 'ArrowUp':
        this.advanceFocus(-1, {
          expand: event.shiftKey,
        });
        event.stopImmediatePropagation();
        event.preventDefault();
        break;

      case 'ArrowDown':
        this.advanceFocus(1, {
          expand: event.shiftKey,
        });
        event.stopImmediatePropagation();
        event.preventDefault();
        break;

      case 'ArrowRight':
        if (this.isTree) {
          this.invalidateCache();
          const firstSelected = this.selectedRows[0];
          if (firstSelected)
            firstSelected.treeExpand();
        }
        event.stopImmediatePropagation();
        event.preventDefault();
        break;

      case 'ArrowLeft':
        if (this.isTree) {
          this.invalidateCache();
          const firstSelected = this.selectedRows[0];
          if (firstSelected)
            firstSelected.treeExpand();
        }
        event.stopImmediatePropagation();
        event.preventDefault();
        break;

      case 'Tab': {
        event.stopImmediatePropagation();
        event.preventDefault();
        const focusableElements = 'a:link, a:visited, input:not([type="hidden"]), button, map, [tabindex]:not([tabindex="-1"])';
        const allFocusables = Array.from(document.body.querySelectorAll(focusableElements));
        const exceptions = new Set(document.querySelectorAll(`.tabulator :-moz-any(${focusableElements})`));
        const focusable = allFocusables.filter(element => !exceptions.has(element) && element.offsetWidth > 0 && element.offsetHeight > 0);
        let focusedIndex = focusable.indexOf(this.$table.element);
        if (event.shiftKey) {
          focusedIndex--;
          if (focusedIndex < 0)
            focusedIndex = focusable.length - 1;
        }
        else {
          focusedIndex++;
          if (focusedIndex >= focusable.length)
            focusedIndex = 0;
        }
        focusable[focusedIndex].focus();
      }; break;

      case 'Enter': {
        event.stopImmediatePropagation();
        event.preventDefault();
        const rows = this.selectedRows;
        if (rows.length > 0 && typeof this.$onItemsOpen == 'function')
          this.$onItemsOpen(rows.map(row => this.$normalizer(row.getData())));
      }; break;

      case 'Delete': {
        event.stopImmediatePropagation();
        event.preventDefault();
        const rows = this.selectedRows;
        if (rows.length > 0 && typeof this.$onItemsDelete == 'function')
          this.$onItemsDelete(rows.map(row => this.$normalizer(row.getData())));
      }; break;
    }
  }

  onFocus(event) {
    if (event.target.closest('input[type="checkbox"]')) {
      // When a checkbox in unfocused table is clicked, the table gets focus
      // before the row for the checkbox gets selected, thus both the initial
      // row and the checkbox row gets selected unexpectedly. To suppress such
      // unexpected selection, we need to ignore such case.
      event.target.blur();
      return;
    }
    if (event.target == this.$table.element) {
      return;
    }
    event.stopImmediatePropagation();
    event.preventDefault();
    this.$table.element.focus();
  }

  get isTree() {
    return this.$table.options.dataTree;
  }

  invalidateCache() {
    this.$cachedTopLevelRows = undefined;
    this.$cachedAllRows = undefined;
    this.$cachedLastRow = undefined;
  }

  get topLevelRows() {
    if (this.$cachedTopLevelRows !== undefined)
      return this.$cachedTopLevelRows;

    const rows = [];
    for (let i = 0, maxi = this.$table.getRows('all').length; i < maxi; i++) {
      rows.push(this.$table.getRowFromPosition(i, true));
    }
    return rows;
  }

  get allRows() {
    if (this.$cachedAllRows !== undefined)
      return this.$cachedAllRows;

    const rows = this.topLevelRows;
    if (!this.isTree)
      return this.$cachedAllRows = rows;

    const allRows = [];
    for (const row of rows) {
      allRows.push(row, ...row.getTreeChildren());
    }
    return this.$cachedAllRows = allRows;
  }

  get selectedRows() {
    return this.$table.getSelectedRows();
  }

  get firstRow() {
    return this.$table.getRowFromPosition(0, true);
  }

  get lastRow() {
    if (this.$cachedLastRow !== undefined)
      return this.$cachedLastRow;

    const rows = this.topLevelRows;
    if (rows.length == 0)
      return null;
    const last = rows[rows.length - 1];
    return this.$cachedLastRow = this.getLastChildRow(last) || last;
  }

  getLastChildRow(row) {
    if (this.isTree && typeof row.getTreeChildren == 'function') {
      const rows = row.getTreeChildren();
      if (rows.lenggth > 0)
        return this.getLastChildRow(rows[rows.length - 1]);
      return rows[rows.length - 1];
    }
    return row;
  }

  getPrevVisibleRow(row) {
    if (!row)
      return null;

    const prevRow = row.getPrevRow();
    if (prevRow)
      return prevRow;

    if (this.isTree && typeof row.getTreeParent == 'function') {
      const parent = row.getTreeParent();
      return this.getPrevVisibleRow(parent) || parent;
    }

    return null;
  }

  getNextVisibleRow(row) {
    if (this.isTree && typeof row.getTreeChildren == 'function') {
      const rows = row.getTreeChildren();
      if (rows.lenggth > 0)
        return rows[0];
    }

    const nextRow = row.getNextRow();
    if (nextRow ||
        !this.isTree ||
        typeof row.getTreeParent != 'function')
      return nextRow;

    const parent = row.getTreeParent();
    if (parent)
      return this.getNextVisibleRow(parent);

    return null;
  }

  getRowsBetween(startRow, endRow) {
    if (startRow == endRow)
      return [startRow];
    const allRows = this.allRows;
    let foundCount = 0;
    const rows = new Set();
    for (const row of allRows) {
      if (row == startRow || row == endRow) {
        rows.add(row);
        foundCount++;
      }
      else if (foundCount == 1) {
        rows.add(row);
      }
      else if (foundCount > 1) {
        break;
      }
    }
    return Array.from(rows);
  }

  getParentRow(row) {
    if (this.isTree &&
        row &&
        typeof row.getTreeParent == 'function')
      return row.getTreeParent();
    return null;
  }

  advanceFocus(direction, { expand } = {}) {
    const rows = this.selectedRows;
    this.$table.deselectRow(rows);
    const focusedRow = this.$table.getRow(this.$lastFocusedId) || rows[0] || null;
    let anchorRow = this.$table.getRow(this.$lastAnchorId) || focusedRow;
    let toBeFocusedRow;
    let scrollPosition;
    if (direction < 0) { // backward
      toBeFocusedRow = focusedRow && this.getPrevVisibleRow(focusedRow);
      if (toBeFocusedRow) {
        scrollPosition = 'top';
      }
      else {
        toBeFocusedRow = this.lastRow;
        scrollPosition = 'bottom';
      }
      if (!anchorRow) {
        anchorRow = this.lastRow;
        this.$lastAnchorId = anchorRow && anchorRow.getIndex();
      }
    }
    else { // forward
      toBeFocusedRow = focusedRow && this.getNextVisibleRow(focusedRow);
      if (toBeFocusedRow) {
        scrollPosition = 'bottom';
      }
      else {
        toBeFocusedRow = this.firstRow;
        scrollPosition = 'top';
      }
      if (!anchorRow) {
        anchorRow = this.firstRow;
        this.$lastAnchorId = anchorRow && anchorRow.getIndex();
      }
    }
    if (expand && this.$canMultiselect) {
      const toBeSelectedRows = this.getRowsBetween(anchorRow, toBeFocusedRow);
      this.$table.selectRow(toBeSelectedRows);
      this.$lastFocusedId = toBeFocusedRow.getIndex();
    }
    else {
      this.$table.selectRow(toBeFocusedRow);
      this.$lastFocusedId = this.$lastAnchorId = toBeFocusedRow.getIndex();
    }
    if (typeof this.$onFocusMoved == 'function')
      this.$onFocusMoved.call(this, toBeFocusedRow, { expand });
    this.$table.scrollToRow(toBeFocusedRow, scrollPosition, false);
  }

  reserveToBackupCurrentData() {
    if (this.throttledBackup)
      clearTimeout(this.throttledBackup);
    this.throttledBackup = setTimeout(() => {
      this.throttledBackup = 0;
      this.$itemsBeforeMoved = this.$table.getData();
    }, 150);
  }

  rowAdded() {
    this.reserveToBackupCurrentData();
  }

  rowUpdated() {
    this.reserveToBackupCurrentData();
  }

  rowDeleted() {
    this.reserveToBackupCurrentData();
  }

  canMoveRow(row, { oldItems, newParent }) {
    if (typeof this.$canMoveRow == 'function' &&
        !this.$canMoveRow(row, { oldItems, newParent })) {
      return false;
    }

    return true;
  }

  detectPossibleNewParent(row) {
    const prevRow = row.getPrevRow();
    const prevRowParent = this.getParentRow(prevRow);
    const nextRow = row.getNextRow();
    const nextRowParent = this.getParentRow(nextRow);
    const currentParent = this.getParentRow(row);

    if (!prevRow && !nextRow)
      return null;

    if (!nextRow) {
      //   - prevRow
      //   <dropped here>
      // or
      //   - prevRowParent
      //     - prevRow
      //     <dropped here>
      if (!currentParent)
        return null;
      return prevRowParent;
    }

    if (!prevRow) {
      //   <dropped here>
      //   - nextRow
      return null;
    }

    if (prevRowParent == nextRowParent) {
      //   - prevRowParent/nextRowParent
      //     - prevRow
      //     <dropped at here>
      //     - nextRow
      // or
      //   - prevRow
      //   <dropped at here>
      //   - nextRow
      return prevRowParent;
    }

    if (nextRowParent && prevRow == nextRowParent) {
      //   - prevRow/nextRowParent
      //     <dropped at here>
      //     - nextRow
      return prevRow;
    }

    if (prevRowParent && nextRowParent && prevRowParent != nextRowParent) {
      //   - nextRowParent
      //     - prevRowParent
      //       -prevRow
      //     <dropped at here>
      //     - nextRow
      // or
      //   - nextRowParent
      //     - ???
      //       - prevRowParent
      //         -prevRow
      //     <dropped here>
      //     - nextRow
      return prevRowParent == currentParent ? prevRowParent : nextRowParent;
    }

    return null;
  }

  getItemFromTreeById(id, items = null) {
    if (!items)
      items = this.$table.getData();

    for (const item of items) {
      if (item.id == id)
        return this.$normalizer(item);
      if (Array.isArray(item._children)) {
        const found = this.getItemFromTreeById(id, item._children);
        if (found)
          return this.$normalizer(found);
      }
    }
    return null;
  }

  rowMoved(row) {
    const oldItems = this.$itemsBeforeMoved;
    let newItems = this.$table.getData().map(this.$normalizer);
    const movedItem = this.$normalizer(row.getData());

    const newParent = this.isTree && this.detectPossibleNewParent(row);
    log('movedItem: ', movedItem.id, ', newParent ', newParent, newParent && newParent.getData());
    if (!this.canMoveRow(row, { oldItems, newParent })) {
      this.$table.setData(oldItems.slice(0));
      return;
    }

    const oldParent = this.getParentRow(row);
    if (this.isTree && (oldParent || newParent)) {
      const oldIndex = oldParent ? this.$normalizer(oldParent.getData())._children.indexOf(movedItem) : -1;

      const prevRow = row.getPrevRow();
      const prevItemIndex = prevRow && newParent ? this.$normalizer(newParent.getData())._children.indexOf(this.$normalizer(prevRow.getData())) : -1;

      const nextRow = row.getNextRow();
      const nextItemIndex = nextRow && newParent ? this.$normalizer(newParent.getData())._children.indexOf(this.$normalizer(nextRow.getData())) : -1;

      log('moving in a tree ', { oldIndex, prevItemIndex, nextItemIndex });

      const oldParentData = oldParent && this.getItemFromTreeById(oldParent.getData().id, newItems);
      const oldChildren   = oldParentData && oldParentData._children && oldParentData._children.map(this.$normalizer);
      const newParentData = newParent && this.getItemFromTreeById(newParent.getData().id, newItems);
      const newChildren   = newParentData && newParentData._children && newParentData._children.map(this.$normalizer);

      let updated = false;
      if (oldParent && !newParent) {
      }
      else if (!oldParent && newParent) {
      }
      else if (oldParent && newParent) {
        if (oldParent == newParent) {
          const marker = {};
          if (oldIndex > -1)
            oldChildren.splice(oldIndex, 1, marker);

          if (prevItemIndex > -1)
            oldChildren.splice(prevItemIndex + 1, 0, movedItem);
          else if (nextItemIndex > -1)
            oldChildren.splice(nextItemIndex, 0, movedItem);
          else
            oldChildren.push(movedItem);

          if (oldIndex > -1)
            oldChildren.splice(oldChildren.indexOf(marker), 1);

          oldParentData._children = oldChildren;
          log('oldChildren => ', oldChildren);

          updated = true;
        }
        else {
          if (oldIndex > -1)
            oldChildren.splice(oldIndex, 1);

          if (prevItemIndex > -1)
            newChildren.splice(prevItemIndex + 1, 0, movedItem);
          else if (nextItemIndex > -1)
            newChildren.splice(nextItemIndex, 0, movedItem);
          else
            newChildren.push(movedItem);

          oldParentData._children = oldChildren;
          newParentData._children = newChildren;
          log('oldChildren => ', oldChildren);
          log('newChildren => ', newChildren);

          updated = true;
        }

        if (updated) {
          const params = {
            oldParent: oldParentData,
            newParent: newParentData,
          };
          if (oldParent && typeof this.$onItemDetached == 'function')
            this.$onItemDetached(movedItem, params);
          if (newParent && typeof this.$onItemAttached == 'function')
            this.$onItemAttached(movedItem, params);
          if (typeof this.$onItemMoved == 'function')
            this.$onItemMoved(movedItem, -1);
          if (typeof this.$onItemsMoved == 'function')
            this.$onItemsMoved([movedItem]);

          this.$table.setData(newItems);

          return;
        }
      }

      log('not handled: rollback changes');
      this.$table.setData(oldItems.slice(0));
      return;
    }

    log(`oldItems `, oldItems.slice(0));
    const oldIndex = oldItems.findIndex(item => item.id == movedItem.id);
    const newIndex = newItems.findIndex(item => item.id == movedItem.id);
    log(`${oldIndex} => ${newIndex}`);

    let movedItems = this.$table.getSelectedData();
    if (movedItems.length == 0 || !movedItems.includes(movedItem))
      movedItems = [movedItem];
    movedItems = new Set(movedItems);

    const marker = { id: null };
    newItems.splice(newIndex, 1, marker);
    log(`newItems `, newItems.slice(0));

    let startIndex = -1;
    let endIndex = -1;
    let lastWasSelected = false;
    newItems = newItems.filter((item, index) => {
      if (!item.id || movedItems.has(item)) {
        lastWasSelected = true;
        if (startIndex < 0)
          startIndex = index;
        return !item.id;
      }
      if (lastWasSelected) {
        endIndex = index;
        lastWasSelected = false;
      }
      return true;
    });
    startIndex = Math.min(startIndex, oldIndex, newIndex);
    endIndex = Math.max(endIndex, oldIndex, newIndex);
    log(`${startIndex} - ${endIndex}`);
    newItems.splice(newItems.indexOf(marker), 1, ...movedItems);
    log(' => ', newItems.slice(0));

    if (typeof this.$onItemMoved == 'function') {
      for (let i = startIndex; i <= endIndex; i++) {
        this.$onItemMoved(newItems[i], i);
      }
    }

    if (typeof this.$onItemsMoved == 'function') {
      this.$onItemsMoved(Array.from(movedItems));
    }

    this.$table.setData(newItems.map(this.$exporter));
    this.$itemsBeforeMoved = newItems.map(this.$exporter);
  }

  dataSorted() {
    this.$itemsBeforeMoved = this.$table.getData();
  }

  dataTreeRowExpanded() {
    this.$itemsBeforeMoved = this.$table.getData();
  }

  dataTreeRowCollapsed() {
    this.$itemsBeforeMoved = this.$table.getData();
  }
}
