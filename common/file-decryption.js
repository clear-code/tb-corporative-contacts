/*
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
*/
'use strict';

import {
  configs,
} from '/common/common.js';

async function getSecretKey(password) {
  const passwordUint8Array = (new TextEncoder()).encode(password);
  const keyMaterial = await crypto.subtle.importKey(
    'raw',
    configs.importEncryptedSourceSecretKeyDigestHash ?
      await crypto.subtle.digest(
        { name: configs.importEncryptedSourceSecretKeyDigestHash },
        passwordUint8Array
      ) :
      passwordUint8Array,
    { name: 'PBKDF2' },
    false,
    ['deriveKey']
  );
  const saltUint8Array = Uint8Array.from(configs.importEncryptedSourceSecretKeySalt);
  return crypto.subtle.deriveKey(
    {
      name: 'PBKDF2',
      salt: saltUint8Array,
      iterations: configs.importEncryptedSourceSecretKeyIterations,
      hash: configs.importEncryptedSourceSecretKeyHash,
    },
    keyMaterial,
    { name: 'AES-GCM', length: configs.importEncryptedSourceSecretKeyLength },
    false,
    ['decrypt']
  );
}

export async function decryptFileContents(file, { password }) {
  const encryptedDataAndIv = new Uint8Array(await file.arrayBuffer());
  const offset = configs.importEncryptedSourceIVLength / 8;
  const extractedIv   = encryptedDataAndIv.slice(0, offset);
  const extractedData = encryptedDataAndIv.slice(offset);
  const decryptedArrayBuffer = await crypto.subtle.decrypt(
    { name: 'AES-GCM',
      iv: extractedIv },
    await getSecretKey(password),
    extractedData
  );
  const decryptedData = new Uint8Array(decryptedArrayBuffer);
  return decryptedData;
}
