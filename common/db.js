/*
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
*/
'use strict';

import {
  log
} from './common.js';
//import * as Constants from './constants.js';
import * as Encryption from '/common/encryption.js';

const DB_NAME = 'CorporativeContacts';
const DB_VERSION = 3;

const STORE_ADDRESSBOOK_METADATA = 'addressbook-metadata';
const STORE_CONTACT_METADATA = 'contact-metadata';
const STORE_MAILING_LIST_METADATA = 'mailing-list-metadata';
const STORE_ENCRYPTED_FIELDS = 'encrypted-fields';

const mPromisedDB = new Promise((resolve, reject) => {
  const request = window.indexedDB.open(DB_NAME, DB_VERSION);
  request.onupgradeneeded = event => {
    const db = event.target.result;
    //const transaction = event.target.transaction;
    if (event.oldVersion < 1) {
      const addressBookMetadataStore = db.createObjectStore(STORE_ADDRESSBOOK_METADATA, {
        keyPath: 'addressBookId',
      });
      addressBookMetadataStore.createIndex('addressBookIdToData', 'addressBookId', { unique: true });
      const contactMetadataStore = db.createObjectStore(STORE_CONTACT_METADATA, {
        keyPath: 'contactId',
      });
      contactMetadataStore.createIndex('contactIdToData', 'contactId', { unique: true });
      contactMetadataStore.createIndex('addressBookToData', 'addressBookId');
      contactMetadataStore.createIndex('parentToData', 'parentId');
    }
    if (event.oldVersion < 2) {
      const mailingListMetadataStore = db.createObjectStore(STORE_MAILING_LIST_METADATA, {
        keyPath: 'mailingListId',
      });
      mailingListMetadataStore.createIndex('mailingListIdToData', 'mailingListId', { unique: true });
      mailingListMetadataStore.createIndex('addressBookToData', 'addressBookId');
    }
    if (event.oldVersion < 3) {
      const encryptedFieldsStore = db.createObjectStore(STORE_ENCRYPTED_FIELDS, {
        keyPath: 'id',
      });
      encryptedFieldsStore.createIndex('idToData', 'id', { unique: true });
      encryptedFieldsStore.createIndex('groupToData', 'groupId');
    }
  };
  request.onsuccess = event => {
    resolve(event.target.result);
  };
  request.onerror = event => {
    const error = new Error(`Cannot access to the DB: ${event.target.errorCode}`);
    console.error(error);
    reject(error);
  };
});


const mCachedAddressBookMetadata = new Map();

export async function setAddressBookMetadata(addressBook, metadata) {
  mCachedAddressBookMetadata.set(addressBook.id, metadata);
  log('setAddressBookMetadata ', { addressBook, metadata });
  const record = { addressBookId: addressBook.id, metadata };
  const db = await mPromisedDB;
  const transaction = db.transaction([STORE_ADDRESSBOOK_METADATA], 'readwrite');
  return new Promise((resolve, _reject) => {
    const store = transaction.objectStore(STORE_ADDRESSBOOK_METADATA);
    const request = store.put(record);
    request.onsuccess = _event => {
      log(` => setAddressBookMetadata(${addressBook.id}): success`);
      resolve(true);
    };
    request.onerror = event => {
      log(` => setAddressBookMetadata(${addressBook.id}): fail`, event);
      resolve(false);
    };
  });
}

export async function getAddressBookMetadata(addressBook) {
  log('getAddressBookMetadata ', addressBook);
  if (mCachedAddressBookMetadata.has(addressBook.id))
    return mCachedAddressBookMetadata.get(addressBook.id);
  const db = await mPromisedDB;
  const transaction = db.transaction([STORE_ADDRESSBOOK_METADATA]);
  return new Promise((resolve, _reject) => {
    const store = transaction.objectStore(STORE_ADDRESSBOOK_METADATA);
    const request = store.get(addressBook.id);
    request.onsuccess = event => {
      const record = event.target.result;
      log(` => getAddressBookMetadata(${addressBook.id}): success `, record);
      resolve(record && record.metadata);
    };
    request.onerror = event => {
      log(` => getAddressBookMetadata(${addressBook.id}): fail`, event);
      resolve(null);
    };
  });
}


export async function setContactMetadata(contact, metadata) {
  log('setContactMetadata ', { contact, metadata });
  const record = { contactId: contact.id, metadata, addressBookId: contact.parentId, parentId: contact.organizationId };
  const db = await mPromisedDB;
  const transaction = db.transaction([STORE_CONTACT_METADATA], 'readwrite');
  return new Promise((resolve, _reject) => {
    const store = transaction.objectStore(STORE_CONTACT_METADATA);
    const request = store.put(record);
    request.onsuccess = _event => {
      log(` => setContactMetadata(${contact.id}): success`);
      resolve(true);
    };
    request.onerror = event => {
      log(` => setContactMetadata(${contact.id}): fail`, event);
      resolve(false);
    };
  });
}

export async function getContactMetadata(contact) {
  log('getContactMetadata ', contact);
  const db = await mPromisedDB;
  const transaction = db.transaction([STORE_CONTACT_METADATA]);
  return new Promise((resolve, _reject) => {
    const store = transaction.objectStore(STORE_CONTACT_METADATA);
    const request = store.get(contact.id);
    request.onsuccess = event => {
      const record = event.target.result;
      log(` => getContactMetadata(${contact.id}): success `, record);
      resolve(record && record.metadata);
    };
    request.onerror = event => {
      log(` => getContactMetadata(${contact.id}): fail `, event);
      resolve(null);
    };
  });
}

async function getContactsMetadataFromParent(parent) {
  log('getContactsMetadataFromParent ', parent);
  const db = await mPromisedDB;
  const transaction = db.transaction([STORE_CONTACT_METADATA]);
  return new Promise((resolve, _reject) => {
    const store = transaction.objectStore(STORE_CONTACT_METADATA);
    const keyRange = IDBKeyRange.bound(parent.id, `${parent.id}\uffff`);
    const index = parent.type == 'addressBook' ? 'addressBookToData' : 'parentToData';
    const request = store.index(index).openCursor(keyRange);
    const found = {};
    request.onsuccess = event => {
      const result = event.target.result;
      if (result) {
        if (result.value) {
          const record = result.value;
          found[record.contactId] = record.metadata;
          result.continue();
        }
      }
      else {
        log(` => getContactsMetadataFromParent(${parent.id}): success `, found);
        resolve(found);
      }
    };
    request.onerror = event => {
      log(` => getContactsMetadataFromParent(${parent.id}): fail `, event);
      resolve(found);
    };
  });
}


export async function setMailingListMetadata(mailingList, metadata) {
  log('setMailingListMetadata ', { mailingList, metadata });
  const record = { mailingListId: mailingList.id, metadata };
  const db = await mPromisedDB;
  const transaction = db.transaction([STORE_MAILING_LIST_METADATA], 'readwrite');
  return new Promise((resolve, _reject) => {
    const store = transaction.objectStore(STORE_MAILING_LIST_METADATA);
    const request = store.put(record);
    request.onsuccess = _event => {
      log(` => setMailingListMetadata(${mailingList.id}): success`);
      resolve(true);
    };
    request.onerror = event => {
      log(` => setMailingListMetadata(${mailingList.id}): fail`, event);
      resolve(false);
    };
  });
}

export async function getMailingListMetadata(mailingList) {
  log('getMailingListMetadata ', mailingList);
  const db = await mPromisedDB;
  const transaction = db.transaction([STORE_MAILING_LIST_METADATA]);
  return new Promise((resolve, _reject) => {
    const store = transaction.objectStore(STORE_MAILING_LIST_METADATA);
    const request = store.get(mailingList.id);
    request.onsuccess = event => {
      const record = event.target.result;
      log(` => getMailingListMetadata(${mailingList.id}): success `, record);
      resolve(record && record.metadata);
    };
    request.onerror = event => {
      log(` => getMailingListMetadata(${mailingList.id}): fail`, event);
      resolve(null);
    };
  });
}

async function getMailingListsMetadataFromParent(parent) {
  log('getMailingListsMetadataFromParent ', parent);
  const db = await mPromisedDB;
  const transaction = db.transaction([STORE_MAILING_LIST_METADATA]);
  return new Promise((resolve, _reject) => {
    const store = transaction.objectStore(STORE_MAILING_LIST_METADATA);
    const keyRange = IDBKeyRange.bound(parent.id, `${parent.id}\uffff`);
    const request = store.index('addressBookToData').openCursor(keyRange);
    const found = {};
    request.onsuccess = event => {
      const result = event.target.result;
      if (result) {
        if (result.value) {
          const record = result.value;
          found[record.mailingListId] = record.metadata;
          result.continue();
        }
      }
      else {
        log(` => getMailingListsMetadataFromParent(${parent.id}): success `, found);
        resolve(found);
      }
    };
    request.onerror = event => {
      log(` => getMailingListsMetadataFromParent(${parent.id}): fail `, event);
      resolve(found);
    };
  });
}


export async function setItemMetadata(item, metadata) {
  switch (item.type) {
    case 'contact':
      return setContactMetadata(item, metadata);
    case 'mailingList':
      return setMailingListMetadata(item, metadata);
    default:
      throw new Error('unknown type item');
  }
}

export async function getItemMetadata(item) {
  switch (item.type) {
    case 'contact':
      return getContactMetadata(item);
    case 'mailingList':
      return getMailingListMetadata(item);
    default:
      throw new Error('unknown type item');
  }
}

export async function getItemsMetadataFromAddressBook(addressBook) {
  const [contactsMetadata, mailingListsMetadata] = await Promise.all([
    getContactsMetadataFromParent(addressBook),
    getMailingListsMetadataFromParent(addressBook),
  ]);
  return {
    ...contactsMetadata,
    ...mailingListsMetadata,
  };
}


const mCachedEncryptedFields = new Map();

export async function setEncryptedFields({ id, groupId, fields }) {
  log('setEncryptedFields ', { id, groupId, fields });
  mCachedEncryptedFields.set(id, fields);
  const record = { id, groupId };
  record.fields = JSON.stringify(await Encryption.encrypt(JSON.stringify(fields)));
  const db = await mPromisedDB;
  const transaction = db.transaction([STORE_ENCRYPTED_FIELDS], 'readwrite');
  return new Promise((resolve, _reject) => {
    const store = transaction.objectStore(STORE_ENCRYPTED_FIELDS);
    const request = store.put(record);
    request.onsuccess = _event => {
      log(` => setEncryptedFields(${id}/${groupId}): success`);
      resolve(true);
    };
    request.onerror = event => {
      log(` => setEncryptedFields(${id}/${groupId}): fail`, event);
      resolve(false);
    };
  });
}

export async function getEncryptedFields(id) {
  log('getEncryptedFields ', id);
  if (mCachedEncryptedFields.has(id))
    return mCachedEncryptedFields.get(id);
  const db = await mPromisedDB;
  const transaction = db.transaction([STORE_ENCRYPTED_FIELDS]);
  return new Promise((resolve, _reject) => {
    const store = transaction.objectStore(STORE_ENCRYPTED_FIELDS);
    const request = store.get(id);
    request.onsuccess = event => {
      const record = event.target.result;
      if (!record || !record.fields) {
        log(` => getEncryptedFields(${id}): success, but no record `, event.target);
        return resolve(null);
      }
      Encryption.decrypt(JSON.parse(record.fields))
        .then(decrypted => JSON.parse(decrypted || '{}'))
        .catch(_error => ({}))
        .then(async decryptedFields => {
          // New data can be saved while loading data from the DB.
          // We need to merge them, and save merged fields if necessary.
          const mergedFields = {
            ...decryptedFields,
            ...(mCachedEncryptedFields.get(id) || {}),
          };
          mCachedEncryptedFields.set(id, mergedFields);
          if (JSON.stringify(mergedFields) != JSON.stringify(decryptedFields))
            setEncryptedFields({ id, fields: mergedFields });
          resolve(mergedFields);
          log(` => getEncryptedFields(${id}): success `, record);
        });
    };
    request.onerror = event => {
      log(` => getEncryptedFields(${id}): fail`, event);
      resolve(null);
    };
  });
}

export async function grabEncryptedFields(groupId) {
  log('grabEncryptedFields ', groupId);
  const db = await mPromisedDB;
  const transaction = db.transaction([STORE_ENCRYPTED_FIELDS]);
  return new Promise((resolve, _reject) => {
    const store = transaction.objectStore(STORE_ENCRYPTED_FIELDS);
    const keyRange = IDBKeyRange.bound(groupId, `${groupId}\uffff`);
    const request = store.index('groupToData').openCursor(keyRange);
    const found = {};
    const promises = [];
    request.onsuccess = event => {
      const result = event.target.result;
      if (result) {
        if (result.value) {
          const record = result.value;
          promises.push(Encryption.decrypt(record.fields)
            .then(decrypted => JSON.parse(decrypted || '{}'))
            .catch(_error => ({}))
            .then((decryptedFields) => {
              found[record.id] = decryptedFields;
            }));
          result.continue();
        }
      }
      else {
        log(` => grabEncryptedFields(${groupId}): success `, found);
        Promise.all(promises).then(() => resolve(found));
      }
    };
    request.onerror = event => {
      log(` => grabEncryptedFields(${groupId}): fail `, event);
      Promise.all(promises).then(() => resolve(found));
    };
  });
}


browser.addressBooks.onDeleted.addListener(async (addressBookId) => {
  mCachedAddressBookMetadata.delete(addressBookId);

  const db = await mPromisedDB;
  const transaction = db.transaction([
    STORE_ADDRESSBOOK_METADATA,
    STORE_CONTACT_METADATA,
    STORE_MAILING_LIST_METADATA,
    STORE_ENCRYPTED_FIELDS,
  ], 'readwrite');

  const addressBookMetadataStore = transaction.objectStore(STORE_ADDRESSBOOK_METADATA);
  const addressBookRequest = addressBookMetadataStore.delete(addressBookId);
  addressBookRequest.onsuccess = _event => {
    log('address book metadata successfully deleted');
  };
  addressBookRequest.onerror = event => {
    log('address book to delete contact metadata: ', event);
  };

  await Promise.all([
    (async () => {
      const contactMetadataStore = transaction.objectStore(STORE_CONTACT_METADATA);
      const deletedContactIds = await new Promise((resolve, reject) => {
        const keyRange = IDBKeyRange.bound(addressBookId, `${parent.id}\uffff`);
        const contactDeleteRequest = contactMetadataStore.index('addressBookToData').openCursor(keyRange);
        const deletedContactIds = [];
        contactDeleteRequest.onsuccess = event => {
          const result = event.target.result;
          if (result) {
            if (result.value) {
              const record = result.value;
              deletedContactIds.push(record.contactId);
              result.continue();
            }
          }
          else {
            log('contact metadata successfully deleted');
            resolve(deletedContactIds);
          }
        };
        contactDeleteRequest.onerror = event => {
          log('failed to delete contact metadata: ', event);
          reject();
        };
      });
      log('to be deleted contacts: ', deletedContactIds);
      await Promise.all(deletedContactIds.map(id => {
        const request = contactMetadataStore.delete(id);
        request.onsuccess = _event => {
          log('contact metadata successfully deleted');
        };
        request.onerror = event => {
          log('failed to delete contact metadata: ', event);
        };
      }));
    })(),
    (async () => {
      const mailingListMetadataStore = transaction.objectStore(STORE_MAILING_LIST_METADATA);
      const deletedMailingListIds = await new Promise((resolve, reject) => {
        const keyRange = IDBKeyRange.bound(addressBookId, `${parent.id}\uffff`);
        const mailingListDeleteRequest = mailingListMetadataStore.index('addressBookToData').openCursor(keyRange);
        const deletedMailingListIds = [];
        mailingListDeleteRequest.onsuccess = event => {
          const result = event.target.result;
          if (result) {
            if (result.value) {
              const record = result.value;
              deletedMailingListIds.push(record.mailingListId);
              result.continue();
            }
          }
          else {
            log('mailing list metadata successfully deleted');
            resolve(deletedMailingListIds);
          }
        };
        mailingListDeleteRequest.onerror = event => {
          log('failed to delete mailing list metadata: ', event);
          reject();
        };
      });
      log('to be deleted mailing lists: ', deletedMailingListIds);
      await Promise.all(deletedMailingListIds.map(id => {
        const request = mailingListMetadataStore.delete(id);
        request.onsuccess = _event => {
          log('contact metadata successfully deleted');
        };
        request.onerror = event => {
          log('failed to delete contact metadata: ', event);
        };
      }));
    })(),
    (async () => {
      const encryptedFieldsStore = transaction.objectStore(STORE_ENCRYPTED_FIELDS);
      const deletedIds = await new Promise((resolve, reject) => {
        const keyRange = IDBKeyRange.bound(addressBookId, `${addressBookId}\uffff`);
        const encryptedFieldsDeleteRequest = encryptedFieldsStore.index('groupToData').openCursor(keyRange);
        const deletedDataIds = [];
        encryptedFieldsDeleteRequest.onsuccess = event => {
          const result = event.target.result;
          if (result) {
            if (result.value) {
              const record = result.value;
              deletedDataIds.push(record.id);
              result.continue();
            }
          }
          else {
            log('encrypted fields successfully deleted');
            resolve(deletedDataIds);
          }
        };
        encryptedFieldsDeleteRequest.onerror = event => {
          log('failed to delete encrypted fields: ', event);
          reject();
        };
      });
      log('to be deleted encrypted fields: ', deletedIds);
      await Promise.all(deletedIds.map(id => {
        mCachedEncryptedFields.delete(id);
        const request = encryptedFieldsStore.delete(id);
        request.onsuccess = _event => {
          log('encrypted fields successfully deleted');
        };
        request.onerror = event => {
          log('failed to delete encrypted fields: ', event);
        };
      }));
    })(),
  ]);
});

browser.contacts.onDeleted.addListener(async (_parentId, contactId) => {
  mCachedEncryptedFields.delete(contactId);

  const db = await mPromisedDB;
  const transaction = db.transaction([
    STORE_CONTACT_METADATA,
    STORE_ENCRYPTED_FIELDS,
  ], 'readwrite');

  const contactsMetadataStore = transaction.objectStore(STORE_CONTACT_METADATA);
  const contactsMetadataDeleteRequest = contactsMetadataStore.delete(contactId);
  contactsMetadataDeleteRequest.onsuccess = _event => {
    log('contact metadata successfully deleted');
  };
  contactsMetadataDeleteRequest.onerror = event => {
    log('failed to delete contact metadata: ', event);
  };

  const encryptedFieldsStore = transaction.objectStore(STORE_ENCRYPTED_FIELDS);
  const encryptedFieldsRequest = encryptedFieldsStore.delete(contactId);
  encryptedFieldsRequest.onsuccess = _event => {
    log('encrypted fields successfully deleted');
  };
  encryptedFieldsRequest.onerror = event => {
    log('failed to delete encrypted fields: ', event);
  };
});

browser.mailingLists.onDeleted.addListener(async (_parentId, mailingListId) => {
  const db = await mPromisedDB;
  const transaction = db.transaction([
    STORE_MAILING_LIST_METADATA,
    STORE_ENCRYPTED_FIELDS,
  ], 'readwrite');

  const mailingListMetadataStore = transaction.objectStore(STORE_MAILING_LIST_METADATA);
  const mailingListMetadataDeleteRequest = mailingListMetadataStore.delete(mailingListId);
  mailingListMetadataDeleteRequest.onsuccess = _event => {
    log('mailing list metadata successfully deleted');
  };
  mailingListMetadataDeleteRequest.onerror = event => {
    log('failed to delete mailing list: ', event);
  };

  const encryptedFieldsStore = transaction.objectStore(STORE_ENCRYPTED_FIELDS);
  const encryptedFieldsRequest = encryptedFieldsStore.delete(mailingListId);
  encryptedFieldsRequest.onsuccess = _event => {
    log('encrypted fields successfully deleted');
  };
  encryptedFieldsRequest.onerror = event => {
    log('failed to delete encrypted fields: ', event);
  };
});
