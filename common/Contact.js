/*
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
*/
'use strict';

import EventListenerManager from '/extlib/EventListenerManager.js';

import {
  clone,
  configs,
  /* for fields encryption support
  wait,
  */
  log,
} from '/common/common.js';
import { ChatProvider } from '/common/ChatProvider.js';
import * as Constants from '/common/constants.js';
import { CustomPersonalField, CustomOrganizationField } from '/common/CustomField.js';
import * as DB from '/common/db.js';
import * as VCard from '/common/import/vcard.js';

const KNOWN_CONTACT_FIELDS = new Set([
  'FirstName',
  'LastName',
  'PhoneticFirstName',
  'PhoneticLastName',
  'DisplayName',
  'SpouseName',
  'FamilyName',
  'NickName',
  'PrimaryEmail',
  'SecondEmail',
  'ChatName',
  'PreferMailFormat',
  'WorkPhone',
  'HomePhone',
  'FaxNumber',
  'PagerNumber',
  'CellularNumber',
  'HomeCountry',
  'HomeZipCode',
  'HomeState',
  'HomeCity',
  'HomeAddress',
  'HomeAddress2',
  'WebPage2',
  'BirthYear',
  'BirthMonth',
  'BirthDay',
  'AnniversaryYear',
  'AnniversaryMonth',
  'AnniversaryDay',
  'JobTitle',
  'Company',
  'Department',
  'WorkCountry',
  'WorkZipCode',
  'WorkState',
  'WorkCity',
  'WorkAddress',
  'WorkAddress2',
  'WebPage1',
  'Other',
  'Custom1',
  'Custom2',
  'Custom3',
  'Custom4',
  'Notes',
  ...ChatProvider.BUILTIN_PROVIDERS.map(provider => provider.field),
  'Photo',
]);

const KNOWN_CONTACT_VIRTUAL_FIELDS = new Set([
  'NameField1',
  'NameField2',
  'PhoneticNameField1',
  'PhoneticNameField2',
]);

const KNOWN_CONTACT_EXTRA_FIELDS = new Set([
  'userDefinedDisplayName',
  'namePrefix',
  'nameSuffix',
  'gender',
  'birth',
  'org',
  'categories',
  'photoURI',
  'logoSourceURI',
  'logoURI',
  'soundSourceURI',
  'soundURI',
  'etag',
]);

const NO_SEARCH_CONTACT_FIELDS = new Set([
  'PreferMailFormat',
  'etag',
  Constants.CONTACT_EXTRA_FIELDS_STORE,
]);

const KNOWN_MAILING_LIST_FIELDS = new Set([
  'parentId',
  'name',
  'description',
  'nickName',
]);

const DEFAULT_DISPLAY_NAME_FORMAT = '{{DisplayName}}';
/* for fields encryption support
const EXTRA_STATUS_SAVING = 'saving';
*/


export class Item {
  constructor(raw) {
    this.$raw = raw;
    this.$dirtyProperties = new Set();
    this.$index = -1;
    /* for fields encryption support
    this.$decryptedFields = {};
    */
    this.initialized = Promise.resolve(true);
    this.invalidateCache();
  }

  get canBecomeMember() {
    return false;
  }

  get mergable() {
    return false;
  }

  destroy() {
    this.$raw = { id: this.$raw.id };
  }

  async apply(item) {
    if (item.id && this.$raw.id && item.id != this.$raw.id)
      throw new Error('cannot apply item information of different item');
    await this.initialized;
    const updated = {};
    for (const [field, value] of Object.entries(item.properties)) {
      if (value != this[field]) {
        this[field] = value;
        updated[field] = value;
        this.$dirtyProperties.delete(field);
      }
    }
    this.invalidateCache();
    this.addToAvailableCategories();
    return updated;
  }

  invalidateCache() {
    this.invalidateExportedCache();
  }

  invalidateExportedCache() {
    this.$exported = undefined;
    this.$exportedKeys = undefined;
  }

  get raw() {
    return this.$raw;
  }

  get id() {
    return this.$raw.id;
  }

  get type() {
    return this.$raw.type;
  }

  get index() {
    return this.$index;
  }
  set index(newValue) {
    if (newValue == this.$index)
      return newValue;
    return this.$index = newValue;
  }

  get visibleIndex() {
    return this.index + 1;
  }
  set visibleIndex(newValue) {
    return newValue;
  }

  get label() {
    return '';
  }
  set label(value) {
    return this.label;
  }

  get email() {
    return '';
  }
  set email(value) {
    return this.email;
  }

  get address() {
    return '';
  }
  set address(value) {
    return this.address;
  }

  get url() {
    return '';
  }
  set url(value) {
    return this.url;
  }

  get backgroundColor() {
    return Item.addressBookBackgroundColors.get(this.parentId);
  }

  set backgroundColor(newValue) {
    return newValue;
  }

  get textColor() {
    return Item.addressBookTextColors.get(this.parentId);
  }

  set textColor(newValue) {
    return newValue;
  }

  get vCardFileName() {
    return `${this.id}.${VCard.SUFFIX}`;
  }

  async getAddressBookMetadata() {
    const metadata = (await DB.getAddressBookMetadata({ id: this.parentId })) || {};
    return metadata;
  }

  get dirty() {
    return this.$dirtyProperties.size > 0;
  }

  addToAvailableCategories() {
    for (const category of this.categories) {
      Item.availableCategories.add(category);
      Item.onAvailableCategoryAdded.dispatch(category);
    }
  }

  export(properties) {
    if (!properties)
      properties = Constants.ITEM_FIELDS;
    const exportedKeys = properties.join('\n');
    if (this.$exported && this.$exportedKeys == exportedKeys)
      return this.$exported;

    this.$exportedKeys = exportedKeys;
    const exported = {};
    for (const property of properties) {
      if (property in this)
        exported[property] = this[property];
    }
    return this.$exported = exported;
  }
}

Item.addressBookBackgroundColors = new Map();
Item.addressBookTextColors = new Map();

Item.setAddressBookColor = (id, color) => {
  if (!color) {
    Item.addressBookBackgroundColors.delete(id);
    Item.addressBookTextColors.delete(id);
    return;
  }
  Item.addressBookBackgroundColors.set(id, color);
  Item.addressBookTextColors.set(id, getReadableTextColorFor(color));
};

// https://stackoverflow.com/questions/3116260/given-a-background-color-how-to-get-a-foreground-color-that-makes-it-readable-o
function getReadableTextColorFor(hexColor) {
  const matched = hexColor.match(/^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i);
  const [, r, g, b] = matched;
  const luminance = (0.2126 * parseInt(r, 16)) + (0.7152 * parseInt(g, 16)) + (0.0722 * parseInt(b, 16));
  return luminance < 140 ? '#ffffff' : '#000000';
}

Item.availableCategories = new Set();
Item.onAvailableCategoryAdded = new EventListenerManager();

/* for fields encryption support
Item.unencryptedContactFields = new Set();
*/


export class Contact extends Item {
  constructor(raw) {
    raw.type = 'contact'; // for newly created instance
    super(raw);

    if (raw.id)
      Contact.instances.set(raw.id, this);

    this.$promisedFieldUpdates = new Set();

    for (const key of KNOWN_CONTACT_FIELDS) {
      if (key in this)
        continue;
      Object.defineProperty(this, key, {
        enumerable: true,
        configurable: true,
        get() {
          return (
            this.$raw &&
            this.$raw.properties &&
            this.$raw.properties[key]
          );
        },
        set(newValue) {
          if (!this.$raw ||
              (this.$raw.properties &&
               newValue != this.$raw.properties[key]))
            this.$dirtyProperties.add(key);
          if (this.$raw &&
              this.$raw.properties)
            this.$raw.properties[key] = newValue;
          this.invalidateCacheFor(key);
          return newValue;
        },
      });
    }

    for (const key of Object.keys(raw)) {
      if (key in this)
        continue;
      Object.defineProperty(this, key, {
        enumerable: true,
        configurable: true,
        get() {
          return (
            this.$raw &&
            this.$raw[key]
          );
        },
        set(newValue) {
          if (!this.$raw ||
              newValue != this.$raw[key])
            this.$dirtyProperties.add(key);
          if (this.$raw)
            this.$raw[key] = newValue;
          return newValue;
        },
      });
    }

    /* for fields encryption support
    if (this.id && !this.isSaving) {
    */
    if (this.id) {
      this.initialized = this.initialized.then(async () => {
        /* for fields encryption support
      this.initialized = Promise.all([
        this.getAddressBookMetadata(),
        this.initialized,
      ]).then(async ([metadata]) => {
        if (metadata.encrypted) {
          await this.loadDecryptedFields();
          for (const [field, value] of Object.entries(this.$decryptedFields)) {
            this.$raw.properties[field] = value;
          }
        }
        */
        await this.updateDisplayName();
        if (this.dirty)
          await this.save();
        this.addToAvailableCategories();
        return true;
      });
    }
    else {
      this.addToAvailableCategories();
    }
  }

  get canBecomeMember() {
    return true;
  }

  get mergable() {
    return true;
  }

  destroy() {
    super.destroy();
    Contact.instances.delete(this.$raw.id)
  }

  async apply(item) {
    await this.initialized;

    const oldValues = {
      visibleDisplayName: this.visibleDisplayName,
      label:       this.label,
      email:       this.email,
      url:         this.url,
      address:     this.address,
      availableChatProviderIds: this.availableChatProviderIds,
    };

    const updated = await super.apply(item);

    const nameField1 = browser.i18n.getMessage('fieldForNameField1');
    if (nameField1 in updated)
      updated.NameField1 = updated[nameField1];

    const nameField2 = browser.i18n.getMessage('fieldForNameField2');
    if (nameField2 in updated)
      updated.NameField2 = updated[nameField2];

    if (this.visibleDisplayName != oldValues.visibleDisplayName)
      updated.visibleDisplayName = this.visibleDisplayName;
    if (this.label != oldValues.label)
      updated.label = this.label;
    if (this.email != oldValues.email)
      updated.email = this.email;
    if (this.url != oldValues.url)
      updated.url = this.url;
    if (this.address != oldValues.address)
      updated.address = this.address;
    if (this.availableChatProviderIds.join('\n') != oldValues.availableChatProviderIds.join('\n'))
      updated.availableChatProviderIds = this.availableChatProviderIds;

    this.invalidateCache();
    return updated;
  }

  invalidateCache() {
    super.invalidateCache();
    this.$gender = undefined;
    this.$birthDate = undefined;
    this.$birthDay = undefined;
    this.$deathDate = undefined;
    this.$deathDay = undefined;
    this.$anniversaryDate = undefined;
    this.$anniversaryDay = undefined;
    this.$organizations = undefined;
    this.$organizationId = undefined;
    this.$categories = undefined;
    this.$displayWorkAddress = undefined;
    this.$displayHomeAddress = undefined;
    this.$vCard = undefined;
    this.$searchString = undefined;
    this.$availableChatProviderIds = undefined;
  }

  invalidateCacheFor(field) {
    switch (field) {
      case 'WorkAddress':
      case 'WorkAddress2':
      case 'WorkCity':
      case 'WorkState':
      case 'WorkZipCode':
      case 'WorkCountry':
        this.$displayWorkAddress = undefined;
        this.$vCard = undefined;
        this.$searchString = undefined;
        break;

      case 'HomeAddress':
      case 'HomeAddress2':
      case 'HomeCity':
      case 'HomeState':
      case 'HomeZipCode':
      case 'HomeCountry':
        this.$displayHomeAddress = undefined;
        this.$vCard = undefined;
        this.$searchString = undefined;
        break;

      case 'BirthYear':
      case 'BirthMonth':
      case 'BirthDay':
        this.$birthDate = undefined;
        this.$birthDay = undefined;
        this.$vCard = undefined;
        this.$searchString = undefined;
        break;

      case 'AnniversaryYear':
      case 'AnniversaryMonth':
      case 'AnniversaryDay':
        this.$anniversaryDate = undefined;
        this.$anniversaryDay = undefined;
        this.$vCard = undefined;
        this.$searchString = undefined;
        break;

      case '_GoogleTalk':
      case '_AimScreenName':
      case '_Yahoo':
      case '_Skype':
      case '_QQ':
      case '_MSN':
      case '_ICQ':
      case '_JabberId':
      case '_IRC':
        this.$searchString = undefined;
        this.$availableChatProviderIds = undefined;
        break;

      case Constants.CONTACT_EXTRA_FIELDS_STORE:
        this.$extra = undefined;
        this.$deathDate = undefined;
        this.$deathDay = undefined;
        this.$organizations = undefined;
        this.$organizationId = undefined;
        this.$categories = undefined;
        this.$vCard = undefined;
        this.$searchString = undefined;
        this.$availableChatProviderIds = undefined;
        break;

      default:
        this.$vCard = undefined;
        this.$searchString = undefined;
        break;
    }
    this.invalidateExportedCache();
  }

  get label() {
    const organization = this.organizations && this.organizations[this.organizations.length - 1] ||
      this.Department ||
      this.Organization;
    const displayName = this.visibleDisplayName;
    const label = [];
    if (organization)
      label.push(organization);
    if (displayName)
      label.push(displayName);
    if (label.length == 0 &&
        (this.PrimaryEmail ||
         this.SecondEmail))
      label.push(this.PrimaryEmail || this.SecondEmail);
    return label.join(' ');
  }
  set label(value) {
    return this.label;
  }

  get email() {
    return (this.PrimaryEmail || this.SecondEmail || '').trim();
  }
  set email(value) {
    return this.email;
  }

  get address() {
    return (this.displayWorkAddress || this.displayHomeAddress || '').trim();
  }
  set address(value) {
    return this.address;
  }

  get url() {
    return (this.WebPage1 || this.WebPage2 || '').trim();
  }
  set url(value) {
    return this.url;
  }

  get NameField1() {
    return this[browser.i18n.getMessage('fieldForNameField1')];
  }

  set NameField1(newValue) {
    return this[browser.i18n.getMessage('fieldForNameField1')] = newValue;
  }

  get NameField2() {
    return this[browser.i18n.getMessage('fieldForNameField2')];
  }

  set NameField2(newValue) {
    return this[browser.i18n.getMessage('fieldForNameField2')] = newValue;
  }

  get PhoneticNameField1() {
    return this[`Phonetic${browser.i18n.getMessage('fieldForNameField1')}`];
  }

  set PhoneticNameField1(newValue) {
    return this[`Phonetic${browser.i18n.getMessage('fieldForNameField1')}`] = newValue;
  }

  get PhoneticNameField2() {
    return this[`Phonetic${browser.i18n.getMessage('fieldForNameField2')}`];
  }

  set PhoneticNameField2(newValue) {
    return this[`Phonetic${browser.i18n.getMessage('fieldForNameField2')}`] = newValue;
  }

  get generatedDisplayName() {
    const displayName = [];
    if (this.NameField1)
      displayName.push(this.NameField1);
    if (this.NameField2)
      displayName.push(this.NameField2);
    return displayName.join(' ');
  }

  get userDefinedDisplayName() {
    return this.getFieldValue('extra.userDefinedDisplayName') || '';
  }

  set userDefinedDisplayName(newValue) {
    if (this.userDefinedDisplayName != newValue) {
      this.setFieldValue('extra.userDefinedDisplayName', newValue);
      this.updateExtraFields();
    }
    return newValue;
  }

  get hasUserDefinedDisplayName() {
    return this.visibleDisplayName != this.generatedDisplayName;
  }

  get visibleDisplayName() {
    return this.userDefinedDisplayName || this.generatedDisplayName;
  }

  set visibleDisplayName(newValue) {
    if (!this.$raw.properties)
      return newValue;
    if (newValue != this.visibleDisplayName)
      this.userDefinedDisplayName = newValue;
    return newValue;
  }

  get namePrefix() {
    return this.getFieldValue('extra.namePrefix');
  }

  set namePrefix(newValue) {
    if (this.namePrefix != newValue) {
      this.setFieldValue('extra.namePrefix', newValue);
      this.updateExtraFields();
    }
    return newValue;
  }

  get nameSuffix() {
    return this.getFieldValue('extra.nameSuffix');
  }

  set nameSuffix(newValue) {
    if (this.nameSuffix != newValue) {
      this.setFieldValue('extra.nameSuffix', newValue);
      this.updateExtraFields();
    }
    return newValue;
  }

  get birthDate() {
    if (this.$birthDate !== undefined)
      return this.$birthDate;

    if (this.BirthYear) {
      const date = new Date(0);
      date.setFullYear(this.BirthYear);
      date.setMonth(this.BirthMonth ? Math.max(0, parseInt(this.BirthMonth) - 1) : 0);
      date.setDate(this.BirthDay || 1);
      return this.$birthDate = date;
    }
    else {
      return this.$birthDate = null;
    }
  }

  get birthDay() {
    if (this.$birthDay !== undefined)
      return this.$birthDay;

    if (!this.birthDate)
      return this.$birthDay = null;

    return this.$birthDay = [
      this.birthDate.getFullYear(),
      (this.birthDate.getMonth() + 1).toString().padStart(2, '0'),
      this.birthDate.getDate().toString().padStart(2, '0'),
    ].join('-');
  }

  set birthDay(newValue) {
    if (newValue == this.birthDay)
      return newValue;

    const matched = String(newValue).match(/^(\d+)-(\d{1,2})-(\d{1,2})$/);
    if (matched) {
      const [, year, month, day] = matched;
      this.BirthYear  = year;
      this.BirthMonth = parseInt(month).toString().padStart(2, '0');
      this.BirthDay   = parseInt(day).toString().padStart(2, '0');
      this.$dirtyProperties.add('BirthYear');
      this.$dirtyProperties.add('BirthMonth');
      this.$dirtyProperties.add('BirthDay');
    }
    return newValue;
  }

  get deathDate() {
    if (this.$deathDate !== undefined)
      return this.$deathDate;

    const deathDate = this.getFieldValue('extra.x-death-date');
    if (deathDate) {
      try {
        const date = new Date(deathDate);
        return this.$deathDate = date;
      }
      catch(error) {
        console.log('failed to parse x-death-date: ', deathDate, error);
      }
    }
    return this.$deathDate = null;
  }

  get deathDay() {
    if (this.$deathDay !== undefined)
      return this.$deathDay;

    if (!this.deathDate)
      return this.$deathDay = null;

    return this.$deathDay = [
      this.deathDate.getFullYear(),
      (this.deathDate.getMonth() + 1).toString().padStart(2, '0'),
      this.deathDate.getDate().toString().padStart(2, '0'),
    ].join('-');
  }

  set deathDay(newValue) {
    if (newValue == this.deathDay)
      return newValue;

    if (/^(\d+)-(\d{1,2})-(\d{1,2})$/.test(String(newValue))) {
      this.setFieldValue('extra.x-death-date', newValue);
      this.updateExtraFields();
    }
    return newValue;
  }

  get anniversaryDate() {
    if (this.$anniversaryDate !== undefined)
      return this.$anniversaryDate;

    if (this.AnniversaryYear) {
      const date = new Date(0);
      date.setFullYear(this.AnniversaryYear);
      date.setMonth(this.AnniversaryMonth ? Math.max(0, parseInt(this.AnniversaryMonth) - 1) : 0);
      date.setDate(this.AnniversaryDay || 1);
      return this.$anniversaryDate = date;
    }
    else {
      return this.$anniversaryDate = null;
    }
  }

  get anniversaryDay() {
    if (this.$anniversaryDay !== undefined)
      return this.$anniversaryDay;

    if (!this.anniversaryDate)
      return this.$anniversaryDay = null;

    return this.$anniversaryDay = [
      this.anniversaryDate.getFullYear(),
      (this.anniversaryDate.getMonth() + 1).toString().padStart(2, '0'),
      this.anniversaryDate.getDate().toString().padStart(2, '0'),
    ].join('-');
  }

  set anniversaryDay(newValue) {
    if (newValue == this.anniversaryDay)
      return newValue;

    const matched = String(newValue).match(/^(\d+)-(\d{1,2})-(\d{1,2})$/);
    if (matched) {
      const [, year, month, day] = matched;
      this.AnniversaryYear  = year;
      this.AnniversaryMonth = parseInt(month).toString().padStart(2, '0');
      this.AnniversaryDay   = parseInt(day).toString().padStart(2, '0');
      this.$dirtyProperties.add('AnniversaryYear');
      this.$dirtyProperties.add('AnniversaryMonth');
      this.$dirtyProperties.add('AnniversaryDay');
    }
    return newValue;
  }

  get genderCode() {
    return this.getFieldValue('extra.gender');
  }

  set genderCode(newValue) {
    if (this.getFieldValue('extra.gender') != newValue) {
      this.setFieldValue('extra.gender', newValue);
      this.$gender = undefined;
      this.updateExtraFields();
    }
    return newValue;
  }

  get gender() {
    if (this.$gender !== undefined)
      return this.$gender;
    if (!this.genderCode)
      return '';
    return this.$gender = browser.i18n.getMessage(`contact_field_gender_${this.genderCode}`) || '';
  }

  set gender(newValue) { // just for Tabulator
    return newValue;
  }

  get etag() {
    return this.getFieldValue('extra.etag');
  }

  set etag(newValue) {
    if (this.getFieldValue('extra.etag') != newValue) {
      this.setFieldValue('extra.etag', newValue);
      this.updateExtraFields();
    }
    return newValue;
  }

  get extra() {
    if (this.$extra)
      return this.$extra;

    try {
      this.$extra = JSON.parse(
        /* for fields encryption support
        this.$decryptedFields[Constants.CONTACT_EXTRA_FIELDS_STORE] ||
        */
        this[Constants.CONTACT_EXTRA_FIELDS_STORE] ||
        '{}'
      );
    }
    catch(error) {
      console.error(error);
      this.$extra = {};
    }
    return this.$extra;
  }

  updateExtraFields() {
    const oldFields = this[Constants.CONTACT_EXTRA_FIELDS_STORE];
    const currentFields = JSON.stringify(this.extra);
    if (oldFields == currentFields)
      return;
    log('updateExtraFields ', oldFields + '\n=>\n' + currentFields, new Error().stack);
    this[Constants.CONTACT_EXTRA_FIELDS_STORE] = currentFields;
  }

  /* for fields encryption support
  get extraStatus() {
    return this[Constants.CONTACT_EXTRA_STATUS_STORE];
  }

  set extraStatus(newValue) {
    this[Constants.CONTACT_EXTRA_STATUS_STORE] = newValue;
    if (this.id) {
      const updates = {};
      updates[Constants.CONTACT_EXTRA_STATUS_STORE] = newValue;
      this.$promisedFieldUpdates.add(browser.contacts.update(this.id, updates));
    }
    return newValue;
  }

  get isSaving() {
    return this.extraStatus == EXTRA_STATUS_SAVING;
  }
  */

  async getDisplayNameFormat() {
    const metadata = await this.getAddressBookMetadata();
    return (metadata.customDisplayNameFormat || configs.defaultCustomDisplayNameFormat).trim() || DEFAULT_DISPLAY_NAME_FORMAT;
  }

  async updateDisplayName() {
    const updated = {};
    const originalDisplayName = this.DisplayName;
    if (!this.userDefinedDisplayName &&
        originalDisplayName) {
      // backup original value
      this.userDefinedDisplayName = originalDisplayName;
      this.updateExtraFields();
      updated.userDefined = originalDisplayName;
      log('backup originalDisplayName as userDefinedDisplayName: ', this.id, originalDisplayName);
    }
    const format = await this.getDisplayNameFormat();
    const exposedDisplayName = this.fillDisplayNameFormat(format);
    if (originalDisplayName != exposedDisplayName) {
      this.DisplayName = exposedDisplayName;
      updated.exposed = originalDisplayName;
    }
    if (originalDisplayName == exposedDisplayName &&
        /\{\{(visible)?DisplayNamm\}\}/.test(format)) {
      const additionalPart = this.fillDisplayNameFormat(format.replace(/\{\{(visible)?DisplayNamm\}\}/g, ''));
      if (additionalPart) {
        // restore original display name
        this.userDefinedDisplayName = exposedDisplayName.replace(
          additionalPart,
          ''
        );
        this.updateExtraFields();
        updated.restored = this.userDefinedDisplayName;
      }
    }
    if (Object.keys(updated) > 0) {
      log('updated display name: ', this.id, {
        original: originalDisplayName,
        ...updated,
      });
    }
  }
  fillDisplayNameFormat(format) {
    return format.replace(/\{\{([^\}]+)\}\}/g, (matched, field) => {
      if (field == 'DisplayName')
        return this.visibleDisplayName || '';
      return this.getFieldValue(field) || '';
    }).trim();
  }

  /* for fields encryption support
  async loadDecryptedFields() {
    const loadedFields = this.id && await DB.getEncryptedFields(this.id).catch(_error => null);
    // Fields can be updated while loading data from the DB.
    // We need to merge them recursively.
    const extra = Constants.CONTACT_EXTRA_FIELDS_STORE;
    if (this.$decryptedFields[extra]) {
      const loadedExtraFields = loadedFields && loadedFields[extra] && JSON.parse(loadedFields[extra]);
      this.$decryptedFields[extra] = JSON.stringify({
        ...(loadedExtraFields || {}),
        ...JSON.parse(this.$decryptedFields[extra]),
      });
    }
    return this.$decryptedFields = {
      ...(loadedFields || {}),
      ...this.$decryptedFields,
    };
  }
  */

  async save() {
    await this.updateDisplayName();

    const promisedUpdates = Array.from(this.$promisedFieldUpdates);
    this.$promisedFieldUpdates.clear();
    await Promise.all(promisedUpdates);

    this.updateExtraFields();
    const shouldCreate = !this.id || !(await browser.contacts.get(this.id).catch(_error => false));
    if (!shouldCreate && !this.dirty)
      return;

    if (!shouldCreate && this.$dirtyProperties.has('parentId'))
      return this.moveTo(this.parentId);

    /* for fields encryption support
    while (this.isSaving) {
      await wait(100);
    }

    this.extraStatus = EXTRA_STATUS_SAVING;
    */

    let updates;
    if (shouldCreate) {
      updates = { ...this.$raw.properties };
    }
    else {
      updates = {};
      for (const field of this.$dirtyProperties) {
        if (field in this.$raw.properties)
          updates[field] = this.$raw.properties[field];
      }
    }
    this.$dirtyProperties.clear();

    const promises = [];

    /* for fields encryption support
    const metadata = await this.getAddressBookMetadata();
    const updatedEncryptedFields = new Set();
    if (metadata.encrypted) {
      for (const [field, value] of Object.entries(updates)) {
        if (Item.unencryptedContactFields.has(field) ||
            field == Constants.CONTACT_EXTRA_STATUS_STORE)
          continue;
        delete updates[field];
        this.$decryptedFields[field] = value;
        updatedEncryptedFields.add(field);
      }
    }

    let created = false;
    */
    if (!shouldCreate) {
      log('Updating contact: ', this.id, updates);
      if (Object.keys(updates).length > 0)
        promises.push(browser.contacts.update(this.id, updates));
    }
    else {
      log('Creating contact: ', updates);
      promises.push((async () => {
        try {
          const createdId = await (this.id ?
            browser.contacts.create(this.parentId, this.id, updates) :
            browser.contacts.create(this.parentId, updates)
          );
          log('Created as: ', createdId);
          this.$raw = await browser.contacts.get(createdId);
          this.invalidateCache();
          /* for fields encryption support
          created = true;
          */
        }
        catch(error) {
          console.error(error);
        }
      })())
    }

    /* for fields encryption support
    if (metadata.encrypted &&
        (shouldCreate ||
         updatedEncryptedFields.size > 0)) {
      if (!this.id) // we need to wait id is generated for a new contact
        await Promise.all(promises);
      await this.loadDecryptedFields();
      log('Saving encrypted fields: ', this.id, this.$decryptedFields, { shouldCreate, updatedEncryptedFields });
      promises.push(DB.setEncryptedFields({
        id:       this.id,
        parentId: this.parentId,
        fields:   this.$decryptedFields,
      }));
      if (created) {
        Contact.waitUntilCreated(this.id).then(async () => {
          const created = Contact.get(this.id);
          const updated = await created.apply({ properties: this.$decryptedFields });
          Contact.onUpdated.dispatch(this.id, updated, created);
        });
      }
    }
    */

    await Promise.all(promises);

    /* for fields encryption support
    this.extraStatus = '';
    */
  }

  async moveTo(addressBookId) {
    const currentParentId = (await browser.contacts.get(this.id)).parentId;
    if (addressBookId == currentParentId)
      return;
    const id = this.id;
    const properties = clone(this.$raw.properties);
    await browser.contacts.delete(id);

    const moved = new Contact({
      id,
      parentId: addressBookId,
      properties,
    });
    await moved.save();
    await Contact.waitUntilCreated(moved.id);
    return Contact.get(moved.id);
  }

  async copyTo(addressBookId) {
    const properties = clone(this.$raw.properties);
    const copied = new Contact({
      parentId: addressBookId,
      properties,
    });
    await copied.save();
    await Contact.waitUntilCreated(copied.id);
    return Contact.get(copied.id);
  }

  get internalCategories() {
    return (this.organizations.length == 0 && this.categories.length == 0) ?
      [Constants.UNCATEGORIZED_ID] :
      this.categories;
  }

  set internalCategories(newValue) {
    return newValue;
  }

  get org() {
    return this.organizations;
  }

  set org(newValue) {
    return this.organizations = newValue;
  }

  get organizations() {
    if (!this.$organizations)
      this.initOrganizations();
    return this.$organizations;
  }

  set organizations(newValue) {
    const joinedValue = (Array.isArray(newValue) ?
      newValue.join(VCard.VALUE_SEPARATOR) :
      String(newValue)
    ).replace(VCard.TRAILING_BLANK_ORGANIZATIONS_SANITIZER, '');
    if (joinedValue == this.organizations.join(VCard.VALUE_SEPARATOR))
      return newValue;
    this.setFieldValue('extra.org', joinedValue.split(VCard.VALUE_SEPARATOR));
    this.updateExtraFields();
    this.initOrganizations();
    return newValue;
  }

  get organizationId() {
    if (!this.$organizationId)
      this.initOrganizations();
    return this.$organizationId;
  }

  initOrganizations() {
    const organizations = [];
    const rawOrganizations = this.getFieldValue('extra.org');
    if (Array.isArray(rawOrganizations) && rawOrganizations.length > 0) {
      // clear blank fields at end
      while (rawOrganizations.length > 0 && !rawOrganizations[rawOrganizations.length - 1]) {
        rawOrganizations.pop();
      }
      organizations.push(...rawOrganizations);
    }
    else {
      if (this.Company)
        organizations.push(...this.Company.split(VCard.VALUE_SEPARATOR));
      if (this.Department)
        organizations.push(...this.Department.split(VCard.VALUE_SEPARATOR));
    }

    this.$organizations = organizations;
    this.$organizationId = `${this.$raw.parentId}${VCard.VALUE_SEPARATOR}organization${VCard.VALUE_SEPARATOR}${this.organizations.join(VCard.VALUE_SEPARATOR)}`;
  }

  get categories() {
    if (!this.$categories)
      this.$categories = this.getFieldValue('extra.categories') || [];
    return this.$categories;
  }

  set categories(newValue) {
    const joinedValue = Array.isArray(newValue) ? newValue.join(VCard.CATEGORIES_SEPARATOR) : String(newValue);
    if (joinedValue == this.categories.join(VCard.CATEGORIES_SEPARATOR))
      return newValue;
    const categories = joinedValue.split(VCard.CATEGORIES_SEPARATOR).filter(category => !!category);
    this.$categories = categories;
    this.setFieldValue('extra.categories', categories);
    this.addToAvailableCategories();
    this.updateExtraFields();
    return newValue;
  }

  get displayWorkAddress() {
    if (this.$displayWorkAddress !== undefined)
      return this.$displayWorkAddress;

    const displayWorkAddress = [];
    if (browser.i18n.getMessage('displayAddressFormat') == Constants.ADDRESS_FORMAT_TOP_DOWN) {
      if (this.WorkCountry)
        displayWorkAddress.push(this.WorkCountry);
      if (this.WorkZipCode)
        displayWorkAddress.push(this.WorkZipCode);
      if (this.WorkState)
        displayWorkAddress.push(this.WorkState);
      if (this.WorkCity)
        displayWorkAddress.push(this.WorkCity);
      if (this.WorkAddress)
        displayWorkAddress.push(this.WorkAddress);
      if (this.WorkAddress2)
        displayWorkAddress.push(this.WorkAddress2);
    }
    else {
      if (this.WorkAddress)
        displayWorkAddress.push(this.WorkAddress);
      if (this.WorkAddress2)
        displayWorkAddress.push(this.WorkAddress2);
      if (this.WorkCity)
        displayWorkAddress.push(this.WorkCity);
      if (this.WorkState)
        displayWorkAddress.push(this.WorkState);
      if (this.WorkZipCode)
        displayWorkAddress.push(this.WorkZipCode);
      if (this.WorkCountry)
        displayWorkAddress.push(this.WorkCountry);
    }
    return this.$displayWorkAddress = displayWorkAddress.join(' ');
  }

  set displayWorkAddress(newValue) {
    return newValue;
  }

  get displayHomeAddress() {
    if (this.$displayHomeAddress !== undefined)
      return this.$displayHomeAddress;

    const displayHomeAddress = [];
    if (browser.i18n.getMessage('displayAddressFormat') == Constants.ADDRESS_FORMAT_TOP_DOWN) {
      if (this.HomeCountry)
        displayHomeAddress.push(this.HomeCountry);
      if (this.HomeZipCode)
        displayHomeAddress.push(this.HomeZipCode);
      if (this.HomeState)
        displayHomeAddress.push(this.HomeState);
      if (this.HomeCity)
        displayHomeAddress.push(this.HomeCity);
      if (this.HomeAddress)
        displayHomeAddress.push(this.HomeAddress);
      if (this.HomeAddress2)
        displayHomeAddress.push(this.HomeAddress2);
    }
    else {
      if (this.HomeAddress)
        displayHomeAddress.push(this.HomeAddress);
      if (this.HomeAddress2)
        displayHomeAddress.push(this.HomeAddress2);
      if (this.HomeCity)
        displayHomeAddress.push(this.HomeCity);
      if (this.HomeState)
        displayHomeAddress.push(this.HomeState);
      if (this.HomeZipCode)
        displayHomeAddress.push(this.HomeZipCode);
      if (this.HomeCountry)
        displayHomeAddress.push(this.HomeCountry);
    }
    return this.$displayHomeAddress = displayHomeAddress.join(' ');
  }

  set displayHomeAddress(newValue) {
    return newValue;
  }

  get sortString() {
    return this.getFieldValue('extra.sort-string') || '';
  }

  set sortString(newValue) {
    return newValue;
  }

  getFieldValue(name) {
    const parts = name.split('.');
    if (parts.length > 1) {
      let slot = this;
      const lastKey = parts.pop();
      for (const key of parts) {
        if (!slot)
          return null;
        slot = slot[key] || null;
      }
      if (slot == this.extra && !KNOWN_CONTACT_EXTRA_FIELDS.has(lastKey))
        return Array.isArray(slot[lastKey]) ? slot[lastKey][0] : slot[lastKey];
      return slot[lastKey];
    }
    else {
      return this[name];
    }
  }

  setFieldValue(name, value) {
    const parts = name.split('.');
    if (parts.length > 1) {
      let slot = this;
      const lastKey = parts.pop();
      for (const key of parts) {
        if (!slot)
          break
        slot = slot[key];
      }
      if (slot) {
        if (slot == this.extra && !KNOWN_CONTACT_EXTRA_FIELDS.has(lastKey))
          slot[lastKey] = [value];
        else
          slot[lastKey] = value;
        return value;
      }
      throw new Error(`${name} is not accessible`);
    }
    else {
      return this[name] = value;
    }
  }

  get vCard() {
    if (this.$vCard)
      return this.$vCard;

    const vcard = [];

    if (this.DisplayName)
      vcard.push(`FN:${this.userDefinedDisplayName}`);

    const additionalName = [];
    if (this.SpouseName)
      additionalName.push(this.SpouseName);
    if (this.FamilyName)
      additionalName.push(this.FamilyName);
    vcard.push('N:' + [
      this.LastName || '',
      this.FirstName || '',
      additionalName.join(' ') || '',
      this.namePrefix || '',
      this.nameSuffix || '',
    ].join(VCard.VALUE_SEPARATOR));

    if (this.PhoneticFirstName)
      vcard.push(`X-PHONETIC-FIRST-NAME:${this.PhoneticFirstName}`);
    if (this.PhoneticLastName)
      vcard.push(`X-PHONETIC-LAST-NAME:${this.PhoneticLastName}`);

    if (this.NickName)
      vcard.push(`NICKNAME:${this.NickName}`);

    if (this.genderCode)
      vcard.push(`GENDER:${this.genderCode.toUpperCase()}`);

    const emails = [];
    if (this.PrimaryEmail)
      emails.push(this.PrimaryEmail);
    if (this.SecondEmail)
      emails.push(this.SecondEmail);
    emails.forEach((email, index) => {
      const preferred = (index == 0 && emails.length > 1) ? ',pref' : '';
      vcard.push(`EMAIL;TYPE=internet${preferred}:${email}`);
    });

    if (this.Notes)
      vcard.push(`NOTE:${this.Notes.replace(/\n/g, ' ')}`);

    for (const field of CustomPersonalField.getAll()) {
      if (field.bindTo)
        vcard.push(`${field.vCardField}:${this[field.bindTo]}`);
      else
        vcard.push(`${field.vCardField}:${this.getFieldValue(field.fieldPath)}`);
    }

    for (const field of CustomOrganizationField.getAll()) {
      if (field.bindTo)
        vcard.push(`${field.vCardField}:${this[field.bindTo]}`);
      else
        vcard.push(`${field.vCardField}:${this.getFieldValue(field.fieldPath)}`);
    }

    if (this.organizations.length > 0) {
      vcard.push(`ORG:${this.organizations.join(VCard.VALUE_SEPARATOR)}`);
    }

    if (this.categories.length > 0)
      vcard.push(`CATEGORIES:${this.categories.filter(category => !!category).join(VCard.CATEGORIES_SEPARATOR)}`);

    if (this.JobTitle)
      vcard.push(`TITLE:${this.JobTitle}`);

    if (this.birthDate)
      vcard.push(`BDAY:${this.birthDate.toISOString().split('T')[0].replace(/-/g, '')}`);

    if (this.extra.birth)
      vcard.push(`BIRTH:${this.extra.birth}`);

    if (this.deathDate)
      vcard.push(`DEATH-DATE:${this.deathDate.toISOString().split('T')[0].replace(/-/g, '')}`);

    if (this.anniversaryDate)
      vcard.push(`ANNIVERSARY:${this.anniversaryDate.toISOString().split('T')[0].replace(/-/g, '')}`);

    if (this.WorkAddress2 || this.WorkAddress || this.WorkCity || this.WorkState || this.WorkZipCode || this.WorkCountry) {
      vcard.push('ADR;TYPE=work:' + [
        '',
        this.WorkAddress2 || '',
        this.WorkAddress || '',
        this.WorkCity || '',
        this.WorkState || '',
        this.WorkZipCode || '',
        this.WorkCountry || '',
      ].join(VCard.VALUE_SEPARATOR));
    }

    if (this.HomeAddress2 || this.HomeAddress || this.HomeCity || this.HomeState || this.HomeZipCode || this.HomeCountry) {
      vcard.push('ADR;TYPE=home:' + [
        '',
        this.HomeAddress2 || '',
        this.HomeAddress || '',
        this.HomeCity || '',
        this.HomeState || '',
        this.HomeZipCode || '',
        this.HomeCountry || '',
      ].join(VCard.VALUE_SEPARATOR));
    }

    if (this.HomePhone) {
      for (const entry of this.HomePhone.split(VCard.VALUE_SEPARATOR)) {
        vcard.push(`TEL;TYPE=home:${entry}`);
      }
    }
    if (this.WorkPhone) {
      for (const entry of this.WorkPhone.split(VCard.VALUE_SEPARATOR)) {
        vcard.push(`TEL;TYPE=work:${entry}`);
      }
    }
    if (this.FaxNumber) {
      for (const entry of this.FaxNumber.split(VCard.VALUE_SEPARATOR)) {
        vcard.push(`TEL;TYPE=fax:${entry}`);
      }
    }
    if (this.PagerNumber) {
      for (const entry of this.PagerNumber.split(VCard.VALUE_SEPARATOR)) {
        vcard.push(`TEL;TYPE=pager:${entry}`);
      }
    }
    if (this.CellularNumber) {
      for (const entry of this.CellularNumber.split(VCard.VALUE_SEPARATOR)) {
        vcard.push(`TEL;TYPE=cell:${entry}`);
      }
    }

    if (this.WebPage1)
      vcard.push(`URL:${this.WebPage1}`);
    if (this.WebPage2)
      vcard.push(`URL:${this.WebPage2}`);

    if (this.PreferMailFormat && this.PreferMailFormat != '0')
      vcard.push(`X-MOZILLA-HTML:${this.PreferMailFormat == '2' ? 'TRUE' : 'FALSE'}`);

    if (this.PhotoURI) {
      vcard.push(`PHOTO;VALUE=uri:${this.PhotoURI}`);
    }
    else {
      const photoURI = this.getFieldValue('extra.photoURI');
      if (photoURI)
        vcard.push(`PHOTO;ENCODING=b;TYPE=PNG:${photoURI.replace(/^data:image\/[^\/]+;base64,/i, '')}`);
    }

    const logoSourceURI = this.getFieldValue('extra.logoSourceURI');
    const logoURI = this.getFieldValue('extra.logoURI');
    if (logoSourceURI) {
      vcard.push(`LOGO;VALUE=uri:${logoSourceURI.replace(/^data:image\/[^\/]+;base64,/i, '')}`);
    }
    else if (logoURI) {
      vcard.push(`PHOTO;ENCODING=b;TYPE=PNG:${logoURI.replace(/^data:image\/[^\/]+;base64,/i, '')}`);
    }

    if (this._GoogleTalk)
      vcard.push(`X-CHAT-ACCOUNT;TYPE=Gtalk:${this._GoogleTalk}`);
    if (this._AimScreenName)
      vcard.push(`X-CHAT-ACCOUNT;TYPE=AIM:${this._AimScreenName}`);
    if (this._Yahoo)
      vcard.push(`X-CHAT-ACCOUNT;TYPE=Yahoo:${this._Yahoo}`);
    if (this._Skype)
      vcard.push(`X-CHAT-ACCOUNT;TYPE=Skype:${this._Skype}`);
    if (this._QQ)
      vcard.push(`X-CHAT-ACCOUNT;TYPE=QQ:${this._QQ}`);
    if (this._MSN)
      vcard.push(`X-CHAT-ACCOUNT;TYPE=MSN:${this._MSN}`);
    if (this._ICQ)
      vcard.push(`X-CHAT-ACCOUNT;TYPE=ICQ:${this._ICQ}`);
    if (this._JabberId)
      vcard.push(`X-CHAT-ACCOUNT;TYPE=XMMP:${this._JabberId}`);
    if (this._IRC)
      vcard.push(`X-CHAT-ACCOUNT;TYPE=IRC:${this._IRC}`);

    for (const [field, values] of Object.entries(this.extra)) {
      if (KNOWN_CONTACT_EXTRA_FIELDS.has(field))
        continue;
      if (Array.isArray(values)) {
        for (const value of values) {
          vcard.push(`${field.toUpperCase()}:${value}`);
        }
      }
      else {
        vcard.push(`${field.toUpperCase()}:${values}`);
      }
    }

    return this.$vCard = [
      'BEGIN:VCARD',
      'VERSION:4.0',
      `UID:${this.id}`,
      ...vcard,
      'END:VCARD',
    ].join('\n');
  }

  get searchString() {
    if (this.$searchString !== undefined)
      return this.$searchString;

    return this.$searchString = [
      ...[
        ...KNOWN_CONTACT_FIELDS,
        ...KNOWN_CONTACT_EXTRA_FIELDS
      ].map(field => {
        if (NO_SEARCH_CONTACT_FIELDS.has(field))
          return '';
        return this.getFieldValue(field);
      }),
      ...configs.customPersonalInformationFields
        .concat(configs.customOrganizationInformationFields)
        .map(field => this.getFieldValue(`extra.${field.field}`)),
    ].join(' ');


  }

  set searchString(newValue) {
    return newValue;
  }

  get availableChatProviderIds() {
    if (this.$availableChatProviderIds !== undefined)
      return this.$availableChatProviderIds;
    const ids = [];
    for (const provider of ChatProvider.getAll()) {
      if (provider.hasAccount(this))
        ids.push(provider.id);
    }
    return this.$availableChatProviderIds = ids;
  }
}

Contact.KNOWN_FIELDS = KNOWN_CONTACT_FIELDS;
Contact.KNOWN_VIRTUAL_FIELDS = KNOWN_CONTACT_VIRTUAL_FIELDS;
Contact.KNOWN_EXTRA_FIELDS = KNOWN_CONTACT_EXTRA_FIELDS;

Contact.instances = new Map();

Contact.get = id => {
  return Contact.instances.get(id);
};

Contact.getAll = () => {
  return Array.from(Contact.instances.values());
};

Contact.waitUntilCreated = async id => {
  return new Promise((resolve, _reject) => {
    const onCreated = contact => {
      if (contact.id != id)
        return;
      Contact.onCreated.removeListener(onCreated);
      resolve();
    };
    Contact.onCreated.addListener(onCreated);
    setTimeout(() => { // already created case
      if (!Contact.get(id))
        return;
      Contact.onCreated.removeListener(onCreated);
      resolve();
    }, 250);
  });
};

Contact.onCreated = new EventListenerManager();
Contact.onUpdated = new EventListenerManager();
Contact.onRemoved = new EventListenerManager();
Contact.onMoved   = new EventListenerManager();

Contact.autoTrack = () => {
  browser.contacts.onCreated.addListener(async (contact, _id) => {
    log('contacts.onCreated: ', contact);
    const existingInstance = Contact.get(contact.id);
    const instance = existingInstance || new Contact(contact);
    await instance.initialized;
    if (existingInstance &&
        instance.parentId &&
        contact.parentId &&
        instance.parentId != contact.parentId) {
      log(` => moved from ${instance.parentId} to ${contact.parentId}`);
      const oldParentId = instance.parentId;
      instance.parentId = contact.parentId;
      Contact.onMoved.dispatch(contact.id, oldParentId, contact.parentId);
      return;
    }
    Contact.onCreated.dispatch(instance);
  });

  browser.contacts.onUpdated.addListener(async (contact, changedProperties) => {
    log('contacts.onUpdated: ', contact, changedProperties);
    const instance = Contact.get(contact.id) || new Contact(contact);
    const updated = await instance.apply(contact);
    Contact.onUpdated.dispatch(contact.id, updated, instance);
  });

  browser.contacts.onDeleted.addListener(async (parentId, contactId) => {
    log('contacts.onDeleted: ', contactId);
    const instance = Contact.get(contactId);
    if (instance) {
      await instance.initialized;
      if (instance.parentId != parentId) {
        log(' => moved card (already moved) ', contactId);
        return;
      }
      instance.destroy();
    }
    Contact.onRemoved.dispatch(parentId, contactId);
  });
};


export class MailingList extends Item {
  constructor(raw) {
    raw.type = 'mailingList'; // for newly created instance
    super(raw);

    if (raw.id)
      MailingList.instances.set(raw.id, this);

    this.$dirtyMetadata = new Set();

    for (const key of KNOWN_MAILING_LIST_FIELDS) {
      if (key in this)
        continue;
      Object.defineProperty(this, key, {
        enumerable: true,
        configurable: true,
        get() { return this.$raw[key]; },
        set(newValue) {
          if (newValue != this.$raw[key]) {
            this.$dirtyProperties.add(key);
            this.invalidateCacheFor(key);
          }
          return this.$raw[key] = newValue;
        },
      });
    }

    for (const key of KNOWN_CONTACT_FIELDS) {
      if (key in this)
        continue;
      Object.defineProperty(this, key, {
        enumerable: true,
        configurable: true,
        get() { return ''; },
        set(newValue) { return newValue; },
      });
    }

    this.$completelyInitialized = false;
    this.initialized = this.loadMetadata().then(() => {
      this.addToAvailableCategories();
      this.$completelyInitialized = true;
      return true;
    });

    this.contacts = [];
    this.$contactById = new Map();
    this.initialized = this.initialized.then(async finished => {
      const contacts = raw.contacts || await browser.mailingLists.listMembers(this.id);
      for (const contact of contacts) {
        const wrappedContact = Contact.get(contact.id) || new Contact({ ...contact, parentId: raw.parentId });
        this.addContact(wrappedContact);
      }
      return finished;
    });
  }

  async loadMetadata() {
    this.$metadata = this.id && (await DB.getMailingListMetadata(this.$raw)) || {};
    return true;
  }

  destroy() {
    super.destroy();
    MailingList.instances.delete(this.$raw.id)
  }

  invalidateCache() {
    super.invalidateCache();
    this.$vCard = undefined;
    this.$searchString = undefined;
    this.$availableChatProviderIds = undefined;
  }

  invalidateCacheFor(field) {
    switch (field) {
      case 'name':
      case 'description':
      case 'nickName':
        this.$searchString = undefined;
        break;

      default:
        break;
    }
    this.invalidateExportedCache();
  }

  getMetadata(field) {
    return this.$metadata && this.$metadata[field];
  }

  setMetadata(field, newValue) {
    if (newValue == this.getMetadata(field))
      return newValue;
    if (!this.$metadata)
      throw new Error('not initialized yet');
    this.$dirtyMetadata.add(field);
    return this.$metadata[field] = newValue;
  }

  async applyMetadata(updates) {
    await this.initialized;
    const updated = {};
    for (const [field, value] of Object.entries(updates)) {
      if (value != this[field]) {
        this[field] = value;
        updated[field] = value;
        this.$dirtyMetadata.delete(field);
      }
    }
    return updated;
  }

  getFieldValue(field) {
    return this[field];
  }

  setFieldValue(field, newValue) {
    return this[field] = newValue;
  }

  get label() {
    return this.name;
  }
  set label(value) {
    return this.label;
  }

  get email() {
    return this.contacts.map(contact => contact.email).filter(email => !!email);
  }
  set email(value) {
    return this.email;
  }

  get address() {
    return this.contacts.map(contact => contact.address).filter(address => !!address);
  }
  set address(value) {
    return this.address;
  }

  get url() {
    return this.contacts.map(contact => contact.url).filter(url => !!url);
  }
  set url(value) {
    return this.url;
  }

  get visibleDisplayName() {
    return this.DisplayName;
  }

  set visibleDisplayName(newValue) {
    return this.DisplayName = newValue;
  }

  get DisplayName() {
    return this.name;
  }

  set DisplayName(newValue) {
    return this.name = newValue;
  }

  get Notes() {
    return this.description;
  }

  set Notes(newValue) {
    return this.description = newValue;
  }

  get NickName() {
    return this.nickName;
  }

  set NickName(newValue) {
    return this.nickName = newValue;
  }

  get dirty() {
    return super.dirty || this.$dirtyMetadata.size > 0;
  }

  async save() {
    if (!this.dirty)
      return;

    const promises = [];

    if (this.$dirtyProperties.has('parentId'))
      await this.moveTo(this.parentId);

    if (this.$dirtyProperties.size > 0) {
      const updates = {};
      const createParams = this.createParams;
      for (const field of this.$dirtyProperties) {
        if (field in createParams &&
            field in this.$raw)
          updates[field] = this.$raw[field];
      }
      this.$dirtyProperties.clear();
      if (this.id) {
        if (Object.keys(updates) > 0) {
          log('Updating mailing list: ', this.id, updates);
          promises.push(browser.mailingLists.update(this.id, updates));
        }
      }
      else {
        log('Creating mailing list: ', updates);
        try {
          const createdId = await browser.mailingLists.create(this.parentId, updates);
          log('Created as: ', createdId);
          this.$raw = await browser.mailingLists.get(createdId);
        }
        catch(error) {
          console.error(error);
        }
      }
    }

    if (this.$dirtyMetadata.size > 0) {
      log('Updating mailing list metadata: ', this.id, this.$metadata);
      promises.push(DB.setMailingListMetadata(this.$raw, this.$metadata).then(async () => {
        const updates = {};
        for (const field of this.$dirtyMetadata) {
          updates[field] = this.$metadata[field];
        }
        browser.runtime.sendMessage({
          type:          Constants.NOTIFY_MAILING_LIST_METADATA_UPDATED,
          mailingListId: this.id,
          updates,
        });
        this.$dirtyMetadata.clear();
        MailingList.onUpdated.dispatch(this.id, updates, this);
      }));
    }

    if (promises.length > 0)
      await Promise.all(promises);
  }

  get createParams() {
    return {
      name:        this.name,
      description: this.description,
      nickName:    this.nickName,
    };
  }

  async moveTo(addressBookId) {
    const currentParentId = (await browser.mailingLists.get(this.id)).parentId;
    if (addressBookId == currentParentId)
      return;
    const id = this.id;
    const properties = clone(this.createParams);
    const movedId = await browser.mailingLists.create(addressBookId, properties);
    await MailingList.waitUntilCreated(movedId);
    const instance = MailingList.get(movedId);
    /*
    await Promise.all(this.contacts.map(async contact => {
      if (contact.parentId == addressBookId)
        instance.addContact(contact);
      else
        instance.addContact(await contact.copyTo(addressBookId));
    }));
    */
    await browser.mailingLists.delete(id);
    return instance;
  }

  async copyTo(addressBookId) {
    const properties = clone(this.createParams);
    const copiedId = await browser.mailingLists.create(addressBookId, properties);
    await MailingList.waitUntilCreated(copiedId);
    const instance = MailingList.get(copiedId);
    /*
    await Promise.all(this.contacts.map(async contact => {
      if (contact.parentId == addressBookId)
        instance.addContact(contact);
      else
        instance.addContact(await contact.copyTo(addressBookId));
    }));
    */
    return instance;
  }

  get internalCategories() {
    return (this.categories.length == 0) ?
      [Constants.UNCATEGORIZED_ID] :
      this.categories;
  }

  set internalCategories(newValue) {
    return newValue;
  }

  get organizations() {
    return [];
  }

  set organizations(newValue) {
    return newValue;
  }

  get organizationId() {
    return '';
  }

  get categories() {
    if (!this.$metadata)
      throw new Error('not initialized yet');
    if (!this.$categories)
      this.$categories = this.getMetadata('categories') || [];
    return this.$categories;
  }

  set categories(newValue) {
    const joinedValue = Array.isArray(newValue) ? newValue.join(VCard.CATEGORIES_SEPARATOR) : String(newValue);
    if (joinedValue == this.categories.join(VCard.CATEGORIES_SEPARATOR))
      return newValue;
    const categories = joinedValue.split(VCard.CATEGORIES_SEPARATOR).filter(category => !!category);
    this.$categories = categories;
    this.setMetadata('categories', categories);
    this.addToAvailableCategories();
    return newValue;
  }

  get vCard() {
    if (this.$vCard)
      return this.$vCard;

    const vcard = [];

    if (this.name)
      vcard.push(`FN:${this.name}`);
    if (this.description)
      vcard.push(`NOTE:${this.description}`);
    if (this.nickName)
      vcard.push(`NICKNAME:${this.nickName}`);

    if (this.categories.length > 0)
      vcard.push(`CATEGORIES:${this.categories.filter(category => !!category).join(VCard.CATEGORIES_SEPARATOR)}`);

    vcard.push(...this.contacts.map(contact => `MEMBER:urn:uuid:${contact.id}`));

    return this.$vCard = [
      'BEGIN:VCARD',
      'VERSION:4.0',
      `UID:${this.id}`,
      ...vcard,
      'END:VCARD',
    ].join('\n');
  }

  get searchString() {
    if (this.$searchString !== undefined)
      return this.$searchString;

    return this.$searchString = [
      this.name,
      this.description,
      this.nickName,
    ].join(' ');
  }

  set searchString(newValue) {
    return newValue;
  }

  get availableChatProviderIds() {
    if (this.$availableChatProviderIds !== undefined)
      return this.$availableChatProviderIds;
    return this.$availableChatProviderIds = [...new Set(this.contacts.map(contact => contact.availableChatProviderIds).flat())];
  }

  async addContact(newContact) {
    if (typeof newContact == 'string')
      newContact = Contact.get(newContact);

    if (!newContact ||
        this.$contactById.has(newContact.id) ||
        (newContact.parentId != this.id &&
         newContact.parentId != this.parentId))
      return;

    const existingMembers = await browser.mailingLists.listMembers(this.id);
    const local = !this.$completelyInitialized;
    if (!local &&
        // Don't over-add a contact to a mailing list if it is already a member.
        // It actually doesn't cause member duplication but it looks to trigger
        // "member-added" internal event on the Thunderbird itself.
        // You will see ghost duplicated members if the mailing list is active
        // on the Thunderbird's native address book while such an operation.
        // Such ghosts go away after the list of members are re-rendered on
        // Thunderbird's address book UI, but they are very annoying.
        existingMembers.every(member => member.id == this.id))
      await browser.mailingLists.addMember(this.id, newContact.id);

    const index = this.contacts.findIndex(contact => contact.id == newContact.id);
    if (index < 0)
      this.contacts.push(newContact);

    this.$contactById.set(newContact.id, newContact);

    this.invalidateCache();

    MailingList.onContactAdded.dispatch(this, newContact, { local });
  }

  async removeContact(contactId) {
    const contact = contactId && typeof contactId == 'object' ? contactId : Contact.get(contactId);
    if (contactId && typeof contactId == 'object')
      contactId = contactId.id;

    if (!contactId ||
        !this.$contactById.has(contactId) ||
        (contact &&
         contact.parentId != this.id &&
         contact.parentId != this.parentId))
      return;

    const local = !this.$completelyInitialized;
    if (!local)
      await browser.mailingLists.removeMember(this.id, contactId);

    const index = this.contacts.findIndex(contact => contact.id == contactId);
    if (index > -1)
      this.contacts.splice(index, 1);

    this.$contactById.delete(contactId);

    this.invalidateCache();

    MailingList.onContactDeleted.dispatch(this, contactId, { local });
  }

  export(properties) {
    const exported = super.export(properties);
    exported.contacts = this.contacts.map(contact => contact.export(properties));
    return exported;
  }
}

MailingList.KNOWN_FIELDS = KNOWN_MAILING_LIST_FIELDS;

MailingList.instances = new Map();

MailingList.get = id => {
  return MailingList.instances.get(id);
};

MailingList.getAll = () => {
  return Array.from(MailingList.instances.values());
};

MailingList.waitUntilCreated = async id => {
  return new Promise((resolve, _reject) => {
    const onCreated = contact => {
      if (contact.id != id)
        return;
      MailingList.onCreated.removeListener(onCreated);
      resolve();
    };
    MailingList.onCreated.addListener(onCreated);
    setTimeout(() => { // already created case
      if (!MailingList.get(id))
        return;
      MailingList.onCreated.removeListener(onCreated);
      resolve();
    }, 250);
  });
};

MailingList.onCreated = new EventListenerManager();
MailingList.onUpdated = new EventListenerManager();
MailingList.onRemoved = new EventListenerManager();
MailingList.onContactAdded   = new EventListenerManager();
MailingList.onContactDeleted = new EventListenerManager();

MailingList.autoTrack = () => {
  browser.mailingLists.onCreated.addListener(async (mailingList, _id) => {
    if (MailingList.get(mailingList.id))
      return;
    log('mailingLists.onCreated: ', mailingList);
    const instance = new MailingList(mailingList);
    await instance.initialized;
    MailingList.onCreated.dispatch(instance);
  });

  browser.mailingLists.onUpdated.addListener(async mailingList => {
    log('mailingLists.onUpdated: ', mailingList);
    const instance = MailingList.get(mailingList.id) || new MailingList(mailingList);
    const updated = await instance.apply(mailingList);
    MailingList.onUpdated.dispatch(mailingList.id, updated, instance);
  });

  browser.mailingLists.onDeleted.addListener((parentId, mailingListId) => {
    log('mailingLists.onDeleted: ', mailingListId);
    const instance = MailingList.get(mailingListId);
    if (instance)
      instance.destroy();
    MailingList.onRemoved.dispatch(parentId, mailingListId);
  });

  browser.mailingLists.onMemberAdded.addListener(async contact => {
    log('mailingLists.onMemberAdded: ', contact);
    await MailingList.waitUntilCreated(contact.parentId);
    const instance = MailingList.get(contact.parentId);
    await instance.initialized;
    const wrappedContact = Contact.get(contact.id);
    if (wrappedContact)
      instance.addContact(wrappedContact);
  });

  browser.mailingLists.onMemberRemoved.addListener(async (parentId, contactId) => {
    log('mailingLists.onMemberAdded: ', parentId, contactId);
    await MailingList.waitUntilCreated(parentId);
    const instance = MailingList.get(parentId);
    await instance.initialized;
    instance.removeContact(contactId);
  });
};

browser.runtime.onMessage.addListener((message, _sender) => {
  switch (message && message.type) {
    case Constants.NOTIFY_MAILING_LIST_METADATA_UPDATED: {
      const instance = MailingList.get(message.mailingListId);
      if (instance) {
        instance.applyMetadata(message.updates).then(updated => {
          instance.initialized.then(() => {
            MailingList.onUpdated.dispatch(instance.id, updated, instance);
          });
        });
      }
      else {
        browser.mailingLists.get(message.mailingListId).then(mailingList => {
          const newInstance = MailingList.get(mailingList.id) || new MailingList(mailingList);
          newInstance.initialized.then(() => {
            MailingList.onUpdated.dispatch(mailingList.id, message.updates, newInstance);
          });
        });
      }
    }; break;
  }
});


export function normalizeItem(item) {
  switch (item.type) {
    case 'contact':
      return Contact.get(item.id) || new Contact(item);
    case 'mailingList':
      return MailingList.get(item.id) || new MailingList(item);
    default:
      throw new Error('unknown type item: ' + item.type);
  }
}


/* for fields encryption support
configs.$loaded.then(() => {
  Item.unencryptedContactFields = new Set(configs.unencryptedContactFields);
});
*/
configs.$addObserver(key => {
  switch (key) {
    /* for fields encryption support
    case 'unencryptedContactFields':
      Item.unencryptedContactFields = new Set(configs.unencryptedContactFields);
      break;
    */

    case 'customPersonalInformationFields':
    case 'customOrganizationInformationFields':
      for (const contact of Contact.getAll()) {
        contact.invalidateCache();
      }
      break;

    default:
      break;
  }
});
