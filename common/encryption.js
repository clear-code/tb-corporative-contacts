/*
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
*/
'use strict';

// Original: https://gist.github.com/piroor/a312990473fbbef94ce63309278874f0#file-aes-gcm-encryption-js

import {
  configs,
} from './common.js';

const encryptionKeyInitialized = configs.$loaded.then(() => {
  if (!configs.encryptionKey)
    configs.encryptionKey = btoa(Array.from(crypto.getRandomValues(new Uint8Array(128)), char => String.fromCharCode(char)).join(''));
});

async function getKey(salt = null) {
  await encryptionKeyInitialized;
  const storedKey = (new TextEncoder()).encode(configs.encryptionKey);
  const digest = await crypto.subtle.digest({ name: 'SHA-256' }, storedKey);
  const keyMaterial = await crypto.subtle.importKey(
    'raw',
    digest,
    { name: 'PBKDF2' },
    false,
    ['deriveKey']
  );
  if (!salt)
    salt = crypto.getRandomValues(new Uint8Array(16));
  const key = await crypto.subtle.deriveKey(
    {
      name: 'PBKDF2',
      salt,
      iterations: 100000,
      hash: 'SHA-256'
    },
    keyMaterial,
    { name: 'AES-GCM', length: 256 },
    false,
    ['encrypt', 'decrypt']
  );
  return [key, salt];
}

function getFixedField() {
  let value = configs.encryption96bitIVFixedField;
  if (value)
    return Uint8Array.from(JSON.parse(value));
  value = crypto.getRandomValues(new Uint8Array(12));
  configs.encryption96bitIVFixedField = JSON.stringify(Array.from(value));
  return value;
}

function getInvocationField() {
  let counter = configs.encryption32bitLastCounter;
  if (counter)
    counter = Uint32Array.from(JSON.parse(counter));
  else
    counter = new Uint32Array(1);
  counter[0]++;
  configs.encryption32bitLastCounter = JSON.stringify(Array.from(counter));
  return counter;
}

export async function encrypt(input) {
  const [key, salt]    = await getKey();
  const fixedPart      = getFixedField();
  const invocationPart = getInvocationField();
  const iv = Uint8Array.from([...fixedPart, ...new Uint8Array(invocationPart.buffer)]);
  const encryptedData = await crypto.subtle.encrypt(
    { name: 'AES-GCM', iv },
    key,
    (new TextEncoder()).encode(JSON.stringify(input))
  );
  const encryptedBinaryString = Array.from(new Uint8Array(encryptedData), char => String.fromCharCode(char)).join('');
  return JSON.stringify([
    btoa(encryptedBinaryString),
    Array.from(invocationPart),
    Array.from(salt)
  ]);
}

export async function decrypt(encryptedResult) {
  const [encryptedData, invocationPart, salt] = JSON.parse(encryptedResult);
  const [key,] = await getKey(Uint8Array.from(salt));
  const invocationPartTypedArray = new Uint32Array(1);
  invocationPartTypedArray[0] = invocationPart;
  const iv = Uint8Array.from([...getFixedField(), ...(new Uint8Array(invocationPartTypedArray.buffer))]);
  const encryptedBinaryString = atob(encryptedData);
  const encryptedBytes = Uint8Array.from(encryptedBinaryString.split(''), char => char.charCodeAt(0));
  const decryptedData = await crypto.subtle.decrypt(
    { name: 'AES-GCM', iv },
    key,
    encryptedBytes
  );
  const decodedDecryptedData = (new TextDecoder()).decode(new Uint8Array(decryptedData));
  return JSON.parse(decodedDecryptedData);
}
