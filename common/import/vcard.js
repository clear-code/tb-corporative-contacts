/*
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
*/
'use strict';

/*
import {
  log,
} from '/common/common.js';
*/
import * as Constants from '../constants.js';
import { CustomPersonalField, CustomOrganizationField } from '../CustomField.js';

export const VALUE_SEPARATOR = ';';
export const CATEGORIES_SEPARATOR = ',';
export const MIME_TYPE = 'text/x-vcard';
export const SUFFIX = 'vcf';

const CARD_MATCHER = /(?<=^|\r?\n)begin:vcard\r?\n[\w\W]+?\nend:vcard(?=\r?\n|$)/gi;
const LINE_MATCHER = /^(?:\s*[\r\n]+)?(?:([^;:\r\n\s]+)(;[^:\r\n]+)?:([^\r\n]*?)|\s+([^\r\n]+))$/gm;
const ATTRIBUTES_MATCHER = /(?<=^|;)([^=;]+)(?:=([^;]*))?(?=;|$)/g;
const IGNORE_PROPERTIES = new Set([
  'begin',
  'end',
]);
export const TRAILING_BLANK_ORGANIZATIONS_SANITIZER = new RegExp(`(\s*${VALUE_SEPARATOR})+$`);
const CATEGORIES_SPLITTER = new RegExp(`\s*${CATEGORIES_SEPARATOR}\s*`);

export async function parse(vCards, { forceCreate, addressBookIdField } = {}) {
  const cards = [];
  let hasSortString = false;
  for (const matched of vCards.matchAll(CARD_MATCHER)) {
    const promisedCard = parseVCard(matched[0], { forceCreate, addressBookIdField });
    cards.push(promisedCard);
    promisedCard.then(card => {
      if (!hasSortString &&
          card &&
          card.customValues['sort-string'])
        hasSortString = true;
    });
  }
  const filteredCards = (await Promise.all(cards)).filter(card => !!card);
  if (hasSortString)
    filteredCards.sort((a, b) => (a.customValues['sort-string'] || '') - (b.customValues['sort-string'] || ''));
  return filteredCards;
}

async function parseVCard(vCard, { forceCreate, addressBookIdField } = {}) {
  const parsed = {};
  let lastKey = null;
  for (const matched of vCard.matchAll(LINE_MATCHER)) {
    const [, key, rawAttributes, value, continuous] = matched;
    if (continuous) {
      if (lastKey && parsed[lastKey])
        parsed[lastKey][parsed[lastKey].length - 1].value += continuous;
      continue;
    }
    lastKey = key.toLowerCase();
    if (IGNORE_PROPERTIES.has(lastKey))
      continue;
    const attributes = parseAttribute(rawAttributes);
    if (parsed.hasOwnProperty(lastKey))
      parsed[lastKey].push({ attributes, value });
    else
      parsed[lastKey] = [{ attributes, value }];
  }
  return translate(parsed, { forceCreate, addressBookIdField });
}

function parseAttribute(attributes) {
  const parsed = {};
  if (!attributes)
    return parsed;

  //log('parseAttribute ', attributes);
  for (const matched of attributes.matchAll(ATTRIBUTES_MATCHER)) {
    const [, key, value] = matched;
    //log('matched: ', matched, { key, value });
    parsed[key.toLowerCase()] = value || true;
  }
  //log(' => ', parsed);
  return parsed;
}

async function translate(parsedVCard, { forceCreate, addressBookIdField } = {}) {
  //log('translate: ', parsedVCard);
  let addressBookId = null;
  const promises = [];
  const properties = {};
  const translated = { properties };
  const customValues = {};
  let organizations;
  for (const [key, values] of Object.entries(parsedVCard)) {
    switch (key) {
      case 'version': {
        const rawVersion = values[0].value;
        const version = parseFloat(rawVersion);
        if (isNaN(version))
          throw new Error(`unknown vCard version: ${rawVersion}`);
        if (version > 4.99)
          throw new Error(`too high vCard version: ${rawVersion}`);
        if (version < 3.0)
          throw new Error(`too low vCard version: ${rawVersion}`);
        customValues.version = version;
      }; break;

      case 'uid': {
        translated.id = joinAllValues(values);
        if (forceCreate) {
          promises.push(browser.contacts.get(translated.id).catch(_error => null).then(existingContact => {
            if (existingContact)
              delete translated.id;
          }));
        }
      }; break;

      case 'email':
        if (values.length > 0)
          properties.PrimaryEmail = values[0].value;
        if (values.length > 1)
          properties.SecondEmail = joinAllValues(values.slice(1), ',');
        break;

      case 'fn':
        properties.DisplayName = joinAllValues(values);
        break;

      case 'nickname':
        properties.NickName = joinAllValues(values);
        break;

      case 'note':
        properties.Notes = joinAllValues(values);
        break;

      case 'org':
        organizations = joinAllValues(values).replace(TRAILING_BLANK_ORGANIZATIONS_SANITIZER).split(VALUE_SEPARATOR);
        break;

      case 'categories':
        customValues.categories = joinAllValues(values, CATEGORIES_SEPARATOR).split(CATEGORIES_SPLITTER);
        break;

      case 'title':
        properties.JobTitle = joinAllValues(values);
        break;

      case 'bday': {
        const dateString = joinAllValues(values).replace(/^(\d{4})(0[1-9]|1[0-2])(0[1-9]|[12][0-9]|3[01])$/, '$1-$2-$3');
        try {
          const date = new Date(dateString);
          properties.BirthYear = date.getFullYear().toString();
          properties.BirthMonth = (date.getMonth() + 1).toString();
          properties.BirthDay = date.getDate().toString();
        }
        catch(error) {
          console.log('failed to parse BDAY: ', dateString, error);
        }
      }; break;

      case 'birth':
        customValues.birth = joinAllValues(values);
        break;

      case 'anniversary':
      case 'x-anniversary': {
        const dateString = joinAllValues(values).replace(/^(\d{4})(0[1-9]|1[0-2])(0[1-9]|[12][0-9]|3[01])$/, '$1-$2-$3');
        try {
          const date = new Date(dateString);
          properties.AnniversaryYear = date.getFullYear().toString();
          properties.AnniversaryMonth = (date.getMonth() + 1).toString();
          properties.AnniversaryDay = date.getDate().toString();
        }
        catch(error) {
          console.log('failed to parse ANNIVERSARY: ', dateString, error);
        }
      }; break;

      case 'n': {
        const names = joinAllValues(values).split(VALUE_SEPARATOR);
        if (names.length > 0)
          properties.LastName = names[0];
        if (names.length > 1)
          properties.FirstName = names[1];
        if (names.length > 2)
          properties.SpouseName = names[2];
        if (names.length > 3)
          customValues.namePrefix = [names[3]];
        if (names.length > 4)
          customValues.namePrefix = [names[4]];
      }; break;

      case 'gender':
        customValues.gender = joinAllValues(values);
        break;

      case 'adr': {
        const typedValues = splitValuesByType(values, ['home', 'work']);
        if (typedValues.home) {
          const parts = joinAllValues(typedValues.home).split(VALUE_SEPARATOR);
          const address2 = [];
          if (parts.length > 0)
            address2.unshift(parts[0]);
          if (parts.length > 1)
            address2.unshift(parts[1]);
          if (address2.length > 0)
            properties.HomeAddress2 = address2.join(' ');
          if (parts.length > 2)
            properties.HomeAddress = parts[2];
          if (parts.length > 3)
            properties.HomeCity = parts[3];
          if (parts.length > 4)
            properties.HomeState = parts[4];
          if (parts.length > 5)
            properties.HomeZipCode = parts[5];
          if (parts.length > 6)
            properties.HomeCountry = parts[6];
        }
        if (typedValues.work) {
          const parts = joinAllValues(typedValues.work).split(VALUE_SEPARATOR);
          const address2 = [];
          if (parts.length > 0)
            address2.unshift(parts[0]);
          if (parts.length > 1)
            address2.unshift(parts[1]);
          if (address2.length > 0)
            properties.WorkAddress2 = address2.join(' ');
          if (parts.length > 2)
            properties.WorkAddress = parts[2];
          if (parts.length > 3)
            properties.WorkCity = parts[3];
          if (parts.length > 4)
            properties.WorkState = parts[4];
          if (parts.length > 5)
            properties.WorkZipCode = parts[5];
          if (parts.length > 6)
            properties.WorkCountry = parts[6];
        }
      }; break;

      case 'tel': {
        const typedValues = splitTelValues(values);
        if (typedValues.home)
          properties.HomePhone = joinAllValues(typedValues.home);
        if (typedValues.work)
          properties.WorkPhone = joinAllValues(typedValues.work);
        if (typedValues.fax)
          properties.FaxNumber = joinAllValues(typedValues.fax);
        if (typedValues.pager)
          properties.PagerNumber = joinAllValues(typedValues.pager);
        if (typedValues.cell)
          properties.CellularNumber = joinAllValues(typedValues.cell);
      }; break;

      case 'url':
        if (values.length > 0)
          properties.WebPage1 = values[0].value;
        if (values.length > 1)
          properties.WebPage2 = values[1].value;
        break;

      case 'x-mozilla-html': {
        const value = joinAllValues(values).toLowerCase();
        if (value == 'true')
          properties.PreferMailFormat = '2'; // Ci.nsIAbPreferMailFormat.html
        else if (value == 'false')
          properties.PreferMailFormat = '1'; // Ci.nsIAbPreferMailFormat.plaintext
      }; break;

      case 'x-phonetic-first-name':
        properties.PhoneticFirstName = joinAllValues(values);
        break;

      case 'x-phonetic-last-name':
        properties.PhoneticLastName = joinAllValues(values);
        break;

      case 'photo': {
        const value = values.find(value => value.attributes.pref) || values[0];
        if (value.attributes.value == 'uri' || value.attributes.value == 'url') {
          promises.push(loadImage(value.value).then(dataURL => {
            properties.PhotoType = 'web';
            properties.PhotoURI = value.value;
            customValues.photoURI = dataURL;
          }));
        }
        else if (value.attributes.encoding == 'b') {
          customValues.photoURI = `data:image/${String(value.attributes.type || 'unknown').toLowerCase()};base64,${value.value}`;
        }
      }; break;

      case 'logo': {
        const value = values.find(value => value.attributes.pref) || values[0];
        if (value.attributes.value == 'uri' || value.attributes.value == 'url') {
          promises.push(loadImage(value.value).then(dataURL => {
            customValues.logoSourceURI = value.value;
            customValues.logoURI = dataURL;
          }));
        }
        else if (value.attributes.encoding == 'b') {
          customValues.logoURI = `data:image/${String(value.attributes.type || 'unknown').toLowerCase()};base64,${value.value}`;
        }
      }; break;

      case 'x-chat-account':
        for (const value of values) {
          switch ((value.attributes.type).toLowerCase()) {
            case 'gtalk':
              properties._GoogleTalk = value.value;
              break;
            case 'aim':
              properties._AimScreenName = value.value;
              break;
            case 'yahoo':
              properties._Yahoo = value.value;
              break;
            case 'skype':
              properties._Skype = value.value;
              break;
            case 'qq':
              properties._QQ = value.value;
              break;
            case 'msn':
              properties._MSN = value.value;
              break;
            case 'icq':
              properties._ICQ = value.value;
              break;
            case 'xmmp':
              properties._JabberId = value.value;
              break;
            case 'irc':
              properties._IRC = value.value;
              break;
          }
        }
        break;

      case addressBookIdField:
        if (addressBookIdField) {
          addressBookId = joinAllValues(values);
          break;
        }
      default:
        if (CustomPersonalField.get(key)) {
          const bindTo = CustomPersonalField.get(key).bindTo;
          if (bindTo) {
            properties[bindTo] = joinAllValues(values);
            break;
          }
        }
        else if (CustomOrganizationField.get(key)) {
          const bindTo = CustomOrganizationField.get(key).bindTo;
          if (bindTo) {
            properties[bindTo] = joinAllValues(values);
            break;
          }
        }
        customValues[key] = values.map(value => value.value);
        break;
    }
  }

  if (promises.length > 0)
    await Promise.all(promises);

  if (organizations && organizations.length > 0)
    customValues.org = organizations;

  if (Object.keys(customValues).length > 0)
    properties[Constants.CONTACT_EXTRA_FIELDS_STORE] = JSON.stringify(customValues);

  //log(' => ', translated);
  const result = {
    contact: translated,
    customValues,
  };
  if (addressBookId)
    result.addressBookId = addressBookId;
  return result;
}

function joinAllValues(values, separator = null) {
  return values.map(value => {
    if (value && value.hasOwnProperty('value'))
      return value.value;
    else
      return value;
  }).join(separator || VALUE_SEPARATOR);
}

function splitValuesByType(values, defaultTypes = []) {
  const typed = {};
  const restValues = values.filter(value => {
    const type = String(value.attributes.type).toLowerCase();
    if (type) {
      if (typed.hasOwnProperty(type))
        typed[type].push(value.value);
      else
        typed[type] = [value.value];
      return false;
    }
    return true;
  });
  if (restValues.length > 0) {
    const lastType = defaultTypes[defaultTypes.length - 1];
    for (const type of defaultTypes) {
      const value = (type == lastType) ? joinAllValues(restValues) : restValues.shift().value;
      if (typed.hasOwnProperty(type))
        typed[type].push(value);
      else
        typed[type] = [value];
    }
  }
  return typed;
}

function splitTelValues(values) {
  for (const value of values) {
    if (/fax/i.test(value.value)) {
      value.attributes.type = 'fax';
    }
    else if (/phs|ピッチ/i.test(value.value)) {
      value.attributes.type = 'pager';
    }
    else if (/携帯|スマホ|スマフォ|スマートフォン/i.test(value.value)) {
      value.attributes.type = 'cell';
    }
    else if (/外線|内線|社内|所内/i.test(value.value)) {
      value.attributes.type = 'work';
    }
  }
  return splitValuesByType(values, ['home', 'work', 'fax', 'pager', 'cell']);
}

async function loadImage(url) {
  const image = await new Promise((resolve, reject) => {
    const loader = new Image();
    loader.addEventListener('load', () => resolve(loader), { once: true });
    loader.addEventListener('error', reject, { once: true });
    loader.src = url;
  });

  const canvas = document.createElement('canvas');
  canvas.width = image.width;
  canvas.height = image.height;
  canvas.setAttribute('style', `
    visibility: hidden;
    pointer-events: none;
    position: fixed
  `);
  document.body.appendChild(canvas);

  const context = canvas.getContext('2d');
  context.clearRect(0, 0, image.width, image.height);
  context.drawImage(image, 0, 0, image.width, image.height);
  let data;
  try {
    data = canvas.toDataURL('image/png');
  }
  catch(_e) {
    // it can fail due to security reasons
  }

  canvas.parentNode.removeChild(canvas);
  return data;
}
