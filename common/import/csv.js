/*
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
*/
'use strict';

import { RFC4180 } from '/extlib/rfc4180.js';

/*
import {
  log,
} from '/common/common.js';
*/
import { Contact } from '/common/Contact.js';
import * as VCard from './vcard.js';

export const MIME_TYPE = 'text/csv';
export const SUFFIX = 'csv';
export const SUFFIX_TAB = 'tsv';

const mCSVParser = new RFC4180(true, ',');
//const mTSVParser = new RFC4180(true, '\t');

// See RFC2426
const KNOWN_VCARD_FIELDS = new Set(`
SOURCE
NAME
PROFILE
FN
N
NICKNAME
PHOTO
BDAY
ADR
LABEL
TEL
EMAIL
MAILER
TZ
GEO
TITLE
ROLE
LOGO
AGENT
ORG
CATEGORIES
NOTE
PRODID
REV
SORT-STRING
SOUND
URL
UID
VERSION
CLASS
KEY
`.toLowerCase().trim().split(/\s+/));

const KNOWN_CONTACT_FIELDS_SET = new Set(Contact.KNOWN_FIELDS);

const LOCALIZED_LABEL_TO_INTERNAL_FIELD = {};
for (const field of Contact.KNOWN_FIELDS) {
  LOCALIZED_LABEL_TO_INTERNAL_FIELD[browser.i18n.getMessage(`csv_field_${field}`)] = field;
}

export async function parse(csv, { forceCreate, addressBookIdField } = {}) {
  //let rows;
  //if (mCSVParser.isCSV(csv))
  const rows = mCSVParser.toArray(csv.trim());
  //else if (mTSVParser.isCSV(csv))
  //  rows = mTSVParser.toArray(csv);
  //else
  //  throw new Error('invalid format');

  const header = rows.shift();
  const vCardFields = header.filter(field => KNOWN_VCARD_FIELDS.has(field.toLowerCase()));
  if (vCardFields.length > (header.length / 2)) {
    // if 50% or more fields have name same to VCard spec, treat them as VCard fields.
    return VCard.parse(rows.map(row => {
      const fields = row.map((column, index) => `${header[index]}:${column}`).join('\n');
      return `
BEGIN:VCARD
VERSION:4.0
${fields}
END:VCARD
      `.trim();
    }).join('\n'), { forceCreate, addressBookIdField });
  }

  const fields = header.map(localizedField => LOCALIZED_LABEL_TO_INTERNAL_FIELD[localizedField] || localizedField);
  const translateds = [];
  for (const row of rows) {
    translateds.push(translateRow(row, { fields, forceCreate, addressBookIdField }));
  }
  return (await Promise.all(translateds)).filter(translated => !!translated);
}

async function translateRow(row, { fields, forceCreate, addressBookIdField }) {
  let addressBookId = null;
  const properties = {};
  const translated = { properties };
  for (let i = 0, maxi = row.length; i < maxi; i++) {
    const field = fields[i];
    const value = row[i];
    if (KNOWN_CONTACT_FIELDS_SET.has(field)) {
      properties[field] = value || '';
    }
    else {
      if (addressBookIdField &&
          field == addressBookIdField) {
        addressBookId = value || '';
      }
      else {
        translated[field] = value || '';
      }
    }
  }
  if (translated.id && forceCreate) {
    const existingContact = await browser.contacts.get(translated.id).catch(_error => null);
    if (existingContact)
      delete translated.id;
  }
  return {
    contact: translated,
    addressBookId,
  };
}
