/*
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
*/
'use strict';

import Configs from '/extlib/Configs.js';

import * as Constants from '/common/constants.js';

const OVERRIDE_DEFAULT_CONFIGS = {}; /* Replace this for more customization on an enterprise use. */

export const configs = new Configs({
  staticAddressBooks: {
  /*
    "public": {
      "name": "Public",
      "color": "#32a852",
      "syncType": "cardDAV",
      "cardDAVURL": "https://carddav.example.com/public",
      "cardDAVUserName": "adam",
      "cardDAVPassword": "secret",
      "cardDAVAuthMethod": "basic",
      "periodicalSync": true,
      "periodicalSyncIntervalMinutes": 10
    },
    "shared": {
      "name": "Shared",
      "color": "#f28157",
      "syncType": "cardDAV",
      "cardDAVURL": "https://carddav.example.com/shared",
      "cardDAVUserName": "adam",
      "cardDAVPassword": "secret",
      "cardDAVAuthMethod": "digest",
      "periodicalSync": true,
      "periodicalSyncIntervalMinutes": 10
    },
    "private": {
      "name": "Private",
      "syncType": "local"
    }
  */
  },

  customChatProviders: [
  /*
    { "field": "x-zoom-id",
      "label": "Zoom" },
    { "field": "x-teams-id",
      "label": "Microsoft Teams",
      "linkMatcher": "[^@]+@[^@]+",
      "linkPrefix":  "msteams:/l/chat/0/0?users=",
      "contactsSeparator": "," },
  */
  ],
  preferredChatProvider: '', // field name ("_Skype", etc.)

  customPersonalInformationFields: [
  /*
    { "field":  "x-company-name",
      "label":  "Company",
      "bindTo": "Company" },
    { "field":  "x-group-name",
      "label":  "Department",
      "bindTo": "Department" },
  */
  ],
  customOrganizationInformationFields: [
  /*
    { "field":  "x-company-name",
      "label":  "Company",
      "bindTo": "Company" },
    { "field":  "x-group-name",
      "label":  "Department",
      "bindTo": "Department" },
  */
  ],

  mapProvider: 'OpenStreetMap',
  openUIInWindow: false,
  openInDefaultBrowser: true,
  editItemOnDblClick: true,

  organizationLabels: browser.i18n.getMessage('defaultOrganizationLabels').split(';'),
  uncategorizedLabel: browser.i18n.getMessage('defaultUncategorizedLabel'),

  showOrganizationsInTree: true,
  showCategoriesInTree:    true,
  showUncategorizedInTree: true,

  showToolbarSyncButton:              true,
  showToolbarStartCompositionButton:  true,
  showToolbarOptionsButton:           true,
  showToolbarSearchField:             true,
  showToolbarCreateMailingListButton: true,
  showToolbarShowItemButton:          true,
  showToolbarDeleteItemsButton:       true,
  showToolbarAddressBooksFilter:      true,

  syncOnStartup: true,
  syncOnStartupDelay: 0,
  defaultPeriodicalSync: true,
  defaultPeriodicalSyncIntervalMinutes: 30,

  defaultCardDAVAuthMethod: Constants.AUTH_METHOD_AUTO,

  defaultCustomDisplayNameFormat: '{{DisplayName}}',

  lastAddressBooksFilter: 'all',

  itemsBoxSizes: {
    'address-books': 250,
    'items': 400,
    'extra': 350,
  },
  showContactForMiddleLevelOrganizations: false,

  itemsDialogWidth: 800,
  itemsDialogHeight: 700,
  itemsDialogLeft: null,
  itemsDialogTop: null,

  addressBookDialogWidth: 400,
  //addressBookDialogHeight: 500,
  addressBookDialogLeft: null,
  addressBookDialogTop: null,

  contactDialogWidth: 600,
  contactDialogHeight: 700,
  contactDialogLeft: null,
  contactDialogTop: null,
  //contactBoxSizes: {
  //},

  mailingListDialogWidth: 600,
  mailingListDialogHeight: 700,
  mailingListDialogLeft: null,
  mailingListDialogTop: null,
  //mailingListBoxSizes: {
  //},

  mergeContactsDialogWidth: 800,
  mergeContactsDialogHeight: 700,
  mergeContactsDialogLeft: null,
  mergeContactsDialogTop: null,
  //mergeContactsBoxSizes: {
  //},

  addressBooks_defaultOrder: [
    // array of AddressBookNode.id or AddressBookNode.name
  ],
  subGroups_defaultOrder: {
    // key: AddressBookNode.id or AddressBookNode.name
    // value: Object (key: sub group path (A;B;C), value: index)
  },
  addressBooks_defaultExpanded: {
    // key: AddressBookNode.id or AddressBookNode.name
    // value: Boolean
  },
  subGroups_defaultExpanded: {
    // key: AddressBookNode.id or AddressBookNode.name
    // value: Object (key: sub group path (A;B;C), value: Boolean)
  },

  columnVisibility_selected: true,
  columnVisibility_NameField1: false,
  columnVisibility_NameField2: false,
  columnVisibility_PhoneticNameField1: false,
  columnVisibility_PhoneticNameField2: false,
  columnVisibility_visibleDisplayName: true,
  columnVisibility_SpouseName: false,
  columnVisibility_FamilyName: false,
  columnVisibility_namePrefix: false,
  columnVisibility_nameSuffix: false,
  columnVisibility_NickName: false,
  columnVisibility_PrimaryEmail: true,
  columnVisibility_SecondEmail: false,
  columnVisibility_ChatName: false,
  columnVisibility_PreferMailFormat: false,
  columnVisibility_WorkPhone: false,
  columnVisibility_HomePhone: false,
  columnVisibility_FaxNumber: false,
  columnVisibility_PagerNumber: false,
  columnVisibility_CellularNumber: false,
  columnVisibility_displayHomeAddress: false,
  columnVisibility_WebPage2: false,
  columnVisibility_birthDay: false,
  columnVisibility_anniversaryDay: false,
  columnVisibility_JobTitle: false,
  columnVisibility_Department: false,
  columnVisibility_Company: false,
  columnVisibility_displayWorkAddress: false,
  columnVisibility_WebPage1: false,
  columnVisibility_Other: false,
  columnVisibility_Custom1: false,
  columnVisibility_Custom2: false,
  columnVisibility_Custom3: false,
  columnVisibility_Custom4: false,
  columnVisibility_Notes: false,
  columnVisibility_categories: false,
  columnVisibility__GoogleTalk: false,
  columnVisibility__AimScreenName: false,
  columnVisibility__Yahoo: false,
  columnVisibility__Skype: false,
  columnVisibility__QQ: false,
  columnVisibility__MSN: false,
  columnVisibility__ICQ: false,
  columnVisibility__JabberId: false,
  columnVisibility__IRC: false,
  columnVisibility_Photo: false,
  columnVisibility_gender: false,

  itemsTabMailPopularityEnabled: false,
  itemsTabTechnicalEnabled: true,
  itemsTabVCardEnabled: true,

  itemsSearchChunkSize: 2000,
  itemsSearchInterval: 50,

  itemsLoadChunkSize: 2000,
  itemsLoadInterval: 50,

  itemsRenderChunkSize: 2000,
  itemsRenderInterval: 50,

  itemsPrintChunkSize: 1000,
  itemsPrintInterval: 50,

  encryption96bitIVFixedField: null,
  encryption32bitLastCounter: null,
  encryptionKey: null,

  unencryptableContactFields: [
    /*
    'DisplayName',
    'PrimaryEmail',
    'SecondEmail',
    */
  ],

  importEncryptedSourceSecretKeyDigestHash: 'SHA-512',
  importEncryptedSourceSecretKeyHash: 'SHA-512',
  importEncryptedSourceSecretKeySalt: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
  importEncryptedSourceSecretKeyIterations: 100000,
  importEncryptedSourceSecretKeyLength: 256,
  importEncryptedSourceIVLength: 128,
  importEncryptedSourceStaticTextEncoding: '',

  importFieldForAddressBookId: '',
  importFieldForAddressBookIdToIdMapping: {
    // "company": uuid,
    // "private": uuid
  },
  importFieldForAddressBookIdToNameMapping: {
    // "company": "社内",
    // "private": "プライベート"
  },
  addressBooksToBeClearedBeforeImport: [
    // "company",
    // "private",
    // "社内",
    // "プライベート",
    // uuid
  ],
  lastImportEncodingForType: {},

  printHeadings:                true,
  printFieldNames:              true,
  printDisplayName:             true,
  printPersonalInformation:     true,
  printOrganizationInformation: true,
  printCategory:                true,
  printAddress:                 true,
  printPhone:                   true,
  printEmail:                   true,
  printImpp:                    true,
  printUrl:                     true,
  printNotes:                   true,


  heartbeatInterval: 1000,
  connectionTimeoutDelay: 500,


  configsVersion: 0,
  debug: false,

  ...OVERRIDE_DEFAULT_CONFIGS
}, {
  syncKeys: [] // sync storage values can be resolved with invalidly saved values, so we always ignmore sync storage...
  /*
  localKeys: [
    'configsVersion',
    'debug'
  ]
  */
});

export function log(message, ...args) {
  if (!configs || !configs.debug)
    return;

  const nest   = (new Error()).stack.split('\n').length;
  let indent = '';
  for (let i = 0; i < nest; i++) {
    indent += ' ';
  }
  console.log(`corporative-contacts: ${indent}${message}`, ...args);
}

export async function wait(task = 0, timeout = 0) {
  if (typeof task != 'function') {
    timeout = task;
    task    = null;
  }
  return new Promise((resolve, _reject) => {
    setTimeout(async () => {
      if (task)
        await task();
      resolve();
    }, timeout);
  });
}

export function nextFrame() {
  return new Promise((resolve, _reject) => {
    window.requestAnimationFrame(resolve);
  });
}


export function toDOMDocumentFragment(source, parent) {
  const range = document.createRange();
  range.selectNodeContents(parent);
  range.collapse(false);
  const fragment = range.createContextualFragment(source.trim());
  range.detach();
  return fragment;
}

export function appendContents(parent, source) {
  const range = document.createRange();
  range.selectNodeContents(parent);
  range.collapse(false);
  const fragment = range.createContextualFragment(source.trim());
  range.insertNode(fragment);
  range.detach();
}

export function sanitizeForHTMLText(text) {
  return String(text || '')
    .replace(/&/g, '&amp;')
    .replace(/</g, '&lt;')
    .replace(/>/g, '&gt;')
    .replace(/"/g, '&quot;');
}

export function clone(object) {
  return JSON.parse(JSON.stringify(object));
}

export function orderItems(items, { getItemIndex, onInitialized, onOrdered }) {
  const orderedItems = new Array(items.length);
  const unorderedItems = new Set();
  const maxIndex = items.length - 1;
  for (const item of items) {
    if (typeof onInitialized == 'function')
      onInitialized(item);
    const index = Math.min(maxIndex, getItemIndex(item));
    if (index < 0) {
      unorderedItems.add(item);
    }
    else {
      const oldItem = orderedItems[index];
      if (oldItem)
        unorderedItems.add(oldItem);
      orderedItems[index] = item;
      if (typeof onOrdered == 'function')
        onOrdered(item, index);
    }
  }

  let index = 0;
  for (const item of unorderedItems) {
    while (orderedItems[index]) {
      index++;
    }
    orderedItems[index] = item;
    if (typeof onOrdered == 'function')
      onOrdered(item, index);
    index++;
  }
  log('orderItems ', orderedItems);
  return orderedItems;
}

export async function doProgressively({ items, task, chunkSize, interval } = {}) {
  chunkSize = Math.max(1, chunkSize || 0);
  interval = Math.min(0, Math.max(10, interval || 0));
  for (let i = 0, maxi = items.length; i < maxi; i += chunkSize) {
    const chunk = items.slice(i, i + chunkSize);
    const result = task(chunk);
    if (result &&
        typeof result == 'object' &&
        typeof result.then == 'function') {
      const [resolvedResult] = await Promise.all([result, wait(interval)]);
      if (resolvedResult === false) {
        return false;
      }
    }
    else {
      if (result === false) {
        return false;
      }
      await wait(interval);
    }
  }
  return true;
}

export async function openURL(url, { windowId } = {}) {
  return openURLs([url], { windowId });
}

export async function openURLs(urls, { windowId } = {}) {
  let active = true;
  const extraOptions = {};
  if (windowId)
    extraOptions.windowId = windowId;
  const promises = [];
  for (const url of urls) {
    if (typeof browser.windows.openDefaultBrowser == 'function' &&
        configs.openInDefaultBrowser) {
      promises.push(browser.windows.openDefaultBrowser(url));
    }
    else {
      promises.push(browser.tabs.create({
        active,
        url,
        ...extraOptions,
      }));
    }
    active = false;
  }
  return Promise.all(promises);
}
