/*
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
*/
'use strict';

import EventListenerManager from '/extlib/EventListenerManager.js';

import {
  configs,
  doProgressively,
} from '/common/common.js';

export class ItemsSearcher {
  constructor() {
    this.$lastSearchStartedTime = 0;
    this.activeQuery = '';
    this.active = false;

    this.onCleared = new EventListenerManager();
    this.onStarted = new EventListenerManager();
    this.onFound = new EventListenerManager();
    this.onFinished = new EventListenerManager();
    this.onCanceled = new EventListenerManager();

    this.onSearchInput = this.onSearchInput.bind(this);
  }

  bindTo(field) {
    this.$field = field;
    this.$field.addEventListener('keydown', this.onSearchInput);
    this.$field.addEventListener('input', this.onSearchInput);
  }

  onSearchInput(event) {
    if (event.isComposing)
      return;

    if (this.$throttled)
      clearTimeout(this.$throttled);

    if (event.key == 'Escape')
      this.$field.value = '';

    this.$throttled = setTimeout(() => {
      this.$throttled = null;
      this.search(this.$field.value);
    }, 150);
  }

  clear() {
    this.active = false;
    this.$lastSearchStartedTime = 0;
    this.activeQuery = '';
    this.$field.value = '';
    this.onCleared.dispatch(this);
  }

  async search(query) {
    query = String(query).trim();
    if (query == this.activeQuery)
      return;

    this.$lastSearchStartedTime = Date.now();
    this.activeQuery = query;

    if (query.length == '')
      return this.clear();

    const startedAt = this.$lastSearchStartedTime;

    const items = (await this.onStarted.dispatch(this)).flat();
    this.active = true;
    const matcher = this.createMatcher(query);
    let promisedLastOnFound;
    await doProgressively({
      items,
      task: items => {
        if (this.$lastSearchStartedTime != startedAt) {
          this.onCanceled.dispatch(this);
          return false;
        }
        const found = items.filter(item => (
          (!matcher ||
           matcher.test(item.searchString))
        ));
        if (found.length > 0) {
          if (promisedLastOnFound)
            promisedLastOnFound = promisedLastOnFound.then(() => this.onFound.dispatch(this, found, query));
          else
            promisedLastOnFound = this.onFound.dispatch(this, found, query);
        }
      },
      chunkSize: configs.itemsSearchChunkSize,
      interval:  configs.itemsSearchInterval,
    });
    this.onFinished.dispatch(this);
  }

  createMatcher(query) {
    if (/^\/(.+)\/(i)?$/.test(query))
      return new RegExp(RegExp.$1, RegExp.$2 || '');
  
    const patterns = ['^'];
    for (const term of query.trim().split(/\s+/)) {
      if (term.length > 1 && term.startsWith('-')) {
        patterns.push(`(?!.*?${this.sanitizeForRegExp(term.slice(1))})`);
      }
      else {
        patterns.push(`(?=.*?${this.sanitizeForRegExp(term)})`);
      }
    }
    return new RegExp(patterns.join(''), 'i');
  }

  sanitizeForRegExp(term) {
    return term.replace(/[-[/\]{}()*+?.,\\^$|#\s]/g, '\\$&');
  }
}
