/*
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
*/
'use strict';

import * as Dialog from '/extlib/dialog.js';

import {
  configs,
  log,
  wait,
} from '/common/common.js';
import { AddressBook, SubGroup } from '/common/AddressBook.js';
import { CardDAVBridge } from './CardDAVBridge.js';
import * as CustomField from '/common/CustomField.js';
import * as Connection from './connection.js';
import * as Constants from '/common/constants.js';
import * as Import from './import.js';
import { Item, Contact, MailingList } from '/common/Contact.js';

const URL_ITEMS_UI = browser.runtime.getURL('/ui/items/items.html');

AddressBook.syncEnabled = true;
AddressBook.autoTrack();
Contact.autoTrack();
MailingList.autoTrack();

CustomField.bindToConfigs(configs);

function onConfigChanged(key) {
  switch (key) {
    case 'openUIInWindow':
      if (configs[key]) {
        closeUI().then(async closed => {
          await wait(150);
          if (closed)
            openUIInWindow();
        });
      }
      else {
        closeUI().then(async closed => {
          await wait(150);
          if (closed)
            openUIInTab();
        });
      }
      break;

  }
}
configs.$addObserver(onConfigChanged);

let mInitiallyLoadedResolver;
const mInitiallyLoaded = new Promise(resolev => {
  mInitiallyLoadedResolver = resolev;
});

configs.$loaded.then(async () => {
  const addressBooks = await browser.addressBooks.list(true);
  const promises = [];
  for (const addressBook of addressBooks) {
    const wrappedAddressBook = new AddressBook(addressBook);
    promises.push(wrappedAddressBook.initialized.then(() => {
      if (!wrappedAddressBook.color) {
        wrappedAddressBook.setRandomColor();
        wrappedAddressBook.save();
      }
      return wrappedAddressBook.startLoad();
    }));
  }
  Promise.all(promises).then(mInitiallyLoadedResolver);

  const wrappedAddressBooks = AddressBook.getAll();
  await Promise.all(wrappedAddressBooks.map(addressBook => addressBook.initialized));

  for (const [staticId, definition] of Object.entries(configs.staticAddressBooks || {})) {
    let addressBook = wrappedAddressBooks.find(addressBook => addressBook.staticId == staticId)
    log('static address book: ', staticId, addressBook);
    if (!addressBook) {
      log(` => CREATE STATIC ADDRESS BOOK ${staticId}`);
      const id = await browser.addressBooks.create({
        name: definition.name || staticId,
      });
      addressBook = new AddressBook(await browser.addressBooks.get(id));
      await addressBook.initialized;
      addressBook.staticId = staticId;
    }
    let updated = false;
    for (const [field, value] of Object.entries(definition)) {
      log('  FIELD: ', field, addressBook[field], value);
      if (addressBook[field] == value)
        continue;
      addressBook[field] = value;
      updated = true;
    }
    log('  UPDATED: ', updated, addressBook.staticId);
    if (updated)
      addressBook.save();

    if (addressBook.isRemote) {
      addressBook.tryConnect().then(succeeded => {
        log('  trial result: ', succeeded, addressBook.staticId);
        addressBook.hidden = !succeeded;
        addressBook.save();
      });
    }
    else {
      addressBook.hidden = false;
      addressBook.save();
    }
  }
});

browser.browserAction.onClicked.addListener(async (tab, _info) => {
  if (configs.openUIInWindow)
    openUIInWindow();
  else
    openUIInTab(tab);
});

async function openUIInWindow() {
  const focused = await browser.runtime.sendMessage({
    type: Constants.COMMAND_FOCUS_TO_ITEMS_DIALOG
  }).catch(_error => false);
  if (focused)
    return;

  const dialogParams = {
    url:    `${URL_ITEMS_UI}?mode=window`,
    width:  configs.itemsDialogWidth,
    height: configs.itemsDialogHeight,
  };
  if (typeof configs.itemsDialogLeft == 'number')
    dialogParams.left = configs.itemsDialogLeft;
  if (typeof configs.itemsDialogTop == 'number')
    dialogParams.top = configs.itemsDialogTop;
  return Dialog.open(
    dialogParams,
    {}
  );
}

async function openUIInTab(tab) {
  const tabs = await browser.tabs.query({
    //url: `${URL_ITEMS_UI}*` // this causes error...
  });
  for (const tab of tabs) {
    if (!tab.url || !tab.url.startsWith(URL_ITEMS_UI))
      continue;
    browser.tabs.update(tab.id, { active: true });
    return;
  }
  browser.tabs.create({
    url:      `${URL_ITEMS_UI}?mode=tab`,
    windowId: tab && tab.windowId,
    active:   true,
  });
}

async function closeUI() {
  const tabs = await browser.tabs.query({
    //url: `${URL_ITEMS_UI}*` // this causes error...
  });
  for (const tab of tabs) {
    if (!tab.url || !tab.url.startsWith(URL_ITEMS_UI))
      continue;
    browser.tabs.remove(tab.id);
    return true;
  }
  return false;
}

Connection.onMessage.addListener((message, clientId) => {
  switch (message && message.type) {
    case Constants.COMMAND_FETCH_ALL_ADDRESS_BOOKS:
      Promise.all(AddressBook.getAll().map(addressBook => addressBook.export())).then(addressBooks =>
        Connection.sendMessage({
          clientId,
          type: Constants.COMMAND_PUSH_ALL_ADDRESS_BOOKS,
          addressBooks,
        })
      );
      break;

    case Constants.COMMAND_PUSH_GROUP_UPDATE: {
      const group = AddressBook.get(message.id) || SubGroup.get(message.id);
      if (!group)
        break;
      group.initialized.then(() => {
        for (const [field, value] of Object.entries(message.fields)) {
          group[field] = value;
        }
        group.invalidateExportedCache();
      });
    }; break;

    case Constants.COMMAND_MOVE_UNCATEGORIZED_TO_END: {
      const addressBook = AddressBook.get(message.id);
      if (!addressBook)
        break;
      addressBook.initialized.then(() => {
        addressBook.moveUncategorizedToEnd();
      });
    }; break;

    case Constants.COMMAND_SORT_CHILDREN_BY_INDEX: {
      const group = AddressBook.get(message.id) || SubGroup.get(message.id);
      if (!group)
        break;
      group.initialized.then(() => {
        group.sortChildrenByIndex();
        if (group.type == 'addressBook')
          group.moveUncategorizedToEnd();
      });
    }; break;

    case Constants.COMMAND_FETCH_ALL_CONTACTS: {
      Promise.all(Contact.getAll().map(async contact => ((await contact.initialized), contact).export())).then(contacts =>
        Connection.sendMessage({
          clientId,
          type: Constants.COMMAND_PUSH_ALL_CONTACTS,
          contacts,
        })
      );
    }; break;

    case Constants.COMMAND_FETCH_ALL_MAILING_LISTS: {
      Promise.all(MailingList.getAll().map(async mailingList => ((await mailingList.initialized), mailingList).export())).then(mailingLists =>
        Connection.sendMessage({
          clientId,
          type: Constants.COMMAND_PUSH_ALL_MAILING_LISTS,
          mailingLists,
        })
      );
    }; break;

    case Constants.COMMAND_FETCH_ITEMS_OF: {
      const parent = AddressBook.get(message.parentId) || SubGroup.get(message.parentId);
      if (!parent)
        break;
      Promise.all(parent.items.map(async item => ((await item.initialized), item).export())).then(items =>
        Connection.sendMessage({
          clientId,
          type:     Constants.COMMAND_PUSH_ITEMS_OF,
          parentId: message.parentId,
          items:    items.sort((a, b) => a.type == b.type ? 0 : a.type == 'mailingList' ? -1 : 1),
        })
      );
    }; break;

    case Constants.COMMAND_MOVE_ITEM_TO: {
      const item = Contact.get(message.id) || MailingList.get(message.id);
      if (!item)
        break;
      const sourceParentId = item.parentId;
      item.moveTo(message.destinationParentId).then(async item => {
        await item.initialized;
        Connection.sendMessage({
          clientId,
          type: Constants.COMMAND_PUSH_MOVED_ITEM,
          item: item.export(),
          sourceItemId: message.id,
          sourceParentId,
        });
      });
    }; break;

    case Constants.COMMAND_COPY_ITEM_TO: {
      const item = Contact.get(message.id) || MailingList.get(message.id);
      if (!item)
        break;
      const sourceParentId = item.parentId;
      item.copyTo(message.destinationParentId).then(async item => {
        await item.initialized;
        Connection.sendMessage({
          clientId,
          type: Constants.COMMAND_PUSH_COPIED_ITEM,
          item: item.export(),
          sourceItemId: message.id,
          sourceParentId,
        });
      });
    }; break;

    case Constants.COMMAND_FETCH_ALL_AVAILABLE_CATEGORIES:
      Connection.sendMessage({
        clientId,
        type:       Constants.COMMAND_PUSH_ALL_AVAILABLE_CATEGORIES,
        categories: Array.from(Item.availableCategories),
      });
      break;

    case Constants.COMMAND_PUSH_ITEM_UPDATE: {
      const item = Contact.get(message.id) || MailingList.get(message.id);
      if (item) {
        for (const [field, value] of Object.entries(message.fields)) {
          item[field] = value;
        }
        item.save();
      }
    }; break;
  }
});

browser.runtime.onMessage.addListener((message, _sender) => {
  switch (message && message.type) {
    case Constants.COMMAND_IMPORT_CONTACTS:
      return (async () => {
        const addressBook = message.addressBookId && await browser.addressBooks.get(message.addressBookId);
        return Import.importContacts({
          contacts:        message.contacts,
          addressBookName: message.addressBookName,
          addressBook,
          replaceAll:      message.replaceAll,
        });
      })();

    case Constants.COMMAND_FETCH_RAW_CONTACT: { // Workaround for https://gitlab.com/clear-code/tb-corporative-contacts/-/issues/49
      const item = Contact.get(message.id);
      return item && Promise.resolve(item.$raw);
    }; break;

    default:
      break;
  }
});


AddressBook.onCreated.addListener(async addressBook => {
  await mInitiallyLoaded;
  await addressBook.initialized;
  Connection.sendMessage({
    type:        Constants.COMMAND_PUSH_NEW_ADDRESS_BOOK,
    addressBook: addressBook.export(),
  });
});

AddressBook.onUpdated.addListener(async (addressBookId, updatedFields, addressBook) => {
  delete updatedFields.cardDAVPassword;
  if ('synchronizing' in updatedFields ||
      'readOnly' in updatedFields)
    updatedFields.status = addressBook.status;
  await mInitiallyLoaded;
  Connection.sendMessage({
    type:   Constants.COMMAND_PUSH_GROUP_UPDATE,
    id:     addressBookId,
    fields: updatedFields,
  });
});

AddressBook.onAllChildrenDisappeared.addListener(async addressBook => {
  await mInitiallyLoaded;
  Connection.sendMessage({
    type: Constants.COMMAND_PUSH_ALL_CHILDREN_DISAPPEARED,
    id:   addressBook.id,
  });
});

AddressBook.onRemoved.addListener(async id => {
  await mInitiallyLoaded;
  Connection.sendMessage({
    type: Constants.COMMAND_PUSH_ADDRESS_BOOK_DELETE,
    id,
  });
});

AddressBook.onChildAdded.addListener(async (addressBook, subGroup) => {
  await mInitiallyLoaded;
  await subGroup.initialized;
  Connection.sendMessage({
    type:     Constants.COMMAND_PUSH_NEW_GROUP_CHILD,
    parentId: addressBook.id,
    child:    subGroup.export(),
  });
});

AddressBook.onChildRemoved.addListener(async (addressBook, subGroupId) => {
  await mInitiallyLoaded;
  Connection.sendMessage({
    type:     Constants.COMMAND_PUSH_GROUP_CHILD_DELETE,
    parentId: addressBook.id,
    childId:  subGroupId,
  });
});

AddressBook.onItemAdded.addListener(async (addressBook, item) => {
  await mInitiallyLoaded;
  await item.initialized;
  Connection.sendMessage({
    type:     Constants.COMMAND_PUSH_NEW_ITEM,
    parentId: addressBook.id,
    item:     item.export(),
  });
});

/*
SubGroup.onUpdated.addListener(async (subGroupId, updatedFields, _subGroup) => {
  await mInitiallyLoaded;
  Connection.sendMessage({
    type:   Constants.COMMAND_PUSH_GROUP_UPDATE,
    id:     subGroupId,
    fields: updatedFields,
  });
});
*/

SubGroup.onAllChildrenDisappeared.addListener(async subGroup => {
  await mInitiallyLoaded;
  Connection.sendMessage({
    type: Constants.COMMAND_PUSH_ALL_CHILDREN_DISAPPEARED,
    id:   subGroup.id,
  });
});

SubGroup.onRemoved.addListener(async (id, parent) => {
  await mInitiallyLoaded;
  Connection.sendMessage({
    type:     Constants.COMMAND_PUSH_GROUP_CHILD_DELETE,
    parentId: parent.id,
    childId:  id,
  });
});

SubGroup.onChildAdded.addListener(async (parentGroup, subGroup) => {
  await mInitiallyLoaded;
  await subGroup.initialized;
  Connection.sendMessage({
    type:     Constants.COMMAND_PUSH_NEW_GROUP_CHILD,
    parentId: parentGroup.id,
    child:    subGroup.export(),
  });
});

SubGroup.onChildRemoved.addListener(async (parentGroup, subGroupId) => {
  await mInitiallyLoaded;
  Connection.sendMessage({
    type:     Constants.COMMAND_PUSH_GROUP_CHILD_DELETE,
    parentId: parentGroup.id,
    childId:  subGroupId,
  });
});

SubGroup.onItemAdded.addListener(async (group, item) => {
  await mInitiallyLoaded;
  await item.initialized;
  Connection.sendMessage({
    type:     Constants.COMMAND_PUSH_NEW_ITEM,
    parentId: group.id,
    item:     item.export(),
  });
});

Contact.onUpdated.addListener(async (contactId, updatedFields, _contact) => {
  await mInitiallyLoaded;
  Connection.sendMessage({
    type:   Constants.COMMAND_PUSH_ITEM_UPDATE,
    id:     contactId,
    fields: updatedFields,
  });
});

Contact.onRemoved.addListener(async (_parentId, contactId) => {
  await mInitiallyLoaded;
  Connection.sendMessage({
    type: Constants.COMMAND_PUSH_ITEM_DELETE,
    id:   contactId,
  });
});

Contact.onMoved.addListener(async (contactId, oldParentId, newParentId) => {
  await mInitiallyLoaded;
  Connection.sendMessage({
    type: Constants.COMMAND_PUSH_ITEM_MOVE,
    id:   contactId,
    oldParentId,
    newParentId,
  });
});

MailingList.onUpdated.addListener(async (mailingListId, updatedFields, _mailingList) => {
  await mInitiallyLoaded;
  Connection.sendMessage({
    type:   Constants.COMMAND_PUSH_ITEM_UPDATE,
    id:     mailingListId,
    fields: updatedFields,
  });
});

MailingList.onRemoved.addListener(async (_parentId, mailingListId) => {
  await mInitiallyLoaded;
  Connection.sendMessage({
    type: Constants.COMMAND_PUSH_ITEM_DELETE,
    id:   mailingListId,
  });
});

MailingList.onContactAdded.addListener(async (mailingList, contact) => {
  await mInitiallyLoaded;
  await contact.initialized;
  Connection.sendMessage({
    type: Constants.COMMAND_PUSH_MAILING_LIST_CONTACT_ADD,
    id:   mailingList.id,
    contact: contact.export(),
  });
});

MailingList.onContactDeleted.addListener(async (mailingList, contactId) => {
  await mInitiallyLoaded;
  Connection.sendMessage({
    type: Constants.COMMAND_PUSH_MAILING_LIST_CONTACT_REMOVE,
    id:   mailingList.id,
    contactId,
  });
});

Item.onAvailableCategoryAdded.addListener(category => {
  Connection.sendMessage({
    type: Constants.COMMAND_PUSH_NEW_AVAILABLE_CATEGORY,
    category,
  });
});



// for debugging
window.configs = configs;
window.Item = Item;
window.Contact = Contact;
window.MailingList = MailingList;
window.AddressBook = AddressBook;
window.SubGroup = SubGroup;
window.CardDAVBridge = CardDAVBridge;
