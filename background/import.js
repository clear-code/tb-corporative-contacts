/*
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
*/
'use strict';

import { AddressBook } from '/common/AddressBook.js';
import { Contact } from '/common/Contact.js';

export async function importContacts({ contacts, addressBook, addressBookName, replaceAll }) {
  let addressBookId = addressBook && addressBook.id;
  let created = false;
  if (addressBookId) {
    const addressBook = AddressBook.get(addressBookId) || new AddressBook(await browser.addressBooks.get(addressBookId));
    await addressBook.initialized;
    if (replaceAll)
      await addressBook.deleteAllItems();
  }
  else {
    addressBookId = await browser.addressBooks.create({ name: addressBookName });
    if (!addressBookId)
      throw new Error(`failed to create new address book ${addressBookName}`);
    created = true;
    const addressBook = AddressBook.get(addressBookId) || new AddressBook(await browser.addressBooks.get(addressBookId));
    await addressBook.initialized.then(() => {
      addressBook.setRandomColor();
      console.log(addressBook.id, addressBook.color);
      addressBook.save();
    });
  }

  let createdCount = 0;
  const promisedContacts = [];
  await Promise.all(contacts.map(async contact => {
    try {
      const [existingContact, wrapped] = await Promise.all([
        contact.id && await browser.contacts.get(contact.id).catch(_error => null),
        (async () => {
          const wrapped = new Contact({
            ...contact,
            parentId: addressBookId,
          });
          if (contact.properties.DisplayName)
            wrapped.visibleDisplayName = contact.properties.DisplayName;
          await wrapped.save();
          return wrapped;
        })(),
      ]);
      if (!existingContact)
        createdCount++;
      promisedContacts.push(browser.contacts.get(wrapped.id));
    }
    catch(error) {
      console.log('failed to import: ', contact, error);
    }
  }));
  if (createdCount == 0 && created)
    await browser.addressBooks.delete(addressBookId);

  const [ importedAddressBook, ...importedContacts ] = await Promise.all([
    browser.addressBooks.get(addressBookId),
    ...promisedContacts,
  ]);
  return {
    addressBook: importedAddressBook,
    contacts:    importedContacts,
  };
}
