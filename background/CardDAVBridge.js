/*
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
*/
'use strict';

import { dav } from '/extlib/dav.js';
import RichConfirm from '/extlib/RichConfirm.js';

import {
  configs,
  log,
  sanitizeForHTMLText,
} from '/common/common.js';
import { AddressBook } from '/common/AddressBook.js';
import * as Constants from '/common/constants.js';
import { Contact } from '/common/Contact.js';
import * as Import from './import.js';
import * as VCard from '/common/import/vcard.js';

let mLastPrompt = Promise.resolve();

export class CardDAVBridge {
  constructor(wrappedAddressBook) {
    this.$wrappedAddressBook = wrappedAddressBook;
    this.$cards = new Map();

    CardDAVBridge.instances.set(wrappedAddressBook.id, this);
  }

  async getURL() {
    const url = this.$wrappedAddressBook.cardDAVURL;
    if (!/\$UserName\$/i.test(url))
      return url;
    const userName = await this.getUserName();
    return url.replace(/\$UserName\$/gi, userName);
  }

  async getUserName() {
    const userName = this.$wrappedAddressBook.cardDAVUserName;
    if (!/\$DefaultAccountLocalPart\$/i.test(userName))
      return userName;
    const guessedUserName = await this.guessDefaultUserAccountName();
    return userName.replace(/\$DefaultAccountLocalPart\$/gi, guessedUserName);
  }

  async getPassword() {
    const password = this.$wrappedAddressBook.cardDAVPassword;
    if (password)
      return password;

    const userName = (await this.getUserName()) || '';
    let result;
    mLastPrompt = mLastPrompt.then(async () => {
      if (this.$wrappedAddressBook.inheritCardDAVAuthInfoFrom)
        throw new Error('never ask password for inherited account');
      if (this.$passwordPromptSuppressed)
        throw new Error('password prompt suppressed');
      result = await RichConfirm.showInPopup({
        modal: true,
        type: 'common-dialog',
        content: [
          `<p style="margin-bottom: 0.5em;"
             >${sanitizeForHTMLText(browser.i18n.getMessage('promptCardDAVAuthInfo_message', [this.$wrappedAddressBook.name]))}</p>`,
          `<p style="display: flex; flex-directon: column; align-items: stretch;"
             ><input type="text"
                     name="userName"
                     placeholder=${JSON.stringify(sanitizeForHTMLText(browser.i18n.getMessage('promptCardDAVAuthInfo_userNamePlaceholder')))}
                     style="width: 100%;"></p>`,
          `<p style="display: flex; flex-directon: column; align-items: stretch;"
             ><input type="password"
                     name="password"
                     placeholder=${JSON.stringify(sanitizeForHTMLText(browser.i18n.getMessage('promptCardDAVAuthInfo_passwordPlaceholder')))}
                     style="width: 100%;"></p>`,
        ].join(''),
        onShown(container, { userName }) {
          container.querySelector('[name="userName"]').value = userName;
        },
        inject: {
          userName,
        },
        checkMessage: browser.i18n.getMessage('promptCardDAVAuthInfo_suppressPrompt'),
        buttons: [
          browser.i18n.getMessage('promptCardDAVAuthInfo_accept'),
          browser.i18n.getMessage('promptCardDAVAuthInfo_cancel'),
        ],
      });
    }).catch(console.error);
    await mLastPrompt;
    if (result && result.checked)
      this.$passwordPromptSuppressed = true;
    if (!result || result.buttonIndex != 0)
      throw new Error('password input canceled');

    if (result.values.userName != userName)
      this.$wrappedAddressBook.cardDAVUserName = result.values.userName;
    if (result.values.password)
      this.$wrappedAddressBook.cardDAVPassword = result.values.password;
    this.$wrappedAddressBook.save();

    return this.$wrappedAddressBook.cardDAVPassword;
  }

  get authMethod() {
    return this.$wrappedAddressBook.cardDAVAuthMethod;
  }

  get cardDAVSyncToken() {
    return this.$wrappedAddressBook.cardDAVSyncToken;
  }

  set cardDAVSyncToken(newValue) {
    return this.$wrappedAddressBook.cardDAVSyncToken = newValue;
  }

  async guessDefaultUserAccountName() {
    const accounts = await browser.accounts.list();
    for (const account of accounts) {
      const identities = account.identities;
      if (!identities ||
          identities.length == 0)
        continue;
      for (const identity of identities) {
        const email = identity.email;
        if (email &&
            /^\s*([^[]+)@.+$/.test(email))
          return RegExp.$1;
      }
    }
    return null;
  }

  async createNewSender() {
    const [username, password] = await Promise.all([
      this.getUserName(),
      this.getPassword(),
    ]);
    return new dav.transport.Basic(new dav.Credentials({
      username,
      password,
      authType: this.authMethod,
    }));
  }

  get initialized() {
    if (this.$initialized)
      return this.$initialized;

    return this.$initialized = new Promise(async (resolve, reject) => {
      log('Fetching account information');
      await this.$wrappedAddressBook.initialized;
      try {
        const xhr = await this.createNewSender();
        // the URL must be got after sender, because the embedded user name can be changed.
        const server = await this.getURL();
        const account = await dav.createAccount({
          accountType: 'carddav',
          server,
          xhr,
        });
        this.$addressBook = account.addressBooks[0];
        log('fetched addressbook: ', this.$addressBook);
        resolve(true);
      }
      catch(error) {
        log('Failed to fetch account information: ', error);
        reject(error);
      }
    });
  }

  contactToVCard(contact) {
    const addressBook = this.$addressBook;
    const url = (new URL(contact.vCardFileName, addressBook.url)).toString();
    const props = {
      addressData: contact.vCard,
      getetag:     contact.etag,
    };
    return new dav.VCard({
      addressBook,
      data: {
        href: url,
        props
      },
      url,
      ...props,
    });
  }

  async onContactCreated(contact, { local } = {}) {
    if (this.$wrappedAddressBook.readOnly)
      return;

    await this.initialized;

    const oldCard = this.$cards.get(contact.id)
    if (oldCard && oldCard.etag == contact.etag)
      return;

    const addressBook = this.$addressBook;
    const card = this.contactToVCard(contact);
    if (!local) {
      log('Pushing created contact to server: ', contact);
      const response = await dav.createCard(addressBook, {
        data:     contact.vCard,
        xhr:      await this.createNewSender(),
        filename: contact.vCardFileName,
      });
      const etag = response.getResponseHeader('etag');
      if (etag) {
        contact.etag = etag;
        contact.save();
      }
    }
    this.$cards.set(contact.id, card);

    addressBook.objects = addressBook.objects || [];
    const index = addressBook.objects.findIndex(object => object.url == card.url);
    if (index < 0)
      addressBook.objects.push(card);
  }

  async onContactUpdated(contact, { local } = {}) {
    if (this.$wrappedAddressBook.readOnly)
      return;

    await this.initialized;

    const oldCard = this.$cards.get(contact.id)
    if (oldCard && oldCard.etag == contact.etag)
      return;

    const card = this.contactToVCard(contact);
    if (!local) {
      log('Pushing updated contact to server: ', contact);
      await dav.updateCard(card, {
        xhr: await this.createNewSender(),
      });
    }
    this.$cards.set(contact.id, card);

    const addressBook = this.$addressBook;
    addressBook.objects = addressBook.objects || [];
    const index = addressBook.objects.findIndex(object => object.url == card.url);
    if (index > -1)
      addressBook.objects.splice(index, 1, card);
  }

  async onContactDeleted(contactId, { local } = {}) {
    if (this.$wrappedAddressBook.readOnly)
      return;

    const card = this.$cards.get(contactId);
    if (!card)
      return;

    if (!local) {
      await this.initialized;
      log('Deleting contact from server: ', contactId);
      await dav.deleteCard(card, {
        xhr: await this.createNewSender(),
      });
    }

    this.$cards.delete(contactId);

    const addressBook = this.$addressBook;
    addressBook.objects = addressBook.objects || [];
    const index = addressBook.objects.findIndex(object => object.url == card.url);
    if (index > -1)
      addressBook.objects.splice(index, 1);
  }

  async getCardIds() {
    return new Set((await Promise.all((this.$addressBook && this.$addressBook.objects || []).map(async vcard => {
      const contactSources = await VCard.parse(vcard.addressData, {
        addressBookIdField: configs.importFieldForAddressBookId,
      });
      return contactSources.map(contactSource => contactSource.contact.id).filter(id => !!id);
    }))).flat());
  }

  async sync({ force } = {}) {
    this.$wrappedAddressBook.synchronizing = true;
    await this.$wrappedAddressBook.save();
    try {
      await this.initialized;

      log('Fetching contacts, force = ', force);
      const addressBook = this.$addressBook;
      if (this.syncToken)
        addressBook.syncToken = this.syncToken;
      const oldIds = await this.getCardIds();
      const result = await dav.syncAddressBook(addressBook, {
        syncMethod: force ? 'basic' : 'webdav',
        xhr: await this.createNewSender(),
      });

      log('Start to import concacts from the server: ', { addressBook, result });

      // step 1: parse vCards parallelly
      const syncItems = (await Promise.all(result.objects.map(async vcard => {
        const contactSources = vcard.addressData ? (await VCard.parse(vcard.addressData, { addressBookIdField: configs.importFieldForAddressBookId })).map(result => result.contact) : [];
        const newEtag = vcard.etag;
        return Promise.all(contactSources.map(async contactSource => {
          this.$cards.set(contactSource.id, vcard);
          const rawContact = contactSource.id && await browser.contacts.get(contactSource.id).catch(_error => null);
          let oldEtag;
          try {
            if (rawContact && rawContact.properties[Constants.CONTACT_EXTRA_FIELDS_STORE])
              oldEtag = JSON.parse(rawContact.properties[Constants.CONTACT_EXTRA_FIELDS_STORE]).etag;
          }
          catch(_error) {
          }
          return {
            contactSource,
            rawContact,
            oldEtag,
            newEtag,
          };
        }));
      }))).flat();

      // step 2: import vCards to Thunderbird's address book database parallelly
      const importedContacts = (await Promise.all(syncItems.map(async syncItem => {
        if (syncItem.oldEtag == syncItem.newEtag)
          return [];
        log('Importing: ', syncItem.contactSource);
        const { contacts } = await Import.importContacts({
          contacts: [syncItem.contactSource],
          addressBook: this.$wrappedAddressBook,
        });
        return contacts.map(contact => {
          const wrapped = contact.id && Contact.get(contact.id) || new Contact(contact);
          wrapped.etag = syncItem.newEtag;
          wrapped.save();
          return wrapped;
        });
      }))).flat();

      // step 3: delte cards removed on the server
      const newIds = await this.getCardIds();
      const promisedDeletions = [];
      for (const id of oldIds) {
        if (newIds.has(id))
          continue;
        promisedDeletions.push(browser.contacts.delete(id).catch(_error => {}));
      }
      await Promise.all(promisedDeletions);

      this.syncToken = result.syncToken;
      await this.$wrappedAddressBook.save();

      log(`Total number of contacts fetched from the CardDAV server: ${importedContacts.length}, ${promisedDeletions.length} contacts are removed.`);
    }
    catch(error) {
      log('Failed to synchronize: ', error, error.stack);
    }
    this.$wrappedAddressBook.synchronizing = false;
    await this.$wrappedAddressBook.save();
  }

  async tryConnect() {
    const sender = await this.createNewSender();
    const request = dav.request.propfind({
      props: [],
      depth: 0,
    });
    const url = await this.getURL();
    try {
      const results = await sender.send(request, url);
      if (results.length > 0 &&
          results.some(result =>
            result.props && result.props.resourcetype && result.props.resourcetype.length > 0)) {
        log('successfully connected to the remote address book: ', url);
        return true;
      }
    }
    catch(error) {
      log('failed to connect to the remote address book: ', url, error);
    }
    log('trial failed: ', url);
    return false;
  }
}

CardDAVBridge.instances = new Map();

CardDAVBridge.get = addressBook => {
  return CardDAVBridge.instances.get(addressBook.id) || new CardDAVBridge(addressBook);
};

CardDAVBridge.getAll = () => {
  return Array.from(CardDAVBridge.instances.values());
};


AddressBook.onItemAdded.addListener((addressBook, contact, { local } = {}) => {
  if (addressBook.syncType != Constants.SYNC_TYPE_CARDDAV)
    return;
  CardDAVBridge.get(addressBook).onContactCreated(contact, { local }).catch(console.error);
});

AddressBook.onItemUpdated.addListener((addressBook, contact, { local } = {}) => {
  if (addressBook.syncType != Constants.SYNC_TYPE_CARDDAV)
    return;
  CardDAVBridge.get(addressBook).onContactUpdated(contact, { local }).catch(console.error);
});

AddressBook.onItemRemoved.addListener((addressBook, contactId, { local } = {}) => {
  if (addressBook.syncType != Constants.SYNC_TYPE_CARDDAV)
    return;
  CardDAVBridge.get(addressBook).onContactDeleted(contactId, { local }).catch(console.error);
});

AddressBook.onSyncRequested.addListener((addressBook, options = {}) => {
  if (addressBook.syncType != Constants.SYNC_TYPE_CARDDAV)
    return;
  CardDAVBridge.get(addressBook).sync(options).catch(console.error);
});

AddressBook.onTryConnectRequested.addListener(async addressBook => {
  if (addressBook.syncType != Constants.SYNC_TYPE_CARDDAV)
    return false;
  return CardDAVBridge.get(addressBook).tryConnect();
});
