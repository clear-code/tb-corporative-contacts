/*
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
*/
'use strict';

import EventListenerManager from '/extlib/EventListenerManager.js';

import {
  configs,
  log,
} from '/common/common.js';
import * as Constants from '/common/constants.js';

export const onMessage = new EventListenerManager();
export const onConnected = new EventListenerManager();
export const onDisconnected = new EventListenerManager();

const mConnections = new Map();
const mReceivers = new Map();

export function init() {
  const matcher = new RegExp(`^${Constants.COMMAND_REQUEST_CONNECT_PREFIX}(.+)$`);
  browser.runtime.onConnect.addListener(port => {
    if (!matcher.test(port.name))
      return;
    const clientId   = RegExp.$1;
    const connection = { port };
    mConnections.set(clientId, connection);
    let connectionTimeoutTimer = null;
    const updateTimeoutTimer = () => {
      if (connectionTimeoutTimer) {
        clearTimeout(connectionTimeoutTimer);
        connectionTimeoutTimer = null;
      }
      connectionTimeoutTimer = setTimeout(async () => {
        log(`Missing heartbeat from window ${clientId}. Maybe disconnected or resumed.`);
        try {
          const pong = await browser.runtime.sendMessage({
            type: Constants.COMMAND_PING_TO_CLIENT,
            clientId
          });
          if (pong) {
            log(`Client ${clientId} responded. Keep connected.`);
            return;
          }
        }
        catch(_error) {
        }
        log(`Client ${clientId} did not respond. Disconnect now.`);
        cleanup(); // eslint-disable-line no-use-before-define
        port.disconnect();
      }, configs.heartbeatInterval + configs.connectionTimeoutDelay);
    };
    const cleanup = _diconnectedPort => {
      if (!port.onMessage.hasListener(receiver)) // eslint-disable-line no-use-before-define
        return;
      if (connectionTimeoutTimer) {
        clearTimeout(connectionTimeoutTimer);
        connectionTimeoutTimer = null;
      }
      mConnections.delete(clientId);
      port.onMessage.removeListener(receiver); // eslint-disable-line no-use-before-define
      mReceivers.delete(clientId);
      onDisconnected.dispatch(clientId);
    };
    const receiver = message => {
      if (Array.isArray(message))
        return message.forEach(receiver);
      if (message.type == Constants.COMMAND_HEARTBEAT)
        updateTimeoutTimer();
      onMessage.dispatch(message, clientId);
    };
    port.onMessage.addListener(receiver);
    mReceivers.set(clientId, receiver);
    onConnected.dispatch(clientId);
    port.onDisconnect.addListener(cleanup);
  });
}

export function sendMessage(message) {
  if (!mConnections)
    return false;

  if (message.clientId) {
    const connection = mConnections.get(message.clientId);
    if (!connection)
      return false;
    sendMessageToPort(connection.port, message);
    return true;
  }

  // broadcast
  for (const connection of mConnections.values()) {
    if (!connection)
      continue;
    sendMessageToPort(connection.port, message);
  }
  return true;
}

const mReservedTasks = new WeakMap();

// Se should not send messages immediately, instead we should throttle
// it and bulk-send multiple messages, for better user experience.
// Sending too much messages in one event loop may block everything
// and makes Thunderbird like frozen.
function sendMessageToPort(port, message) {
  const task = mReservedTasks.get(port) || { messages: [] };
  task.messages.push(message);
  mReservedTasks.set(port, task);
  if (!task.onFrame) {
    task.onFrame = () => {
      delete task.onFrame;
      const messages = task.messages;
      task.messages = [];
      port.postMessage(messages);
    };
    // We should not use window.requestAnimationFrame for throttling,
    // because it is quite lagged on some environment. Thunderbird may
    // decelerate the method for an invisible document (the background
    // page).
    //window.requestAnimationFrame(task.onFrame);
    setTimeout(task.onFrame, 0);
  }
}

init();
