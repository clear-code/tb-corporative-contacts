/*
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
*/
'use strict';

import {
  configs,
  sanitizeForHTMLText,
  toDOMDocumentFragment,
  clone,
} from '/common/common.js';
import * as Dialog from '/extlib/dialog.js';
import { DOMUpdater } from '/extlib/dom-updater.js';
import * as FileDecryption from '/common/file-decryption.js';
import '/extlib/l10n.js';
import Options from '/extlib/Options.js';
import RichConfirm from '/extlib/RichConfirm.js';

let options;

function onConfigChanged(key) {
  switch (key) {
    case 'debug':
      document.documentElement.classList.toggle('debugging', configs.debug);
      break;
  }
}
configs.$addObserver(onConfigChanged);


function onRowInput(event) {
  const field = event.target.closest('input, textarea');
  if (!field)
    return;
  if (field.throttleInputTimer)
    clearTimeout(field.throttleInputTimer);
  const key    = event.target.closest('[data-key]').dataset.key;
  field.throttleInputTimer = setTimeout(() => {
    delete field.throttleInputTimer;
    const row   = field.closest('.row');
    const items = clone(configs[key]);
    const item  = items[row.dataset.index];
    item[field.dataset.field] = field.value;
    configs[key] = items;
  }, 250);
}

function rebuildRowsFor(key) {
  const rows     = document.querySelector(`[data-key=${JSON.stringify(key)}] .custom-items`);
  const fields   = document.querySelector(`[data-key=${JSON.stringify(key)}]`).dataset.fields.split(/\s+/);
  const headers  = fields.map(field => '<td class="field">' + sanitizeForHTMLText(browser.i18n.getMessage('config_' + key + '_title_' + field)) + '</td>').join('');
  const contents = [
    `
      <tr id="header-row-${key}">
        ${headers}
        <td class="buttons"></td>
      </tr>
    `.trim(),
    ...configs[key].map((item, index) => createRow({ item, index, fields })),
  ].join('');
  DOMUpdater.update(rows, toDOMDocumentFragment(contents, rows));
}

function addRow(event) {
  const key    = event.target.closest('[data-key]').dataset.key;
  const fields = event.target.closest('[data-fields]').dataset.fields.split(/\s+/);
  const item   = { id: createNewId() };
  for (const field of fields) {
    item[field] = '';
  }
  configs[key] = configs[key].concat([item]);
  rebuildRowsFor(key);
  event.target.closest('[data-key]').querySelector(`tr[data-index="${configs[key].length - 1}"] input`).focus();
}

function createNewId() {
  return `item-${Date.now()}-${Math.round(Math.random() * 65000)}`;
}

function restoreDefaults(event) {
  const key    = event.target.closest('[data-key]').dataset.key;
  const fields = event.target.closest('[data-fields]').dataset.fields.split(/\s+/);
  let added = false;
  const items = clone(configs[key]);
  for (const defaultItem of configs.$default[key]) {
    if (items.some(item => fields.every(field => item[field] == defaultItem[field])))
      continue;
    items.push({
      ...defaultItem,
      id: createNewId(),
    });
    added = true;
  }
  if (!added)
    return;
  configs[key] = items;
  rebuildRowsFor(key);
}

function createRow({ index, fields, item } = {}) {
  const inputFields = fields.map(field => `
    <td class="field">
      <input type="text"
             value=${JSON.stringify(item[field] ? sanitizeForHTMLText(item[field]) : '')}
             data-field=${JSON.stringify(sanitizeForHTMLText(field))}>
    </td>
  `.trim()).join('');

  return `
    <tr id="row-${item.id}"
        data-index="${index}">
      ${inputFields}
      <td class="buttons">
        <button class="up"
                title=${JSON.stringify(sanitizeForHTMLText(browser.i18n.getMessage('config_customFields_up')))}
                >▲</button>
        <button class="down"
                title=${JSON.stringify(sanitizeForHTMLText(browser.i18n.getMessage('config_customFields_down')))}
                >▼</button>
        <button class="remove"
                title=${JSON.stringify(sanitizeForHTMLText(browser.i18n.getMessage('config_customFields_remove')))}
                >✖</button>
      </td>
    </tr>
  `.trim();
}

function onRowControlButtonClick(event) {
  const button   = event.target.closest('button');
  if (!button)
    return;
  const key      = event.target.closest('[data-key]').dataset.key;
  const items    = clone(configs[key]);
  const row      = button.closest('tr[data-index]');
  const rowIndex = parseInt(row.dataset.index);
  const item     = items[rowIndex];
  if (button.classList.contains('remove')) {
    items.splice(rowIndex, 1);
    configs[key] = items;
    rebuildRowsFor(key);
  }
  else if (button.classList.contains('up')) {
    if (rowIndex > 0) {
      items.splice(rowIndex, 1);
      items.splice(rowIndex - 1, 0, item);
      configs[key] = items;
      rebuildRowsFor(key);
    }
  }
  else if (button.classList.contains('down')) {
    if (rowIndex < items.length - 1) {
      items.splice(rowIndex, 1);
      items.splice(rowIndex + 1, 0, item);
      configs[key] = items;
      rebuildRowsFor(key);
    }
  }
}


// file decryption for debugging

const mEncryptedFileField = document.createElement('input');
mEncryptedFileField.setAttribute('type', 'file');
mEncryptedFileField.setAttribute('accept', `.encrypted,.*`);
mEncryptedFileField.setAttribute('style', 'display: none;');
document.body.appendChild(mEncryptedFileField);

const mDecryptedFileLink = document.createElement('a');
mDecryptedFileLink.setAttribute('type', 'application/octet-stream');
mDecryptedFileLink.setAttribute('style', 'display: none;');
document.body.appendChild(mDecryptedFileLink);

mEncryptedFileField.addEventListener('change', async _event => {
  const chosenFile = mEncryptedFileField.files.item(0);

  const result = await RichConfirm.show({
    modal: true,
    type: 'dialog',
    content: [
      `<p style="margin-bottom: 0.5em;">Password:</p>`,
      `<p style="display: flex; flex-directon: column; align-items: stretch;"
         ><input type="password"
                 name="password"
                 style="width: 100%;"></p>`,
    ].join(''),
    buttons: [
      'OK',
      'Cancel',
    ],
  });
  if (result.buttonIndex != 0)
    return;

  const password = result.values.password;
  const decrypted = await FileDecryption.decryptFileContents(chosenFile, { password });

  mDecryptedFileLink.setAttribute('download', chosenFile.name.replace(/\.encrypted$/, ''));
  mDecryptedFileLink.href = URL.createObjectURL(new Blob([decrypted], { type: 'application/octet-stream' }));
  mDecryptedFileLink.click();
});

Dialog.initButton(document.querySelector('#decryptFileButton'), () => mEncryptedFileField.click());


window.addEventListener('DOMContentLoaded', async () => {
  await configs.$loaded;
  options = new Options(configs);
  options.onReady();

  const customPersonalFields = document.querySelector('#custom-personal-fields');
  customPersonalFields.addEventListener('input', onRowInput);
  customPersonalFields.addEventListener('change', onRowInput);
  Dialog.initButton(customPersonalFields, onRowControlButtonClick);
  Dialog.initButton(document.querySelector('#add-new-custom-personal-field'), addRow);
  Dialog.initButton(document.querySelector('#restore-default-custom-personal-fields'), restoreDefaults);
  rebuildRowsFor(customPersonalFields.closest('[data-key]').dataset.key);

  const customOrganizationFields = document.querySelector('#custom-organization-fields');
  customOrganizationFields.addEventListener('input', onRowInput);
  customOrganizationFields.addEventListener('change', onRowInput);
  Dialog.initButton(customOrganizationFields, onRowControlButtonClick);
  Dialog.initButton(document.querySelector('#add-new-custom-organization-field'), addRow);
  Dialog.initButton(document.querySelector('#restore-default-custom-organization-fields'), restoreDefaults);
  rebuildRowsFor(customOrganizationFields.closest('[data-key]').dataset.key);

  const customChatProviders = document.querySelector('#custom-chat-providers');
  customChatProviders.addEventListener('input', onRowInput);
  customChatProviders.addEventListener('change', onRowInput);
  Dialog.initButton(customChatProviders, onRowControlButtonClick);
  Dialog.initButton(document.querySelector('#add-new-custom-chat-provider'), addRow);
  Dialog.initButton(document.querySelector('#restore-default-custom-chat-providers'), restoreDefaults);
  rebuildRowsFor(customChatProviders.closest('[data-key]').dataset.key);

  options.buildUIForAllConfigs(document.querySelector('#debug-configs'));
  onConfigChanged('debug');
  onConfigChanged('customPersonalInformationFields');
  onConfigChanged('customOrganizationInformationFields');

  for (const container of document.querySelectorAll('section, fieldset, p, div')) {
    const allFields = container.querySelectorAll('input, textarea, select');
    if (allFields.length == 0)
      continue;
    const lockedFields = container.querySelectorAll('.locked input, .locked textarea, .locked select, input.locked, textarea.locked, select.locked');
    container.classList.toggle('locked', allFields.length == lockedFields.length);
  }

  document.documentElement.classList.add('initialized');
}, { once: true });
